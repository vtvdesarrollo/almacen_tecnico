<?php

// Clase Principal.
class classbdConsultas {

    var $ObjDb = "";
    var $orderby, $groupby, $limite;
    var $ipauditoria;
    //var $prefix_table="f5."; //esquerma real
    var $prefix_table = "f5."; //esquerma desarrollo
    function classbdConsultas() {
        // Librerias Comunes
        require_once("../database/classdb.php");
        require_once("../librerias/classlibPc.php");
        $this->ipauditoria = new classlibPc();
        $this->orderby = array();
        $this->groupby = array();
        $this->limite = array();
    }

    //consultas nuevas bd
    /**
     * $conect = ruta del archivo de conexion a la base de datos
     * $tiposervicio = 1 para indicar persona, 2 para indicar materiales
     * 
     * por defecto es 2 por concepto filosofico del inventario.
     */
    function select_servicios($conect, $tiposervicio="2") {
        $nameTabla['inventario.select_servicios'] = "inventario.select_servicios";
        /**
         * tabla:
         * inventario.select_servicios
         * campos:
         * materialtipomaterialinv.idtipomaterial, 
         * inventario.concatena(materialtipomaterialinv.idtipoinventario::text) AS idtiposinventario, 
         * upper(materialtipomaterialinv.valorinventariado) AS valorinventariado, 
         * materialtipomaterialinv.desctipomaterial, 
         * materialtipomaterialinv.id_fintipoinventario
         */
        $campos['idtipomaterial'] = "idtipomaterial";
        $campos['idtiposinventario'] = "idtiposinventario";
        $campos['valorinventariado'] = "valorinventariado";
        $campos['desctipomaterial'] = "desctipomaterial";
        $campos['id_fintipoinventario'] = "id_fintipoinventario";
        if ($tiposervicio != 'todos') {
            $condicion['id_fintipoinventario'] = $tiposervicio;
        }
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $this->orderby, $this->groupby, $this->limite);
    }

    /**
    *
    *   esta funcion sirve para determinar los datos de un tiporecurso.
    *
    */
    function select_info_tiporecurso($conect, $idtipomaterial) {
        $nameTabla['inventario.select_servicios'] = "inventario.select_servicios";
        /**
         * tabla:
         * inventario.select_servicios
         * campos:
         * materialtipomaterialinv.idtipomaterial, 
         * inventario.concatena(materialtipomaterialinv.idtipoinventario::text) AS idtiposinventario, 
         * upper(materialtipomaterialinv.valorinventariado) AS valorinventariado, 
         * materialtipomaterialinv.desctipomaterial, 
         * materialtipomaterialinv.id_fintipoinventario
         */
        $campos['idtipomaterial'] = "idtipomaterial";
        $campos['idtiposinventario'] = "idtiposinventario";
        $campos['valorinventariado'] = "valorinventariado";
        $campos['desctipomaterial'] = "desctipomaterial";
        $campos['id_fintipoinventario'] = "id_fintipoinventario";
        $condicion['idtipomaterial'] = $idtipomaterial;
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $this->orderby, $this->groupby, $this->limite);
    }

    function select_servicios2($conect, $tiposervicio="2") {
        $nameTabla['inventario.select_servicios'] = "inventario.select_servicios2";
        /**
         * tabla:
         * nventario.select_servicios2
         * campos
         *  inventario.concatena(rel_tipomaterial_servicios.idtipomaterial::text) AS idstipomaterial, 
         * rel_tipomaterial_servicios.valorinventariado, 
         * rel_tipomaterial_servicios.id_fintipoinventario
         * 
         */
        $campos['idstipomaterial'] = "idstipomaterial";
        $campos['valorinventariado'] = "valorinventariado";
        $campos['id_fintipoinventario'] = "id_fintipoinventario";
        $condicion['id_fintipoinventario'] = $tiposervicio;
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $this->orderby, $this->groupby, $this->limite);
    }

    function get_recursos_sys_inventario($conect, $idmaterialgeneral=null, $idtipoinventario=null, $idtipomaterial=null, $id_gerencia=null, $id_division=null, $id_fintipoinventario=null) {
        $nameTabla['inventario.inventariostok'] = "inventario.inventariostok";
        /**
         * tabla
         * inventario.inventariostok
         * campos
         * vistageneral_inventario.idmaterialgeneral, 
         * vistageneral_inventario.idtipoinventario, 
         * vistageneral_inventario.desctipoinventario, 
         * vistageneral_inventario.desctipomaterial, 
         * vistageneral_inventario.inventario_valor, 
         * vistageneral_inventario.busqueda, 
         * vistageneral_inventario.idtipomaterial, 
         * vistageneral_inventario.id_gerencia, 
         * vistageneral_inventario.id_division, 
         * vistageneral_inventario.id_fintipoinventario
         */
        $campos['idmaterialgeneral'] = "idmaterialgeneral";
        $campos['idtipoinventario'] = "idtipoinventario";
        $campos['desctipoinventario'] = "desctipoinventario";
        $campos['desctipomaterial'] = "desctipomaterial";
        $campos['inventario_valor'] = "inventario_valor";
        $campos['idtipomaterial'] = "idtipomaterial";
        $campos['id_gerencia'] = "id_gerencia";
        $campos['id_division'] = "id_division";
        $campos['id_fintipoinventario'] = "id_fintipoinventario";
        if ($idmaterialgeneral != null) {
            $condicion['idmaterialgeneral'] = $idmaterialgeneral;
        }
        if ($idtipoinventario != null) {
            $condicion['idtipoinventario'] = $idtipoinventario;
        }
        if ($idtipomaterial != null) {
            $condicion['idtipomaterial'] = $idtipomaterial;
        }
        if ($id_gerencia != null) {
            $condicion['id_gerencia'] = $id_gerencia;
        }
        if ($id_division != null) {
            $condicion['id_division'] = $id_division;
        }
        if ($id_fintipoinventario != null) {
            $condicion['id_fintipoinventario'] = $id_fintipoinventario;
        }

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $this->orderby, $this->groupby, $this->limite);
    }

    /**
     * resuelve la informacion del inventario y del recurso seleccionado.
     * @param type $conect ruta de conecion y valores a la base de datos
     * @param type $idmaterialgeneral el idmaterialgeneral del inventario
     * @return type 
     */
    function get_info_recurso_inventario($conect, $idmaterialgeneral) {
        $nameTabla['inventario.vistageneral_inventario'] = "inventario.vistageneral_inventario";

        /**
         * Tabla
         * inventario.vistageneral_inventario AS 
         * Campos
         * materialgeneral.idmaterialgeneral, 
         * materialgeneral.idtipoinventario, 
         * tipoinventario.desctipoinventario, 
         * tipomaterial.desctipomaterial, 
         * inventario.concatena((caracteristica.desccararacteristica || ':'::text) || vista_inventario.valorinventariado) AS inventario_valor, 
         * inventario.concatena(((tipomaterial.desctipomaterial || ' '::text) || (caracteristica.desccararacteristica || ':'::text)) || vista_inventario.valorinventariado) AS busqueda, 
         * materialgeneral.estado_seleccion, 
         * materialgeneral.expiracion_seleccion, 
         * tipomaterial.idtipomaterial, 
         * tipoinventario.id_gerencia, 
         * tipoinventario.id_division, 
         * tipoinventario.id_fintipoinventario
         */
        $campos['idmaterialgeneral'] = "idmaterialgeneral";
        $campos['idtipoinventario'] = "idtipoinventario";
        $campos['desctipoinventario'] = "desctipoinventario";
        $campos['desctipomaterial'] = "desctipomaterial";
        $campos['inventario_valor'] = "inventario_valor";
        $campos['estado_seleccion'] = "estado_seleccion";
        $campos['idtipomaterial'] = "idtipomaterial";
        $campos['id_gerencia'] = "id_gerencia";
        $campos['id_division'] = "id_division";
        $campos['id_fintipoinventario'] = "id_fintipoinventario";
        $condicion['idmaterialgeneral'] = $idmaterialgeneral;
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $this->orderby, $this->groupby, $this->limite);
    }

    /**
    */
    function guardarcomentarios($conect, $idstatuspauta, $mensaje){
        $nameTabla = $this->prefix_table . 't_estatus_pauta';
        $campos['observaciones'] = $mensaje;
        $condicion['id_estatus_pauta'] = $idstatuspauta;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function desacsignar_recursos($conect, $idpauta, $idmaterialgeneral, $cedula_session) {
        $nameTabla = $this->prefix_table . 't_detalle_servicio';
        $campos['fecha_exp'] = date("Y-m-d");
        $campos['user_exp'] = $cedula_session;
        $condicion['id_pauta'] = $idpauta;
        $condicion['id_recurso_asignado'] = $idmaterialgeneral;
        $condicion['fecha_exp'] = '2222-12-31';
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function status_recurso($conect, $pauta, $t_inventario, $status){
        $nameTabla[$this->prefix_table . 'recur_val_estatus'] = $this->prefix_table . "recur_val_estatus";
        $campos['id_recurso'] = "id_recurso";
        $campos['cantidad'] = "cantidad";
        $campos['id_fintipoinventario'] = "id_fintipoinventario";
    
        $condicion = "id_pauta=".$pauta." and id_fintipoinventario=".$t_inventario;
        //$condicion['id_fintipoinventario'] = $t_inventario;


        if($status!=""){
            $condicion.=" and (";
        }
        $estatus=explode(";",$status);

        for($i=0;$i<count($estatus);$i++){
            if($i>0){
               $condicion.=" or "; 
            }
            $condicion.="id_estatus=".$estatus[$i];
        }
        if($i>0){
            $condicion.=")";
        }

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelectLibre($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

///////////////////////////////f5_new//////////////////////////////////////////////////
///////////////////////////Aviso de la Intranet///////////////////////////////////////

    function selectrecurasig($conect, $cedula) {
        $nameTabla['inventario.vista_inventario'] = "inventario.vista_inventario";
        $campos['idmaterialgeneral'] = "idmaterialgeneral";
        $condicion['valorinventariado'] = "'" . $cedula . "'";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectasigpara($conect, $id) {
        $nameTabla[$this->prefix_table . 't_detalle_servicio'] = $this->prefix_table . "t_detalle_servicio";
        $campos['id_pauta'] = "id_pauta";
        $condicion['id_recurso_asignado'] = $id;
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

///////////////////////////select///////////////////////////////////////////////////
    function selectservicio($conect) {
        $nameTabla[$this->prefix_table . 't_tipo_servicio'] = $this->prefix_table . "t_tipo_servicio";
        $campos['id_tipo_servicio'] = "id_tipo_servicio";
        $campos['descripcion'] = "descripcion";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectpauta($conect) {
        $nameTabla[$this->prefix_table . 't_tipo_pauta'] = $this->prefix_table . "t_tipo_pauta";
        $campos['id_tipo_pauta'] = "id_tipo_pauta";
        $campos['descripcion'] = "descripcion";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    /*function selectprograma($conect) {
        $nameTabla[$this->prefix_table . 't_programa'] = $this->prefix_table . "t_programa";
        $campos['id_program'] = "id_program";
        $campos['descripcion'] = "descripcion";
        $condicion['fecha_exp'] = "'2222-12-31'";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }*/

    function selecttipoevento($conect) {
        $nameTabla[$this->prefix_table . 't_tipo_evento'] = $this->prefix_table . "t_tipo_evento";
        $campos['id_tipo_evento'] = "id_tipo_evento";
        $campos['descripcion'] = "descripcion";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selecttipotraje($conect) {
        $nameTabla[$this->prefix_table . 't_tipo_traje'] = $this->prefix_table . "t_tipo_traje";
        $campos['id_tipo_traje'] = "id_tipo_traje";
        $campos['descripcion'] = "descripcion";
        $condicion['fecha_exp'] = "'2222-12-31'";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectlocacion($conect) {
        $nameTabla[$this->prefix_table . 't_locacion'] = $this->prefix_table . "t_locacion";
        $campos['id_locacion'] = "id_locacion";
        $campos['descripcion'] = "descripcion";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectdirector($conect) {
        $nameTabla[$this->prefix_table . 't_locacion'] = $this->prefix_table . "t_locacion";
        $campos['id_locacion'] = "id_locacion";
        $campos['descripcion'] = "descripcion";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectcoordinador($conect) {
        $nameTabla[$this->prefix_table . 't_locacion'] = $this->prefix_table . "t_locacion";
        $campos['id_locacion'] = "id_locacion";
        $campos['descripcion'] = "descripcion";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }


//////////////////////////////insert//////////////////////////////////////////////

    function insertservicio_f5($conect, $descripcion, $id_tipo_servicio) {
        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_servicio";
        // Campos para hacer el insert
        $campos['descripcion'] = $descripcion;
        $campos['id_tipo_servicio'] = $id_tipo_servicio;
        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    }

    function inserttipotraje($conect, $descripcion) {
        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_tipo_traje";
        // Campos para hacer el insert
        $campos['descripcion'] = $descripcion;
        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    }

    

    function insertevento($conect, $descripcion) {
        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_evento";
        // Campos para hacer el insert
        $campos['descripcion'] = $descripcion;
        $campo_return = "id_evento";
        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsertRn($nameTabla, $campos, $campo_return);
    }

/////////////////////////////////////////////Usuario/////////////////////////////////////////////////////

    function selectexiste($conect, $cedula) {
        $nameTabla['usuario.t_datos_personales'] = "usuario.t_datos_personales";
        $campos['cedula'] = "cedula";
        $condicion['cedula'] = $cedula;
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    /*
     * revisa en tabla datos del usuario.
     */

    function selectdatosexiste($conect, $cedula) {
        $nameTabla['usuario.t_datos_personales'] = "usuario.t_datos_personales";
        $campos['correo'] = "correo";
        $campos['telefono1'] = "telefono1";
        $campos['telefono2'] = "telefono2";
        $condicion['cedula'] = $cedula;
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectexisteacceso($conect, $cedula, $id_aplicacion) {
        $nameTabla['usuario.t_acceso'] = "usuario.t_acceso";
        $campos['cedula'] = "cedula";
        $campos['id_tipo_usuario'] = "id_tipo_usuario";
        $condicion['cedula'] = "'" . $cedula . "'";
        $condicion['id_aplicacion'] = "'" . $id_aplicacion . "'";
        $condicion['fecha_exp'] = "'2222-12-31'";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function updatecorreo($conect, $cedula, $correo) {
        // Tabla para hacer la consulta
        $nameTabla = 'usuario.t_datos_personales';
        $campos['correo'] = $correo;
        $condicion['cedula'] = $cedula;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function updatetlf1($conect, $cedula, $telefono1) {
        // Tabla para hacer la consulta
        $nameTabla = 'usuario.t_datos_personales';
        //$campos['telefono1']=$telefono1;
        if ($telefono1 != "") {
            $campos['telefono1'] = $telefono1;
        }
        $condicion['cedula'] = $cedula;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function updatetlf2($conect, $cedula, $telefono2) {
        // Tabla para hacer la consulta
        $nameTabla = 'usuario.t_datos_personales';
        //$campos['telefono2']=$telefono2;
        if ($telefono2 != "") {
            $campos['telefono2'] = $telefono2;
        }
        $condicion['cedula'] = $cedula;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    /**
    * funcion que selecciona a los usuarios del sistema.
    */
    /*function selectusuariof5($conect, $cedula) {
        $nameTabla[ $this->prefix_table .'usuarios'] =  $this->prefix_table ."usuarios";
        $campos['nombre_usuario'] = "nombre_usuario";
        $campos['apellido_usuario'] = "apellido_usuario";
        $campos['correo'] = "correo";
        $campos['desc_usuario']= "desc_usuario";
        $campos['de_gerencia']= "de_gerencia";
        $campos['de_division']= "de_division";
        
        $condicion['cedula'] = "'" . $cedula . "'";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }*/
    function selectusuario($conect, $cedula) {
        $nameTabla['usuario.usuarios_almacen_tecnico'] =  'usuario.usuarios_almacen_tecnico';
        $campos['nombre_usuario'] = "nombre_usuario";
        $campos['apellido_usuario'] = "apellido_usuario";
        $campos['correo'] = "correo";
        
        $condicion['cedula'] = "'" . $cedula . "'";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectidtipousuario($conect, $cedula) {
        $nameTabla['usuario.t_acceso'] = "usuario.t_acceso";
        $campos['id_tipo_usuario'] = "id_tipo_usuario";
        $condicion['cedula'] = "'" . $cedula . "'";
        $condicion['id_aplicacion'] = 15;
        $condicion['fecha_exp'] = "'2222-12-31'";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selecttipousuario($conect, $idtipousuario) {
        $nameTabla['usuario.t_tipo_usuario'] = "usuario.t_tipo_usuario";
        $campos['desc_usuario'] = "desc_usuario";
        $condicion['id_tipo_usuario'] = "'" . $idtipousuario . "'";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function insertusuarios($conect, $nombre, $apellido, $cedula, $correo, $telefono1, $telefono2) {
        // Nombre de la Tabla
        $nameTabla = "usuario.t_datos_personales";
        // Campos para hacer el insert
        $campos['nombre_usuario'] = $nombre;
        $campos['apellido_usuario'] = $apellido;
        $campos['cedula'] = $cedula;
        $campos['correo'] = $correo;
        if ($telefono1 != "") {
            $campos['telefono1'] = $telefono1;
        }
        if ($telefono2 != "") {
            $campos['telefono2'] = $telefono2;
        }
        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    }

    function insertclaveusuarios($conect, $cedula, $clave1, $id_modulo, $ced_sesion, $tipousuario, $idgerencia, $iddivision, $id_aplicacion) {
        // Nombre de la Tabla
        $nameTabla = "usuario.t_acceso";
        // Campos para hacer el insert
        $campos['cedula'] = $cedula;
        $campos['clave'] = $clave1;
        $campos['id_modulo'] = $id_modulo;
        $campos['id_aplicacion'] = $id_aplicacion;
        $campos['niv_con'] = 1;
        $campos['niv_eli '] = 1;
        $campos['niv_inc '] = 1;
        $campos['niv_mod '] = 1;
        $campos['ced_trans '] = $ced_sesion;
        $campos['fecha_trans '] = date("Y-m-d");
        $campos['id_tipo_usuario '] = $tipousuario;
        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    }

    //////////////////////////reinicio y consulta de clave/////////////////////////////////

    function updateclave($conect, $cedula) {
        // Tabla para hacer la consulta
        $nameTabla = 'usuario.t_acceso';
        $campos['clave'] = 123456;
        $condicion['cedula'] = $cedula;
        $condicion['fecha_exp'] = '2222-12-31';
        $condicion['id_aplicacion'] = 15;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function selectclave($conect, $cedula) {
        $nameTabla['usuario.t_acceso'] = "usuario.t_acceso";
        $campos['clave'] = "clave";
        $condicion['cedula'] = "'" . $cedula . "'";
        $condicion['id_aplicacion'] = 15;
        $condicion['fecha_exp'] = "'2222-12-31'";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function updateusuario($conect, $cedula, $id_aplicacion) {
        // Tabla para hacer la consulta
        $nameTabla = 'usuario.t_acceso';
        $campos['fecha_exp'] = date("Y-m-d");
        $condicion['cedula'] = $cedula;
        $condicion['id_aplicacion'] = $id_aplicacion;
        $condicion['fecha_exp'] = '2222-12-31';
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

///////////////////////eliminar usuario ////////////////////////////////////////

    /*function updateusuariof5($conect, $cedula, $id_aplicacion) {
        // Tabla para hacer la consulta
        $nameTabla = 'usuario.t_acceso';
        $campos['fecha_exp'] = date("Y-m-d");
        $condicion['cedula'] = $cedula;
        $condicion['id_aplicacion'] = $id_aplicacion;
        $condicion['fecha_exp'] = '2222-12-31';
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }*/

///////////////////////perfil del usuario ////////////////////////////////////////

    function selectperfilactual($conect, $cedula) {
        $nameTabla['usuario.t_acceso'] = "usuario.t_acceso";
        $campos['id_tipo_usuario'] = "id_tipo_usuario";
        $condicion['cedula'] = "'" . $cedula . "'";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function updatecambiarperfil($conect, $cedula, $perfilact, $perfilmod) {
        // Tabla para hacer la consulta
        $nameTabla = 'usuario.t_acceso';
        $campos['id_tipo_usuario'] = $perfilmod;

        $condicion['cedula'] = $cedula;
        $condicion['id_tipo_usuario'] = $perfilact;
        $condicion['fecha_exp'] = '2222-12-31';
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function selectdescperfil($conect, $perfilmod) {
        $nameTabla['usuario.t_tipo_usuario'] = "usuario.t_tipo_usuario";

        $campos['desc_usuario'] = "desc_usuario";

        $condicion['id_tipo_usuario'] = $perfilmod;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selecttipodescripcion($conect) {
        $nameTabla['almacen_tecnico.t_descripciones'] = "almacen_tecnico.t_descripciones";

        $campos['id_descripcion'] = "id_descripcion";
        $campos['descripcion'] = "descripcion";
        
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }
////////////////////////////////////////////////ARTICULOS/////////////////////////////////////////////////////////////
    function selectarticulo($conect, $id) {
        $nameTabla['almacen_tecnico.t_articulo'] = "almacen_tecnico.t_articulo";
        $campos['tipo_articulo'] = "tipo_articulo";
        $campos['descripcion'] = "descripcion";
        $campos['marca'] = "marca";
        $campos['modelo'] = "modelo";
        $campos['bien_nac'] = "bien_nac";
        $campos['serial'] = "serial";
        $campos['siglas'] = "siglas";
        $campos['id_grupo'] = "id_grupo";
        $campos['peldano'] = "peldano";
        $campos['estante'] = "estante";
        $campos['cantidad'] = "cantidad";
        $campos['costo'] = "costo";
        $campos['id_estatus'] = "id_estatus";
        $campos['id_estado'] = "id_estado";
        $campos['id_categoria'] = "id_categoria";
        $campos['id_unidad_medida'] = "id_unidad_medida";
        $campos['fecha_exp'] = "fecha_exp";
        $campos['fecha_reg'] = "fecha_reg";
        $campos['user_reg'] = "user_reg"; 
        $campos['imagen'] = "imagen";
        $campos['cant_min'] = "cant_min";
        $campos['id_descripcion'] = "id_descripcion";

        $condicion['id_articulo'] = "$id";
        $condicion['fecha_exp'] ="'2222-12-31'";
        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectarticuloprestados($conect, $idsolicitud, $fecha_condicion) {
        $nameTabla['inventario_imagen.articulos_prestados'] = "inventario_imagen.articulos_prestados";
        $campos['tipo'] = "tipo";
        $campos['descripcion'] = "descripcion";
        $campos['marca'] = "marca";
        $campos['modelo'] = "modelo";
        $campos['color'] = "color";
        $campos['talla_medida'] = "talla_medida";
        $campos['unidad_medida'] = "unidad_medida";
        $campos['bien_nac'] = "bien_nac";
        $campos['serial'] = "serial";
        $campos['costo'] = "costo";
        $campos['observacion'] = "observacion";
        $campos['tipo_articulo'] = "tipo_articulo";
        $campos['id_desc_estado'] = "id_desc_estado";
        $campos['imagen'] = "imagen";
        $campos['cantidad'] = "cantidad";
        $campos['id_articulo'] = "id_articulo";
        $campos['fecha_exp'] = "fecha_exp";

        if($fecha_condicion=='2222-12-31'){
          $condicion ="id_solicitud = ".$idsolicitud." and fecha_exp = '".$fecha_condicion."'";
        }else{
         
            $condicion ="id_solicitud = ".$idsolicitud." and fecha_exp != '2222-12-31'";
           
        }
        
        /*$this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);*/
        
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelectLibre($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }


    function selectarticulodespachados($conect, $idsolicitud) {
        $nameTabla['almacen_tecnico.t_material_desp'] = "almacen_tecnico.t_material_desp";
       
        $campos['id_articulo'] = "id_articulo";
        $campos['cantidad'] = "cantidad";

        //$condicion['id_despacho'] = "$idsolicitud";

        $condicion ="id_despacho = ".$idsolicitud." and fecha_exp = '2222-12-31' and cantidad != '0'";
           
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelectLibre($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectarticulodesp($conect, $idarticulo, $id_solicitud) {
        $nameTabla['almacen_tecnico.t_material_desp'] = "almacen_tecnico.t_material_desp";
       
        $campos['id_articulo'] = "id_articulo";
        $campos['cantidad'] = "cantidad";

        //$condicion['id_despacho'] = "$idsolicitud";

        $condicion ="id_articulo = ".$idarticulo." and fecha_exp = '2222-12-31' and cantidad != '0' and id_despacho = ".$id_solicitud." ";
           
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelectLibre($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectdespachos($conect) {
        $nameTabla['inventario_imagen.articulos_despachados'] = "inventario_imagen.articulos_despachados";
        $campos['tipo'] = "tipo";
        $campos['descripcion'] = "descripcion";
        $campos['marca'] = "marca";
        $campos['modelo'] = "modelo";
        $campos['color'] = "color";
        $campos['talla_medida'] = "talla_medida";
        $campos['unidad_medida'] = "unidad_medida";
        $campos['bien_nac'] = "bien_nac";
        $campos['serial'] = "serial";
        $campos['costo'] = "costo";
        $campos['observacion'] = "observacion";
        $campos['id_desc_estado'] = "id_desc_estado";
        $campos['imagen'] = "imagen";
        $campos['cantidad'] = "cantidad";
        $campos['id_articulo'] = "id_articulo";
        $campos['id_despacho'] = "id_despacho";
        $campos['resp_despacho'] = "resp_despacho";
        $campos['user_reg'] = "user_reg";
        $campos['fecha_sol'] = "fecha_sol";
        $campos['id_destino'] = "id_destino";
        $campos['id_desc_dest'] = "id_desc_dest";

        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }   

    function selectgrupo($conect, $id) {
        $nameTabla['almacen_tecnico.t_grupos'] = "almacen_tecnico.t_grupos";
        $campos['descripcion'] = "descripcion";
        $campos['id_categoria'] = "id_categoria";
        $campos['id_asig'] = "id_asig";
        $campos['id_desc_estado'] = "id_desc_estado";
        
        $condicion['id_grupo'] = "'".$id."'";
        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectarticulodegrupo($conect, $id) {
        $nameTabla['almacen_tecnico.t_articulo'] = "almacen_tecnico.t_articulo";
        
        $campos['id_articulo'] = "id_articulo";
       

        $condicion['id_grupo'] = "'".$id."'";
        $condicion['fecha_exp'] ="'2222-12-31'";
        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function articulos($conect, $id_vestuario, $id_genero) {
        $nameTabla['almacen_tecnico.t_articulo'] = "almacen_tecnico.t_articulo";
        
        $campos['cantidad'] = "cantidad";
        $campos['id_vestuario'] = "id_vestuario";
        $campos['id_genero'] = "id_genero";

        
        $condicion['fecha_exp'] ="'2222-12-31'";
        $condicion['id_vestuario'] ="'$id_vestuario'";
        $condicion['id_genero'] ="'$id_genero'";
        

        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectingreso_maquillaje($conect, $fechadesde, $fechahasta) {
        $nameTabla['inventario_imagen.ingresos_maquillaje'] = "inventario_imagen.ingresos_maquillaje";
        $campos['id_ingresos'] = "id_ingresos";
        $campos['descripcion'] = "descripcion";
        $campos['marca'] = "marca";
        $campos['color'] = "color";
        $campos['costo'] = "costo";
        $campos['cantidad'] = "cantidad";
        $campos['fecha_reg'] = "fecha_reg";
        $campos['user_reg'] = "user_reg";
        $campos['desc_unidad_medida'] = "desc_unidad_medida";

        $condicion ="fecha_reg  between '".$fechadesde."' and '".$fechahasta."'";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelectLibre($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
   }

    function selectingreso_vestuario($conect, $fechadesde, $fechahasta) {
        $nameTabla['inventario_imagen.ingresos_vestuario'] = "inventario_imagen.ingresos_vestuario";
        $campos['id_ingresos'] = "id_ingresos";
        $campos['descripcion'] = "descripcion";
        $campos['marca'] = "marca";
        $campos['modelo'] = "modelo";
        $campos['color'] = "color";
        $campos['talla_medida'] = "talla_medida";
        $campos['costo'] = "costo";
        $campos['cantidad'] = "cantidad";
        $campos['fecha_reg'] = "fecha_reg";
        $campos['user_reg'] = "user_reg";

        $condicion ="fecha_reg  between '".$fechadesde."' and '".$fechahasta."'";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelectLibre($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
   }

    function selectingreso_utileria($conect, $fechadesde, $fechahasta) {
        $nameTabla['inventario_imagen.ingresos_utileria'] = "inventario_imagen.ingresos_utileria";
        $campos['id_ingresos'] = "id_ingresos";
        $campos['descripcion'] = "descripcion";
        $campos['marca'] = "marca";
        $campos['color'] = "color";
        $campos['talla_medida'] = "talla_medida";
        $campos['desc_unidad_medida'] = "desc_unidad_medida";
        $campos['cantidad'] = "cantidad";
        $campos['costo'] = "costo";
        $campos['fecha_reg'] = "fecha_reg";
        $campos['user_reg'] = "user_reg";

        $condicion ="fecha_reg  between '".$fechadesde."' and '".$fechahasta."'";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelectLibre($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
   }

    function selectingreso_almacen($conect, $fechadesde, $fechahasta) {
        $nameTabla['inventario_imagen.ingresos_almacen'] = "inventario_imagen.ingresos_almacen";
        $campos['id_ingresos'] = "id_ingresos";
        $campos['descripcion'] = "descripcion";
        $campos['marca'] = "marca";
        $campos['color'] = "color";
        $campos['talla_medida'] = "talla_medida";
        $campos['desc_unidad_medida'] = "desc_unidad_medida";
        $campos['cantidad'] = "cantidad";
        $campos['costo'] = "costo";
        $campos['fecha_reg'] = "fecha_reg";
        $campos['user_reg'] = "user_reg";

        $condicion ="fecha_reg  between '".$fechadesde."' and '".$fechahasta."'";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelectLibre($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
   }

    function selectvestuario($conect) {
        $nameTabla['inventario_imagen.vestuario'] = "inventario_imagen.vestuario";
        $campos['descripcion'] = "descripcion";
        $campos['marca'] = "marca";
        $campos['modelo'] = "modelo";
        $campos['color'] = "color";
        $campos['talla_medida'] = "talla_medida";
        $campos['desc_unidad_medida'] = "desc_unidad_medida";
        $campos['costo'] = "costo";
        $campos['observacion'] = "observacion";
        $campos['desc_estado'] = "desc_estado";
        $campos['cantidad'] = "cantidad";
        $campos['fecha_reg'] = "fecha_reg";
        $campos['tipo_articulo'] = "tipo_articulo";

        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }
    function selectutileria($conect) {
        $nameTabla['inventario_imagen.utileria'] = "inventario_imagen.utileria";
        $campos['descripcion'] = "descripcion";
        $campos['marca'] = "marca";
        $campos['color'] = "color";
        $campos['talla_medida'] = "talla_medida";
        $campos['unidad_medida'] = "unidad_medida";
        $campos['bien_nac'] = "bien_nac";
        $campos['serial'] = "serial";
        $campos['costo'] = "costo";
        $campos['observacion'] = "observacion";
        $campos['desc_estado'] = "desc_estado";
        $campos['cantidad'] = "cantidad";
        $campos['desc_unidad_medida'] = "desc_unidad_medida";
        $campos['fecha_reg'] = "fecha_reg";
        $campos['tipo_articulo'] = "tipo_articulo";

        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }




    function selectalmacen($conect) {
        $nameTabla['inventario_imagen.almacen'] = "inventario_imagen.almacen";
        $campos['descripcion'] = "descripcion";
        $campos['marca'] = "marca";
        $campos['color'] = "color";
        $campos['talla_medida'] = "talla_medida";
        $campos['unidad_medida'] = "unidad_medida";
        $campos['bien_nac'] = "bien_nac";
        $campos['serial'] = "serial";
        $campos['costo'] = "costo";
        $campos['observacion'] = "observacion";
        $campos['desc_estado'] = "desc_estado";
        $campos['cantidad'] = "cantidad";
        $campos['desc_unidad_medida'] = "desc_unidad_medida";
        $campos['fecha_reg'] = "fecha_reg";
        $campos['tipo_articulo'] = "tipo_articulo";

        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectmaquillaje($conect) {
        $nameTabla['inventario_imagen.maquillaje'] = "inventario_imagen.maquillaje";
        $campos['descripcion'] = "descripcion";
        $campos['marca'] = "marca";
        $campos['modelo'] = "modelo";
        $campos['color'] = "color";
        $campos['costo'] = "costo";
        $campos['observacion'] = "observacion";
        $campos['desc_estado'] = "desc_estado";
        $campos['cantidad'] = "cantidad";
        $campos['desc_unidad_medida'] = "desc_unidad_medida";
        $campos['fecha_reg'] = "fecha_reg";
        $campos['tipo_articulo'] = "tipo_articulo";

        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectusuarios($conect) {
        $nameTabla['usuario.usuarios_almacen_tecnico'] = "usuario.usuarios_almacen_tecnico";
        $campos['cedula'] = "cedula";
        $campos['nombre_usuario'] = "nombre_usuario";
        $campos['apellido_usuario'] = "apellido_usuario ";
        $campos['desc_usuario'] = "desc_usuario";
        
        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectmateriales($conect,$grupo=null,$categoria=null,$estado=null,$tipo_art=null,$marca=null,$estante=null,$peldano=null) {
        $nameTabla['almacen_tecnico.t_articulos_almacen'] = "almacen_tecnico.articulos_almacen";

        $campos['descripcion'] = "descripcion";
        $campos['marca'] = "marca";
        $campos['modelo'] = "modelo";
        $campos['bien_nac'] = "bien_nac";
        $campos['serial'] = "serial";
        $campos['grupo'] = "grupo";
        $campos['cantidad'] = "cantidad";
        $campos['desc_unidad_medida'] = "desc_unidad_medida";
        $campos['costo'] = "costo";
        $campos['estatus'] = "estatus";
        $campos['estado'] = "estado";
        $campos['categoria'] = "categoria";
        $campos['tipo_articulo'] = "tipo_articulo";

        if ($grupo != null ) {
            $condicion['id_grupo'] = "'".$grupo."'";
        }
        if ($categoria != null) {
            $condicion['id_categoria'] = "'".$categoria."'";
        }
        if ($estado != null) {
            $condicion['id_estado'] = "'".$estado."'";
        }
        if ($marca != null) {
            $condicion['marca'] = "'".$marca."'";
        }
        if ($estante != null) {
            $condicion['estante'] = "'".$estante."'";
        }
        if ($peldano != null) {
            $condicion['peldano'] = "'".$peldano."'";
        }
        if ($tipo_art != null) {
            $condicion['tipo_articulo'] = $tipo_art."'";
        }



        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectubicacion_consumible($conect,$grupo=null,$categoria=null,$estado=null,$tipo_art=null,$marca=null,$estante=null,$peldano=null) {
        $nameTabla['almacen_tecnico.ubicacion_consumibles'] = "almacen_tecnico.ubicacion_consumibles";

        $campos['descripcion'] = "descripcion";
        $campos['marca'] = "marca";
        $campos['modelo'] = "modelo";
        $campos['bien_nac'] = "bien_nac";
        $campos['serial'] = "serial";
        $campos['grupo'] = "grupo";
        $campos['cantidad'] = "cantidad";
        $campos['desc_unidad_medida'] = "desc_unidad_medida";
        $campos['costo'] = "costo";
        $campos['estatus'] = "estatus";
        $campos['estado'] = "estado";
        $campos['categoria'] = "categoria";
        $campos['tipo_articulo'] = "tipo_articulo";
        $campos['peldano'] = "peldano";
        $campos['estante'] = "estante";

        if ($grupo != null ) {
            $condicion['id_grupo'] = "'".$grupo."'";
        }
        if ($categoria != null) {
            $condicion['id_categoria'] = "'".$categoria."'";
        }
        if ($estado != null) {
            $condicion['id_estado'] = "'".$estado."'";
        }
        if ($marca != null) {
            $condicion['marca'] = "'".$marca."'";
        }
        if ($estante != null) {
            $condicion['estante'] = "'".$estante."'";
        }
        if ($peldano != null) {
            $condicion['peldano'] = "'".$peldano."'";
        }
        if ($tipo_art != null) {
            $condicion['tipo_articulo'] = $tipo_art."'";
        }


        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function consumible_existencia_min($conect) {
        $nameTabla['almacen_tecnico.articulos_existencia_min'] = "almacen_tecnico.articulos_existencia_min";

        $campos['descripcion'] = "descripcion";
        $campos['marca'] = "marca";
        $campos['modelo'] = "modelo";
        $campos['cantidad'] = "cantidad";
        $campos['desc_unidad_medida'] = "desc_unidad_medida";
        $campos['costo'] = "costo";
        $campos['categoria'] = "categoria";
        $campos['peldano'] = "peldano";
        $campos['estante'] = "estante";
        $campos['cant_min'] = "cant_min";

        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectmaterialesequipos($conect) {
        $nameTabla['almacen_tecnico.articulos_equipos'] = "almacen_tecnico.articulos_equipos";

        $campos['descripcion'] = "descripcion";
        $campos['marca'] = "marca";
        $campos['modelo'] = "modelo";
        $campos['bien_nac'] = "bien_nac";
        $campos['serial'] = "serial";
        $campos['grupo'] = "grupo";
        $campos['cantidad'] = "cantidad";
        $campos['desc_unidad_medida'] = "desc_unidad_medida";
        $campos['costo'] = "costo";
        $campos['estatus'] = "estatus";
        $campos['estado'] = "estado";
        $campos['categoria'] = "categoria";

        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectmaterialesconsumibles($conect) {
        $nameTabla['almacen_tecnico.articulos_consumibles'] = "almacen_tecnico.articulos_consumibles";

        $campos['descripcion'] = "descripcion";
        $campos['marca'] = "marca";
        $campos['modelo'] = "modelo";
        $campos['bien_nac'] = "bien_nac";
        $campos['serial'] = "serial";
        $campos['grupo'] = "grupo";
        $campos['cantidad'] = "cantidad";
        $campos['desc_unidad_medida'] = "desc_unidad_medida";
        $campos['costo'] = "costo";
        $campos['estatus'] = "estatus";
        $campos['estado'] = "estado";
        $campos['categoria'] = "categoria";

        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }


    function selectentradas($conect, $id_entrada) {
        $nameTabla['almacen_tecnico.t_entrada'] = "almacen_tecnico.t_entrada";
        $campos['id_entrada'] = "id_entrada";
        $campos['id_solicitud'] = "id_solicitud";
        $campos['resp_entrada'] = "resp_entrada";
        $campos['user_reg'] = "user_reg";
        $campos['fecha_entrada'] = "fecha_entrada";
        $campos['hora_entrada'] = "hora_entrada";
        $campos['observacion'] = "observacion";
        $campos['hora_reg'] = "hora_reg";
        $campos['fecha_reg'] = "fecha_reg";

        $condicion['id_entrada'] = $id_entrada;
        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectdatosentrada($conect, $id_solicitud) {
        $nameTabla['almacen_tecnico.t_entrada'] = "almacen_tecnico.t_entrada";
        $campos['observacion'] = "observacion";
        $campos['resp_entrada'] = "resp_entrada";
        $campos['fecha_entrada'] = "fecha_entrada";
        
        $condicion['id_solicitud'] = $id_solicitud;
        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectcantarticuloprestados($conect, $idsolicitud) {
        $nameTabla['almacen_tecnico.articulos_prestados'] = "almacen_tecnico.articulos_prestados";
        $campos['id_articulo'] = "id_articulo";

        $condicion['id_solicitud'] = $idsolicitud;
        $condicion['fecha_exp'] ="'2222-12-31'";
        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectgrupoprestado($conect, $idsolicitud, $id_incidencia) {
        $nameTabla['almacen_tecnico.t_grupo_sol'] = "almacen_tecnico.t_grupo_sol";
        $campos['id_grupo'] = "id_grupo";


        $condicion['id_solicitud'] = $idsolicitud;
        $condicion['id_incidencia'] = "'".$id_incidencia."'";
        //$condicion['fecha_ent'] ="'2222-12-31'";
        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectgrupoprestadoinc($conect, $idsolicitud, $id_incidencia) {
        $nameTabla['almacen_tecnico.t_grupo_sol'] = "almacen_tecnico.t_grupo_sol";
        $campos['id_grupo'] = "id_grupo";


        $condicion['id_solicitud'] = $idsolicitud;
        $condicion['id_incidencia'] = "'".$id_incidencia."'";
        //$condicion['fecha_ent'] ="'2222-12-31'";
        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectgruposprestados($conect, $idsolicitud) {
        $nameTabla['almacen_tecnico.t_grupo_sol'] = "almacen_tecnico.t_grupo_sol";
        $campos['id_grupo'] = "id_grupo";


        $condicion['id_solicitud'] = $idsolicitud;
        $condicion['fecha_ent'] ="'2222-12-31'";
        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectsumdesp($conect, $idsolicitud) {
        $nameTabla['almacen_tecnico.t_material_desp'] = "almacen_tecnico.t_material_desp";
        $campos['id_articulo'] = "id_articulo";
        $campos['cantidad'] = "cantidad";

        //$condicion['id_despacho'] = $idsolicitud;
        //$condicion['cantidad'] != $dif;
        //$condicion['fecha_exp'] = "'2222-12-31'";

        $condicion ="id_despacho = ".$idsolicitud." and fecha_exp = '2222-12-31' and cantidad != '0'" ;
        
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelectLibre($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }
    function selectgrupoentregados($conect, $idsolicitud) {
        $nameTabla['almacen_tecnico.t_grupo_sol'] = "almacen_tecnico.t_grupo_sol";
        $campos['id_grupo'] = "id_grupo";
        $campos['fecha_ent'] = "fecha_ent";
         
        $condicion ="id_solicitud = ".$idsolicitud." and fecha_ent != '2222-12-31'";
         
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelectLibre($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }
    function selectgrupoporentregar($conect, $idsolicitud) {
        $nameTabla['almacen_tecnico.t_grupo_sol'] = "almacen_tecnico.t_grupo_sol";
        $campos['id_grupo'] = "id_grupo";
        $campos['fecha_ent'] = "fecha_ent";
         
        $condicion ="id_solicitud = ".$idsolicitud." and fecha_ent = '2222-12-31'";
         
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelectLibre($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectfechagrupoentregado($conect, $idsolicitud, $id_grupo) {
        $nameTabla['almacen_tecnico.t_grupo_sol'] = "almacen_tecnico.t_grupo_sol";
        $campos['fecha_ent'] = "fecha_ent";


        $condicion['id_solicitud'] = $idsolicitud;
        $condicion['id_grupo'] = "'".$id_grupo."'";
        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectfechacamaraentregado($conect, $idsolicitud, $id_grupo) {
        $nameTabla['almacen_tecnico.t_camara_sol'] = "almacen_tecnico.t_camara_sol";
        $campos['fecha_ent'] = "fecha_ent";


        $condicion['id_solicitud'] = $idsolicitud;
        $condicion['id_grupo'] = "'".$id_grupo."'";
        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectequipoprestado($conect, $idsolicitud) {
        $nameTabla['almacen_tecnico.t_material_sol'] = "almacen_tecnico.t_material_sol";
        $campos['id_articulo'] = "id_articulo";


        $condicion['id_solicitud'] = $idsolicitud;
        $condicion['fecha_ent'] ="'2222-12-31'";
        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectequipoentregados($conect, $idsolicitud) {
        $nameTabla['almacen_tecnico.t_material_sol'] = "almacen_tecnico.t_material_sol";
        $campos['id_articulo'] = "id_articulo";


        /*$condicion['id_solicitud'] = $idsolicitud;
        $condicion['fecha_exp'] !="'2222-12-31'";*/
        $condicion ="id_solicitud = ".$idsolicitud." and fecha_ent != '2222-12-31'";
        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelectLibre($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectequipoporentregar($conect, $idsolicitud) {
        $nameTabla['almacen_tecnico.t_material_sol'] = "almacen_tecnico.t_material_sol";
        $campos['id_articulo'] = "id_articulo";


        /*$condicion['id_solicitud'] = $idsolicitud;
        $condicion['fecha_exp'] !="'2222-12-31'";*/
        $condicion ="id_solicitud = ".$idsolicitud." and fecha_ent = '2222-12-31'";
        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelectLibre($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectfechaequipoentregado($conect, $idsolicitud, $id_articulo) {
        $nameTabla['almacen_tecnico.t_material_sol'] = "almacen_tecnico.t_material_sol";
        $campos['fecha_ent'] = "fecha_ent";


        $condicion['id_solicitud'] = $idsolicitud;
        $condicion['id_articulo'] = $id_articulo;
        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectcamaraprestada($conect, $idsolicitud) {
        $nameTabla['almacen_tecnico.t_camara_sol'] = "almacen_tecnico.t_camara_sol";
        $campos['id_grupo'] = "id_grupo";


        $condicion['id_solicitud'] = $idsolicitud;
        $condicion['fecha_ent'] ="'2222-12-31'";
        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectcamaraspresrep($conect, $fechadesde, $fechahasta, $id_grupo) {
        $nameTabla['almacen_tecnico.t_camara_sol'] = "almacen_tecnico.t_camara_sol";
        $campos['id_grupo'] = "id_grupo";
        $campos['id_solicitud'] = "id_solicitud";
        $campos['fecha_sol'] = "fecha_sol";
        $campos['user_reg'] = "user_reg";
        $campos['fecha_ent'] = "fecha_ent";
        $campos['user_exp'] = "user_exp";
        $campos['hora_exp'] = "hora_exp";
        
        $condicion ="id_grupo= '".$id_grupo."' and (fecha_sol  between '".$fechadesde."' and '".$fechahasta."')";
        $orderby['id_solicitud'] = "id_solicitud";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelectLibre($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
   }

    function selectcamaraentregadas($conect, $idsolicitud) {
        $nameTabla['almacen_tecnico.t_camara_sol'] = "almacen_tecnico.t_camara_sol";
        $campos['id_grupo'] = "id_grupo";


        /*$condicion['id_solicitud'] = $idsolicitud;
        $condicion['fecha_exp'] !="'2222-12-31'";*/
        $condicion ="id_solicitud = ".$idsolicitud." and fecha_ent != '2222-12-31'";
        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelectLibre($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectcamaraporentregar($conect, $idsolicitud) {
        $nameTabla['almacen_tecnico.t_camara_sol'] = "almacen_tecnico.t_camara_sol";
        $campos['id_grupo'] = "id_grupo";


        /*$condicion['id_solicitud'] = $idsolicitud;
        $condicion['fecha_exp'] !="'2222-12-31'";*/
        $condicion ="id_solicitud = ".$idsolicitud." and fecha_ent = '2222-12-31'";
        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelectLibre($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }



    function selectdatosgrupo($conect, $id_grupo) {
        $nameTabla['almacen_tecnico.t_grupos'] = "almacen_tecnico.t_grupos";
        $campos['id_grupo'] = "id_grupo";
        $campos['descripcion'] = "descripcion";
        $campos['imagen'] = "imagen";
        $campos['id_asig'] = "id_asig";



        $condicion['id_grupo'] = "'".$id_grupo."'";
        $condicion['fecha_exp'] ="'2222-12-31'";
        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectequipoprestados($conect, $idsolicitud, $id_incidencia) {
        $nameTabla['almacen_tecnico.t_material_sol'] = "almacen_tecnico.t_material_sol";
        $campos['id_articulo'] = "id_articulo";
        

        $condicion['id_solicitud'] = $idsolicitud;
        $condicion['id_incidencia'] = "'".$id_incidencia."'";
        //$condicion['fecha_ent'] ="'2222-12-31'";
        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    } 

    function selectequipoprestadosinc($conect, $idsolicitud, $id_incidencia) {
        $nameTabla['almacen_tecnico.t_material_sol'] = "almacen_tecnico.t_material_sol";
        $campos['id_articulo'] = "id_articulo";
        

        $condicion['id_solicitud'] = $idsolicitud;
        $condicion['id_incidencia'] = "'".$id_incidencia."'";
        //$condicion['fecha_ent'] ="'2222-12-31'";
        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    } 

    function selectequiposprestados($conect, $idsolicitud) {
        $nameTabla['almacen_tecnico.t_material_sol'] = "almacen_tecnico.t_material_sol";
        $campos['id_articulo'] = "id_articulo";
        

        $condicion['id_solicitud'] = $idsolicitud;
        $condicion['fecha_ent'] ="'2222-12-31'";
        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }
    function selectdatosequipo($conect, $id_articulo) {
        $nameTabla['almacen_tecnico.t_articulo'] = "almacen_tecnico.t_articulo";
        $campos['id_articulo'] = "id_articulo";
        $campos['tipo_articulo'] = "tipo_articulo";
        $campos['descripcion'] = "descripcion";
        $campos['marca'] = "marca";
        $campos['modelo'] = "modelo";
        $campos['bien_nac'] = "bien_nac";
        $campos['serial'] = "serial";
        $campos['siglas'] = "siglas";
        $campos['id_grupo'] = "id_grupo";
        $campos['peldano'] = "peldano";
        $campos['estante'] = "estante";
        $campos['cantidad'] = "cantidad";
        $campos['costo'] = "costo";
        $campos['id_estado'] = "id_estado";
        $campos['id_categoria'] = "id_categoria";
        $campos['id_unidad_medida'] = "id_unidad_medida";
        $campos['imagen'] = "imagen";
        

        $condicion['id_articulo'] = $id_articulo;
        $condicion['fecha_exp'] ="'2222-12-31'";
        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }


    function selectccamaraprestada($conect, $idsolicitud) {
        $nameTabla['almacen_tecnico.t_camara_sol'] = "almacen_tecnico.t_camara_sol";
        $campos['id_grupo'] = "id_grupo";
        

        $condicion['id_solicitud'] = $idsolicitud;
        //$condicion['id_incidencia'] = "'".$id_incidencia."'";
        //$condicion['fecha_ent'] ="'2222-12-31'";
        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectccamarasprestadas($conect, $idsolicitud, $id_incidencia) {
        $nameTabla['almacen_tecnico.t_camara_sol'] = "almacen_tecnico.t_camara_sol";
        $campos['id_grupo'] = "id_grupo";
        

        $condicion['id_solicitud'] = $idsolicitud;
        $condicion['id_incidencia'] = "'".$id_incidencia."'";
        //$condicion['fecha_ent'] ="'2222-12-31'";
        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectccamarasprestada($conect, $idsolicitud) {
        $nameTabla['almacen_tecnico.t_camara_sol'] = "almacen_tecnico.t_camara_sol";
        $campos['id_grupo'] = "id_grupo";
        

        $condicion['id_solicitud'] = $idsolicitud;
        //$condicion['fecha_ent'] ="'2222-12-31'";
        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectccamaraprestadainc($conect, $idsolicitud, $id_incidencia) {
        $nameTabla['almacen_tecnico.t_camara_sol'] = "almacen_tecnico.t_camara_sol";
        $campos['id_grupo'] = "id_grupo";
        

        $condicion['id_solicitud'] = $idsolicitud;
        $condicion['id_incidencia'] = "'".$id_incidencia."'";;
        //$condicion['fecha_ent'] ="'2222-12-31'";
        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }


    function selectsolicitud($conect, $idsolicitud) {
        $nameTabla['almacen_tecnico.t_prestamo'] = "almacen_tecnico.t_prestamo";
        $campos['id_prestamo'] = "id_prestamo";
        $campos['tipo_prestamo'] = "tipo_prestamo";


        $condicion['id_prestamo'] = $idsolicitud;
        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectdespachossum($conect, $idsolicitud) {
        $nameTabla['almacen_tecnico.t_despacho'] = "almacen_tecnico.t_despacho";
        $campos['id_despacho'] = "id_despacho";
        
        $condicion['id_despacho'] = $idsolicitud;
        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function updateartpres($conect, $art, $user_exp){
        // Tabla para hacer la consulta
        $nameTabla = 'almacen_tecnico.t_material_sol';
        $campos['fecha_ent'] = date("Y-m-d");
        $campos['user_exp'] = $user_exp;
        $campos['hora_exp'] = date("H:i:s");

        $condicion['id_articulo'] = $art;
        $condicion['fecha_ent'] = '2222-12-31';
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function updategrupres($conect, $art, $user_exp){
        // Tabla para hacer la consulta
        $nameTabla = 'almacen_tecnico.t_grupo_sol';
        $campos['fecha_ent'] = date("Y-m-d");
        $campos['user_exp'] = $user_exp;
        $campos['hora_exp'] = date("H:i:s");


        $condicion['id_grupo'] = $art;
        $condicion['fecha_ent'] = '2222-12-31';
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function updatecampres($conect, $art, $user_exp){
        // Tabla para hacer la consulta
        $nameTabla = 'almacen_tecnico.t_camara_sol';
        $campos['fecha_ent'] = date("Y-m-d");
        $campos['user_exp'] = $user_exp;
        $campos['hora_exp'] = date("H:i:s");

        $condicion['id_grupo'] = $art;
        $condicion['fecha_ent'] = '2222-12-31';
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }


    function updateestadopart($conect, $art, $id_estado){
        // Tabla para hacer la consulta
        $nameTabla = 'almacen_tecnico.t_articulo';
        $campos['id_estado'] = $id_estado;

        $condicion['id_articulo'] = $art;
        $condicion['fecha_exp'] = '2222-12-31';
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function updateestadopgrupo($conect, $art, $id_estado){
        // Tabla para hacer la consulta
        $nameTabla = 'almacen_tecnico.t_grupos';
        $campos['id_desc_estado'] = $id_estado;

        $condicion['id_grupo'] = $art;
        $condicion['fecha_exp'] = '2222-12-31';
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }


    function updatedispart($conect, $art, $id_estatus){
        // Tabla para hacer la consulta
        $nameTabla = 'almacen_tecnico.t_articulo';
        $campos['id_estatus'] = $id_estatus;

        $condicion['id_articulo'] = $art;
        $condicion['fecha_exp'] = '2222-12-31';
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function updatedispgrupo($conect, $art, $id_estatus){
        // Tabla para hacer la consulta
        $nameTabla = 'almacen_tecnico.t_grupos';
        $campos['id_estatus'] = $id_estatus;

        $condicion['id_grupo'] = $art;
        $condicion['fecha_exp'] = '2222-12-31';
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function updatedispartgrupo($conect, $art, $id_estatus){
        // Tabla para hacer la consulta
        $nameTabla = 'almacen_tecnico.t_articulo';
        $campos['id_estatus'] = $id_estatus;

        $condicion['id_grupo'] = $art;
        $condicion['fecha_exp'] = '2222-12-31';
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function updatesolicitud($conect, $idsolicitud, $id_estatus_prestamo){
        // Tabla para hacer la consulta
        $nameTabla = 'almacen_tecnico.t_prestamo';
        $campos['id_estatus_prestamo'] = $id_estatus_prestamo;

        $condicion['id_prestamo'] = $idsolicitud;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function updatesolicitudsusp($conect, $idsolicitud, $id_estatus_prestamo, $quiensol, $obs_susp){
        // Tabla para hacer la consulta
        $nameTabla = 'almacen_tecnico.t_prestamo';
        $campos['id_estatus_prestamo'] = $id_estatus_prestamo;
        $campos['resp_susp'] = $quiensol;
        $campos['obs_susp'] = $obs_susp;



        $condicion['id_prestamo'] = $idsolicitud;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function updatearticulos($conect, $tipoart, $descripcion, $marca, $modelo, $biennac, $serial, $siglas, $id_grupo, 
            $id_categoria, $peldano, $estante, $id_estado, $observacion, $unidad_medida, $costo, 
            $cantidad,$id,$tipodesc){
        // Tabla para hacer la consulta
        $nameTabla = 'almacen_tecnico.t_articulo';
        $campos['tipo_articulo'] = $tipoart;
        $campos['descripcion'] = $descripcion;
        $campos['marca'] = $marca;
        $campos['modelo'] = $modelo;
        $campos['bien_nac'] = $biennac;
        $campos['serial'] = $serial;
        $campos['siglas'] = $siglas;
        $campos['id_grupo'] = $id_grupo;
        $campos['peldano'] = $peldano;
        $campos['estante'] = $estante;
        $campos['cantidad'] = $cantidad;
        $campos['costo'] = $costo;
        $campos['id_estado'] = $id_estado;
        $campos['id_categoria'] = $id_categoria;
        $campos['id_unidad_medida'] = $unidad_medida;
        $campos['id_descripcion'] = $tipodesc;
       
        $condicion['id_articulo'] = $id;
        $condicion['fecha_exp'] = '2222-12-31';
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function updategrupos($conect, $id_categoria, $descripcion, $id, $id_asig, $id_desc_estado){
        // Tabla para hacer la consulta
        $nameTabla = 'almacen_tecnico.t_grupos';
        $campos['descripcion'] = $descripcion;
        $campos['id_categoria'] = $id_categoria;
        $campos['id_asig'] = $id_asig;
        $campos['id_desc_estado'] = $id_desc_estado;
       
        $condicion['id_grupo'] = $id;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }


    function updatefile1($conect, $file, $id){
        // Tabla para hacer la consulta
        $nameTabla = 'almacen_tecnico.t_articulo';
        $campos['imagen'] = $file;

        $condicion['id_articulo'] = $id;
        $condicion['fecha_exp'] = '2222-12-31';
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function updatefile2($conect, $file, $id){
        // Tabla para hacer la consulta
        $nameTabla = 'almacen_tecnico.t_grupos';
        $campos['imagen'] = $file;

        $condicion['id_grupo'] = $id;
        $condicion['fecha_exp'] = '2222-12-31';
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }


    function updatearticulo($conect, $id, $user_exp) {
        // Tabla para hacer la consulta
        $nameTabla = 'almacen_tecnico.t_articulo';
        $campos['fecha_exp'] = date("Y-m-d");
        $campos['user_exp'] = $user_exp;
        $condicion['fecha_exp'] = '2222-12-31';
        $condicion['id_articulo'] = $id;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function updategrupo($conect, $id, $user_exp) {
        // Tabla para hacer la consulta
        $nameTabla = 'almacen_tecnico.t_grupos';
        $campos['fecha_exp'] = date("Y-m-d");
        $campos['user_exp'] = $user_exp;
        $condicion['fecha_exp'] = '2222-12-31';
        $condicion['id_grupo'] = $id;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }


    function updeteartgrupo($conect, $id,$id_grupo) {
        // Tabla para hacer la consulta
        $nameTabla = 'almacen_tecnico.t_articulo';
        $campos['id_grupo'] = $id_grupo;
        
        $condicion['fecha_exp'] = '2222-12-31';
        $condicion['id_articulo'] = $id;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function updateestado($conect, $variable, $user_exp) {
        // Tabla para hacer la consulta
        $nameTabla = 'almacen_tecnico.t_des_estado';
        $campos['fecha_exp'] = date("Y-m-d");
        $campos['user_exp'] = $user_exp;

        $condicion['fecha_exp'] = '2222-12-31';
        $condicion['id_desc_estado'] = $variable;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function updatecategoria($conect, $variable, $user_exp) {
        // Tabla para hacer la consulta
        $nameTabla = 'almacen_tecnico.t_categoria';
        $campos['fecha_exp'] = date("Y-m-d");
        $campos['user_exp'] = $user_exp;

        $condicion['fecha_exp'] = '2222-12-31';
        $condicion['id_categoria'] = $variable;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function updateunidadmedida($conect, $variable, $user_exp) {
        // Tabla para hacer la consulta
        $nameTabla = 'almacen_tecnico.t_unidad_medida';
        $campos['fecha_exp'] = date("Y-m-d");
        $campos['user_exp'] = $user_exp;

        $condicion['fecha_exp'] = '2222-12-31';
        $condicion['id_unidad_medida'] = $variable;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function updateprograma($conect, $variable, $user_exp) {
        // Tabla para hacer la consulta
        $nameTabla = 'almacen_tecnico.t_programa';
        $campos['fecha_exp'] = date("Y-m-d");
        $campos['user_exp'] = $user_exp;

        $condicion['fecha_exp'] = '2222-12-31';
        $condicion['id_programa'] = $variable;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function updatetipodesc($conect, $variable, $user_exp) {
        // Tabla para hacer la consulta
        $nameTabla = 'almacen_tecnico.t_descripciones';
        $campos['fecha_exp'] = date("Y-m-d");
        $campos['user_exp'] = $user_exp;

        $condicion['fecha_exp'] = '2222-12-31';
        $condicion['id_descripcion'] = $variable;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }


    function updatestock($conect, $idarticulo, $costo, $cantidad) {
        // Tabla para hacer la consulta
        $nameTabla = 'almacen_tecnico.t_articulo';
        $campos['costo'] =$costo;
        $campos['cantidad'] =$cantidad;
       
        $condicion['fecha_exp'] = '2222-12-31';
        $condicion['id_articulo'] = $idarticulo;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }


//////////////////////////////////Solicitudes//////////////////////////////////////////////////////////
    function selectpersonal($conect) {
        $nameTabla['v_sistemas_vtv_datos_trabajador'] = "v_sistemas_vtv_datos_trabajador";
        $campos['cedula'] = "cedula";
        $campos['nombres'] = "nombres";
        $campos['apellidos'] = "apellidos";

        $condicion ="cargo ilike '%PRODUCTOR%' or (gerencia ilike '%SERVICIOS A LA PRODUCCION%') or (gerencia ilike '%PROGRAMAS%') or (gerencia ilike '%SERVICIOS INFORMATIVOS%')";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelectLibre($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectpersonalA($conect) {
        $nameTabla['v_sistemas_vtv_datos_trabajador'] = "v_sistemas_vtv_datos_trabajador";
        $campos['cedula'] = "cedula";
        $campos['nombres'] = "nombres";
        $campos['apellidos'] = "apellidos";

        $condicion ="gerencia ilike '%programas%' or gerencia ilike '%servicios a la produccion%' ";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelectLibre($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectpersonalC($conect) {
        $nameTabla['v_sistemas_vtv_datos_trabajador'] = "v_sistemas_vtv_datos_trabajador";
        $campos['cedula'] = "cedula";
        $campos['nombres'] = "nombres";
        $campos['apellidos'] = "apellidos";

        $condicion ="cargo ilike '%camarografo%' or cargo ilike '%asistente de camara%'";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelectLibre($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectpersonalaut($conect) {
        $nameTabla['v_sistemas_vtv_datos_trabajador'] = "v_sistemas_vtv_datos_trabajador";
        $campos['cedula'] = "cedula";
        $campos['nombres'] = "nombres";
        $campos['apellidos'] = "apellidos";

        $condicion ="cargo ilike '%camarografo%' or cargo ilike '%asistente de camara%' or gerencia ilike '%servicios a la produccion%' or gerencia ilike '%programas%'";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelectLibre($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

   
    function selectpersonalresp($conect, $cedula) {
       $nameTabla['v_sistemas_vtv_datos_trabajador'] = "v_sistemas_vtv_datos_trabajador";
        $campos['cedula'] = "cedula";
        $campos['nombres'] = "nombres";
        $campos['apellidos'] = "apellidos";
        $campos['cargo'] = "cargo";


        $condicion['cedula'] = "'".$cedula."'";
        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    } 



    function selectgerencia($conect) {
        $nameTabla['srh_gerencia'] = "srh_gerencia";
        $campos['codger'] = "codger";
        $campos['denger'] = "denger";

        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectgerenciadesc($conect, $idgerencia) {
        $nameTabla['srh_gerencia'] = "srh_gerencia";
        $campos['codger'] = "codger";
        $campos['denger'] = "denger";

        $condicion['codger'] = "'".$idgerencia."'";
        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectdatossol($conect, $idsolicitud) {
        $nameTabla['almacen_tecnico.t_prestamo'] = "almacen_tecnico.t_prestamo";

        $campos['id_destino'] = "id_destino";
        $campos['id_desc_dest'] = "id_desc_dest";
        $campos['resp_prestamo'] = "resp_prestamo";
        $campos['user_reg'] = "user_reg";
        $campos['fecha_reg'] = "fecha_reg";
        $campos['fecha_sol'] = "fecha_sol";
        $campos['fecha_exp'] = "fecha_exp";
        $campos['id_estatus_prestamo'] = "id_estatus_prestamo";
        $campos['observacion'] = "observacion";
        $campos['hora_prestamo'] = "hora_prestamo";
        $campos['hora_reg'] = "hora_reg";
        $campos['tipo_prestamo'] = "tipo_prestamo";
        $campos['resp_susp'] = "resp_susp";
        $campos['obs_susp'] = "obs_susp";

        $condicion['id_prestamo'] = "$idsolicitud";
        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectdespachosum($conect, $idsolicitud) {
        $nameTabla['almacen_tecnico.t_despacho'] = "almacen_tecnico.t_despacho";

        $campos['id_despacho'] = "id_despacho";
        $campos['resp_despacho'] = "resp_despacho";
        $campos['id_destino'] = "id_destino";
        $campos['id_desc_dest'] = "id_desc_dest";
        $campos['user_reg'] = "user_reg";
        $campos['fecha_reg'] = "fecha_reg";
        $campos['fecha_sol'] = "fecha_sol";
        $campos['observacion'] = "observacion";
        
        $condicion['id_despacho'] = "$idsolicitud";
        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectprograma($conect, $id_desc_dest) {
        $nameTabla['almacen_tecnico.t_programa'] = "almacen_tecnico.t_programa";

        $campos['id_programa'] = "id_programa";
        $campos['descripcion'] = "descripcion";

        $condicion['id_programa'] = "$id_desc_dest";

        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }
    function selectidasig($conect, $id_grupo) {
        $nameTabla['almacen_tecnico.t_grupos'] = "almacen_tecnico.t_grupos";

        $campos['id_asig'] = "id_asig";

        $condicion['id_grupo'] = "'".$id_grupo."'";
        $condicion['fecha_exp'] = "'2222-12-31'";
        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectidprestamogrupo($conect, $id_grupo) {
        $nameTabla['almacen_tecnico.t_grupo_sol'] = "almacen_tecnico.t_grupo_sol";

        $campos['id_solicitud'] = "id_solicitud";

        $condicion['id_grupo'] = "'".$id_grupo."'";
        $condicion['fecha_ent'] = "'2222-12-31'";
        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }
    function selectidprestamocamara($conect, $id_grupo) {
        $nameTabla['almacen_tecnico.t_camara_sol'] = "almacen_tecnico.t_camara_sol";

        $campos['id_solicitud'] = "id_solicitud";

        $condicion['id_grupo'] = "'".$id_grupo."'";
        $condicion['fecha_ent'] = "'2222-12-31'";
        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }
    function selectidprestamoequipo($conect, $id_articulo) {
        $nameTabla['almacen_tecnico.t_material_sol'] = "almacen_tecnico.t_material_sol";

        $campos['id_solicitud'] = "id_solicitud";

        $condicion['id_articulo'] = "$id_articulo";
        $condicion['fecha_ent'] = "'2222-12-31'";
        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }
    function selectdatosart($conect, $id_articulo) {
        $nameTabla['almacen_tecnico.t_articulo'] = "almacen_tecnico.t_articulo";

        $campos['id_grupo'] = "id_grupo";
        
        $condicion['id_articulo'] = "$id_articulo";
        $condicion['fecha_exp'] = "'2222-12-31'";
        $this->ObjDb = new classdb($conect); 
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function insertentrada($conect, $idsolicitud, $resp_entrada, $user_reg, $fecha, $observacion, $hora) {

        // Nombre de la Tabla
        $nameTabla = "almacen_tecnico.t_entrada";
        // Campos para hacer el insert
        $campos["id_solicitud"] = $idsolicitud;
        $campos['resp_entrada'] = $resp_entrada;
        $campos['fecha_entrada'] = $fecha;
        $campos['hora_entrada'] = $hora;
        $campos['user_reg'] = $user_reg;
        $campos['observacion'] = $observacion;
        $campos['hora_reg'] = date("H:i:s");
        $campos['fecha_reg'] = date("Y-m-d");

        $campo_return = "id_entrada";
        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsertRn($nameTabla, $campos, $campo_return);
    }

    function updatefechaentrada($conect, $idsolicitud) {
        // Tabla para hacer la consulta
        $nameTabla = "almacen_tecnico.t_prestamo";
        $campos['fecha_ent'] = date("Y-m-d");

        $condicion['id_prestamo'] = $idsolicitud;
        $condicion['fecha_ent'] = '2222-12-31';
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    } 

    function updateresponsable($conect, $idgrupo, $responsable) {
        // Tabla para hacer la consulta
        $nameTabla = "almacen_tecnico.t_grupos";
        $campos['resp_asig'] = $responsable;
        $campos['asig'] = 1;

        $condicion['id_grupo'] = $idgrupo;
        $condicion['fecha_exp'] = '2222-12-31';
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    } 

    function insertstock($conect, $idarticulo, $costo, $cantidad, $user_reg) {

        // Nombre de la Tabla
        $nameTabla = "almacen_tecnico.t_ingresos";
        // Campos para hacer el insert
        $campos["id_articulo"] = $idarticulo;
        $campos['costo'] = $costo;
        $campos['cantidad'] = $cantidad;
        $campos['user_reg'] = $user_reg;
        $campos['fecha_reg'] = date("Y-m-d");
        $campos['hora_reg'] = date("H:i:s");

        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////    
    /*
    function selectdescgerencia($conect, $perfilmod) {
        $nameTabla['usuario.t_tipo_usuario'] = "usuario.t_tipo_usuario";

        $campos['desc_gerencia'] = "desc_gerencia";

        $condicion['id_gerencia'] = $idgerencia;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }*/
////////////////////////////guardarpauta_f5/////////////////////////////////////

    function insertlugar($conect, $lugar) {
        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_lugar";
        // Campos para hacer el insert
        $campos['descripcion'] = $lugar;

        $campo_return = "id_lugar";
        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsertRn($nameTabla, $campos, $campo_return);
    }

    function insertcitacion($conect, $fecha1, $hora1, $id_lugarcitacion) {
        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_citacion";
        // Campos para hacer el insert
        $campos['fecha'] = $fecha1;
        $campos['hora'] = $hora1;
        $campos['id_lugar'] = $id_lugarcitacion;

        $campo_return = "id_citacion";
        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsertRn($nameTabla, $campos, $campo_return);
    }

    function insertmontaje($conect, $fecha2, $hora2, $id_lugarmontaje) {
        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_montaje";
        // Campos para hacer el insert
        $campos['fecha'] = $fecha2;
        $campos['hora'] = $hora2;
        //$campos['id_lugar']=$id_lugarmontaje;

        $campo_return = "id_montaje";
        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsertRn($nameTabla, $campos, $campo_return);
    }

    function insertemision($conect, $fecha3, $hora3, $id_lugaremision) {
        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_emision_grabacion";
        // Campos para hacer el insert
        $campos['fecha'] = $fecha3;
        $campos['hora'] = $hora3;
        //$campos['id_lugar']=$id_lugaremision;

        $campo_return = "id_emision_grabacion";
        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsertRn($nameTabla, $campos, $campo_return);
    }

    function insertretorno($conect, $fecha4, $hora4, $id_lugarretorno) {
        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_retorno";
        // Campos para hacer el insert
        $campos['fecha'] = $fecha4;
        $campos['hora'] = $hora4;
        //$campos['id_lugar']=$id_lugarretorno;

        $campo_return = "id_retorno";
        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsertRn($nameTabla, $campos, $campo_return);
    }

    function insertdatospauta($conect, $ced_sesion, $pauta, $locacion, $tipotraje, $programa, $evento, $id_citacion, $id_montaje, $id_emision, $id_retorno, $tipoevento, $lugar_pauta, $des_evento) {
        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_pauta";
        // Campos para hacer el insert
        $campos['fecha_reg'] = date("Y-m-d");
        $campos['user_reg'] = $ced_sesion;
        $campos['id_tipo_pauta'] = $pauta;
        $campos['id_locacion'] = $locacion;
        $campos['id_tipo_traje'] = $tipotraje;
        $campos['id_program'] = $programa;
        $campos['id_citacion'] = $id_citacion;
        $campos['id_montaje'] = $id_montaje;
        $campos['id_emision_grabacion'] = $id_emision;
        $campos['id_retorno'] = $id_retorno;
        $campos['id_tipo_evento'] = $tipoevento;
        $campos['lugar'] = strtoupper($lugar_pauta);
        $campos['descripcion'] = strtoupper($des_evento);

        $campo_return = "id_pauta";
        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsertRn($nameTabla, $campos, $campo_return);
    }

    function insertestatus($conect, $id_pauta, $estatus, $cedula, $observaciones=null) {
        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_estatus_pauta";
        // Campos para hacer el insert
        $campos['id_pauta'] = $id_pauta;
        $campos['id_estatus'] = $estatus;
        $campos['user_reg'] = $cedula;
        if($observaciones!=null){
            $campos['observaciones'] = $observaciones;
        }
        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    }

/////////////////////////////Mostrar Pauta////////////////////////////////////////////////


    function selectservicios($conect) {
        $nameTabla['inventario.materialtipomaterialinv'] = "inventario.materialtipomaterialinv";
        $campos['idtipomaterial'] = "idtipomaterial";
        $campos['valorinventariado'] = "valorinventariado";
        $campos['desctipomaterial'] = "desctipomaterial";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectidspauta($conect, $pauta) {
        $nameTabla[$this->prefix_table . 't_pauta'] = $this->prefix_table . "t_pauta";
        $campos['id_pauta'] = "id_pauta";
        $campos['id_tipo_pauta'] = "id_tipo_pauta";
        $campos['id_locacion'] = "id_locacion";
        $campos['id_tipo_traje'] = "id_tipo_traje";
        $campos['id_program'] = "id_program";
        $campos['id_citacion'] = "id_citacion";
        $campos['id_montaje'] = "id_montaje";
        $campos['id_emision_grabacion'] = "id_emision_grabacion";
        $campos['id_retorno'] = "id_retorno";
        $campos['id_tipo_evento'] = "id_tipo_evento";
        $campos['user_reg'] = "user_reg";
        $campos['lugar'] = "lugar";
        $campos['descripcion'] = "descripcion";
        $condicion['id_pauta'] = $pauta;
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function updatecitacion($conect, $id_citacion, $fecha, $hora, $id_lugar) {
        // Tabla para hacer la consulta
        $nameTabla = $this->prefix_table . 't_citacion';
        $campos['fecha'] = $fecha;
        $campos['hora'] = $hora;
        $campos['id_lugar'] = $id_lugar;

        $condicion['id_citacion'] = $id_citacion;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function updateemision($conect, $id_emision_grabacion, $fecha, $hora) {
        // Tabla para hacer la consulta
        $nameTabla = $this->prefix_table . 't_emision_grabacion';
        $campos['fecha'] = $fecha;
        $campos['hora'] = $hora;
        $condicion['id_emision_grabacion'] = $id_emision_grabacion;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function updatemontaje($conect, $id_montaje, $fecha, $hora) {
        // Tabla para hacer la consulta
        $nameTabla = $this->prefix_table . 't_montaje';
        $campos['fecha'] = $fecha;
        $campos['hora'] = $hora;
        $condicion['id_montaje'] = $id_montaje;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function updateretorno($conect, $id_retorno, $fecha, $hora) {
        // Tabla para hacer la consulta
        $nameTabla = $this->prefix_table . 't_retorno';
        $campos['fecha'] = $fecha;
        $campos['hora'] = $hora;
        $condicion['id_retorno'] = $id_retorno;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function updatepauta($conect, $id_pauta, $id_tipo_pauta, $id_locacion, $id_tipo_traje, $id_program, $id_tipo_evento, $lugar, $descripcion) {
        // Tabla para hacer la consulta
        $nameTabla = $this->prefix_table . 't_pauta';
        $campos['id_tipo_pauta'] = $id_tipo_pauta;
        $campos['id_locacion'] = $id_locacion;
        $campos['id_tipo_traje'] = $id_tipo_traje;
        $campos['id_program'] = $id_program;
        $campos['id_tipo_evento'] = $id_tipo_evento;
        $campos['lugar'] = strtoupper($lugar);
        $campos['descripcion'] = strtoupper($descripcion);
        $condicion['id_pauta'] = $id_pauta;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function selectdescprograma($conect, $id_program) {
        $nameTabla[$this->prefix_table . 't_programa'] = $this->prefix_table . "t_programa";

        $campos['id_program'] = "id_program";
        $campos['descripcion'] = "descripcion";
        $condicion['id_program'] = $id_program;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, null, $this->groupby, $this->limite);
    }

    function selectdesclocacion($conect, $id_locacion) {
        $nameTabla[$this->prefix_table . 't_locacion'] = $this->prefix_table . "t_locacion";

        $campos['id_locacion'] = "id_locacion";
        $campos['descripcion'] = "descripcion";
        $condicion['id_locacion'] = $id_locacion;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectdesctipotraje($conect, $id_tipo_traje) {
        $nameTabla[$this->prefix_table . 't_tipo_traje'] = $this->prefix_table . "t_tipo_traje";

        //$campos['id_tipo_traje'] = "id_tipo_traje";
        $campos['descripcion'] = "descripcion";
        $condicion['id_tipo_traje'] = $id_tipo_traje;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectdescproductor($conect, $cedula) {
        $nameTabla['sigai.grlpersona'] = "sigai.grlpersona";

        $campos['de_corto'] = "de_corto";
        $condicion['nu_documen'] = $cedula;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectnombreyapellido($conect, $cedula) {
        $nameTabla['sigai.grlpersona'] = "sigai.grlpersona";

        $campos['de_corto'] = "de_corto";
        $condicion['nu_documen'] = $cedula;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectnombreyapellidof5($conect, $cedula) {
        $nameTabla['usuario.t_datos_personales'] = "usuario.t_datos_personales";

        $campos['nombre_usuario'] = "nombre_usuario";
        $campos['apellido_usuario'] = "apellido_usuario";
        $condicion['cedula'] = $cedula;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectcargoquienaprobo($conect, $cedula) {
        $nameTabla['sigai.datos_trabajador'] = "sigai.datos_trabajador";

        $campos['de_cargo'] = "de_cargo";
        $condicion['nu_documen'] = $cedula;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectdesctipoevento($conect, $id_tipo_evento) {
        $nameTabla[$this->prefix_table . 't_tipo_evento'] = $this->prefix_table . "t_tipo_evento";

        $campos['id_tipo_evento'] = "id_tipo_evento";
        $campos['descripcion'] = "descripcion";
        $condicion['id_tipo_evento'] = $id_tipo_evento;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectcitacion($conect, $id_citacion) {
        $nameTabla[$this->prefix_table . 't_citacion'] = $this->prefix_table . "t_citacion";

        $campos['id_citacion'] = "id_citacion";
        $campos['fecha'] = "fecha";
        $campos['hora'] = "hora";
        $campos['id_lugar'] = "id_lugar";
        $condicion['id_citacion'] = $id_citacion;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectemision($conect, $id_emision_grabacion) {
        $nameTabla[$this->prefix_table . 't_emision_grabacion'] = $this->prefix_table . "t_emision_grabacion";

        $campos['id_emision_grabacion'] = "id_emision_grabacion";
        $campos['fecha'] = "fecha";
        $campos['hora'] = "hora";
        //$campos['id_lugar']="id_lugar";
        $condicion['id_emision_grabacion'] = $id_emision_grabacion;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectmontaje($conect, $id_montaje) {
        $nameTabla[$this->prefix_table . 't_montaje'] = $this->prefix_table . "t_montaje";

        $campos['id_montaje'] = "id_montaje";
        $campos['fecha'] = "fecha";
        $campos['hora'] = "hora";
        //$campos['id_lugar']="id_lugar";
        $condicion['id_montaje'] = $id_montaje;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectretorno($conect, $id_retorno) {
        $nameTabla[$this->prefix_table . 't_retorno'] = $this->prefix_table . "t_retorno";

        $campos['id_retorno'] = "id_retorno";
        $campos['fecha'] = "fecha";
        $campos['hora'] = "hora";
        //$campos['id_lugar']="id_lugar";
        $condicion['id_retorno'] = $id_retorno;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectlugar($conect, $id_lugar) {
        $nameTabla[$this->prefix_table . 't_lugar'] = $this->prefix_table . "t_lugar";

        $campos['id_lugar'] = "id_lugar";
        $campos['descripcion'] = "descripcion";
        $condicion['id_lugar'] = $id_lugar;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectlugar2($conect, $citacion="") {
        $nameTabla['f5.t_lugar'] = "f5.t_lugar";

        $campos['id_lugar'] = "id_lugar";
        $campos['descripcion'] = "descripcion";
        if ($citacion != "") {
            $condicion['id_lugar <'] = 2;
        } else {
            $condicion['id_lugar >'] = 3;
        }

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

/////////////////////////////////////////////////////////////////////////////////////////
    function selectdatospauta($conect, $id_pauta) {
        $nameTabla[$this->prefix_table . 'sys_main'] = $this->prefix_table . "sys_main";

        $campos['tipo_pauta'] = "tipo_pauta";
        $campos['nombre_programa'] = "nombre_programa";
        $campos['descripcion'] = "descripcion";
        $campos['productor'] = "productor";
        $campos['pauta_locacion'] = "pauta_locacion";
        $campos['tipo_evento'] = "tipo_evento";
        $campos['pauta_traje'] = "pauta_traje";
        $campos['citacion_lugar'] = "citacion_lugar";
        $campos['fecha_citacion'] = "fecha_citacion";
        $campos['hora_citacion'] = "hora_citacion";
        $campos['fecha_montaje'] = "fecha_montaje";
        $campos['hora_montaje'] = "hora_montaje";
        $campos['fecha_emision'] = "fecha_emision";
        $campos['hora_emision'] = "hora_emision";
        $campos['fecha_retorno'] = "fecha_retorno";
        $campos['hora_retorno'] = "hora_retorno";
        $campos['lugar'] = "lugar";


        $condicion['id_pauta'] = $id_pauta;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

/////////////////////////////consulta de estatus/////////////////////////////////////////

    function selectestatus($conect, $pauta) {
        $nameTabla[$this->prefix_table . 't_estatus_pauta'] = $this->prefix_table . "t_estatus_pauta";

        $campos['id_estatus'] = "id_estatus";
        $campos['id_estatus_pauta'] = "id_estatus_pauta";

        $condicion['id_pauta'] = $pauta;
        $condicion['fecha_exp'] = "'2222-12-31'";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectdescestatus($conect, $id_estatus) {
        $nameTabla['almacen_tecnico.t_estatus'] = "almacen_tecnico.t_estatus";

        $campos['descripcion'] = "descripcion";
        $condicion['id_estatus'] = $id_estatus;


        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectlistadepautas($conect, $cedula_sesion, $limiteinicio=0, $limitefin=null) {
        $nameTabla[$this->prefix_table . 't_pauta'] = $this->prefix_table . "t_pauta";
        $campos['id_pauta'] = "id_pauta";
        $condicion['user_reg'] = "'" . $cedula_sesion . "'";
        $orderby['id_pauta'] = "id_pauta DESC";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        if ($limitefin != null) {
            $limite['INICIO'] = $limiteinicio;
            $limite['FIN'] = $limitefin;
        }
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $limite);
    }

    function selectdescpauta($conect, $id) {
        $nameTabla[$this->prefix_table . 't_pauta'] = $this->prefix_table . "t_pauta";

        $campos['id_program'] = "id_program";
        $campos['id_evento'] = "id_evento";
        $condicion['id_pauta'] = $id;


        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectestatuspauta($conect, $id) {
        $nameTabla[$this->prefix_table . 't_estatus_pauta'] = $this->prefix_table . "t_estatus_pauta";

        $campos['id_estatus'] = "id_estatus";
        $condicion['id_pauta'] = $id;


        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectnomevento($conect, $evento) {
        $nameTabla[$this->prefix_table . 't_evento'] = $this->prefix_table . "t_evento";

        $campos['descripcion'] = "descripcion";
        $condicion['id_evento'] = $evento;


        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectnomprog($conect, $prog) {
        $nameTabla[$this->prefix_table . 't_programa'] = $this->prefix_table . "t_programa";

        $campos['descripcion'] = "descripcion";
        $condicion['id_program'] = $prog;


        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectnomestatus($conect, $est) {
        $nameTabla[$this->prefix_table . 't_estatus'] = $this->prefix_table . "t_estatus";

        $campos['descripcion'] = "descripcion";
        $condicion['id_estatus'] = $est;


        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

////////////////////////////Materiales///////////////////////////////////////////////

    /**
     *
     * @param type $conect ruta de conexion a la base de datos..
     * @param type $material el id del material
     * @param type $id_pauta el id de la pauta
     * @param type $ced_sesion la cedula de la persona que ejecuta la session
     * @param type $id_recurso_asignado id del recurso seleccionado
     * @return type id que retorna la insercion del query
     */
    function insertmaterial($conect, $material, $id_pauta, $ced_sesion, $id_recurso_asignado=null) {

        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_detalle_servicio";
        // Campos para hacer el insert
        $campos['id_recurso'] = $material;
        $campos['id_pauta'] = $id_pauta;
        $campos['fecha_reg'] = date("Y-m-d");
        $campos['user_reg'] = $ced_sesion;
        //permite agregar tambien recursos asignados....
        if ($id_recurso_asignado != null) {
            $campos['id_recurso_asignado'] = $id_recurso_asignado;
        }

        $campo_return = "id_detalle_servicio";
        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsertRn($nameTabla, $campos, $campo_return);
    }

    function selectlistmat($conect, $pauta) {
        $nameTabla[$this->prefix_table . 't_detalle_servicio'] = $this->prefix_table . "t_detalle_servicio";

        $campos['id_detalle_servicio'] = "id_detalle_servicio";
        $campos['id_recurso'] = "id_recurso";

        //$campos['cantidad']="cantidad";
        $condicion['id_pauta'] = $pauta;
        $condicion['id_recurso_asignado'] = "'1'";
        $condicion['fecha_exp'] = "'2222-12-31'";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }
    
    function selectlistmat_group($conect, $pauta=null, $id_recurso=null) {
        $nameTabla[$this->prefix_table . 'selectlistmat_group'] = $this->prefix_table . "selectlistmat_group";

        $campos['ids_detalles_servicios'] = "ids_detalles_servicios";
        $campos['id_recurso'] = "id_recurso";
        $campos['cantidad']="cantidad";
        if($pauta!=null){
            $condicion['id_pauta'] = $pauta;
        }
        
        if($id_recurso!=null){
            $condicion['id_recurso'] = $id_recurso;
        }
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectlistmatdesc($conect, $id_recurso) {
        $nameTabla['inventario.materialtipomaterialinv'] = "inventario.materialtipomaterialinv";
        $campos['desctipomaterial'] = "desctipomaterial";
        $condicion['idtipomaterial'] = $id_recurso;
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectdetalles($conect, $id_recurso) {
        $nameTabla['inventario.inventariostok'] = "inventario.inventariostok";

        $campos['desctipomaterial'] = "desctipomaterial";
        $campos['id_fintipoinventario'] = "id_fintipoinventario";

        $condicion['idtipomaterial'] = $id_recurso;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectlistmatger($conect, $id_recurso) {
        $nameTabla['inventario.inventariostok'] = "inventario.inventariostok";

        $campos['id_gerencia'] = "id_gerencia";

        $condicion['idtipomaterial'] = $id_recurso;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    //borra un recurso de la lista de recursos pedidos.
    function updatedelista($conect, $id_detalle_servicio, $cedula_sesion) {
        // Tabla para hacer la consulta
        $nameTabla = $this->prefix_table . 't_detalle_servicio';
        $campos['fecha_exp'] = date("Y-m-d");
        $campos['user_exp'] = $cedula_sesion;


        $condicion['id_detalle_servicio'] = $id_detalle_servicio;
        $condicion['fecha_exp'] = '2222-12-31';

        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function updatedelistaasignados($conect, $id_detalle_servicio, $caracteristicas) {
        // Tabla para hacer la consulta
        $nameTabla = $this->prefix_table . 't_detalle_servicio';
        $campos[' id_recurso_asignado'] = "$caracteristicas";

        $condicion['id_detalle_servicio'] = $id_detalle_servicio;
        $condicion['fecha_exp'] = '2222-12-31';

        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

///////////////////////cambio de estatus de los recursos/////////////////////////////////////


    function insertestatusrecurso($conect, $id_pauta, $id_detalle_servicio, $id_estatus, $cedula_sesion, $id_recurso_asignado) {

        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_estatus_detalle_servicio";
        // Campos para hacer el insert
        $campos['id_pauta'] = $id_pauta;
        $campos['id_estatus'] = $id_estatus;
        $campos['id_detalle_servicio'] = $id_detalle_servicio;
        $campos['id_recurso_asignado'] = $id_recurso_asignado;
        $campos['user_exp'] = $cedula_sesion;


        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    }

    function selectverrecursoasignado($conect, $id_detalle_servicio) {
        $nameTabla[$this->prefix_table . 't_detalle_servicio'] = $this->prefix_table . "t_detalle_servicio";

        $campos['id_recurso_asignado'] = "id_recurso_asignado";

        $condicion['id_detalle_servicio'] = $id_detalle_servicio;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

/////////////////////////////mostrar recursos////////////////////////////////////////////////	

    function updatematerialasignado($conect, $id_pauta, $id_detalle, $caracteristicas) {
        // Tabla para hacer la consulta
        $nameTabla = $this->prefix_table . 't_detalle_servicio';
        $campos['id_recurso_asignado'] = "caracteristicas";

        $condicion['id_pauta'] = $id_pauta;
        $condicion['id_detalle_servicio'] = $id_detalle;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function selectmostrarrecursosger($conect, $pauta) {
        $nameTabla[$this->prefix_table . 'recur_val_ger'] = $this->prefix_table . "recur_val_ger";

        $campos['id_recurso'] = "id_recurso";
        $campos['cantidad'] = "cantidad";

        //$campos['cantidad']="cantidad";
        $condicion['id_pauta'] = $pauta;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    //funcion que muestra los recursos del productor... status 13
    function selectmostrarrecursos($conect, $pauta, $t_inventario="2") {
        $nameTabla[$this->prefix_table . 'recur_val_prodjefe'] = $this->prefix_table . "recur_val_prodjefe";
        $campos['id_recurso'] = "id_recurso";
        $campos['cantidad'] = "cantidad";
        $campos['id_fintipoinventario'] = "id_fintipoinventario";

        $condicion['id_pauta'] = $pauta;
        $condicion['id_fintipoinventario'] = $t_inventario;
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    //funcion que muestra los datos agregados por el jefe de area  status 20
    function selectmostrarrecursosjef_jef($conect, $pauta, $t_inventario="2") {
        $nameTabla[$this->prefix_table . 'recur_val_jefejefe'] = $this->prefix_table . "recur_val_jefejefe";
        $campos['id_recurso'] = "id_recurso";
        $campos['cantidad'] = "cantidad";
        $campos['id_fintipoinventario'] = "id_fintipoinventario";
    
        $condicion['id_pauta'] = $pauta;
        $condicion['id_fintipoinventario'] = $t_inventario;
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectmostrarrecursosual($conect, $pauta) {
        $nameTabla[$this->prefix_table . 'recur_val_ual'] = $this->prefix_table . "recur_val_ual";
        $campos['id_recurso'] = "id_recurso";
        $campos['cantidad'] = "cantidad";
        $condicion['id_pauta'] = $pauta;
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectrecursosvalidados($conect, $pauta) {
        $nameTabla[$this->prefix_table . 'recur_val_ger'] = $this->prefix_table . "recur_val_ger";
        $campos['id_recurso'] = "id_recurso";
        $campos['cantidad'] = "cantidad";
        $condicion['id_pauta'] = $pauta;
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectlistdeeliminados($conect, $pauta) {
        $nameTabla[$this->prefix_table . 't_estatus_detalle_servicio'] = $this->prefix_table . "t_estatus_detalle_servicio";
        $campos['id_detalle_servicio'] = "id_detalle_servicio";
        $campos['fecha_reg'] = "fecha_reg";
        $campos['user_exp'] = "user_exp";

        $condicion['id_pauta'] = $pauta;
        $condicion['id_estatus'] = 16; // eliminados por la ual,solo este estatus por que son las modificaciones de la ual.
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }
    function selectlistdeeliminadosual($conect, $pauta) {
        $nameTabla[$this->prefix_table . 'recur_elim_ual'] = $this->prefix_table . "recur_elim_ual";
        $campos['id_recurso'] = "id_recurso";
        $campos['cantidad'] = "cantidad";

        $condicion['id_pauta'] = $pauta;
        //$condicion['id_estatus'] = 16; // eliminados por la ual,solo este estatus por que son las modificaciones de la ual.
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectlistaeliminados($conect, $id_detalle_servicio) {
        $nameTabla[$this->prefix_table . 't_detalle_servicio'] = $this->prefix_table . "t_detalle_servicio";
        $campos['id_recurso'] = "id_recurso";
        //$campos['cantidad']="cantidad";
        $condicion['id_detalle_servicio'] = $id_detalle_servicio;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

///////////////////////asignar recursos////////////////////////////////////////////////


    function selectcaracteristicas($conect, $id_recurso) {
        $nameTabla['inventario.inventariostok'] = "inventario.inventariostok";

        $campos['idmaterialgeneral'] = "idmaterialgeneral";
        $campos['inventario_valor'] = "inventario_valor";

        //$campos['cantidad']="cantidad";
        $condicion['idtipomaterial'] = $id_recurso;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectdatoscedula($conect, $idmaterialgeneral) {
        $nameTabla['inventario.vistageneral_inventario_f5'] = "inventario.vistageneral_inventario_f5";
        $campos['valorinventariado'] = "valorinventariado";
        $condicion['idmaterialgeneral'] = $idmaterialgeneral;
        $condicion['idcaracteristica'] = 27;
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectdatospnombre($conect, $idmaterialgeneral) {
        $nameTabla['inventario.vistageneral_inventario_f5'] = "inventario.vistageneral_inventario_f5";


        $campos['valorinventariado'] = "valorinventariado";

        $condicion['idmaterialgeneral'] = $idmaterialgeneral;
        $condicion['idcaracteristica'] = 23;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectdatospapellido($conect, $idmaterialgeneral) {
        $nameTabla['inventario.vistageneral_inventario_f5'] = "inventario.vistageneral_inventario_f5";


        $campos['valorinventariado'] = "valorinventariado";

        $condicion['idmaterialgeneral'] = $idmaterialgeneral;
        $condicion['idcaracteristica'] = 25;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectdatosserial($conect, $idmaterialgeneral) {
        $nameTabla['inventario.vistageneral_inventario_f5'] = "inventario.vistageneral_inventario_f5";


        $campos['valorinventariado'] = "valorinventariado";

        $condicion['idmaterialgeneral'] = $idmaterialgeneral;
        $condicion['idcaracteristica'] = 5;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectdatosmarca($conect, $idmaterialgeneral) {
        $nameTabla['inventario.vistageneral_inventario_f5'] = "inventario.vistageneral_inventario_f5";


        $campos['valorinventariado'] = "valorinventariado";

        $condicion['idmaterialgeneral'] = $idmaterialgeneral;
        $condicion['idcaracteristica'] = 3;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectdatosagregados($conect, $idmaterialgeneral) {
        $nameTabla['inventario.vistageneral_inventario_f5'] = "inventario.vistageneral_inventario_f5";


        $campos['valorinventariado'] = "valorinventariado";

        $condicion['idmaterialgeneral'] = $idmaterialgeneral;
        $condicion['idcaracteristica'] = 19;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectdatosbiennac($conect, $idmaterialgeneral) {
        $nameTabla['inventario.vistageneral_inventario_f5'] = "inventario.vistageneral_inventario_f5";


        $campos['valorinventariado'] = "valorinventariado";

        $condicion['idmaterialgeneral'] = $idmaterialgeneral;
        $condicion['idcaracteristica'] = 4;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectcaracteristicas2($conect, $id_recurso) {
        $nameTabla['inventario.inventariostok'] = "inventario.inventariostok";

        $campos['idmaterialgeneral'] = "idmaterialgeneral";
        $campos['inventario_valor'] = "inventario_valor";

        //$campos['cantidad']="cantidad";
        $condicion['idmaterialgeneral'] = $id_recurso;
        $orderby['inventario_valor'] = "inventario_valor ASC";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    /**
     *
     * @param type $conect para pasar el archivo de conexion de la base de datos
     * @param type $pauta para pasar el idpauta
     * @param type $id_recurso para pasar el tipo de recurso del inventario
     * @return type query //resultado de la base de datos.
     */
    function selectlistmatnoasig($conect, $pauta, $id_recurso=null) {
        $nameTabla[$this->prefix_table . 't_detalle_servicio'] = $this->prefix_table . "t_detalle_servicio";

        $campos['id_detalle_servicio'] = "id_detalle_servicio";
        $campos['id_recurso'] = "id_recurso";

        //$campos['cantidad']="cantidad";
        $condicion['id_pauta'] = $pauta;
        $condicion['fecha_exp'] = "'2222-12-31'";
        $condicion['id_recurso_asignado'] = "'1'";

        //filtar tambien por id_recurso.
        if ($id_recurso != null) {
            $condicion['id_recurso'] = $id_recurso;
        }

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectlistmatasig($conect, $pauta=null, $id_recurso_asignado=null) {
        $nameTabla[$this->prefix_table . 't_detalle_servicio'] = $this->prefix_table . "t_detalle_servicio";

        $campos['id_recurso_asignado'] = "id_recurso_asignado";
        $campos['id_recurso'] = "id_recurso";
        $campos['id_detalle_servicio'] = "id_detalle_servicio";

        if ($pauta != null) {
            $condicion['id_pauta'] = $pauta;
        }
    
        if ($id_recurso_asignado == null) {
            $condicion['id_recurso_asignado >'] = 2;
        } else {
            $condicion['id_recurso_asignado'] = $id_recurso_asignado;
        }

        $condicion['fecha_exp'] = "'2222-12-31'";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function select_recursoasignadonoterminado($conect, $pauta=null, $id_recurso_asignado=null){
        $nameTabla[$this->prefix_table . 't_estatus_detalle_servicio'] = $this->prefix_table . "t_estatus_detalle_servicio";
        $campos['id_recurso_asignado'] = "id_recurso_asignado";
        $campos['id_detalle_servicio'] = "id_detalle_servicio";
        $condicion['id_recurso_asignado'] = $id_recurso_asignado;
        $condicion['fecha_exp'] = "'2222-12-31'";
        $condicion['id_estatus!']="35 AND id_estatus!=16 AND id_estatus!=15 AND id_estatus!=33";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectlistmatasig_mod2($conect, $pauta, $idfininventario="2") {
        $nameTabla[$this->prefix_table . 't_detalle_servicio_mod2'] = $this->prefix_table . "t_detalle_servicio_mod2";

        $campos['id_recurso_asignado'] = "id_recurso_asignado";
        $campos['id_recurso'] = "id_recurso";

        //$campos['cantidad']="cantidad";
        $condicion['id_pauta'] = $pauta;
        $condicion['id_fintipoinventario'] = $idfininventario;
        $condicion['id_recurso_asignado >'] = 2;
        $condicion['fecha_exp'] = "'2222-12-31'";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectlistmatasignados($conect, $pauta, $gerencia) {
        $nameTabla[$this->prefix_table . 'datos_recurso_inv'] = $this->prefix_table . "datos_recurso_inv";
        $campos['id_recurso_asignado'] = "id_recurso_asignado";
        $campos['id_recurso'] = "id_recurso";
        $campos['id_detalle_servicio'] = "id_detalle_servicio";
        $condicion['id_pauta'] = $pauta;
        $condicion['id_gerencia'] = $gerencia;
        $condicion['id_recurso_asignado >'] = 2;
        $condicion['fecha_exp'] = "'2222-12-31'";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectlistreccambiados($conect, $id_pauta) {
        $nameTabla[$this->prefix_table . 't_estatus_detalle_servicio'] = $this->prefix_table . "t_estatus_detalle_servicio";

        $campos['id_detalle_servicio'] = "id_detalle_servicio";

        $condicion['id_pauta'] = $id_pauta;
        $condicion['id_estatus'] = 11;
        $condicion['fecha_exp'] = "'2222-12-31'";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectlistmatasig2($conect, $pauta) {
        $nameTabla[$this->prefix_table . 't_detalle_servicio'] = $this->prefix_table . "t_detalle_servicio";

        $campos['id_detalle_servicio'] = "id_detalle_servicio";
        $campos['id_recurso_asignado'] = "id_recurso_asignado";
        $campos['id_recurso'] = "id_recurso";

        $condicion['id_pauta'] = $pauta;
        $condicion['fecha_exp'] = "'2222-12-31'";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectlistmatcambiado($conect, $id_detalle_servicio) {
        $nameTabla[$this->prefix_table . 't_detalle_servicio'] = $this->prefix_table . "t_detalle_servicio";

        $campos['id_recurso'] = "id_recurso";

        $condicion['id_detalle_servicio'] = $id_detalle_servicio;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectlistnojust($conect, $id_detalle_servicio, $id_pauta) {
        $nameTabla[$this->prefix_table . 't_estatus_detalle_servicio'] = $this->prefix_table . "t_estatus_detalle_servicio";
        $campos['id_estatus'] = "id_estatus";
        $condicion['id_detalle_servicio'] = $id_detalle_servicio;
        $condicion['id_pauta'] = $id_pauta;
        $condicion['fecha_exp'] = "'2222-12-31'";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectdatosrec($conect, $pauta) {
        $nameTabla[$this->prefix_table . 't_detalle_servicio'] = $this->prefix_table . "t_detalle_servicio";

        $campos['id_detalle_servicio'] = "id_detalle_servicio";

        //$campos['cantidad']="cantidad";
        $condicion['id_pauta'] = $pauta;
        $condicion['fecha_exp'] = "'2222-12-31'";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

/////////////////////reportes///////////////////////////////////////////
    function selectcontpautas($conect, $fechaini, $fechafin) {
        $nameTabla[$this->prefix_table . 'pautas_por_fechas'] = $this->prefix_table . "pautas_por_fechas";

        $campos['cantidad'] = "cantidad";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelectLibre($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectrecursopersonal($conect, $pauta) {
        $nameTabla[$this->prefix_table . 'datos_recurso_inv'] = $this->prefix_table . "datos_recurso_inv";

        $campos['id_recurso'] = "id_recurso";
        $campos['id_recurso_asignado'] = "id_recurso_asignado";


        $condicion['id_pauta'] = $pauta;
        $condicion['id_fintipoinventario'] = 1;
        $condicion['fecha_exp'] = "'2222-12-31'";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectrecursomaterial($conect, $pauta) {
        $nameTabla[$this->prefix_table . 'datos_recurso_inv'] = $this->prefix_table . "datos_recurso_inv";
        $campos['id_recurso'] = "id_recurso";
        $campos['id_recurso_asignado'] = "id_recurso_asignado";
        $condicion['id_pauta'] = $pauta;
        $condicion['id_fintipoinventario'] = 2;
        $condicion['fecha_exp'] = "'2222-12-31'";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectrecurso($conect, $pauta, $id_recurso_asignado="1"){
        $nameTabla[$this->prefix_table . 'datos_recurso_inv'] = $this->prefix_table . "datos_recurso_inv";
        $campos['id_recurso'] = "id_recurso";
        $campos['desctipomaterial'] = 'desctipomaterial';
        $campos['id_detalle_servicio'] = 'id_detalle_servicio';
        $condicion['id_pauta'] = $pauta;
        $condicion['fecha_exp'] = "'2222-12-31'";
        $condicion['id_recurso_asignado'] = $id_recurso_asignado;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectlistdeasig($conect, $pauta, $estatus) {
        $nameTabla[$this->prefix_table . 't_estatus_detalle_servicio'] = $this->prefix_table . "t_estatus_detalle_servicio";

        $campos['id_detalle_servicio'] = "id_detalle_servicio";
        $campos['id_recurso_asignado'] = "id_recurso_asignado";
        $campos['fecha_reg'] = "fecha_reg";
        $campos['user_exp'] = "user_exp";


        $condicion['id_pauta'] = $pauta;
        $condicion['id_estatus'] = $estatus;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectlistmatprod($conect, $pauta, $estatus) {
        $nameTabla[$this->prefix_table . 't_estatus_detalle_servicio'] = $this->prefix_table . "t_estatus_detalle_servicio";

        $campos['id_detalle_servicio'] = "id_detalle_servicio";
        $campos['fecha_reg'] = "fecha_reg";
        $campos['user_exp'] = "user_exp";


        //$campos['cantidad']="cantidad";
        $condicion['id_pauta']=$pauta;
        $condicion['id_estatus']=$estatus;


        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectlistmatact($conect, $pauta, $estatus) {
        $nameTabla[$this->prefix_table . 't_estatus_detalle_servicio'] = $this->prefix_table . "t_estatus_detalle_servicio";

        $campos['id_detalle_servicio'] = "id_detalle_servicio";
        $campos['fecha_reg'] = "fecha_reg";

        //$campos['cantidad']="cantidad";
        $condicion['id_pauta'] = $pauta;
        $condicion['id_estatus'] = $estatus;
        $condicion['fecha_exp'] = "'2222-12-31'";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectlistmatactrep($conect, $pauta, $estatus) {
        $nameTabla[$this->prefix_table . 't_estatus_detalle_servicio'] = $this->prefix_table . "t_estatus_detalle_servicio";

        $campos['id_detalle_servicio'] = "id_detalle_servicio";
        $campos['id_recurso_asignado'] = "id_recurso_asignado";
        $campos['fecha_reg'] = "fecha_reg";
        $campos['user_exp'] = "user_exp";

        //$campos['cantidad']="cantidad";
        $condicion['id_pauta'] = $pauta;
        $condicion['id_estatus'] = $estatus;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectidsmat($conect, $id_detalle_servicio) {
        $nameTabla[$this->prefix_table . 't_detalle_servicio'] = $this->prefix_table . "t_detalle_servicio";

        $campos['id_recurso'] = "id_recurso";
        $campos['id_recurso_asignado'] = "id_recurso_asignado";

        $condicion['id_detalle_servicio'] = $id_detalle_servicio;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectidmat($conect, $id_detalle_servicio) {
        $nameTabla[$this->prefix_table . 't_detalle_servicio'] = $this->prefix_table . "t_detalle_servicio";

        $campos['id_recurso'] = "id_recurso";


        $condicion['id_detalle_servicio'] = $id_detalle_servicio;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectquienaprobo($conect, $id_pauta, $id_estatus) {
        $nameTabla[$this->prefix_table . 't_estatus_pauta'] = $this->prefix_table . "t_estatus_pauta";

        $campos['user_reg'] = "user_reg";

        $condicion['id_estatus'] = $id_estatus;
        $condicion['id_pauta'] = $id_pauta;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectquienasigno($conect, $id_pauta, $id_recurso_asignado) {
        $nameTabla[$this->prefix_table . 't_estatus_detalle_servicio'] = $this->prefix_table . "t_estatus_detalle_servicio";

        $campos['user_exp'] = "user_exp";
        $campos['fecha_reg'] = "fecha_reg";


        $condicion['id_recurso_asignado'] = $id_recurso_asignado;
        $condicion['id_pauta'] = $id_pauta;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }
 
    function fechaestatusanterior($conect,$id_pauta) {
        $nameTabla['f5.t_estatus_pauta'] = "f5.t_estatus_pauta";

        $campos['id_estatus_pauta'] = "id_estatus_pauta";
        $campos['fecha_exp'] = "fecha_exp";
        $campos['id_estatus'] = "id_estatus";

        $condicion['id_pauta'] = 60;
        $orderby['id_estatus_pauta'] = "id_estatus_pauta DESC";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

///////////////////////////correos usuario///////////////////////////////////////////////


    function selectdatocorreo($conect, $cedula) {
        $nameTabla['usuario.t_datos_personales'] = "usuario.t_datos_personales";

        $campos['correo'] = "correo";

        $condicion['cedula'] = $cedula;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

//////////////////////correos de la gerencia ///////////////////////////////////
    /*function selectgerencia($conect, $gerencia=null) {
        $nameTabla['sigai.grlgerencia'] = "sigai.grlgerencia";
        $campos['id_gerencia'] = "id_gerencia";
        $campos['de_gerencia'] = "de_gerencia";
        //$campos['cantidad']="cantidad";
        if ($gerencia != null) {
            $condicion['id_gerencia'] = $gerencia;
        }
        $condicion['estado'] = "'A'";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }*/

    function selectdivision($conect, $id_gerencia, $id_division=null) {
        $nameTabla['sigai.grldivision'] = "sigai.grldivision";

        $campos['id_division'] = "id_division";
        $campos['de_division'] = "de_division";
        $campos['id_gerencia'] = "id_gerencia";

        //$campos['cantidad']="cantidad";
        if ($id_gerencia != null) {
            $condicion['id_gerencia'] = $id_gerencia;
        }

        if ($id_division != null) {
            $condicion['id_division'] = $id_division;
        }

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function insertcorreos($conect, $idger, $ger, $cedula) {

        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_correo";
        // Campos para hacer el insert
        $campos['id_gerencia'] = $idger;
        $campos['correo'] = $ger;
        $campos['user_reg'] = $cedula;

        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    }

    function insertcorreosdiv($conect, $idger, $iddiv, $div, $cedula) {

        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_correo";
        // Campos para hacer el insert
        $campos['id_gerencia'] = $idger;
        $campos['id_division'] = $iddiv;
        $campos['correo'] = $div;
        $campos['user_reg'] = $cedula;

        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    }

    function selectverificarcorreos($conect, $idger, $iddiv=null) {
        $nameTabla[$this->prefix_table . 't_correo'] = $this->prefix_table . "t_correo";
        $campos['correo'] = "correo";
        $condicion['id_gerencia'] = $idger;
        $condicion['fecha_exp'] = "'2222-12-31'";
        if ($iddiv != null) {
            $condicion['id_division'] = $iddiv;
        }
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function updateexpcorreos($conect, $idger) {
        // Tabla para hacer la consulta
        $nameTabla = $this->prefix_table . 't_correo';
        $campos['fecha_exp'] = date("Y-m-d");

        $condicion['id_gerencia'] = $idger;
        $condicion['fecha_exp'] = '2222-12-31';
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function selectcorreosger($conect, $idger) {
        $nameTabla[$this->prefix_table . 't_correo'] = $this->prefix_table . "t_correo";
        $campos['correo'] = "correo";
        $condicion['id_gerencia'] = $idger;

        $condicion['id_division'] = 0;
        $condicion['fecha_exp'] = "'2222-12-31'";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectcorreousuario($conect, $idger=null, $iddivision=null, $idtipousuario=null) {
        $nameTabla[$this->prefix_table . 'usuarios'] = $this->prefix_table . "usuarios";
        $campos['correo'] = "correo";
        if ($idger != null) {
            $condicion['id_gerencia'] = $idger;
        }
        if ($iddivision != null) {
            $condicion['iddivision'] = $iddivision;
        }
        if ($idtipousuario != null) {
            $condicion['id_tipo_usuario'] = $idtipousuario;
        }
        $condicion['correo !'] = "''";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function tributa_a($conect, $idger=null, $iddivision=null, $idtipousuario=null) {
        $nameTabla[$this->prefix_table . 'usuarios'] = $this->prefix_table . "usuarios";
        $campos['cedula'] = "cedula";
        $campos['nombre_usuario'] = "nombre_usuario";
        $campos['apellido_usuario'] = "apellido_usuario";
        $campos['de_gerencia'] = 'de_gerencia';
        $campos['de_division'] = 'de_division';
        $campos['desc_usuario'] ="desc_usuario";
        if ($idger != null) {
            $condicion['id_gerencia'] = $idger;
        }
        if ($iddivision != null) {
            $condicion['iddivision'] = $iddivision;
        }
        if ($idtipousuario != null) {
            $condicion['id_tipo_usuario'] = $idtipousuario;
        }
        $condicion['correo !'] = "''";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectcorreosdiv($conect, $idger, $iddiv) {
        $nameTabla[$this->prefix_table . 't_correo'] = $this->prefix_table . "t_correo";
        $campos['correo'] = "correo";
        $condicion['id_gerencia'] = $idger;
        $condicion['id_division'] = $iddiv;
        $condicion['fecha_exp'] = "'2222-12-31'";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectidsgerencia($conect, $id_pauta) {
        $nameTabla[$this->prefix_table . 'pauta_gerencia'] = $this->prefix_table . "pauta_gerencia";
        $campos['id_gerencia'] = "id_gerencia";
        $campos['id_division'] = "id_division";
        $condicion['id_pauta'] = $id_pauta;
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectidsdivision($conect, $id_gerencia, $id_pauta) {
        $nameTabla[$this->prefix_table . 'pauta_gerencia'] = $this->prefix_table . "pauta_gerencia";
        $campos['id_division'] = "id_division";
        $condicion['id_gerencia'] = $id_gerencia;
        $condicion['id_pauta'] = $id_pauta;
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

///////////////////////////estatus///////////////////////////////////////////////////////

    function verificarasignacion($conect, $id_recurso_asignado, $id_pauta) {
        $nameTabla[$this->prefix_table . 't_detalle_servicio'] = $this->prefix_table . "t_detalle_servicio";
        $campos['id_recurso_asignado'] = "id_recurso_asignado";
        $condicion['id_recurso_asignado'] = $id_recurso_asignado;
        $condicion['id_pauta'] = $id_pauta;
        $condicion['fecha_exp'] = "'2222-12-31'";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function verificarestatus($conect, $id_detalle_servicio) {
        $nameTabla[$this->prefix_table . 't_estatus_detalle_servicio'] = $this->prefix_table . "t_estatus_detalle_servicio";
        $campos['id_estatus'] = "id_estatus";
        $condicion['id_detalle_servicio'] = $id_detalle_servicio;
        $condicion['fecha_exp'] = "'2222-12-31'";
        $orderby['id_detalle_servicio'] = "id_detalle_servicio DESC";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function eliminadopor($conect, $id_detalle_servicio) {
        $nameTabla[$this->prefix_table . 't_detalle_servicio'] = $this->prefix_table . "t_detalle_servicio";
        $campos['user_exp'] = "user_exp";
        $condicion['id_detalle_servicio'] = $id_detalle_servicio;
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function asignadopor($conect, $id_detalle_servicio) {
        $nameTabla[$this->prefix_table . 't_estatus_detalle_servicio'] = $this->prefix_table . "t_estatus_detalle_servicio";
        $campos['user_exp'] = "user_exp";
        $condicion['id_detalle_servicio'] = $id_detalle_servicio;
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectdatousuario($conect, $cedula) {
        $nameTabla['usuario.t_datos_personales'] = "usuario.t_datos_personales";
        $campos['nombre_usuario'] = "nombre_usuario";
        $campos['apellido_usuario'] = "apellido_usuario";
        $campos['correo'] = "correo";
        $campos['telefono1'] = "telefono1";
        $campos['telefono2'] = "telefono2";
        $condicion['cedula'] = $cedula;
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function verificarestatuspauta($conect, $id_pauta) {
        $nameTabla[$this->prefix_table . 't_estatus_pauta'] = $this->prefix_table . "t_estatus_pauta";
        $campos['id_estatus'] = "id_estatus";
        $condicion['id_pauta'] = $id_pauta;
        $condicion['fecha_exp'] = "'2222-12-31'";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function updateexpestatus($conect, $id_pauta, $observaciones="") {
        // Tabla para hacer la consulta
        $nameTabla = $this->prefix_table . 't_estatus_pauta';
        $campos['fecha_exp'] = date("Y-m-d");
        if ($observaciones != "") {
            $campos['observaciones'] = $observaciones;
        }
        $condicion['id_pauta'] = $id_pauta;
        $condicion['fecha_exp'] = '2222-12-31';
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function updateexpestatusrecursos($conect, $id_detalle_servicio) {
        // Tabla para hacer la consulta
        $nameTabla = $this->prefix_table . 't_estatus_detalle_servicio';
        $campos['fecha_exp'] = date("Y-m-d");
        $condicion['id_detalle_servicio'] = $id_detalle_servicio;
        $condicion['fecha_exp'] = '2222-12-31';
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function insertestatusnuevo($conect, $id_pauta, $estatuspauta, $cedula, $observaciones=null) {
        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_estatus_pauta";
        // Campos para hacer el insert
        $campos['id_pauta'] = $id_pauta;
        $campos['id_estatus'] = $estatuspauta;
        $campos['user_reg'] = $cedula;
        if($observaciones!=null){
            $campos['observaciones'] = $observaciones;
        }
        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    }

    function insertestatusmodif($conect, $id_pauta, $id_estatus, $cedula) {
        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_estatus_pauta";
        // Campos para hacer el insert
        $campos['id_pauta'] = $id_pauta;
        $campos['id_estatus'] = $id_estatus;
        $campos['user_reg'] = $cedula;
        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    }

    function insertestatusmod($conect, $id_pauta, $cedula) {

        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_estatus_pauta";
        // Campos para hacer el insert
        $campos['id_pauta'] = $id_pauta;
        $campos['id_estatus'] = 7;
        $campos['user_reg'] = $cedula;

        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    }

    function insertestatusmod3($conect, $id_pauta, $cedula) {

        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_estatus_pauta";
        // Campos para hacer el insert
        $campos['id_pauta'] = $id_pauta;
        $campos['id_estatus'] = 25;
        $campos['user_reg'] = $cedula;

        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    }

    function insertestatusnuevorech($conect, $id_pauta, $estatuspauta, $obs, $cedula) {

        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_estatus_pauta";
        // Campos para hacer el insert
        $campos['id_pauta'] = $id_pauta;
        $campos['id_estatus'] = $estatuspauta;
        $campos['observaciones'] = $obs;
        $campos['user_reg'] = $cedula;

        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    }

    function insertestatusnuevoanulada($conect, $id_pauta, $estatuspauta, $obs) {

        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_estatus_pauta";
        // Campos para hacer el insert
        $campos['id_pauta'] = $id_pauta;
        $campos['id_estatus'] = $estatuspauta;
        $campos['observaciones'] = $obs;

        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    }

    function selectrecursosdelapauta($conect, $id_pauta) {
        $nameTabla[$this->prefix_table . 't_estatus_detalle_servicio'] = $this->prefix_table . "t_estatus_detalle_servicio";

        $campos['id_detalle_servicio'] = "id_detalle_servicio";

        $condicion['id_pauta'] = $id_pauta;
        $condicion['fecha_exp'] = "'2222-12-31'";

        $orderby['id_detalle_servicio'] = "id_detalle_servicio DESC";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectrecursosdelapautaeditada($conect, $id_pauta) {
        $nameTabla[$this->prefix_table . 't_estatus_detalle_servicio'] = $this->prefix_table . "t_estatus_detalle_servicio";

        $campos['id_detalle_servicio'] = "id_detalle_servicio";

        $condicion = "id_pauta=" . $id_pauta . " and id_estatus in (27) and fecha_exp='2222-12-31'";

        $orderby['id_detalle_servicio'] = "id_detalle_servicio DESC";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelectLibre($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function insertestatusrecursoanulado($conect, $id_pauta, $ced_sesion, $id_detalle_servicio, $estatus_recurso, $id_recurso_asignado) {

        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_estatus_detalle_servicio";
        // Campos para hacer el insert
        $campos['id_pauta'] = $id_pauta;
        $campos['id_estatus'] = $estatus_recurso;
        $campos['id_detalle_servicio'] = $id_detalle_servicio;
        $campos['id_recurso_asignado'] = $id_recurso_asignado;
        $campos['user_exp'] = $ced_sesion;

        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    }

    function selectobsrech($conect, $id_pauta) {
        $nameTabla[$this->prefix_table . 't_estatus_pauta'] = $this->prefix_table . "t_estatus_pauta";

        $campos['observaciones'] = "observaciones";
        $condicion['id_pauta'] = $id_pauta;
        $condicion['fecha_exp'] = "'2222-12-31'";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectestatuspautas($conect, $id_pauta) {
        $nameTabla[$this->prefix_table . 't_estatus_pauta'] = $this->prefix_table . "t_estatus_pauta";

        $campos['id_estatus'] = "id_estatus";
        $condicion['id_pauta'] = $id_pauta;
        $condicion['fecha_exp'] = "'2222-12-31'";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectlistmatjust($conect, $id_pauta) {
        $nameTabla[$this->prefix_table . 't_informe'] = $this->prefix_table . "t_informe";

        $campos['id_detalle_servicio'] = "id_detalle_servicio";
        $campos['id_informe'] = "id_informe";
        $campos['id_estado_informe'] = "id_estado_informe";
        $campos['observaciones'] = "observaciones";

        $condicion['id_pauta'] = $id_pauta;
        $condicion['fecha_exp'] = "'2222-12-31'";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectidrecurso($conect, $id_detalle_servicio) {
        $nameTabla[$this->prefix_table . 't_detalle_servicio'] = $this->prefix_table . "t_detalle_servicio";

        $campos['id_recurso_asignado'] = "id_recurso_asignado";
        $campos['id_recurso'] = "id_recurso";

        $condicion['id_detalle_servicio'] = $id_detalle_servicio;

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    /**
     * SIRVE PARA DEFINIR LOS ESTADOS DE LOS INFORMES..... USADO MUCHO EN EL ULTIMO PROCESO DE PAUTAS.
     * @param type $conect
     * @param type $id_estado_informe
     * @return type 
     */
    function selectestadoinf($conect, $id_estado_informe) {
        $nameTabla[$this->prefix_table . 't_estado_informe'] = $this->prefix_table . "t_estado_informe";
        $campos['descripcion'] = "descripcion";
        $condicion['id_estado_informe'] = $id_estado_informe;
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function insertguardar($conect, $id_pauta, $id_detalle_servicio, $asistio, $observaciones, $ced_sesion) {

        // Nombre de la Tabla
        $nameTabla = $this->prefix_table . "t_informe";
        // Campos para hacer el insert
        $campos['id_pauta'] = $id_pauta;
        $campos['id_detalle_servicio'] = $id_detalle_servicio;
        $campos['id_estado_informe'] = $asistio;
        $campos['observaciones'] = $observaciones;
        $campos['user_reg'] = $ced_sesion;
        $campos['fecha_reg'] = date("Y-m-d");
        $campos['fecha_exp'] = "2222-12-31";


        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    }

    function updateexpestatusinf($conect, $id_informe, $ced_sesion) {
        // Tabla para hacer la consulta
        $nameTabla = $this->prefix_table . 't_informe';
        $campos['fecha_exp'] = date("Y-m-d");
        $campos['user_exp'] = $ced_sesion;

        $condicion['id_informe'] = $id_informe;
        $condicion['fecha_exp'] = '2222-12-31';
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function updateexpestatusrecursosf5($conect, $id_pauta, $id_detalle_servicio) {
        // Tabla para hacer la consulta
        $nameTabla = $this->prefix_table . 't_estatus_detalle_servicio';
        $campos['fecha_exp'] = date("Y-m-d");

        $condicion['id_pauta'] = $id_pauta;
        $condicion['id_detalle_servicio'] = $id_detalle_servicio;
        $condicion['fecha_exp'] = '2222-12-31';
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function SelectFotoCarnet($conect, $ced) {
        $nameTabla['carnet.t_foto'] = "carnet.t_foto";

        $campos['id_foto'] = "id_foto";
        $campos['descripcion'] = "descripcion";
        $campos['nombre'] = "nombre";
        $campos['archivo_bytea'] = "coalesce(archivo_bytea,'-1') as archivo_bytea";
        $campos['mime'] = "mime";
        $campos['size'] = "size";


        $condicion['descripcion'] = "'" . $ced . "'";
        $condicion['fecha_expiracion'] = "'2222-12-31'";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function usuario($conect, $cedula, $clave, $id_aplicacion, $id_modulo, $id_tipo_usuario, $id_submodulo) {
        // Tabla para hacer la consulta
        $nameTabla['usuario.t_acceso'] = "usuario.t_acceso";
        // Campos para seleccionar
        $campos['id_acceso'] = "id_acceso";
        $campos['cedula'] = "cedula";
        $campos['clave'] = "clave";
        $campos['id_modulo'] = "id_modulo";
        $campos['id_aplicacion'] = "id_aplicacion";
        $campos['niv_con'] = "niv_con";
        $campos['niv_eli'] = "niv_eli";
        $campos['niv_inc'] = "niv_inc";
        $campos['niv_mod'] = "niv_mod";
        $campos['ced_trans'] = "ced_trans";
        $campos['fecha_trans'] = "fecha_trans";
        $campos['fecha_exp'] = "fecha_exp";
        $campos["id_tipo_usuario"] = "id_tipo_usuario";
        $campos["id_submodulo"] = "id_submodulo";

        // Condicion
        if ($cedula != "") {
            $condicion['cedula'] = "'" . $cedula . "'";
        }

        if ($clave != "") {
            $condicion['clave'] = "'" . $clave . "'";
        }

        if ($id_aplicacion != "") {
            $condicion['id_aplicacion'] = $id_aplicacion;
        }

        if ($id_modulo != "") {
            $condicion['id_modulo'] = $id_modulo;
        }

        if ($id_tipo_usuario != "") {
            $condicion['id_tipo_usuario >'] = $id_tipo_usuario;
        }

        if ($id_submodulo != "") {
            $condicion['id_submodulo'] = $id_submodulo;
        }

        $condicion['fecha_exp'] = "'2222-12-31'";

        $orderby['cedula'] = "cedula";
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function updateexpirausuario($conect, $cedula, $clave, $id_aplicacion, $id_modulo, $id_tipo_usuario, $id_submodulo) {
        // Tabla para hacer la consulta
        $nameTabla = 'usuario.t_acceso';
        $campos['fecha_exp'] = date("Y-m-d");

        if ($cedula != "") {
            $condicion['cedula'] = $cedula;
        }

        if ($clave != "") {
            $condicion['clave'] = $clave;
        }

        if (($id_aplicacion != "")) {
            $condicion['id_aplicacion'] = $id_aplicacion;
        }

        if (($id_modulo != "")) {
            $condicion['id_modulo'] = $id_modulo;
        }

        if (($id_tipo_usuario != "")) {
            $condicion['id_tipo_usuario'] = $id_tipo_usuario;
        }

        if (($id_submodulo != "")) {
            $condicion['id_submodulo'] = $id_submodulo;
        }

        $condicion['fecha_exp'] = "2222-12-31";
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function insertusuario($conect, $cedula, $clave, $id_aplicacion, $id_modulo, $id_tipo_usuario, $id_submodulo) {

        // Nombre de la Tabla
        $nameTabla = "usuario.t_acceso";
        // Campos para hacer el insert
        $campos['cedula'] = $cedula;
        $campos['clave'] = $clave;
        $campos['id_modulo'] = $id_modulo;
        $campos['id_aplicacion'] = $id_aplicacion;
        $campos['niv_con'] = 1;
        $campos['niv_eli'] = 1;
        $campos['niv_inc'] = 1;
        $campos['niv_mod'] = 1;
        $campos['ced_trans'] = $cedula;
        $campos['fecha_trans'] = date("Y-m-d");
        $campos['id_tipo_usuario'] = $id_tipo_usuario;
        if ($id_submodulo != "") {
            $campos['id_submodulo'] = $id_submodulo;
        }


        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    }

    function datospersonalesusuario($conect, $cedula) {
        // Tabla para hacer la consulta
        $nameTabla['usuario.t_datos_personales'] = "usuario.t_datos_personales";
        // Campos para seleccionar
        $campos['cedula'] = "cedula";
        $campos['nombre_usuario'] = "nombre_usuario";
        $campos['apellido_usuario'] = "apellido_usuario";
        // Condicon
        if ($cedula != "") {
            $condicion['cedula'] = $cedula;
        }
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function datosgerenciadelusuario($conect, $cedula) {
        // Tabla para hacer la consulta
        $nameTabla['sigai.datos_trabajador'] = "sigai.datos_trabajador";
        // Campos para seleccionar
        $campos['id_gerencia'] = "id_gerencia";
        $campos['id_division'] = "id_division";

        // Condicon
        if ($cedula != "") {
            $condicion['nu_documen'] = $cedula;
        }
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function datostrabajadorsigai($conect, $cedula, $id_gerencia="", $id_cargo="") {
        // Tabla para hacer la consulta
        $nameTabla['sigai.datos_trabajador'] = "sigai.datos_trabajador";
        // Campos para seleccionar
        $campos['nu_documen'] = "nu_documen";
        $campos['no_primernombre'] = "no_primernombre";
        $campos['no_segundonombre'] = "no_segundonombre";
        $campos['no_primerapellido'] = "no_primerapellido";
        $campos['no_segundoapellido'] = "no_segundoapellido";
        $campos['de_cargo'] = "de_cargo";
        $campos['de_gerencia'] = "de_gerencia";
        $campos['de_division'] = "de_division";
        $campos['id_cargo'] = "id_cargo";
        $campos['id_gerencia'] = "id_gerencia";
        $campos['id_division'] = "id_division";
        // Condicon
        if ($cedula != "") {
            $d_cedula = 'nu_documen=' . $cedula;
            $and = ' and ';
        } else {
            $d_cedula = "";
            $and = ' ';
        }
        if ($id_gerencia != "") {
            $d_gerencia = $and . 'id_gerencia=' . $id_gerencia;
            $and = ' and ';
        } else {
            $d_gerencia = "";
            $and = ' ';
        }
        if ($id_cargo != "") {
            if ($cedula != "") {
                $and = ' and ';
            }
            $d_cargo = $and . 'id_cargo in' . $id_cargo;
        } else {
            $d_cargo = "";
        }
        $condicion = $d_cedula . $d_gerencia . $d_cargo;

        $orderby['nu_documen'] = "nu_documen";
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelectLibre($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function datostrabajadortelefonico($conect, $cedula, $id_persona="", $minimo="NO") {
        // Tabla para hacer la consulta
        $nameTabla['directorio2.datostrabajadortelefonico'] = "directorio2.datostrabajadortelefonico";
        // Campos para seleccionar
        $campos['id_persona'] = "id_persona";
        $campos['cedula'] = "cedula";
        $campos['nombres'] = "nombres";
        $campos['apellidos'] = "apellidos";
        $campos['desc_gerencia'] = "desc_gerencia";
        $campos['desc_division'] = "desc_division";
        $campos['desc_cargo'] = "desc_cargo";
        $campos['id_cargo'] = "id_cargo";
        $campos['id_gerencia'] = "id_gerencia";
        $campos['id_division'] = "id_division";
        // Condicon
        if ($cedula != "") {
            $condicion['cedula'] = $cedula;
        }

        if ($id_persona != "") {
            $condicion['id_persona'] = $id_persona;
        }

        if ($minimo === 'NO') {
            $limite = $this->limite;
        } else {
            if (strcmp($minimo, "") != 0) {
                $limite['INICIO'] = $minimo;
                $limite['FIN'] = 10;
            } else {
                $limite = array();
            }
        }


        $orderby['cedula'] = "cedula";
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $limite);
    }

    function datosusuariocambioclave($conect, $cedula, $id_persona="", $minimo="NO") {
        // Tabla para hacer la consulta
        $nameTabla['sigai.datos_trabajador'] = "sigai.datos_trabajador";
        // Campos para seleccionar
        $campos['no_primernombre'] = "no_primernombre";
        $campos['no_segundonombre'] = "no_segundonombre";
        $campos['no_primerapellido'] = "no_primerapellido";
        $campos['no_segundoapellido'] = "no_segundoapellido";
        $campos['de_gerencia'] = "de_gerencia";
        $campos['de_division'] = "de_division";
        $campos['de_cargo'] = "de_cargo";
        // Condicon
        if ($cedula != "") {
            $condicion['nu_documen'] = $cedula;
        }


        if ($minimo === 'NO') {
            $limite = $this->limite;
        } else {
            if (strcmp($minimo, "") != 0) {
                $limite['INICIO'] = $minimo;
                $limite['FIN'] = 10;
            } else {
                $limite = array();
            }
        }


        $orderby['nu_documen'] = "nu_documen";
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $limite);
    }

    ///la regla de la consulta relacional son 3 parametros
    function busqueda($conect, $vista_tabla, $campo_rn, $campo_bq, $busqueda, $consulta_relacional, $minimo="") {
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        if ($busqueda != "") {
            // Tabla para hacer la consulta
            $nameTabla[$vista_tabla] = $vista_tabla;
            // Campos para seleccionar
            $campos[$campo_rn] = $campo_rn;
            // Condicon			
            $busqueda = explode(" ", trim($busqueda));
            for ($i = 0; $i < count($busqueda); $i++) {
                if ($i > 0) {
                    $separador.=" AND ";
                }
                $separador.=$campo_bq . " ILIKE '%" . $busqueda[$i] . "%'";
            }
            $condicion = $separador;
            $orderby[$campo_rn] = $campo_rn;
            $resultado = $this->ObjDb->fbdSelectLibre($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
            $arreglo = array();
            $x = 1;
            foreach ($resultado as $llave => $valor) {
                $arreglo = $this->$consulta_relacional($conect, "", $valor[1], $minimo);
                $arreglo2[$x++] = $arreglo[1];
            }
            return $arreglo2;
        } else {
            return $this->$consulta_relacional($conect, "", "", $minimo);
        }
    }

    function busqueda2($conect, $vista_tabla, $campo_rn, $campo_bq, $busqueda, $consulta_relacional, $minimo) {
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        // Tabla para hacer la consulta
        $nameTabla[$vista_tabla] = $vista_tabla;
        // Campos para seleccionar
        //$campos[$campo_rn]=$campo_rn;		
        $campo_rn = explode(",", trim($campo_rn));
        for ($ii = 0; $ii < count($campo_rn); $ii++) {
            $campos[$campo_rn[$ii]] = $campo_rn[$ii];
        }

        if ($busqueda != "") { // Condicon			
            $busqueda = explode(" ", trim($busqueda));
            for ($i = 0; $i < count($busqueda); $i++) {
                if ($i > 0) {
                    $separador.=" AND ";
                }
                $separador.=$campo_bq . " ILIKE '%" . $busqueda[$i] . "%'";
            }
            $condicion = $separador;
            //$orderby[$campo_rn]=$campo_rn;
        } else {
            $condicion == array();
        }

        if ($minimo === 'NO') {
            $limite = $this->limite;
        } else {
            if (strcmp($minimo, "") != 0) {
                $limite['INICIO'] = $minimo;
                $limite['FIN'] = 10;
            } else {
                $limite = array();
            }
        }

        return $this->ObjDb->fbdSelectLibre($nameTabla, $campos, $condicion, $orderby, $this->groupby, $limite);
    }

    function busquedasys_main($conect, $vista_tabla, $campo_rn, $campo_bq, $busqueda, $consulta_relacional, $minimo) {
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        // Tabla para hacer la consulta
        $nameTabla[$vista_tabla] = $vista_tabla;
        // Campos para seleccionar
        //$campos[$campo_rn]=$campo_rn;		
        $campo_rn = explode(",", trim($campo_rn));
        for ($ii = 0; $ii < count($campo_rn); $ii++) {
            $campos[$campo_rn[$ii]] = $campo_rn[$ii];
        }

        if ($busqueda != "") { // Condicon			
            $busqueda = explode(" ", trim($busqueda));
            for ($i = 0; $i < count($busqueda); $i++) {
                if ($i > 0) {
                    $separador.=" AND ";
                }
                $separador.=$campo_bq . " ILIKE '%" . $busqueda[$i] . "%'";
            }
            //$condicion=$separador;
            if ($consulta_relacional != "") {
                $condicion = $separador . " AND " . $consulta_relacional;
            }else{
                $condicion = $separador ;
            }
        } else {
            //$condicion==array();
            $condicion = $consulta_relacional;
        }

        if ($minimo === 'NO') {
            $limite = $this->limite;
        } else {
            if (strcmp($minimo, "") != 0) {
                $limite['INICIO'] = $minimo;
                $limite['FIN'] = 10;
            } else {
                $limite = array();
            }
        }
        return $this->ObjDb->fbdSelectLibre($nameTabla, $campos, $condicion, $this->orderby, $this->groupby, $limite);
    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function unidad_medida($conect) {
        // Tabla para hacer la consulta
        $nameTabla['almacen_tecnico.t_unidad_medida'] = "almacen_tecnico.t_unidad_medida";
        // Campos para seleccionar
        $campos['id_unidad_medida'] = "id_unidad_medida";
        $campos['descripcion'] = "descripcion";

        $condicion['fecha_exp'] ="'2222-12-31'";
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function desc_unidad_medida($conect, $id_unidad_medida) {
        // Tabla para hacer la consulta
        $nameTabla['almacen_tecnico.t_unidad_medida'] = "almacen_tecnico.t_unidad_medida";
        // Campos para seleccionar
        $campos['descripcion'] = "descripcion";

        $condicion['id_unidad_medida'] =$id_unidad_medida;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function desc_desc($conect, $id_descripcion) {
        // Tabla para hacer la consulta
        $nameTabla['almacen_tecnico.t_descripciones'] = "almacen_tecnico.t_descripciones";
        // Campos para seleccionar
        $campos['descripcion'] = "descripcion";

        $condicion['id_descripcion'] =$id_descripcion;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function desc_grupo($conect, $id_grupo) {
        // Tabla para hacer la consulta
        $nameTabla['almacen_tecnico.t_grupos'] = "almacen_tecnico.t_grupos";
        // Campos para seleccionar
        $campos['descripcion'] = "descripcion";

        $condicion['id_grupo'] ="'".$id_grupo."'";
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function desc_categoria($conect, $id_categoria) {
        // Tabla para hacer la consulta
        $nameTabla['almacen_tecnico.t_categoria'] = "almacen_tecnico.t_categoria";
        // Campos para seleccionar
        $campos['descripcion'] = "descripcion";

        $condicion['id_categoria'] =$id_categoria;
        
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectcategoria($conect) {
        // Tabla para hacer la consulta
        $nameTabla['almacen_tecnico.t_categoria'] = "almacen_tecnico.t_categoria";
        // Campos para seleccionar
        $campos['id_categoria'] = "id_categoria";
        
        $condicion['fecha_exp'] ="'2222-12-31'";
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }


    function desc_talla_medida($conect, $talla_medida) {
        // Tabla para hacer la consulta
        $nameTabla['inventario_imagen.t_tallas'] = "inventario_imagen.t_tallas";
        // Campos para seleccionar
        $campos['descripcion'] = "descripcion";

        $condicion['id_talla'] =$talla_medida;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }    

    function desc_estado($conect, $id_estado) {
        // Tabla para hacer la consulta
        $nameTabla['almacen_tecnico.t_des_estado'] = "almacen_tecnico.t_des_estado";
        // Campos para seleccionar
        $campos['desc_estado'] = "desc_estado";

        $condicion['id_desc_estado'] =$id_estado;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function estado($conect) {
        // Tabla para hacer la consulta
        $nameTabla['almacen_tecnico.t_des_estado'] = "almacen_tecnico.t_des_estado";
        // Campos para seleccionar
        $campos['id_desc_estado'] = "id_desc_estado";
        $campos['desc_estado'] = "desc_estado";
            
        $condicion['fecha_exp'] ="'2222-12-31'";
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function grupos($conect) {
        // Tabla para hacer la consulta
        $nameTabla['almacen_tecnico.t_grupos'] = "almacen_tecnico.t_grupos";
        // Campos para seleccionar
        $campos['id_grupo'] = "id_grupo";
        $campos['descripcion'] = "descripcion";
            

        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function gruposasig($conect) {
        // Tabla para hacer la consulta
        $nameTabla['almacen_tecnico.t_grupos'] = "almacen_tecnico.t_grupos";
        // Campos para seleccionar
        $campos['id_grupo'] = "id_grupo";
        $campos['descripcion'] = "descripcion";
            
        $condicion['id_asig'] =1;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function categoria($conect) {
        // Tabla para hacer la consulta
        $nameTabla['almacen_tecnico.t_categoria'] = "almacen_tecnico.t_categoria";
        // Campos para seleccionar
        $campos['id_categoria'] = "id_categoria";
        $campos['descripcion'] = "descripcion";
            
        $condicion['fecha_exp'] = "'2222-12-31'";
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function vestuario($conect) {
        // Tabla para hacer la consulta
        $nameTabla['almacen_tecnico.t_vestuario'] = "almacen_tecnico.t_vestuario";
        // Campos para seleccionar
        $campos['id_vestuario'] = "id_vestuario";
        $campos['desc_vestuario'] = "desc_vestuario";
            

        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function descvestuario($conect, $id_vestuario) {
        // Tabla para hacer la consulta
        $nameTabla['inventario_imagen.t_vestuario'] = "inventario_imagen.t_vestuario";
        // Campos para seleccionar
        $campos['id_vestuario'] = "id_vestuario";
        $campos['desc_vestuario'] = "desc_vestuario";
            
        $condicion['id_vestuario'] =$id_vestuario;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }
    
    function vestuarios($conect, $id_vestuario) {
        // Tabla para hacer la consulta
        $nameTabla['inventario_imagen.vestuario_fem'] = "inventario_imagen.vestuario_fem";
        // Campos para seleccionar
        $campos['id_vestuario'] = "id_vestuario";
        $campos['sum'] = "sum";
            
        $condicion['id_vestuario'] =$id_vestuario;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function vestuariosmas($conect, $id_vestuario) {
        // Tabla para hacer la consulta
        $nameTabla['inventario_imagen.vestuario_mas'] = "inventario_imagen.vestuario_mas";
        // Campos para seleccionar
        $campos['id_vestuario'] = "id_vestuario";
        $campos['sum'] = "sum";
            
        $condicion['id_vestuario'] =$id_vestuario;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function vestuariosunisex($conect, $id_vestuario) {
        // Tabla para hacer la consulta
        $nameTabla['inventario_imagen.vestuario_unisex'] = "inventario_imagen.vestuario_unisex";
        // Campos para seleccionar
        $campos['id_vestuario'] = "id_vestuario";
        $campos['sum'] = "sum";
            
        $condicion['id_vestuario'] =$id_vestuario;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function vestuariosprest($conect, $id_vestuario) {
        // Tabla para hacer la consulta
        $nameTabla['inventario_imagen.vestuario_fem_prest'] = "inventario_imagen.vestuario_fem_prest";
        // Campos para seleccionar
        $campos['id_vestuario'] = "id_vestuario";
        $campos['sum'] = "sum";
            
        $condicion['id_vestuario'] =$id_vestuario;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function vestuariosprestmas($conect, $id_vestuario) {
        // Tabla para hacer la consulta
        $nameTabla['inventario_imagen.vestuario_mas_prest'] = "inventario_imagen.vestuario_mas_prest";
        // Campos para seleccionar
        $campos['id_vestuario'] = "id_vestuario";
        $campos['sum'] = "sum";
            
        $condicion['id_vestuario'] =$id_vestuario;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function vestuariosprestunisex($conect, $id_vestuario) {
        // Tabla para hacer la consulta
        $nameTabla['inventario_imagen.vestuario_unisex_prest'] = "inventario_imagen.vestuario_unisex_prest";
        // Campos para seleccionar
        $campos['id_vestuario'] = "id_vestuario";
        $campos['sum'] = "sum";
            
        $condicion['id_vestuario'] =$id_vestuario;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function vestuariosdisp($conect, $id_vestuario) {
        // Tabla para hacer la consulta
        $nameTabla['inventario_imagen.vestuario_fem_disp'] = "inventario_imagen.vestuario_fem_disp";
        // Campos para seleccionar
        $campos['id_vestuario'] = "id_vestuario";
        $campos['sum'] = "sum";
            
        $condicion['id_vestuario'] =$id_vestuario;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function vestuariosdispmas($conect, $id_vestuario) {
        // Tabla para hacer la consulta
        $nameTabla['inventario_imagen.vestuario_mas_disp'] = "inventario_imagen.vestuario_mas_disp";
        // Campos para seleccionar
        $campos['id_vestuario'] = "id_vestuario";
        $campos['sum'] = "sum";
            
        $condicion['id_vestuario'] =$id_vestuario;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function vestuariosdispunisex($conect, $id_vestuario) {
        // Tabla para hacer la consulta
        $nameTabla['inventario_imagen.vestuario_unisex_disp'] = "inventario_imagen.vestuario_unisex_disp";
        // Campos para seleccionar
        $campos['id_vestuario'] = "id_vestuario";
        $campos['sum'] = "sum";
            
        $condicion['id_vestuario'] =$id_vestuario;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function genero($conect) {
        // Tabla para hacer la consulta
        $nameTabla['inventario_imagen.t_genero'] = "inventario_imagen.t_genero";
        // Campos para seleccionar
        $campos['id_genero'] = "id_genero";
        $campos['genero'] = "genero";
            

        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function gruposdisp($conect, $id_estatus, $id_desc_estado) {
        // Tabla para hacer la consulta
        $nameTabla['almacen_tecnico.grupos_sc'] = "almacen_tecnico.grupos_sc";
        // Campos para seleccionar
        $campos['id_grupo'] = "id_grupo";
        $campos['descripcion'] = "descripcion";
        $campos['categoria'] = "categoria";
        $campos['imagen'] = "imagen";
        $campos['categoria'] = "categoria";
        
        
        $condicion['id_estatus'] = "'".$id_estatus."'";
        $condicion['id_desc_estado'] = "'".$id_desc_estado."'";
        $condicion['fecha_exp'] = "'2222-12-31'";
            
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function equiposdisp($conect, $tipo_articulo, $id_estatus, $id_desc_estado, $id_grupo) {
        // Tabla para hacer la consulta
        $nameTabla['almacen_tecnico.t_articulo'] = "almacen_tecnico.t_articulo";
        // Campos para seleccionar
        $campos['id_articulo'] = "id_articulo";
        $campos['descripcion'] = "descripcion";
        $campos['marca'] = "marca";
        $campos['modelo'] = "modelo";
        $campos['bien_nac'] = "bien_nac";
        $campos['serial'] = "serial";
        $campos['imagen'] = "imagen";
        $campos['cantidad'] = "cantidad";
        $campos['id_unidad_medida'] = "id_unidad_medida";
        $campos['cant_min'] = "cant_min";
        $campos['id_categoria'] = "id_categoria";

        $condicion['tipo_articulo'] ="'".$tipo_articulo."'";
        $condicion['id_grupo'] ="'".$id_grupo."'";
        $condicion['id_estatus'] = "'".$id_estatus."'";
        $condicion['id_estado'] = "'".$id_desc_estado."'";
        $condicion['fecha_exp'] = "'2222-12-31'";
            
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function equiposdispcon($conect, $tipo_articulo, $id_estatus, $id_desc_estado, $id_grupo) {
        // Tabla para hacer la consulta
        $nameTabla['almacen_tecnico.articulos_consumibles_disp'] = "almacen_tecnico.articulos_consumibles_disp";
        // Campos para seleccionar
        $campos['id_articulo'] = "id_articulo";
        $campos['descripcion'] = "descripcion";
        $campos['marca'] = "marca";
        $campos['modelo'] = "modelo";
        $campos['bien_nac'] = "bien_nac";
        $campos['serial'] = "serial";
        $campos['imagen'] = "imagen";
        $campos['cantidad'] = "cantidad";
        $campos['id_unidad_medida'] = "id_unidad_medida";
        $campos['cant_min'] = "cant_min";
        $campos['id_categoria'] = "id_categoria";


        $condicion['tipo_articulo'] ="'".$tipo_articulo."'";
        $condicion['id_grupo'] ="'".$id_grupo."'";
        $condicion['id_estatus'] = "'".$id_estatus."'";
        $condicion['id_estado'] = "'".$id_desc_estado."'";
          
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function equiposdisppc($conect, $tipo_articulo, $id_estatus, $id_desc_estado, $id_grupo, $id_categoria) {
        // Tabla para hacer la consulta
        $nameTabla['almacen_tecnico.articulos_consumibles_disp'] = "almacen_tecnico.articulos_consumibles_disp";
        // Campos para seleccionar
        $campos['id_articulo'] = "id_articulo";
        $campos['descripcion'] = "descripcion";
        $campos['marca'] = "marca";
        $campos['modelo'] = "modelo";
        $campos['bien_nac'] = "bien_nac";
        $campos['serial'] = "serial";
        $campos['imagen'] = "imagen";
        $campos['cantidad'] = "cantidad";
        $campos['id_unidad_medida'] = "id_unidad_medida";
        $campos['cant_min'] = "cant_min";
        $campos['id_categoria'] = "id_categoria";


        $condicion['tipo_articulo'] ="'".$tipo_articulo."'";
        $condicion['id_grupo'] ="'".$id_grupo."'";
        $condicion['id_estatus'] = "'".$id_estatus."'";
        $condicion['id_estado'] = "'".$id_desc_estado."'";
        $condicion['id_categoria'] = $id_categoria;
            
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function camarasdisp($conect, $id_estatus, $id_desc_estado) {
        // Tabla para hacer la consulta
        $nameTabla['almacen_tecnico.grupo_camaras'] = "almacen_tecnico.grupo_camaras";
        // Campos para seleccionar
        $campos['id_grupo'] = "id_grupo";
        $campos['descripcion'] = "descripcion";
        $campos['resp_asig'] = "resp_asig";
        $campos['imagen'] = "imagen";
        $campos['categoria'] = "categoria";
        
        
        $condicion['id_estatus'] = "'".$id_estatus."'";
        $condicion['id_desc_estado'] = "'".$id_desc_estado."'";
        $condicion['fecha_exp'] = "'2222-12-31'";
            
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function camarasdispsa($conect, $id_estatus, $id_desc_estado) {
        // Tabla para hacer la consulta
        $nameTabla['almacen_tecnico.grupo_camaras_sa'] = "almacen_tecnico.grupo_camaras_sa";
        // Campos para seleccionar
        $campos['id_grupo'] = "id_grupo";
        $campos['descripcion'] = "descripcion";
        $campos['resp_asig'] = "resp_asig";
        $campos['imagen'] = "imagen";
        $campos['categoria'] = "categoria";
        
        
        
        $condicion['id_estatus'] = "'".$id_estatus."'";
        $condicion['id_desc_estado'] = "'".$id_desc_estado."'";
        $condicion['fecha_exp'] = "'2222-12-31'";
            
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function materialespres($conect, $id_solicitud) {
        // Tabla para hacer la consulta
        $nameTabla['inventario_imagen.t_material_sol'] = "inventario_imagen.t_material_sol";
        // Campos para seleccionar
        $campos['id_articulo'] = "id_articulo";

        $condicion['id_solicitud'] =$id_solicitud;
            
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function descmateriales($conect, $id_articulo) {
        // Tabla para hacer la consulta
        $nameTabla['inventario_imagen.t_articulo'] = "inventario_imagen.t_articulo";
        // Campos para seleccionar
        
        $campos['id_articulo'] = "id_articulo";
        $campos['descripcion'] = "descripcion";
            
        $condicion['id_articulo'] = $id_articulo;    
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }


    function selectprestamos($conect, $id_prestamo) {
        // Tabla para hacer la consulta
        $nameTabla['almacen_tecnico.t_prestamo'] = "almacen_tecnico.t_prestamo";
        // Campos para seleccionar
        
        $campos['id_destino'] = "id_destino";
        $campos['id_desc_dest'] = "id_desc_dest";
        $campos['resp_prestamo'] = "resp_prestamo";
        $campos['user_reg'] = "user_reg";
        $campos['fecha_reg'] = "fecha_reg";
        $campos['fecha_sol'] = "fecha_sol";
        $campos['fecha_exp'] = "fecha_exp";
        $campos['observacion'] = "observacion";
        $campos['tipo_prestamo'] = "tipo_prestamo";
                
        $condicion['id_prestamo'] = $id_prestamo;    
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }


    function selectdatosincidencia($conect, $id_solicitud) {
        // Tabla para hacer la consulta
        $nameTabla['almacen_tecnico.t_incidencia'] = "almacen_tecnico.t_incidencia";
        // Campos para seleccionar
        
        $campos['resp_incidencia'] = "resp_incidencia";
        $campos['user_reg'] = "user_reg";
        $campos['fecha_reg'] = "fecha_reg";
        $campos['fecha_solicitud'] = "fecha_solicitud";
        $campos['observacion'] = "observacion";
        $campos['id_incidencia'] = "id_incidencia";
                
        $condicion['id_solicitud'] = $id_solicitud;    
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectincidencia($conect, $id_solicitud, $id_incidencia) {
        // Tabla para hacer la consulta
        $nameTabla['almacen_tecnico.t_incidencia'] = "almacen_tecnico.t_incidencia";
        // Campos para seleccionar
        
        $campos['resp_incidencia'] = "resp_incidencia";
        $campos['user_reg'] = "user_reg";
        $campos['fecha_reg'] = "fecha_reg";
        $campos['fecha_solicitud'] = "fecha_solicitud";
        $campos['observacion'] = "observacion";
        $campos['id_incidencia'] = "id_incidencia";
                
        $condicion['id_solicitud'] = $id_solicitud;
        $condicion['id_incidencia'] = "'".$id_incidencia."'";    
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selecttodoslosprestamos($conect) {
        // Tabla para hacer la consulta
        $nameTabla['inventario_imagen.t_prestamo'] = "inventario_imagen.t_prestamo";
        // Campos para seleccionar
        
        $campos['id_destino'] = "id_destino";
        $campos['id_desc_dest'] = "id_desc_dest";
        $campos['resp_prestamo'] = "resp_prestamo";
        $campos['user_reg'] = "user_reg";
        $campos['fecha_reg'] = "fecha_reg";
        $campos['fecha_sol'] = "fecha_sol";
        $campos['fecha_exp'] = "fecha_exp";
        $campos['observacion'] = "observacion";
        $campos['id_prestamo'] = "id_prestamo";
        $campos['hora_prestamo'] = "hora_prestamo";
        $campos['hora_reg'] = "hora_reg";
                    
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectdespacho($conect, $id_despacho) {
        // Tabla para hacer la consulta
        $nameTabla['almacen_tecnico.t_despacho'] = "almacen_tecnico.t_despacho";
        // Campos para seleccionar
        
        $campos['id_destino'] = "id_destino";
        $campos['id_desc_dest'] = "id_desc_dest";
        $campos['resp_despacho'] = "resp_despacho";
        $campos['user_reg'] = "user_reg";
        $campos['fecha_reg'] = "fecha_reg";
        $campos['fecha_sol'] = "fecha_sol";
        $campos['observacion'] = "observacion";
        $campos['hora_despacho'] = "hora_despacho";
        $campos['hora_reg'] = "hora_reg";

                
        $condicion['id_despacho'] = $id_despacho;    
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    

    function selecttipousuarios($conect,$id_aplicacion) {
        // Tabla para hacer la consulta
        $nameTabla['usuario.t_tipo_usuario'] = "usuario.t_tipo_usuario";
        // Campos para seleccionar
        
        $campos['id_tipo_usuario'] = "id_tipo_usuario";
        $campos['desc_usuario'] = "desc_usuario";
        
        $condicion['id_aplicacion'] = $id_aplicacion; 
        $orderby['id_tipo_usuario'] = "id_tipo_usuario";

        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    } 

    function selecttipousuariosadmin($conect,$id_aplicacion) {
        $nameTabla['usuario.t_tipo_usuario'] = "usuario.t_tipo_usuario";
        // Campos para seleccionar
        $campos['id_tipo_usuario'] = "id_tipo_usuario";
        $campos['desc_usuario'] = "desc_usuario";

        $condicion ="id_tipo_usuario != 31 and id_aplicacion=".$id_aplicacion."";
       
        $orderby['id_tipo_usuario'] = "id_tipo_usuario";
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelectLibre($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selecttallas($conect) {
        $nameTabla['almacen_tecnico.t_tallas'] = "almacen_tecnico.t_tallas";
        // Campos para seleccionar
        $campos['id_talla'] = "id_talla";
        $campos['descripcion'] = "descripcion";

        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function programas($conect) {
        // Tabla para hacer la consulta
        $nameTabla['almacen_tecnico.t_programa'] = "almacen_tecnico.t_programa";
        // Campos para seleccionar
        
        $campos['id_programa'] = "id_programa";
        $campos['descripcion'] = "descripcion";
        
        $condicion['fecha_exp'] = "'2222-12-31'";
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function tipodescripcion($conect) {
        // Tabla para hacer la consulta
        $nameTabla['almacen_tecnico.t_descripciones'] = "almacen_tecnico.t_descripciones";
        // Campos para seleccionar
        
        $campos['id_descripcion'] = "id_descripcion";
        $campos['descripcion'] = "descripcion";
        
        $condicion['fecha_exp'] = "'2222-12-31'";
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function insertarticulo($conect, $tipoart, $descripcion, $marca, $modelo, $biennac, $serial, $siglas, $id_grupo, $id_categoria, $peldano, $estante, $id_estado, $observacion, $unidad_medida, $costo, $cantidad, $cantidadmin, $id_estatus, $user_reg, $tipodesc){ 

        // Nombre de la Tabla
        $nameTabla = "almacen_tecnico.t_articulo";
        // Campos para hacer el insert
        $campos['tipo_articulo'] = $tipoart;
        $campos['descripcion'] = $descripcion;
        $campos['marca'] = $marca;
        $campos['modelo'] = $modelo;
        $campos['bien_nac'] = $biennac;
        $campos['serial'] = $serial;
        $campos['siglas'] = $siglas;
        $campos['id_grupo'] = $id_grupo;
        $campos['peldano'] = $peldano;
        $campos['estante'] = $estante;
        $campos['cantidad'] = $cantidad;
        $campos['cant_min'] = $cantidadmin;
        $campos['costo'] = $costo;
        $campos['id_estatus'] = $id_estatus;
        $campos['id_estado'] = $id_estado;
        $campos['id_categoria'] = $id_categoria;
        $campos['id_unidad_medida'] = $unidad_medida;
        $campos['fecha_exp'] = "2222-12-31";
        $campos['fecha_reg'] = date("Y-m-d");
        $campos['user_reg'] = $user_reg;
        $campos['id_descripcion'] = $tipodesc;

        
        $campo_return = "id_articulo";
        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsertRn($nameTabla, $campos, $campo_return);

    }

    function insertgrupo($conect, $id_estatus, $descripcion, $id_categoria, $user_reg, $id_asig, $id_estado, $asig){ 

        // Nombre de la Tabla
        $nameTabla = "almacen_tecnico.t_grupos";
        // Campos para hacer el insert
        $campos['descripcion'] = $descripcion;
        $campos['id_estatus'] = $id_estatus;
        $campos['id_categoria'] = $id_categoria;
        $campos['id_asig'] = $id_asig;
        $campos['id_desc_estado'] = $id_estado;
        $campos['user_reg'] = $user_reg;
        $campos['asig'] = $asig;
        $campos['fecha_reg'] = date("Y-m-d");
       
        $campo_return = "id_del_grupo";
        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsertRn($nameTabla, $campos, $campo_return);
    
    }

    function updateidgrupo($conect, $id_grupo, $id_del_grupo){
        // Tabla para hacer la consulta
        $nameTabla = 'almacen_tecnico.t_grupos';
        $campos['id_grupo'] = $id_grupo;
        
        $condicion['id_del_grupo'] = $id_del_grupo;
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }
    
    function selectincidencias($conect, $id_solicitud) {
        // Tabla para hacer la consulta
        $nameTabla['almacen_tecnico.t_incidencia'] = "almacen_tecnico.t_incidencia";
        // Campos para selecciona
        $campos['id_incidencia'] = "id_incidencia";
        
                
        $condicion['id_solicitud'] = $id_solicitud;    
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }
    
    function insertsolprestamo($conect, $iddestino, $destino, $quiensol, $quienent, $fecha_reg, $fecha_sol, $id_estatus_prestamo, $observacion, $hora, $placa, $tipo_prestamo, $idremoto) {        
    // Nombre de la Tabla
        $nameTabla = "almacen_tecnico.t_prestamo";
        // Campos para hacer el insert
        $campos['id_destino'] = $iddestino;
        $campos['id_desc_dest'] = $destino;
        $campos['resp_prestamo'] = $quiensol;
        $campos['user_reg'] = $quienent;
        $campos['fecha_reg'] = $fecha_reg;
        $campos['fecha_sol'] = $fecha_sol;
        //$campos['fecha_exp'] = $fecha_exp;
        $campos['id_estatus_prestamo'] = $id_estatus_prestamo;
        $campos['observacion'] = $observacion;
        $campos['placa'] = $placa;
        $campos['tipo_prestamo'] = $tipo_prestamo;
        $campos['hora_prestamo'] = $hora;
        $campos['id_remoto'] = $idremoto;
        $campos['hora_reg'] = date("H:i:s");


        $campo_return = "id_prestamo";
        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsertRn($nameTabla, $campos, $campo_return);
    }

    function insertincidencia($conect, $quiensol, $quienent, $fecha_reg, $fecha_sol, $observacion, $hora, $id_solicitud, $numinc) {        
        // Nombre de la Tabla
        $nameTabla = "almacen_tecnico.t_incidencia";
        // Campos para hacer el insert
        $campos['resp_incidencia'] = $quiensol;
        $campos['user_reg'] = $quienent;
        $campos['fecha_reg'] = $fecha_reg;
        $campos['fecha_solicitud'] = $fecha_sol;
        $campos['observacion'] = $observacion;
        $campos['hora_solicitud'] = $hora;
        $campos['id_solicitud'] = $id_solicitud;
        $campos['hora_reg'] = date("H:i:s");
        $campos['id_incidencia'] = $numinc;

        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos, $campo_return);
    }

    function insertsoldespacho($conect, $iddestino, $destino, $quiensol, $quienent, $fecha_reg, $fecha_sol, $id_estatus_prestamo, $observacion, $hora, $idremoto){
        // Nombre de la Tabla
        $nameTabla = "almacen_tecnico.t_despacho";
        // Campos para hacer el insert
        $campos['id_destino'] = $iddestino;
        $campos['id_desc_dest'] = $destino;
        $campos['resp_despacho'] = $quiensol;
        $campos['user_reg'] = $quienent;
        $campos['fecha_reg'] = $fecha_reg;
        $campos['fecha_sol'] = $fecha_sol;
        $campos['id_estatus_despacho'] = $id_estatus_prestamo;
        $campos['observacion'] = $observacion;
        $campos['hora_despacho'] = $hora;
        $campos['id_remoto'] = $idremoto;
        $campos['hora_reg'] = date("H:i:s");
        

        $campo_return = "id_despacho";
        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsertRn($nameTabla, $campos, $campo_return);
    }

    function insertgrupossol($conect, $art, $idprestamo, $fecha_reg, $quienent, $id_incidencia) {

        // Nombre de la Tabla
        $nameTabla = "almacen_tecnico.t_grupo_sol";
        // Campos para hacer el insert
        $campos["id_grupo"] = trim($art);
        $campos['id_solicitud'] = $idprestamo;
        $campos['fecha_sol'] = $fecha_reg;
        $campos['user_reg'] = $quienent;
        $campos['id_incidencia'] = $id_incidencia;

        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    }
    function insertarticulosol($conect, $art, $idprestamo, $fecha_reg, $quienent, $id_incidencia) {

        // Nombre de la Tabla
        $nameTabla = "almacen_tecnico.t_material_sol";
        // Campos para hacer el insert
        $campos["id_articulo"] = trim($art);
        $campos['id_solicitud'] = $idprestamo;
        $campos['fecha_sol'] = $fecha_reg;
        $campos['user_reg'] = $quienent;
        $campos['id_incidencia'] = $id_incidencia;

        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    }
    function insertcamarassol($conect, $art, $idprestamo, $fecha_reg, $quienent, $id_incidencia) {

        // Nombre de la Tabla
        $nameTabla = "almacen_tecnico.t_camara_sol";
        // Campos para hacer el insert
        $campos["id_grupo"] = trim($art);
        $campos['id_solicitud'] = $idprestamo;
        $campos['fecha_sol'] = $fecha_reg;
        $campos['user_reg'] = $quienent;
        $campos['id_incidencia'] = $id_incidencia;


        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    } 

    function insertarticulodesp($conect, $art, $iddespacho, $fecha_reg, $quienent, $cant, $sol_ppal) {

        // Nombre de la Tabla
        $nameTabla = "almacen_tecnico.t_material_desp";
        // Campos para hacer el insert
        $campos["id_articulo"] = trim($art);
        $campos['id_despacho'] = $iddespacho;
        $campos['fecha_reg'] = $fecha_reg;
        $campos['user_reg'] = $quienent;
        $campos['cantidad'] = $cant;
        $campos['sol_ppal'] = $sol_ppal;

        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    } 

    function insertarticulodespdev($conect, $art, $iddespacho, $user_reg, $fecha_reg, $cantact) {

        // Nombre de la Tabla
        $nameTabla = "almacen_tecnico.t_material_desp";
        // Campos para hacer el insert
        $campos["id_articulo"] = trim($art);
        $campos['id_despacho'] = $iddespacho;
        $campos['fecha_reg'] = $fecha_reg;
        $campos['user_reg'] = $user_reg;
        $campos['cantidad'] = $cantact;

        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    } 


    function updateestart($conect, $art, $id_estatus_prestamo) {
        // Tabla para hacer la consulta
        $nameTabla = "almacen_tecnico.t_articulo";
        $campos['id_estatus'] = $id_estatus_prestamo;

        $condicion['id_articulo'] = $art;
        $condicion['fecha_exp'] = '2222-12-31';
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }   

    function updateestgrup($conect, $art, $id_estatus_prestamo) {
        // Tabla para hacer la consulta
        $nameTabla = "almacen_tecnico.t_grupos";
        $campos['id_estatus'] = $id_estatus_prestamo;

        $condicion['id_grupo'] = $art;
        $condicion['fecha_exp'] = '2222-12-31';
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }

    function updateart($conect, $art, $id_estatus_prestamo, $cantidad) {
        // Tabla para hacer la consulta
        $nameTabla = "almacen_tecnico.t_articulo";
        $campos['id_estatus'] = $id_estatus_prestamo;
        $campos['cantidad'] = $cantidad;

        $condicion['id_articulo'] = $art;
        $condicion['fecha_exp'] = '2222-12-31';
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    } 
    function selectartgrupo($conect, $art) {
        // Tabla para hacer la consulta
        $nameTabla['almacen_tecnico.t_articulo'] = "almacen_tecnico.t_articulo";
        // Campos para seleccionar
        
        $campos['id_articulo'] = "id_articulo";
        
        $condicion['id_grupo'] = "'".$art."'";       
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }   

    function selectcantidad($conect, $art, $id_despacho) {
        // Tabla para hacer la consulta
        $nameTabla['almacen_tecnico.t_material_desp'] = "almacen_tecnico.t_material_desp";
        // Campos para seleccionar
        
        $campos['cantidad'] = "cantidad";
        
        $condicion['id_articulo'] = $art;
        $condicion['id_despacho'] = $id_despacho;
        $condicion['fecha_exp'] = "'2222-12-31'";         
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function updatearticulodesp($conect, $art, $iddespacho, $user_exp, $fecha_exp, $resp_devol, $hora_exp, $hora_dev) {
        // Tabla para hacer la consulta
        $nameTabla = "almacen_tecnico.t_material_desp";
        $campos['user_exp'] = $user_exp;
        $campos['fecha_exp'] = $fecha_exp;
        $campos['resp_devol'] = $resp_devol;
        $campos['hora_exp'] = $hora_exp;
        $campos['hora_dev'] = $hora_dev;


        $condicion['id_articulo'] = $art;
        $condicion['id_despacho'] = $iddespacho;
        $condicion['fecha_exp'] = '2222-12-31';
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    } 

    function selectcantdisp($conect, $art) {
        // Tabla para hacer la consulta
        $nameTabla['almacen_tecnico.t_articulo'] = "almacen_tecnico.t_articulo";
        // Campos para seleccionar
        
        $campos['cantidad'] = "cantidad";
        
        $condicion['id_articulo'] = $art;       
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function respasig($conect, $idgrupo) {
        // Tabla para hacer la consulta
        $nameTabla['almacen_tecnico.t_grupos'] = "almacen_tecnico.t_grupos";
        // Campos para seleccionar
        
        $campos['resp_asig'] = "resp_asig";
        
        $condicion['id_grupo'] = "'".$idgrupo."'"; 
        $condicion['fecha_exp'] = "'2222-12-31'";      
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelect($nameTabla, $campos, $condicion, $orderby, $this->groupby, $this->limite);
    }

    function selectprestamosvencidos($conect) {
        $nameTabla['almacen_tecnico.t_prestamo'] = "almacen_tecnico.t_prestamo";

        $campos['id_prestamo'] = "id_prestamo";
        $campos['id_estatus_prestamo'] = "id_estatus_prestamo";
        $campos['fecha_exp'] = "fecha_exp";
       

        $condicion = "id_estatus_prestamo = '4' and fecha_exp= '".date("Y-m-d")."'";
    
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fbdSelectLibre($nameTabla, $campos, $condicion, null, $this->groupby, $this->limite);
    }


    function updateestatus($conect, $id_estatus_prestamo, $id_prestamo) {
        // Tabla para hacer la consulta
        $nameTabla = 'almacen_tecnico.t_prestamo';
        $campos[' id_estatus_prestamo'] = "$id_estatus_prestamo";

        $condicion['id_prestamo'] = $id_prestamo;
       
        // Creando el objeto de la base de datos
        $this->ObjDb = new classdb($conect);
        $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbdUpdate($nameTabla, $campos, $condicion);
    }


    function insertestado($conect, $variable, $user_reg) {

        // Nombre de la Tabla
        $nameTabla = "almacen_tecnico.t_des_estado";
        // Campos para hacer el insert
        
        $campos['desc_estado'] = $variable;
        $campos['user_reg'] = $user_reg;
        $campos['fecha_reg'] = date("Y-m-d");

        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    }

    
    function insertmedida($conect, $variable, $user_reg) {

        // Nombre de la Tabla
        $nameTabla = "almacen_tecnico.t_unidad_medida";
        // Campos para hacer el insert
        
        $campos['descripcion'] = $variable;
        $campos['user_reg'] = $user_reg;
        $campos['fecha_reg'] = date("Y-m-d");

        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    }

    function insertcategoria($conect, $variable, $user_reg) {

        // Nombre de la Tabla
        $nameTabla = "almacen_tecnico.t_categoria";
        // Campos para hacer el insert
        
        $campos['descripcion'] = $variable;
        $campos['user_reg'] = $user_reg;
        $campos['fecha_reg'] = date("Y-m-d");

        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    }

    function insertprograma($conect, $variable, $user_reg) {

        // Nombre de la Tabla
        $nameTabla = "almacen_tecnico.t_programa";
        // Campos para hacer el insert
        
        $campos['descripcion'] = $variable;
        $campos['user_reg'] = $user_reg;
        $campos['fecha_reg'] = date("Y-m-d");

        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    }

    function inserttipodesc($conect, $variable, $user_reg, $id_tipo_art) {

        // Nombre de la Tabla
        $nameTabla = "almacen_tecnico.t_descripciones";
        // Campos para hacer el insert
        
        $campos['descripcion'] = $variable;
        $campos['tipo_articulo'] = $id_tipo_art;
        $campos['user_reg'] = $user_reg;
        $campos['fecha_reg'] = date("Y-m-d");

        $this->ObjDb = new classdb($conect);
        $conector = $this->ObjDb->fdbConectar();
        return $this->ObjDb->fdbInsert($nameTabla, $campos);
    }


}

?>
