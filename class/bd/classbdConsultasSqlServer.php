<?php
// Clase Principal.
class classbdConsultasSqlServer
{
	var $ObjDb="";
	var $orderby, $groupby,$limite;
	function classbdConsultasSqlServer()
	{
		// Librerias Comunes
		require_once("../../../database/classdbsqlserver.php");
		$this->orderby=array();
		$this->groupby=array();
		$this->limite=array();
	}
////////////////////////////////////////////////////////////////////////////////
//TODAS LAS BUSQUEDAS
////////////////////////////////////////////////////////////////////////////////
	/*
	*/
	function primaAntiguedad($conect,$id)
	{
	
		// Tabla para hacer la consulta
		$nameTabla['VISTA_ANTIGUEDAD']="VISTA_ANTIGUEDAD A";
		// Campos para seleccionar
		$campos['A.ID_PERSONA']="A.ID_PERSONA";
		$campos['A.MONTO']="A.MONTO";		
		$condicion="A.ID_PERSONA=".$id."";
		//$orderby['D.ID_CONCEPTO']="D.ID_CONCEPTO";
		
		
		// Creando el objeto de la base de datos
		$this->ObjDb = new classdbsqlserver($conect);
		$this->ObjDb->fdbConectar();
		return $this->ObjDb->fbdSelectLibre($nameTabla,$campos,$condicion,$this->orderby,$this->groupby,$this->limite);
	}
	
	
	
	function ConceptoMonto($conect,$id)
	{
	
		// Tabla para hacer la consulta
		$nameTabla['RHUPERSONAL']="RHUPERSONAL A";
		$nameTabla['GRLPERSONA']="GRLPERSONA B";
		$nameTabla['NOMCONCEPTOSEMPLEADOS']="NOMCONCEPTOSEMPLEADOS C";
		$nameTabla['NOMCONCEPTOS']="NOMCONCEPTOS D";


		// Campos para seleccionar
		$campos['D.DM_DESCRIPCION']="D.DM_DESCRIPCION";
		$campos['C.MO_CONCEPTO']="C.MO_CONCEPTO";		


		
		$condicion="A.ID_PERSONA=B.ID_PERSONA and 
					A.ID_EMPRESA=B.ID_EMPRESA and 
					A.ID_PERSONA=C.ID_PERSONA and
					C.ID_CONCEPTO=D.ID_CONCEPTO and
					C.ESTADO='A' and
					D.TI_CONCEPTO='A' and
					B.ESTADO='A' and														
					A.ID_PERSONA=".$id."and
					D.ID_CONCEPTO in (45,68,114,115,116,205)";
		$orderby['D.ID_CONCEPTO']="D.ID_CONCEPTO";
		
		
		// Creando el objeto de la base de datos
		$this->ObjDb = new classdbsqlserver($conect);
		$this->ObjDb->fdbConectar();
		return $this->ObjDb->fbdSelectLibre($nameTabla,$campos,$condicion,$this->orderby,$this->groupby,$this->limite);
	}
	
	
	
	function IdPersonal($conect,$cedula)
	{
	
		//$sql="SELECT ID_PERSONA FROM GRLPERSONA WHERE GRLPERSONA.NU_DOCUMEN=$ci";

		//SELECT ID_PERSONA FROM GRLPERSONA
		// Tabla para hacer la consulta
		$nameTabla['GRLPERSONA']="GRLPERSONA";
		// Campos para seleccionar
		$campos['ID_PERSONA']="ID_PERSONA";

		// Condicon
		$condicion['NU_DOCUMEN']=$cedula;
		// Creando el objeto de la base de datos
		$this->ObjDb = new classdbsqlserver($conect);
		$this->ObjDb->fdbConectar();
		return $this->ObjDb->fbdSelect($nameTabla,$campos,$condicion,$this->orderby,$this->groupby,$this->limite);
	}
	
	
	
	
	function datosTrabajador($conect,$cedula)
	{
	
		//SELECT ID_PERSONA FROM GRLPERSONA
		// Tabla para hacer la consulta
		$nameTabla['VISTA_VTV_CONSTANCIA']="VISTA_VTV_CONSTANCIA";
		// Campos para seleccionar
		$campos['Cedula']="Cedula";
		$campos['Nombre']="Nombre";
		$campos['Tipo_Trabajador']="Tipo_Trabajador";
		$campos['Cargo']="Cargo";
		$campos['Gerencia']="Gerencia";
		$campos['Codigo_Empleado']="Codigo_Empleado";
		$campos['Ingreso']="Ingreso";
		$campos['Banco']="Banco";	
		$campos['Tipo_Cuenta']="Tipo_Cuenta";
		$campos['Cuenta']="Cuenta";
		// Condicon
		$condicion['Cedula']=$cedula;
		// Creando el objeto de la base de datos
		$this->ObjDb = new classdbsqlserver($conect);
		$this->ObjDb->fdbConectar();
		return $this->ObjDb->fbdSelect($nameTabla,$campos,$condicion,$this->orderby,$this->groupby,$this->limite);
	}
	
	function datosNominaTrabajador($conect,$cedula,$tipo_nomina,$quincena)
	{
	
		//SELECT ID_PERSONA FROM GRLPERSONA
		// Tabla para hacer la consulta
		$nameTabla['VISTA_VTV_CONSTANCIA']="VISTA_VTV_CONSTANCIA";
		// Campos para seleccionar
		$campos['Cedula']="Cedula";
		// Condicon
		$condicion['Cedula']=$cedula;
		$condicion['Tipo_Nomina']=$tipo_nomina;
		$condicion['Periodo']=$quincena;

		
		// Creando el objeto de la base de datos
		$this->ObjDb = new classdbsqlserver($conect);
		$this->ObjDb->fdbConectar();
		return $this->ObjDb->fbdSelect($nameTabla,$campos,$condicion,$this->orderby,$this->groupby,$this->limite);
	}
	
	function NominaTrabajadorPorTipo($conect,$cedula,$tipo_nomina,$quincena,$asi_ded)
	{
	
		//SELECT ID_PERSONA FROM GRLPERSONA
		// Tabla para hacer la consulta
		$nameTabla['VISTA_VTV_CONSTANCIA']="VISTA_VTV_CONSTANCIA";
		// Campos para seleccionar
		$campos['Concepto']="Concepto";
		$campos['Descripcion_concepto']="Descripcion_concepto";
		$campos['Monto_Concepto']="Monto_Concepto";	
		$campos['Fecha_Pago']="Fecha_Pago";	
		$campos['Cantidad_Concepto']="Cantidad_Concepto";	
		
		// Condicon
		$condicion['Cedula']=$cedula;
		$condicion['Tipo_Nomina']=$tipo_nomina;
		$condicion['Periodo']=$quincena;
		$condicion['Tipo_Concepto']="'".$asi_ded."'";
		// Creando el objeto de la base de datos
		$this->ObjDb = new classdbsqlserver($conect);
		$this->ObjDb->fdbConectar();
		return $this->ObjDb->fbdSelect($nameTabla,$campos,$condicion,$this->orderby,$this->groupby,$this->limite);
	}
	
	function DatosSueldo($conect,$cedula)
	{
		// Tabla para hacer la consulta
		$nameTabla['VISTA_CONCEPTOSFIJOS']="VISTA_CONCEPTOSFIJOS";
		// Campos para seleccionar
		$campos['Descripcion']="Descripcion";
		$campos['Monto']="Monto";	
		// Condicon
		$condicion['Cedula']=$cedula;
		// Creando el objeto de la base de datos
		$this->ObjDb = new classdbsqlserver($conect);
		$this->ObjDb->fdbConectar();
		return $this->ObjDb->fbdSelect($nameTabla,$campos,$condicion,$this->orderby,$this->groupby,$this->limite);
	}
	
	

	
}
?>
