<?php
@setlocale('LC_ALL','es_VE.UTF-8');
//header('Cache-Control: no-cache');
/**
 * clase manejadora de html y carga de datos compatible con intranet estructura
 * logica.
 */
if (defined("path")) {
	include("../database/classdb.php");
}else{
	include("../../database/classdb.php");
}
/**
 * optiene dato de post o get
 *
 * @param string $nombre
 * @return string
 */
function getpost($nombre){
	if ($_GET[$nombre]!="") {
		$dato=$_GET[$nombre];
	}elseif ($_POST[$nombre]!="") {
		$dato=$_POST[$nombre];
	}else{
		return false;
	}

	return  utf8_decode($dato);
}
class vtvcore {
	private $t_inicial;

	var $ajaxpag;

	public $menu;

	function __construct($js=null,$css=null,$class_principal="div_principal",$intranetCab="logo_top"){
		$this->t_inicial=microtime();

		if ($this->ajaxpag=="") {
		?>
	<html>
	<head>
		<title></title>
		<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
		<?php

		}


		for($i=0;$i<count($js);$i++){
			?>
		<script type="text/javascript" src="<?php echo $js[$i]; ?>"></script>
			<?php
		}


		for ($i=0;$i<count($css);$i++){
			?>
		<link href="<?php echo $css[$i]; ?>" rel="stylesheet" type="text/css" />
			<?php
		}

		if ($this->ajaxpag=="") {

		if ($this->sincabecera!=true) {

		?>

		<link href="../../estilos/jquery.alerts.css" rel="stylesheet" type="text/css" />

                <!-- libreria compartida de flexigrid gilla-->
		<link href="../../librerias/class_core_libs/flexigrid/css/flexigrid/flexigrid.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="../../librerias/class_core_libs/flexigrid/flexigrid.js"></script>

                <!-- libreria compartida de calendarios y eventos -->
                <link href="../../librerias/class_core_libs/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="../../librerias/class_core_libs/fullcalendar/fullcalendar.min.js"></script>
		<script type="text/javascript" src="../../librerias/class_core_libs/jquery.qtip.js"></script>

		<script type="text/javascript" src="../../librerias/classcore.js"></script>
		<script type="text/javascript" src="../../librerias/jquery.alerts.js"></script>

	</head>
	<body>

		<div id="<?php echo $class_principal; ?>">
		<div id="div_sombra" align="center">
		<div id="div_segundario">
		<?php
			vtvcore::cablib($intranetCab);
			}else{
			?>
				</head>
	<body>
			<?
			}

		}
		/*if ($this->sincabecera==true) {
		}*/
	?>
	<div id="error" style="display:none"></div>
	<?php
	}

	function login(){
		?>

		<script>
		$(document).ready(function(){
			cleanize_menu();
		})
		</script>
	<table class="tabla" style="width:500px;">
		<tr>
			<th colspan="2" class="titulo">INGRESO</th>
		</tr>
		<tr>
			<td valign="top">
				<table width="90%" border="0" align="center" valign="middle" cellpadding="0" cellspacing="5">
					<td width="50%">
						<table width="100%"  border="0" cellspacing="8" cellpadding="0">
							<tr>
								<td class="text2"><div align="right">Introduzca su cedula y clave de usuario haga click en "entrar" o presione "enter" Espere confirmación </div></td>
							</tr>
						</table>
					</td>
					<td width="1">
						<img src="images/log_03.jpg" width="2" height="236">
					</td>
					<td width="50%">
						<table width="100%"  border="0" cellspacing="8" cellpadding="0">
							<tr>
								<td class="text2">Introduzca su <b>cedula</b></td>
							</tr>
							<tr>
								<td>
									<input type="text" maxlength="8"  name="cedula" id="cedula" onKeyPress="return solonum(event);" />
								</td>
							</tr>
							<tr>
								<td class="text2">Introduzca su <b>clave de usuario</b></td>
							</tr>
							<tr>
								<td>
                                                                    <input type="password" name="clave" id="clave" onKeyUp="pressenter(event,'<?php if(defined("dinamic_module")){echo dinamic_module;} ?>');"/>
									<input type="hidden" id="idprograma" value="<?php echo $this->idprograma ?>"/>
									<input type="hidden" id="idmodulo" value="<?php echo $this->idmodulo ?>"/>
									<input type="hidden" id="cxonex" value="<?php
									if (defined("archconexstr")) {
										echo base64_encode(archconexstr)."|@|".base64_encode(cfg_tacceso)."|@|".base64_encode(cfg_tusuario) ;
									}
									?>"/>
								</td>
							</tr>
							<tr>
								<td>
									<button onClick="validarIngreso('<?php if(defined("dinamic_module")){echo dinamic_module;} ?>')" type="button" class="boton2" value="entrar"></button>
								</td>
							</tr>
							<tr>
								<td class="text2">
									<!--confirmación <input name="validarUsuario" type="hidden" value="validarUsuario" />-->
								</td>
							</tr>
						</table>
					</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

<br>
<div name="cont" id="cont"><div name="loading" align="center" id="loading"></div>	</div>
		<?php
	}

	/**
	 * genera el encabezado de una pagina... nolmalmente
	 *
	 */

	function cablib($logoCSS="logo_top"){
		include("classlibCabPie.php");
		$libcap=new classlibCabPie("","");

		if (count($_SESSION)>0 or $_COOKIE[menu]=="activamenu"){
			$menu='<div class="menu_apli">
				<ul id="navmenu-h">';

			if (function_exists("menus")) {
				ob_start();
				menus();
				$menu.=ob_get_contents();
				ob_end_clean();
			}

			if($_COOKIE[menu]!="activamenu"){
				$menu.='<li>
	                    <a href="#" onclick="cerrarsistema();">cerrar sistema</a>
	                </li>';
			}
			$menu.='</ul>
			</div>';
		}
		echo $libcap->flibHtmCab_noestandar($menu,strftime("%A %e %B %Y", mktime(0, 0, 0, date("m"), date("d"), date("y"))),$logoCSS);
	}

	/**
	 * genera el pie de pagina automaticamente
	 */
	function __destruct(){
		if ($this->sincabecera!=true) {
			if ($this->ajaxpag=="") {
		?>
	<div class="cont_bottom"/></div>
	<div id="div-pie-pagina-sistemas" align="center" >
	Desarrollado por la Gerencia de Tecnolog&iacute;a de la Informaci&oacute;n y Comunicaci&oacute;n <br>
	Navegador Recomendado Mozilla Firefox a	Resoluci&oacute;n 1024x768 <br>
	impreso en: <?php echo microtime()-$this->t_inicial; ?> microsegundos
	</div>
	</div>
	</div>
	</body>
</html>
		<?php
			}
		}else{
		?>
			</body>
		</html>
		<?php
		}
	}
}

/**
 * clase manejadora de autenticacion de usuario, sistema de autenticacion
 * intranet
 */

class auth extends classdb {
	//private $query;
	private $cx;
	private $tacceso;
	private $tusuario;

        private $id_modulo;

	function __construct(){
		if(defined("archconexstr")){
			$conex=archconexstr;
			$this->tacceso=cfg_tacceso;
			$this->tusuario=cfg_tusuario;
		}else{
			$conex="sistemas_vtv_5431";
			$this->tacceso="intranet.t_acceso";
			$this->tusuario="intranet.t_intranet_usuario";
		}

		//conexion
		if (defined("path")) {
			auth::classdb("../database/archi_conex/$conex");
		}else{
			auth::classdb("../../database/archi_conex/$conex");
		}

		//auth::classdb("../database/archi_conex/sistemas_vtv_5431");
		$this->cx=auth::fdbConectar();
	}


	/**
	 * function para validacion de usuarios de la intranet y de los sistemas.
	 *
	 * @param numerico $id_aplicacion
	 * @param numerico $id_modulo
	 * @param numerico $cedula
	 * @param cadena $pass
	 * @return bool
	 */
	function chequearusuario_intranet($id_aplicacion,$id_modulo,$cedula,$pass=''){
		if($cedula!=""){
                        if(defined("dinamic_module")){
                            $sql="select * from $this->tacceso where id_aplicacion='$id_aplicacion' and cedula='$cedula' and fecha_exp='2222-12-31' ;";
                        }else{
                            $sql="select * from $this->tacceso where id_aplicacion='$id_aplicacion' and id_modulo='$id_modulo' and cedula='$cedula' and fecha_exp='2222-12-31' ;";
                        }
                        $exec = pg_query($sql) or die($sql);
			if(pg_numrows($exec)>0){
				//validar clave
				$_SESSION['perfil']=pg_fetch_result($exec,0,'niv_con');
				if(defined("archconexstr")){
					//este es el query que se usa para las tablas de usuarios creadas por lflorez.
                                    if(defined("dinamic_module")){
					$sql="select * from $this->tusuario where id_aplicacion='$id_aplicacion' and clave='$pass' and cedula='$cedula' and fecha_exp='2222-12-31'";
                                    }else{
                                        $sql="select * from $this->tusuario where id_aplicacion='$id_aplicacion' and id_modulo='$id_modulo' and clave= '$pass' and cedula='$cedula' and fecha_exp='2222-12-31'";
                                    }

				}else{
					$sql="select * from $this->tusuario where clave= '$pass' and cedula='$cedula' and fecha_exp='2222-12-31'";
				}
				$exec = pg_query($sql) or die($sql);

                                $resultados=pg_fetch_assoc($exec);

                                $this->id_modulo=$resultados['id_modulo'];


				if(pg_numrows($exec)>0){
					return true;
				}else {
					return false;
				}
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

        function get_idmodulo(){
            return $this->id_modulo;
        }




	/**
	 * Matar Conexion.
	 */
	function __destruct(){
		@pg_close($this->cx);
	}
}

class interfaz extends auth {
	private $idprograma;//1
	private $idmodulo;//3
	private $idperfil;// perfil usuario.. es nulo si no se usa
	private $ci;
	private $pass;
	private $page;
	public  $accesoanonimo=false;
	private $contenedor;
	private $addstilo;

	function __construct($sys_path,$root_class,$contenedor="contenedor-sistemas",$addstilo=false){
		include($sys_path);
		$this->accesoanonimo=false;
		$this->contenedor=$contenedor;
		$this->addstilo=$addstilo;
		$this->root_class=$root_class;
	}

	function accesoAnonimo($acceso=false){
		$this->accesoanonimo=$acceso;
	}

	//puede que la pagina requiera de un subcontrolador de clases..
	//si esto lo requiere la variable debe ser otra cosa que no sea false.
	function mostrarpagina($subcontroller=false){
		$this->page=new $this->root_class($this->idprograma,$this->idmodulo,$this->contenedor,$this->addstilo,$this->accesoanonimo);
		parent::__construct();
		if($this->accesoanonimo==false){
			if (getpost('p')==""){
				//cerrando la session anterior.
				session_destroy();
				$p="login";
			}else{
				if($this->chequearusuario_intranet($this->idprograma,$this->idmodulo,$this->ci,$this->pass)){
					if ($subcontroller!=false){
						$p=$subcontroller;
					}else{
						$p=getpost('p');
					}
				}else{
					$p="login";
				}
			}

			if (method_exists($this->page,$p)){
				$this->page->$p();
			}else{
				echo "no existe pagina consultada";
			}
		}else{
                    if (getpost('p')==""){
			$p="inicio";
                    }else{
                        $p=getpost('p');
                    }
			if (method_exists($this->page,$p)) {
				$this->page->$p();
			}else{
				echo "no existe pagina consultada";
			}
		}
	}

	function set_uids($ci,$pass){
		$this->ci=$ci;
		$this->pass=$pass;
	}

	function setids($idprograma,$idmodulo){
		$this->idprograma=$idprograma;
		$this->idmodulo=$idmodulo;

	}

	function __destruct(){
		parent::__destruct();
	}
}
?>
