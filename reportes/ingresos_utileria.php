<?php

header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
ini_set('memory_limit', '9999999999999999999M');
require_once('tcpdf/tcpdf.php');


require_once("../class/bd/classbdConsultas.php");
require_once("../../../librerias/classlibFecHor.php");

class reporte extends TCPDF {

    public $conect_sistemas_vtv;
    public $ObjConsulta;
    public $Objfechahora;
    public $registros;
    public $almacenista;
    public $receptor;

    function __construct($orientation='P', $unit='mm', $format='A4', $unicode=true, $encoding='UTF-8', $diskcache=false) {
        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache);
        $this->conect_sistemas_vtv = "../../../database/archi_conex/sistemas_vtv_5431";
        $this->conect_sigesp = "../../../database/archi_conex/sistema_sigesp";
        $this->ObjConsulta = new classbdConsultas();
        $this->Objfechahora = new classlibFecHor(); 
    }

    function header() {

        // $this->registros = $this->ObjConsulta->select_data_asignacionplani($this->conect_sistemas_vtv, $_GET['idasignacion']);
        //fix array
        //  $this->registros = array_merge($this->registros);
        //$this->registros = array_map("array_merge", $this->registros);

        if ($this->registros[0][1] == "") {
            $this->registros[0][1] = "NO EMPLEADO";
        }


        $this->SetFont('', '', 10);


        $this->almacenista = utf8_encode($this->registros[0][1]);
        $this->receptor = utf8_encode($this->registros[0][2]);

        $titulo1 = "REPORTE DE INGRESOS";

        $htmltable = '<table border="0" width="650px" cellspacing="4">
	  <tr>
	  	
		<td width="160px" rowspan="3"><img src="imagenes/logo_vtv.jpg" style="width: 173px; height: 72px;" alt="logo"/></td>
		<div align="center"><font size="10">FECHA: <b>' . date("d/m/Y H:i:s") . '</b></font></div>
		<td ><br /><br /><div align="center"><font size="10"><b>' . $titulo1 . '</b></font></div></td>
		<!--<td width="180px"><div align="left"><font size="10">Nº. <b>' . str_pad($_GET['idasignacion'], 10, 0, STR_PAD_LEFT) . '</b></font></div></td>-->
	 </tr>
	 </table>
          
';
        $fechaimp = date("d/m/Y H:i:s");

        // echo $htmltable;
        $this->writeHTML($htmltable);

        //$this->Image('../imagenes/bandera.jpg','', $this->GetY()-3, 168);
    }

    function footer() {
        $this->SetFont('', '', 6);
        $this->Ln(3);
        $this->Cell(0, 0, 'FECHA:' . date("d/m/Y H:i:s") . '', 0, 0, 'L');
    }

    function renderizarimagetofile($url, $name, $path="imagenes/") {
        if (($f = fopen($url, 'r')) != false) {
            fclose($f);
            $res = join(file($url));
            if (($f = fopen($path . $name . ".png", "w")) != false) {
                fwrite($f, $res);
                fclose($f);
            }
        }
    }

}

//$pdf2=new MEM_IMAGE();
$pdf = new reporte('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, '', PDF_HEADER_STRING);//PDF_HEADER_TITLE
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, 'LISTADO CONSTANCIAS DE TRABAJO DEL '.$desde.' AL '.$hasta, PDF_HEADER_STRING);//PDF_HEADER_TITLE
// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(15, 38, 20);
$pdf->SetHeaderMargin(15);
$pdf->SetFooterMargin(20);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

$pdf->AddPage('L');
require_once('tcpdf/htmlcolors.php');
$pdf->Ln(2);

$fechadesde = $_GET['fechadesde'];
$fechadesde = $pdf->Objfechahora->flibInvertirEsIn($fechadesde);
$fechahasta = $_GET['fechahasta'];
$fechahasta = $pdf->Objfechahora->flibInvertirEsIn($fechahasta);



$datosmateriales = $pdf->ObjConsulta->selectingreso_utileria($pdf->conect_sistemas_vtv, $fechadesde, $fechahasta);
$contador= (count($datosmateriales));
if($contador==0){
    $materiales ='<tr nobr="true"><th colspan="9"  color="red"><font size="10" ><b>No se encuentran ingresos registrados</b></font></th></tr>';
}else{
    foreach ($datosmateriales as $llave => $valor) {
        $id_ingreso= $valor[1];
        $descripcion= $valor[2];
        $marca= $valor[3];
        $color= $valor[4];
        $talla_medida= $valor[5];
        $desc_unidad_medida= $valor[6];
        $cantidad= $valor[7];
        $costo= $valor[8];
        $fecha_reg= $valor[9];
        $fecha_reg = $pdf->Objfechahora->flibInvertirInEs($fecha_reg);
        $user_reg= $valor[10];
        

        
        $responsable=$pdf->ObjConsulta->selectpersonalresp($pdf->conect_sigesp, $user_reg);
        $nombres=$responsable[1][2];
        $apellidos=$responsable[1][3];
        $cargo=$responsable[1][4];
        
        $materiales.='
        <tr nobr="true"><td align="center"><font size="8">' . $id_ingreso . '</font></td>
        <td align="left"><font size="8">' . $descripcion . '</font></td>
        <td align="left"><font size="8">' . $marca . '</font></td>
        <td align="left"><font size="8">' . $color . '</font></td>
        <td align="center"><font size="8">' . $talla_medida . ' ' . $desc_unidad_medida . '</font></td>
        <td align="center"><font size="8">' . $cantidad . '</font></td>
        <td align="center"><font size="8">' . $costo . '</font></td>
        <td align="center"><font size="8">' . $fecha_reg . '</font></td>
        <td align="center"><font size="8">' . $nombres . ' ' . $apellidos . '</font></td>
        '.$tipo.'
        </tr>';
    } 


}

$columnas='<tr nobr="true">
<th><div align="center"><font size="8"><b>N de Ingreso</b></font></div></th>
<th><div align="center"><font size="8"><b>Descripci&oacute;n</b></font></div></th>
<th><div align="center"><font size="8"><b>Marca</b></font></div></th>
<th><div align="center"><font size="8"><b>Color</b></font></div></th>
<th><div align="center"><font size="8"><b>Medidas</b></font></div></th>
<th><div align="center"><font size="8"><b>Cantidad</b></font></div></th>
<th><div align="center"><font size="8"><b>Costo</b></font></div></th>
<th><div align="center"><font size="8"><b>Fecha de ingreso</b></font></div></th>
<th><div align="center"><font size="8"><b>Responsable</b></font></div></th>
</tr>'; 



/////////////////////////////////////////////////////////////////////////////////////////////////

$titulo = 'INGRESOS DE UTILERIA';
$body = '
<table align="center" border="1">
		<tr nobr="true"><th colspan="9"  bgcolor="DarkGray"><font size="10" ><b>' . $titulo . '</b></font></th></tr>
        ' . $columnas . '
        ' . $materiales . '
</table>

';
//echo $_GET['idasignacion'];
//$regmat = $pdf->ObjConsulta->select_equiposasignacion($pdf->conect_sistemas_vtv, $_GET['idasignacion']);
//$regmat = array_merge($regmat);
//$regmat = array_map(array_merge, $regmat);
//echo $body;

$pdf->Cell(10);
$pdf->writeHTML(utf8_encode($body), true, 0, true, 0);
$pdf->Output("Reporte_recurso_humano_pauta.pdf", 'I');
?>