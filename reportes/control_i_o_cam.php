<?php

header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
ini_set('memory_limit', '9999999999999999999M');
require_once('tcpdf/tcpdf.php');


require_once("../class/bd/classbdConsultas.php");
require_once("../../../librerias/classlibFecHor.php");

class reporte extends TCPDF {

    public $conect_sistemas_vtv;
    public $ObjConsulta;
    public $Objfechahora;
    public $registros;
    public $almacenista;
    public $receptor;

    function __construct($orientation='P', $unit='mm', $format='A4', $unicode=true, $encoding='UTF-8', $diskcache=false) {
        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache);
        $this->conect_sistemas_vtv = "../../../database/archi_conex/sistemas_vtv_5431";
        $this->conect_sigesp = "../../../database/archi_conex/sistema_sigesp";
        $this->ObjConsulta = new classbdConsultas();
        $this->Objfechahora = new classlibFecHor(); 
    }

    function header() {

        // $this->registros = $this->ObjConsulta->select_data_asignacionplani($this->conect_sistemas_vtv, $_GET['idasignacion']);
        //fix array
        //  $this->registros = array_merge($this->registros);
        //$this->registros = array_map("array_merge", $this->registros);

        if ($this->registros[0][1] == "") {
            $this->registros[0][1] = "NO EMPLEADO";
        }


        $this->SetFont('', '', 10);


        $this->almacenista = utf8_encode($this->registros[0][1]);
        $this->receptor = utf8_encode($this->registros[0][2]);

        $titulo1 = "CONTROL DE SALIDA Y ENTRADA DE CAMARA";

        $htmltable = '<table border="0" width="650px" cellspacing="4">
	  <tr>
	  	
		<td width="160px" rowspan="3"><img src="imagenes/logo_vtv.jpg" style="width: 173px; height: 72px;" alt="logo"/></td>
		<div align="center"><font size="10">FECHA: <b>' . date("d/m/Y H:i:s") . '</b></font></div>
		<td ><br /><br /><div align="center"><font size="10"><b>' . $titulo1 . '</b></font></div></td>
		<!--<td width="180px"><div align="left"><font size="10">Nº. <b>' . str_pad($_GET['idasignacion'], 10, 0, STR_PAD_LEFT) . '</b></font></div></td>-->
	 </tr>
	 </table>
          
';
        $fechaimp = date("d/m/Y H:i:s");

        // echo $htmltable;
        $this->writeHTML($htmltable);

        //$this->Image('../imagenes/bandera.jpg','', $this->GetY()-3, 168);
    }

    function footer() {
        $this->SetFont('', '', 6);
        $this->Ln(3);
        $this->Cell(0, 0, 'FECHA:' . date("d/m/Y H:i:s") . '', 0, 0, 'L');
    }

    function renderizarimagetofile($url, $name, $path="imagenes/") {
        if (($f = fopen($url, 'r')) != false) {
            fclose($f);
            $res = join(file($url));
            if (($f = fopen($path . $name . ".png", "w")) != false) {
                fwrite($f, $res);
                fclose($f);
            }
        }
    }

}

//$pdf2=new MEM_IMAGE();
$pdf = new reporte('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, '', PDF_HEADER_STRING);//PDF_HEADER_TITLE
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, 'LISTADO CONSTANCIAS DE TRABAJO DEL '.$desde.' AL '.$hasta, PDF_HEADER_STRING);//PDF_HEADER_TITLE
// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(15, 38, 20);
$pdf->SetHeaderMargin(15);
$pdf->SetFooterMargin(20);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

$pdf->AddPage('l');
require_once('tcpdf/htmlcolors.php');
$pdf->Ln(2);

       
///intervalo de fechas para la consulta de las entradas y salidas de las camaras
$fecha_desde=$_GET['desde'];
$fecha_hasta=$_GET['hasta'];
$id_grupo=$_GET['id_grupo'];


$datosmateriales = $pdf->ObjConsulta->selectcamaraspresrep($pdf->conect_sistemas_vtv, $fecha_desde, $fecha_hasta, $id_grupo);
$contador= (count($datosmateriales));
if($contador==0){
    $materiales ='<tr nobr="true"><th colspan="8"  color="red"><font size="10" ><b>No se encontraron registros para la camara seleccionada</b></font></th></tr>';
}else{
    foreach ($datosmateriales as $llave => $valor) {
        $id_grupo= $valor[1];
        $id_solicitud= $valor[2];
        $fecha_reg= $valor[3];
        $user_reg= $valor[4];
        $fecha_exp= $valor[5];
        $user_exp= $valor[6];
        $hora_exp= $valor[7];

        //Responsable del prestamo
        $datossolicitud = $pdf->ObjConsulta->selectdatossol($pdf->conect_sistemas_vtv, $id_solicitud);
        $resp_prestamo= $datossolicitud[1][3];
        $fecha_prestamo= $datossolicitud[1][6];
        $fecha_prestamo = $pdf->Objfechahora->flibInvertirInEs($fecha_prestamo);
        $hora_prestamo= $datossolicitud[1][10];
        $responsable_out=$pdf->ObjConsulta->selectpersonalresp($pdf->conect_sigesp, $resp_prestamo);
        $nomresp_out=utf8_encode($responsable_out[1][2]);
        $apelresp_out=utf8_encode($responsable_out[1][3]);

        //Observaciones de la entrada 
        $datosentrada = $pdf->ObjConsulta->selectdatosentrada($pdf->conect_sistemas_vtv, $id_solicitud);
        $contin= count($datosentrada);
        if ($contin==0){
            $observacion='<font size="8" color="RED" >  Sin entregar</font>';;
            $fecha_entrada='-';
            $nomresp_in='-';
            $apelresp_in='-';

        }else{
            $observacion=$datosentrada[1][1];
            $resp_entrada=$datosentrada[1][2];
            $fecha_entrada=$datosentrada[1][3];
            $fecha_entrada = $pdf->Objfechahora->flibInvertirInEs($fecha_entrada);
            $responsable_in=$pdf->ObjConsulta->selectpersonalresp($pdf->conect_sigesp, $resp_entrada);
            $nomresp_in=utf8_encode($responsable_in[1][2]);
            $apelresp_in=utf8_encode($responsable_in[1][3]);


            if($observacion==''){
                $observacion="Sin Observaciones";
            }else{
                $observacion=$observacion;
            }  
        }
        

        //Descripcion de la camara a consultar
        $datosgrupo = $pdf->ObjConsulta->desc_grupo($pdf->conect_sistemas_vtv, $id_grupo);
        $descripcion=$datosgrupo[1][1];

        //Almacenistas responsables de las entradas y las salidas
        $almacenista_out=$pdf->ObjConsulta->selectpersonalresp($pdf->conect_sigesp, $user_reg);
        $nomalm_out=utf8_encode($almacenista_out[1][2]);
        $apelalm_out=utf8_encode($almacenista_out[1][3]);
        $almacenista_in=$pdf->ObjConsulta->selectpersonalresp($pdf->conect_sigesp, $user_exp);
        $respin=count($almacenista_in);
        if($respin==0){
            $nombalm_in='-';
            $apelalm_in='-';
        }else{
            $nombalm_in=utf8_encode($almacenista_in[1][2]);
            $apelalm_in=utf8_encode($almacenista_in[1][3]);
        }
        

        $materiales.='
        <tr nobr="true"><td align="center"><font size="8">&nbsp;' . $id_solicitud . ' </font></td>
        <td align="center"><font size="8">&nbsp;' . $fecha_prestamo . ' - ' . $hora_prestamo . '</font></td>
        <td align="center"><font size="8">&nbsp;' . $nomresp_out . ' ' . $apelresp_out . '</font></td>
        <td align="center"><font size="8">&nbsp;' . $nomalm_out . ' ' . $apelalm_out . '</font></td>
        <td align="center"><font size="8">&nbsp;' . $fecha_entrada . ' - ' . $hora_exp . '</font></td>
        <td align="center"><font size="8">' . $nomresp_in . ' ' . $apelresp_in . '</font></td>
        <td align="center"><font size="8">' . $nombalm_in. ' ' . $apelalm_in. '</font></td>
        <td align="left"><font size="8">' . $observacion . '</font></td>
        </tr>';
        
    } 
    
}

//$fecha_desde = $pdf->Objfechahora->flibInvertirInEs($fecha_desde);
//$fecha_hasta = $pdf->Objfechahora->flibInvertirInEs($fecha_hasta);

$columnas1='<tr nobr="true">
<th colspan="3"  ><div align="center"><font size="10"><b>Fecha desde: '. $fecha_desde.'</b></font></div></th>
<th colspan="3"  ><div align="center"><font size="10"><b>Fecha hasta: '. $fecha_hasta.'</b></font></div></th>
</tr>';
$columnas2='<tr nobr="true">
<th colspan="4" bgcolor="DarkGray" ><div align="center"><font size="10"><b>SALIDA</b></font></div></th>
<th colspan="4" bgcolor="DarkGray" ><div align="center"><font size="10"><b>ENTRADA</b></font></div></th>
</tr>';
$columnas3='<tr nobr="true">
<th><div align="center"><font size="9"><b>Solicitud</b></font></div></th>
<th><div align="center"><font size="9"><b>Fecha y Hora</b></font></div></th>
<th><div align="center"><font size="9"><b>Responsable</b></font></div></th>
<th><div align="center"><font size="9"><b>Almacenista que Entrega</b></font></div></th>
<th><div align="center"><font size="9"><b>Fecha y Hora</b></font></div></th>
<th><div align="center"><font size="9"><b>Responsable</b></font></div></th>
<th><div align="center"><font size="9"><b>Almacenista que Recibe</b></font></div></th>
<th><div align="center"><font size="9"><b>Observaciones</b></font></div></th>
</tr>';



/////////////////////////////////////////////////////////////////////////////////////////////////

$tituloppal = 'CAMARA';
$titulo = 'Control';
$body = '
<table align="center" border="1" >
        <tr nobr="true"><th colspan="6"  bgcolor="DarkGray"><font size="10" ><b>FECHAS DE CONSULTA</b></font></th></tr>
        ' . $columnas1 . '
</table>

<table align="center" border="0" >
        <tr nobr="true"><th colspan="7"></th></tr>
       
</table>

<table align="center" border="1" >
        <tr nobr="true"><th colspan="8"  bgcolor="DarkGray"><font size="12" ><b>' . $descripcion . '</b></font></th></tr>
        ' . $columnas2 . '
        ' . $columnas3 . '
        ' . $materiales . '
</table>

';
//echo $_GET['idasignacion'];
//$regmat = $pdf->ObjConsulta->select_equiposasignacion($pdf->conect_sistemas_vtv, $_GET['idasignacion']);
//$regmat = array_merge($regmat);
//$regmat = array_map(array_merge, $regmat);
//echo $body;

$pdf->Cell(10);
$pdf->writeHTML(utf8_encode($body), true, 0, true, 0);
$pdf->Output("Control_i_o_cam.pdf", 'I');
?>