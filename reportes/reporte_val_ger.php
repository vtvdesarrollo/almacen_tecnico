<?php

header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
ini_set('memory_limit', '9999999999999999999M');
require_once('tcpdf/tcpdf.php');

require_once("../class/bd/classbdConsultas.php");
require_once("../../../librerias/classlibFecHor.php");

class reporte extends TCPDF {

    public $conect_sistemas_vtv;
    public $ObjConsulta;
	public $Objfechahora;
    public $registros;
    
	
    public $almacenista;
    public $receptor;

    function __construct($orientation='P', $unit='mm', $format='A4', $unicode=true, $encoding='UTF-8', $diskcache=false) {
        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache);
        $this->conect_sistemas_vtv = "../../../database/archi_conex/sistemas_vtv_5431";
        $this->ObjConsulta = new classbdConsultas();
		$this->Objfechahora=new classlibFecHor();
    }

    function header() {

       // $this->registros = $this->ObjConsulta->select_data_asignacionplani($this->conect_sistemas_vtv, $_GET['idasignacion']);

        //fix array
      //  $this->registros = array_merge($this->registros);
        //$this->registros = array_map("array_merge", $this->registros);

        if($this->registros[0][1]==""){
            $this->registros[0][1]="NO EMPLEADO";
        }


        $this->SetFont('', '', 10);
        
        
        $this->almacenista=utf8_encode($this->registros[0][1]);
        $this->receptor=utf8_encode($this->registros[0][2]);
        
        $titulo1="SOLICITUD DE FACILIDADES TECNICAS";

        $htmltable = '<table border="0" width="650px" cellspacing="4">
	  <tr>
	  	
		<td width="160px" rowspan="3"><img src="imagenes/logo_vtv.jpg" style="width: 173px; height: 72px;" alt="logo"/></td>
		<!--<div align="center"><font size="10">FECHA: <b>' . date("d/m/Y H:i:s") . '</b></font></div>-->
		<td ><br /><br /><div align="center"><font size="10"><b>'.$titulo1.'</b></font></div></td>
		<!--<td width="180px"><div align="left"><font size="10">Nº. <b>' . str_pad($_GET['idasignacion'], 10, 0, STR_PAD_LEFT).'</b></font></div></td>-->
	 </tr>
	 </table>
          
';
		$fechaimp=date("d/m/Y H:i:s");

        // echo $htmltable;
        $this->writeHTML($htmltable);

        //$this->Image('../imagenes/bandera.jpg','', $this->GetY()-3, 168);
    }

    function footer() {
        $this->SetFont('', '', 6);
        $this->Ln(3);
        $this->Cell(0, 0, 'FECHA:'.date("d/m/Y H:i:s").'', 0, 0, 'L');
    }

    function renderizarimagetofile($url, $name, $path="imagenes/") {
        if (($f = fopen($url, 'r')) != false) {
            fclose($f);
            $res = join(file($url));
            if (($f = fopen($path . $name . ".png", "w")) != false) {
                fwrite($f, $res);
                fclose($f);
            }
        }
    }

}

//$pdf2=new MEM_IMAGE();
$pdf = new reporte(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, '', PDF_HEADER_STRING);//PDF_HEADER_TITLE
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, 'LISTADO CONSTANCIAS DE TRABAJO DEL '.$desde.' AL '.$hasta, PDF_HEADER_STRING);//PDF_HEADER_TITLE
// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(15, 38, 20);
$pdf->SetHeaderMargin(15);
$pdf->SetFooterMargin(20);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

$pdf->AddPage('P');
require_once('tcpdf/htmlcolors.php');
$pdf->Ln(2);

//print_r($pdf->registros);
$cedula=$_SESSION['cedula'];
$pauta=$_GET['id_pauta'];

			$idspauta=$pdf->ObjConsulta->selectidspauta($pdf->conect_sistemas_vtv, $pauta);
			$id_pauta=$idspauta[1][1];
			$id_tipo_pauta=$idspauta[1][2];
			$id_locacion=$idspauta[1][3];
			$id_tipo_traje=$idspauta[1][4];
			$id_program=$idspauta[1][5];
			$id_citacion=$idspauta[1][6];
			$id_montaje=$idspauta[1][7];
			$id_emision_grabacion=$idspauta[1][8];
			$id_retorno=$idspauta[1][9];
			$id_tipo_evento=$idspauta[1][10];
			$user_reg=$idspauta[1][11];	
			$lugar=$idspauta[1][12];	
			$descripcion=$idspauta[1][13];	
		
		$descprograma=$pdf->ObjConsulta-> selectdescprograma($pdf->conect_sistemas_vtv,$id_program);
		$idprograma=$descprograma[1][1];
		$descripcionprog=ucwords ($descprograma[1][2]);

		
		$desclocacion=$pdf->ObjConsulta-> selectdesclocacion($pdf->conect_sistemas_vtv,$id_locacion);
		$idlocacion=$desclocacion[1][1];
		$descripcionloc=ucwords ($desclocacion[1][2]);
		
		$desctipotraje=$pdf->ObjConsulta-> selectdesctipotraje($pdf->conect_sistemas_vtv,$id_tipo_traje);
		$idtipotraje=$desctipotraje[1][1];
		$descripciontraje=ucwords ($desctipotraje[1][2]);
		
		$descproductor=$pdf->ObjConsulta-> selectdescproductor($pdf->conect_sistemas_vtv,$user_reg);
		$descripcionprod=strtoupper ($descproductor[1][1]);
		
		$desctipoevento=$pdf->ObjConsulta-> selectdesctipoevento($pdf->conect_sistemas_vtv,$id_tipo_evento);
		$idtipoevento=$desctipoevento[1][1];
		$descripcionteven=ucwords ($desctipoevento[1][2]);
		//echo $descripcionteven;
		
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		
		$datoscitacion=$pdf->ObjConsulta-> selectcitacion($pdf->conect_sistemas_vtv,$id_citacion);
		$idcitacion=$datoscitacion[1][1];
		$fechacitacion=$datoscitacion[1][2];
		$horacitacion=$datoscitacion[1][3];
		$fechacitacion=$pdf->Objfechahora->flibInvertirInEs($fechacitacion);
		$descripcitacion=ucwords ($lugar);
		
		$datosmontaje=$pdf->ObjConsulta-> selectmontaje($pdf->conect_sistemas_vtv,$id_montaje);
		$idmontaje=$datosmontaje[1][1];
		$fechamontaje=$datosmontaje[1][2];
		$horamontaje=$datosmontaje[1][3];
		$fechamontaje=$pdf->Objfechahora->flibInvertirInEs($fechamontaje);
		$descripmontaje=ucwords ($lugar);
		
		$datosemision=$pdf->ObjConsulta-> selectemision($pdf->conect_sistemas_vtv,$id_emision_grabacion);
		$idemision=$datosemision[1][1];
		$fechaemision=$datosemision[1][2];
		$horaemision=$datosemision[1][3];
		$fechaemision=$pdf->Objfechahora->flibInvertirInEs($fechaemision);
		$descripemision=ucwords ($lugar);
		
		$datosretorno=$pdf->ObjConsulta-> selectretorno($pdf->conect_sistemas_vtv,$id_retorno);
		$idretorno=$datosretorno[1][1];
		$fecharetorno=$datosretorno[1][2];
		$horaretorno=$datosretorno[1][3];
		$fecharetorno=$pdf->Objfechahora->flibInvertirInEs($fecharetorno);
		$descripretorno=ucwords ($lugar);
		
		$estatuspauta=$pdf->ObjConsulta-> selectestatuspautas($pdf->conect_sistemas_vtv,$pauta);
		$estatus=$estatuspauta[1][1];
		
		if ($estatus=='9'){
		$obsrech=$pdf->ObjConsulta-> selectobsrech($pdf->conect_sistemas_vtv,$pauta);
		$observaciones=$obsrech[1][1];
		
		$obs="<table class='tabla'><tr><th>Observaciones:</th><td>".$observaciones."</td></tr></table>";
		}
		else {
		$obs="";
		}
		
		///////////////////////////////////////////////////////////////////////////////////////////////
		
		if ($descripretorno=='0'){
		$descripretorno="";
		}
		else
		{
		$descripretorno='
		<tr nobr="true" >
		<th align="left"><font size="10">&nbsp;Retorno:&nbsp;</font><font size="8">'.$descripretorno.'</font></th>
		<th align="left"><font size="10">&nbsp;&nbsp;Fecha:&nbsp;</font><font size="8">'.$fecharetorno.'</font></th>
		<th align="left"><font size="10">&nbsp;&nbsp;Hora:&nbsp;</font><font size="8">'.$horaretorno.'</font></th>
		</tr>
		';
	
		}
		
		if($id_program=='0')
		{$evento='<th align="left"><font size="10">&nbsp;&nbsp;Evento:</font></th><td align="left" width="485px"><font size="8">&nbsp;&nbsp;'.$descripcion.'</font></td>';}
		if($id_program >='1')
		{$evento='<th align="left"><font size="10">&nbsp;&nbsp;Programa:</font></th><td align="left" width="485px"><font size="8">&nbsp;&nbsp;'.$descripcionprog.'</font></td>';}
		
		
		
		$verificarestatuspauta=$pdf->ObjConsulta->verificarestatuspauta($pdf->conect_sistemas_vtv,$id_pauta);
		$estatusactual=$verificarestatuspauta[1][1];
		
		$descestatus=$pdf->ObjConsulta->selectdescestatus($pdf->conect_sistemas_vtv,$estatusactual);
		$descripestatus=ucwords ($descestatus[1][1]);
		
		////////////////////////////////////////////////////////////////////////////////////////////////////
		
			$id_estatus = 5;
			$aprobadopor=$pdf->ObjConsulta->selectquienaprobo($pdf->conect_sistemas_vtv,$id_pauta,$id_estatus);
			$aprobado=$aprobadopor[1][1];
			
			 /*echo"<script>var pagina='../paginas/classlistadereportesdevalidacion.php';						
				alert('Disculpe la pauta no ha sido validada.');
				function redireccionar() { 
				location.href=pagina;
				} 
				setTimeout ('redireccionar()', 0);
				</script>
				";*/
			
			if ($aprobado !=0){
			$desc_aprobado=$pdf->ObjConsulta->selectnombreyapellidof5($pdf->conect_sistemas_vtv,$aprobado);
			$nomb=strtoupper($desc_aprobado[1][1]);
			$apellido=strtoupper($desc_aprobado[1][2]);
			//$nombyapellido=ucwords($nombyapellido);
			
			$aprobadopor2=$pdf->ObjConsulta->selectcargoquienaprobo($pdf->conect_sistemas_vtv,$aprobado);
			$cargo=strtolower($aprobadopor2[1][1]);
			$cargo=ucwords($cargo);
			
			/////////////////////////////////////////////////////////////////////////////////////////////////
			$datoslista=$pdf->ObjConsulta->selectmostrarrecursosger($pdf->conect_sistemas_vtv,$pauta);
			$contador= count($datoslista);
			foreach($datoslista as $llave => $valor){
			$id=$valor[1];
			$cant=$valor[2];
			
			$datosmaterial=$pdf->ObjConsulta->selectlistmatdesc($pdf->conect_sistemas_vtv,$id);
			$desc_id_recurso=strtoupper($datosmaterial[1][1]);
			//$desc_id_recurso=ucwords (strtoupper ($desc_id_recurso));
			//$desc_id_recurso=utf8_encode($desc_id_recurso);


			$materiales2.='<tr nobr="true"><td align="left"><font size="8">'.$desc_id_recurso.'</font></td><td align="center"><font size="8">'.$cant.'</font></td></tr>';
			}

			$validadopor = '<table  align="center" border="0" >
							<tr nobr="true" ><td style="width: 250px;">Validado por:</td></tr>
							</table>	
							<br>
							<table  align="center" border="0" >
							<tr nobr="true" ><td align="center" >__________________</td></tr>
							<tr nobr="true" ><td align="center">'.$nomb.' '.$apellido.'</td></tr>
							<tr nobr="true" ><td align="center">C.I:'.$aprobado.'</td></tr>
							<tr nobr="true" ><td align="center">'.$cargo.'</td></tr>
							</table>';
			
			if($contador==0){
			$materiales2='<tr nobr="true"><td  align="center" colspan="3"><div align="center"><font size="8">La gerencia no ha validado ning&uacute;n recurso de esta pauta</font></div></td></tr>';
			}
		}
		else{

			$materiales2='<tr nobr="true"><td  align="center" colspan="3"><div align="center"><font size="8">La gerencia no ha validado ning&uacute;n recurso de esta pauta</font></div></td></tr>';
			$validadopor = ' ';
		}

		$titulo3="LISTA DE RECURSOS";
		$materiales='
					<table align="center" border="1">
					<tr nobr="true"><th colspan="2"  bgcolor="#8B0000"><font size="10" color="white"><b>'.$titulo3.'</b></font></th></tr>
					<tr nobr="true"><th><div align="center"><font size="10">Descripci&oacute;n</font></div></th><th><div align="center"><font size="10">Cantidad</font></div></th></tr>
					'.$materiales2.'
					</table>';
		
		$titulo='DATOS GENERALES';	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////			
$body = '
		<table align="center" border="1">
		<tr nobr="true"><th colspan="2"  bgcolor="#8B0000"><font size="10" color="white"><b>'.$titulo.'</b></font></th></tr>
		<tr nobr="true"><th align="left" width="100px"><font size="10">&nbsp;&nbsp;N&deg; de pauta:</font></th><td align="left" width="485px"><font size="8">&nbsp;&nbsp;'.$id_pauta.'</font></td></tr>
		<tr nobr="true">'.$evento.'</tr>
		<tr nobr="true"><th align="left"><font size="10">&nbsp;&nbsp;Productor:</font></th><td align="left" width="485px"><font size="8">&nbsp;&nbsp;'.$descripcionprod.'</font></td></tr>
		<tr nobr="true"><th align="left"><font size="10">&nbsp;&nbsp;Locaci&oacute;n:</font></th><td align="left" width="485px"><font size="8">&nbsp;&nbsp;'.$descripcionloc.'</font></td></tr>
		<tr nobr="true"><th align="left"><font size="10">&nbsp;&nbsp;Tipo evento:</font></th><td align="left" width="485px"><font size="8">&nbsp;&nbsp;'.$descripcionteven.'</font></td></tr>
		<tr nobr="true"><th align="left"><font size="10">&nbsp;&nbsp;Tipo traje:</font></th><td align="left" width="485px"><font size="8">&nbsp;&nbsp;'.$descripciontraje.'</font></td></tr>
		<tr nobr="true"><th align="left"><font size="10">&nbsp;&nbsp;Estatus:</font></th><td align="left" width="485px"><font size="8">&nbsp;&nbsp;'.$descripestatus.'</font></td></tr>

		</table>
		<table  align="center" border="1" >
		<tr nobr="true" >
		<th align="left"><font size="10">&nbsp;Citaci&oacute;n:&nbsp;</font><font size="8">'.$descripcitacion.'</font></th>
		<th align="left"><font size="10">&nbsp;&nbsp;Fecha:&nbsp;</font><font size="8">'.$fechacitacion.'</font></th>
		<th align="left"><font size="10">&nbsp;&nbsp;Hora:&nbsp;</font><font size="8">'.$horacitacion.'</font></th>
		</tr>
	
		<tr nobr="true" >
		<th align="left"><font size="10">&nbsp;Montaje:&nbsp;</font><font size="8">'.$descripmontaje.'</font></th>
		<th align="left"><font size="10">&nbsp;&nbsp;Fecha:&nbsp;</font><font size="8">'.$fechamontaje.'</font></th>
		<th align="left"><font size="10">&nbsp;&nbsp;Hora:&nbsp;</font><font size="8">'.$horamontaje.'</font></th>
		</tr>
		
		<tr nobr="true" >
		<th align="left"><font size="10">&nbsp;Emisi&oacute;n:&nbsp;</font><font size="8">'.$descripemision.'</font></th>
		<th align="left"><font size="10">&nbsp;&nbsp;Fecha:&nbsp;</font><font size="8">'.$fechaemision.'</font></th>
		<th align="left"><font size="10">&nbsp;&nbsp;Hora:&nbsp;</font><font size="8">'.$horaemision.'</font></th>
		</tr>
		'.$descripretorno.'
		
		</table>
	
		<table  align="center" border="0" ><tr nobr="true" ><td colspan="3"></td></tr></table>
		'.$materiales.'
		<br>
		<br>
		<br>
		<br>
		'.$validadopor.'	
';

//echo $body;

$pdf->Cell(10);
$pdf->writeHTML(utf8_encode($body), true, 0, true, 0);
$pdf->Output("Reporte_de_aprobacion_pauta'.$id_pauta.'.pdf", 'I');
?>