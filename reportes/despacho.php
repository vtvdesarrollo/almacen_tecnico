<?php

header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
ini_set('memory_limit', '9999999999999999999M');
require_once('tcpdf/tcpdf.php');


require_once("../class/bd/classbdConsultas.php");
require_once("../../../librerias/classlibFecHor.php");

class reporte extends TCPDF {

    public $conect_sistemas_vtv;
    public $ObjConsulta;
    public $Objfechahora;
    public $registros;
    public $almacenista;
    public $receptor;

    function __construct($orientation='P', $unit='mm', $format='A4', $unicode=true, $encoding='UTF-8', $diskcache=false) {
        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache);
        $this->conect_sistemas_vtv = "../../../database/archi_conex/sistemas_vtv_5431";
        $this->conect_sigesp = "../../../database/archi_conex/sistema_sigesp";
        $this->ObjConsulta = new classbdConsultas();
        $this->Objfechahora = new classlibFecHor(); 
    }

    function header() {

        // $this->registros = $this->ObjConsulta->select_data_asignacionplani($this->conect_sistemas_vtv, $_GET['idasignacion']);
        //fix array
        //  $this->registros = array_merge($this->registros);
        //$this->registros = array_map("array_merge", $this->registros);

        if ($this->registros[0][1] == "") {
            $this->registros[0][1] = "NO EMPLEADO";
        }


        $this->SetFont('', '', 10);


        $this->almacenista = utf8_encode($this->registros[0][1]);
        $this->receptor = utf8_encode($this->registros[0][2]);

        $titulo1 = "SOLICITUD DE DESPACHO";

        $htmltable = '<table border="0" width="650px" cellspacing="4">
	  <tr>
	  	
		<td width="160px" rowspan="3"><img src="imagenes/logo_vtv.jpg" style="width: 173px; height: 72px;" alt="logo"/></td>
		<div align="center"><font size="10">FECHA: <b>' . date("d/m/Y H:i:s") . '</b></font></div>
		<td ><br /><br /><div align="center"><font size="10"><b>' . $titulo1 . '</b></font></div></td>
		<!--<td width="180px"><div align="left"><font size="10">Nº. <b>' . str_pad($_GET['idasignacion'], 10, 0, STR_PAD_LEFT) . '</b></font></div></td>-->
	 </tr>
	 </table>
          
';
        $fechaimp = date("d/m/Y H:i:s");

        // echo $htmltable;
        $this->writeHTML($htmltable);

        //$this->Image('../imagenes/bandera.jpg','', $this->GetY()-3, 168);
    }

    function footer() {
        $this->SetFont('', '', 6);
        $this->Ln(3);
        $this->Cell(0, 0, 'FECHA:' . date("d/m/Y H:i:s") . '', 0, 0, 'L');
    }

    function renderizarimagetofile($url, $name, $path="imagenes/") {
        if (($f = fopen($url, 'r')) != false) {
            fclose($f);
            $res = join(file($url));
            if (($f = fopen($path . $name . ".png", "w")) != false) {
                fwrite($f, $res);
                fclose($f);
            }
        }
    }

}

//$pdf2=new MEM_IMAGE();
$pdf = new reporte(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, '', PDF_HEADER_STRING);//PDF_HEADER_TITLE
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, 'LISTADO CONSTANCIAS DE TRABAJO DEL '.$desde.' AL '.$hasta, PDF_HEADER_STRING);//PDF_HEADER_TITLE
// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(15, 38, 20);
$pdf->SetHeaderMargin(15);
$pdf->SetFooterMargin(20);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

$pdf->AddPage('P');
require_once('tcpdf/htmlcolors.php');
$pdf->Ln(2);



$id_solicitud = $_GET['id_solicitud'];



$datossolicitud = $pdf->ObjConsulta->selectdespacho($pdf->conect_sistemas_vtv, $id_solicitud);
$id_destino=$datossolicitud[1][1];
$id_desc_dest=$datossolicitud[1][2];
$resp_despacho=$datossolicitud[1][3];
$user_reg=$datossolicitud[1][4];
$fecha_reg = $datossolicitud[1][5];
$fecha_reg = $pdf->Objfechahora->flibInvertirInEs($fecha_reg);
$fecha_sol=$datossolicitud[1][6];
$fecha_sol = $pdf->Objfechahora->flibInvertirInEs($fecha_sol);
$observacion=strtoupper($datossolicitud[1][7]);

$responsableg=$pdf->ObjConsulta->selectpersonalresp($pdf->conect_sigesp, $user_reg);
$nombres1=$responsableg[1][2];
$apellidos1=$responsableg[1][3];
$cargo1=$responsableg[1][4];

$fecha = '<th align="left" width="150px" bgcolor="DarkGray" ><font size="10" >&nbsp;<b>Fecha:</b></font></th><td align="left" width="435px" ><font size="8">&nbsp;&nbsp;' . $fecha_sol . '</font></td>';

 
if($id_destino==1){//es para una gerencia
    $tipo_destino="Gerencia:";
    $datosgerencia=$pdf->ObjConsulta->selectgerenciadesc($pdf->conect_sigesp, $id_desc_dest);
    $desc_destino=$datosgerencia[1][2];
    $destino = '<th align="left"  width="150px" bgcolor="DarkGray" ><font size="10">&nbsp;&nbsp;<b>'.$tipo_destino.'</b></font></th><td align="left" width="435px" ><font size="8">&nbsp;&nbsp;' . $desc_destino . '</font></td>';
}elseif($id_destino==2){//es un programa
    $tipo_destino=" Programa:";
    $datosdestino = $pdf->ObjConsulta->selectprograma($pdf->conect_sistemas_vtv, $id_desc_dest);
    $desc_destino=$datosdestino[1][2];
    $destino = '<th align="left" width="150px" bgcolor="DarkGray" ><font size="10" >&nbsp;<b>'.$tipo_destino.'</b></font></th><td align="left" width="435px" ><font size="8">&nbsp;&nbsp;' . $desc_destino . '</font></td>';

}elseif($id_destino==3){//es un remoto (ofifial o programa)
    if(is_numeric ($id_desc_dest)==true){
        $tipo_destino=" Programa:";
        $datosdestino = $pdf->ObjConsulta->selectprograma($pdf->conect_sistemas_vtv, $id_desc_dest);
        $desc_destino=$datosdestino[1][2];
        $destino = '<th align="left" width="150px" bgcolor="DarkGray" ><font size="10" >&nbsp;<b>'.$tipo_destino.'</b></font></th><td align="left" width="435px" ><font size="8">&nbsp;&nbsp;' . $desc_destino . '</font></td>';
    }else{
        $destino = '<th align="left" width="150px" bgcolor="DarkGray" ><font size="10" >&nbsp;<b> Remoto:</b></font></th><td align="left" width="435px" ><font size="8">&nbsp;&nbsp;' . strtoupper($id_desc_dest) . '</font></td>';
    }
}else{//es un remoto 
    
    $destino = '<th align="left" width="150px" bgcolor="DarkGray" ><font size="10" >&nbsp;<b> Remoto:</b></font></th><td align="left" width="435px" ><font size="8">&nbsp;&nbsp;' . strtoupper($id_desc_dest) . '</font></td>';

}

$responsable=$pdf->ObjConsulta->selectpersonalresp($pdf->conect_sigesp, $resp_despacho);
$nombres=$responsable[1][2];
$apellidos=$responsable[1][3];
$cargo=$responsable[1][4];

$responsablesol = '<th align="left" bgcolor="DarkGray"><font size="10">&nbsp;&nbsp;<b>Responsable:</b></font></th><td align="left" ><font size="8">&nbsp;&nbsp;' . $nombres . '&nbsp;&nbsp;' . $apellidos . '</font></td>';

if ($observacion==''){
    $observaciones = '<th align="left" bgcolor="DarkGray"><font size="10">&nbsp;&nbsp;<b>Observaciones:</b></font></th><td align="left" ><font size="8">&nbsp;&nbsp;Sin Observaciones</font></td>';

}else{
    $observaciones = '<th align="left" bgcolor="DarkGray"><font size="10">&nbsp;&nbsp;<b>Observaciones:</b></font></th><td align="left" ><font size="8">&nbsp;&nbsp;'.$observacion.'</font></td>';

}

$datosmateriales = $pdf->ObjConsulta->selectarticulodespachados($pdf->conect_sistemas_vtv, $id_solicitud);
$contador= (count($datosmateriales));
if($contador==0){
    $materiales ='<tr nobr="true"><th colspan="4"  color="red"><font size="10" ><b>No existen materiales</b></font></th></tr>';
}else{
    foreach ($datosmateriales as $llave => $valor) {
        $datosequipopres = $pdf->ObjConsulta->selectdatosequipo($pdf->conect_sistemas_vtv, $valor[1]);
        $id_articulo=$datosequipopres[1][1];
        $tipo_articulo=$datosequipopres[1][2];
        $descripcion=$datosequipopres[1][3];
        $marca=$datosequipopres[1][4];
        $modelo=$datosequipopres[1][5];
        $bien_nac=$datosequipopres[1][6];
        $serial=$datosequipopres[1][7];
        //$cantidad=$datosequipopres[1][12];
        $costo=$datosequipopres[1][13];

            $marca="Marca: " . $marca . "";

            if($modelo==""){
                $modelo=="";
            }else{
                $modelo="Modelo: " . $modelo . "";
            }

            if($bien_nac==""){
                $bien_nac=='';
            }else{
                $bien_nac="Bien Nac.: " . $bien_nac . "";
            }

            if($serial==""){
                $serial=="";
            }else{
                $serial="Serial: " . $serial . "";
            }


            $desc_detalle= "" . $modelo . "<br/>" . $marca . "<br/>" . $bien_nac . "<br/>" . $serial . "";

        $id_solicitud = $_GET['id_solicitud'];
        $datosmateriales = $pdf->ObjConsulta->selectarticulodesp($pdf->conect_sistemas_vtv, $id_articulo, $id_solicitud);
        $cantidad=$datosmateriales[1][2];

        $costomat = $costo * $cantidad;
        $total=$total+$costomat;

        $materiales.='<tr nobr="true"><td align="center"><font size="8">' . $cantidad . '</font></td><td align="center"><font size="8">' . $descripcion . '</font></td><td align="left"><font size="8">' . $desc_detalle . '</font></td><td align="center"><font size="8">' . $costo . 'Bs.</font></td></tr>';

    }  
    //$materiales='<tr nobr="true"><td align="left"><font size="8">' . $descripcion . '</font></td><td align="left"><font size="8">' . $desc_detalle . '</font></td></tr>';
}
$costototal = '<th align="right" colspan="3"><font size="10" >&nbsp;<b>Total:&nbsp;&nbsp;&nbsp;</b></font></th><td align="center" ><font size="8">&nbsp;&nbsp;' . $total . 'bs.</font></td>';



$resp= '<td align="center" >_____________________<br/>
        Responsable del despacho<br/>
        ' . $nombres . '&nbsp;' . $apellidos . '<br/>
        ' . $cargo . '<br/>
        C.I:' . $resp_despacho . '<br/>
        </td>';


$entr= '<td align="center" >_____________________<br/>
        Responsable de la entrega<br/>
        ' . $nombres1 . '&nbsp;' . $apellidos1 . '<br/>
        ' . $cargo1 . '<br/>
        C.I:' . $user_reg . '<br/>
        </td>';


/////////////////////////////////////////////////////////////////////////////////////////////////

$titulo = 'DATOS GENERALES';
$titulo2= 'MATERIALES SOLICITADOS';
$body = '
<table align="center" border="1">
		<tr nobr="true"><th colspan="2"  bgcolor="DarkGray"><font size="10" ><b>' . $titulo . '</b></font></th></tr>
        <tr nobr="true"><th align="left" width="150px" bgcolor="DarkGray"><font size="10">&nbsp;&nbsp;<b>N&deg; de solicitud:</b></font></th><td align="left" width="435px"><font size="8">&nbsp;&nbsp;' . $id_solicitud . '</font></td></tr>
        <tr nobr="true">' . $fecha . '</tr>
        <tr nobr="true">' . $destino . '</tr>
        <tr nobr="true">' . $responsablesol . '</tr>
        <tr nobr="true">' . $observaciones . '</tr>

</table>
<table align="center" border="0">
        <tr nobr="true" ><th colspan="2"  ></th></tr>
</table>
<table align="center" border="1">
        <tr nobr="true"><th colspan="4"  bgcolor="DarkGray"><font size="10"><b>' . $titulo2 . '</b></font></th></tr>
        <tr nobr="true"><th><div align="center"><font size="10">Cantidad</font></div></th><th><div align="center"><font size="10">Descripci&oacute;n</font></div></th><th><div align="center"><font size="10">Detalle</font></div></th><th><div align="center"><font size="10">Costo</font></div></th></tr>
        ' . $materiales . '
        <tr nobr="true">'.$costototal.'</tr>
</table>
<br />
<br />
<br />
<br />
<table  align="center" border="0" >
        <tr nobr="true" >'.$resp.' '.$entr.'</tr>
</table>   


';
//echo $_GET['idasignacion'];
//$regmat = $pdf->ObjConsulta->select_equiposasignacion($pdf->conect_sistemas_vtv, $_GET['idasignacion']);
//$regmat = array_merge($regmat);
//$regmat = array_map(array_merge, $regmat);
//echo $body;

$pdf->Cell(10);
$pdf->writeHTML(utf8_encode($body), true, 0, true, 0);
$pdf->Output("despacho_".$id_solicitud.".pdf", 'I');
?>