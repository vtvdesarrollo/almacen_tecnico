<?php

header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
ini_set('memory_limit', '9999999999999999999M');
require_once('tcpdf/tcpdf.php');


require_once("../class/bd/classbdConsultas.php");
require_once("../../../librerias/classlibFecHor.php");

class reporte extends TCPDF {

    public $conect_sistemas_vtv;
    public $ObjConsulta;
    public $Objfechahora;
    public $registros;
    public $almacenista;
    public $receptor;

    function __construct($orientation='P', $unit='mm', $format='A4', $unicode=true, $encoding='UTF-8', $diskcache=false) {
        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache);
        $this->conect_sistemas_vtv = "../../../database/archi_conex/sistemas_vtv_5431";
        $this->conect_sigesp = "../../../database/archi_conex/sistema_sigesp";
        $this->ObjConsulta = new classbdConsultas();
        $this->Objfechahora = new classlibFecHor(); 
    }

    function header() {

        // $this->registros = $this->ObjConsulta->select_data_asignacionplani($this->conect_sistemas_vtv, $_GET['idasignacion']);
        //fix array
        //  $this->registros = array_merge($this->registros);
        //$this->registros = array_map("array_merge", $this->registros);

        if ($this->registros[0][1] == "") {
            $this->registros[0][1] = "NO EMPLEADO";
        }


        $this->SetFont('', '', 10);


        $this->almacenista = utf8_encode($this->registros[0][1]);
        $this->receptor = utf8_encode($this->registros[0][2]);

        $titulo1 = "CLASIFICACI&Oacute;N DE VESTUARIO";

        $htmltable = '<table border="0" width="650px" cellspacing="4">
	  <tr>
	  	
		<td width="160px" rowspan="3"><img src="imagenes/logo_vtv.jpg" style="width: 173px; height: 72px;" alt="logo"/></td>
		<div align="center"><font size="10">FECHA: <b>' . date("d/m/Y H:i:s") . '</b></font></div>
		<td ><br /><br /><div align="center"><font size="10"><b>' . $titulo1 . '</b></font></div></td>
		<!--<td width="180px"><div align="left"><font size="10">Nº. <b>' . str_pad($_GET['idasignacion'], 10, 0, STR_PAD_LEFT) . '</b></font></div></td>-->
	 </tr>
	 </table>
          
';
        $fechaimp = date("d/m/Y H:i:s");

        // echo $htmltable;
        $this->writeHTML($htmltable);

        //$this->Image('../imagenes/bandera.jpg','', $this->GetY()-3, 168);
    }

    function footer() {
        $this->SetFont('', '', 6);
        $this->Ln(3);
        $this->Cell(0, 0, 'FECHA:' . date("d/m/Y H:i:s") . '', 0, 0, 'L');
    }

    function renderizarimagetofile($url, $name, $path="imagenes/") {
        if (($f = fopen($url, 'r')) != false) {
            fclose($f);
            $res = join(file($url));
            if (($f = fopen($path . $name . ".png", "w")) != false) {
                fwrite($f, $res);
                fclose($f);
            }
        }
    }

}

//$pdf2=new MEM_IMAGE();
$pdf = new reporte('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, '', PDF_HEADER_STRING);//PDF_HEADER_TITLE
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, 'LISTADO CONSTANCIAS DE TRABAJO DEL '.$desde.' AL '.$hasta, PDF_HEADER_STRING);//PDF_HEADER_TITLE
// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(15, 38, 20);
$pdf->SetHeaderMargin(15);
$pdf->SetFooterMargin(20);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

$pdf->AddPage('L');
require_once('tcpdf/htmlcolors.php');
$pdf->Ln(2);

/////////////////////////////vestuario femenino///////////////////////////////////////////////////
$tipo_vestuario = $pdf->ObjConsulta->vestuario($pdf->conect_sistemas_vtv);
foreach ($tipo_vestuario as $llave => $valor) {
    $id_vestuario_ini= $valor[1];
    $tipo_vestuarios = $pdf->ObjConsulta->descvestuario($pdf->conect_sistemas_vtv, $id_vestuario_ini);
    $desc_vest=$tipo_vestuarios[1][2];
        

    $vestuarios = $pdf->ObjConsulta->vestuarios($pdf->conect_sistemas_vtv, $id_vestuario_ini);
    $contvestuarios=count($vestuarios);
    if ($contvestuarios=='0'){
        $vestfem.='';

    }else{
        $id_vestuario_ex=$vestuarios[1][1];
        $cantidadex=$vestuarios[1][2];
        if ($cantidadex==''){
            $cantidadex=0;
        }   

        $vestuariosprest = $pdf->ObjConsulta->vestuariosprest($pdf->conect_sistemas_vtv, $id_vestuario_ini);
        $id_vestuario_prest=$vestuariosprest[1][1];
        $cantidadprest=$vestuariosprest[1][2];
        if ($cantidadprest==''){
            $cantidadprest=0;
        } 

        $vestuariosdisp = $pdf->ObjConsulta->vestuariosdisp($pdf->conect_sistemas_vtv, $id_vestuario_ini);
        $id_vestuario_disp=$vestuariosdisp[1][1];
        $cantidaddisp=$vestuariosdisp[1][2];
        if ($cantidaddisp==''){
            $cantidaddisp=0;
        } 

        $vestfem.='<tr nobr="true"><td align="center"><font size="8">' . $desc_vest . '</font></td>
        <td align="center"><font size="8">' .$cantidadex. '</font></td>
        <td align="center"><font size="8">' .$cantidadprest. '</font></td>
        <td align="center"><font size="8">' .$cantidaddisp. '</font></td></tr>';  
    }

}

////////////////////////////vestuario masculino///////////////////////////////////////////
$tipo_vestuario = $pdf->ObjConsulta->vestuario($pdf->conect_sistemas_vtv);
foreach ($tipo_vestuario as $llave => $valor) {
    $id_vestuario_ini= $valor[1];
    $tipo_vestuarios = $pdf->ObjConsulta->descvestuario($pdf->conect_sistemas_vtv, $id_vestuario_ini);
    $desc_vest=$tipo_vestuarios[1][2];
        

    $vestuarios = $pdf->ObjConsulta->vestuariosmas($pdf->conect_sistemas_vtv, $id_vestuario_ini);
    $contvestuarios=count($vestuarios);
    if ($contvestuarios=='0'){
        $vestmas.='';

    }else{
        $id_vestuario_ex=$vestuarios[1][1];
        $cantidadex=$vestuarios[1][2];
        if ($cantidadex==''){
            $cantidadex=0;
        }   

        $vestuariosprest = $pdf->ObjConsulta->vestuariosprestmas($pdf->conect_sistemas_vtv, $id_vestuario_ini);
        $id_vestuario_prest=$vestuariosprest[1][1];
        $cantidadprest=$vestuariosprest[1][2];
        if ($cantidadprest==''){
            $cantidadprest=0;
        } 

        $vestuariosdisp = $pdf->ObjConsulta->vestuariosdispmas($pdf->conect_sistemas_vtv, $id_vestuario_ini);
        $id_vestuario_disp=$vestuariosdisp[1][1];
        $cantidaddisp=$vestuariosdisp[1][2];
        if ($cantidaddisp==''){
            $cantidaddisp=0;
        } 

        $vestmas.='<tr nobr="true"><td align="center"><font size="8">' . $desc_vest . '</font></td>
        <td align="center"><font size="8">' .$cantidadex. '</font></td>
        <td align="center"><font size="8">' .$cantidadprest. '</font></td>
        <td align="center"><font size="8">' .$cantidaddisp. '</font></td></tr>';  
    }

}

$tipo_vestuario = $pdf->ObjConsulta->vestuario($pdf->conect_sistemas_vtv);
foreach ($tipo_vestuario as $llave => $valor) {
    $id_vestuario_ini= $valor[1];
    $tipo_vestuarios = $pdf->ObjConsulta->descvestuario($pdf->conect_sistemas_vtv, $id_vestuario_ini);
    $desc_vest=$tipo_vestuarios[1][2];
        

    $vestuarios = $pdf->ObjConsulta->vestuariosunisex($pdf->conect_sistemas_vtv, $id_vestuario_ini);
    $contvestuarios=count($vestuarios);
    if ($contvestuarios=='0'){
        $vestunisex.='';

    }else{
        $id_vestuario_ex=$vestuarios[1][1];
        $cantidadex=$vestuarios[1][2];
        if ($cantidadex==''){
            $cantidadex=0;
        }   

        $vestuariosprest = $pdf->ObjConsulta->vestuariosprestunisex($pdf->conect_sistemas_vtv, $id_vestuario_ini);
        $id_vestuario_prest=$vestuariosprest[1][1];
        $cantidadprest=$vestuariosprest[1][2];
        if ($cantidadprest==''){
            $cantidadprest=0;
        } 

        $vestuariosdisp = $pdf->ObjConsulta->vestuariosdispunisex($pdf->conect_sistemas_vtv, $id_vestuario_ini);
        $id_vestuario_disp=$vestuariosdisp[1][1];
        $cantidaddisp=$vestuariosdisp[1][2];
        if ($cantidaddisp==''){
            $cantidaddisp=0;
        } 

        $vestunisex.='<tr nobr="true"><td align="center"><font size="8">' . $desc_vest . '</font></td>
        <td align="center"><font size="8">' .$cantidadex. '</font></td>
        <td align="center"><font size="8">' .$cantidadprest. '</font></td>
        <td align="center"><font size="8">' .$cantidaddisp. '</font></td></tr>';  
    }

}

    
if($vestfem== ''){
   $vestfem='<tr nobr="true"><th colspan="4" ><font size="10" color="red"><b>Art&iacute;culos Femeninos No Registrados</b></font></th></tr>';
}
if($vestmas== ''){
        $vestmas='<tr nobr="true"><th colspan="4" ><font size="10" color="red"><b>Art&iacute;culos Masculino No Registrados</b></font></th></tr>';
    }
if($vestunisex== ''){
        $vestunisex='<tr nobr="true"><th colspan="4" ><font size="10" color="red"><b>Art&iacute;culos Unisex No Registrados</b></font></th></tr>';
}
/*$tipo_vestuario = $pdf->ObjConsulta->vestuario($pdf->conect_sistemas_vtv);
foreach ($tipo_vestuario as $llave => $valor) {
    $id_vestuario_ini= $valor[1];

    $vestuarios = $pdf->ObjConsulta->vestuarios($pdf->conect_sistemas_vtv, $id_vestuario_ini);
    $contvestuarios=count($vestuarios);
    if ($contvestuarios=='0'){
        $vestfem.='';

    }else{
        $id_vestuario_ex=$vestuarios[1][1];
        $cantidadex=$vestuarios[1][2];

        $tipo_vestuarios = $pdf->ObjConsulta->descvestuario($pdf->conect_sistemas_vtv, $id_vestuario_ex);
        $desc_vest_ex=$tipo_vestuarios[1][2];
        $vestfem.='<tr nobr="true"><td align="center"><font size="8">' . $desc_vest_ex . '</font></td><td align="center"><font size="8">' .$cantidadex. '</font></td></tr>';
    }
   
    $vestuariosprest = $pdf->ObjConsulta->vestuariosprest($pdf->conect_sistemas_vtv, $id_vestuario_ini);
    $contvestuariosprest=count($vestuariosprest);
    if ($contvestuariosprest=='0'){
        $vestfemprest.='';

    }else{
        $id_vestuario_prest=$vestuariosprest[1][1];
        $cantidadprest=$vestuariosprest[1][2];

        $tipo_vestuarios = $pdf->ObjConsulta->descvestuario($pdf->conect_sistemas_vtv, $id_vestuario_prest);
        $desc_vest_pres=$tipo_vestuarios[1][2];

        $vestfemprest.='<tr nobr="true"><td align="center"><font size="8">' . $desc_vest_pres . '</font></td><td align="center"><font size="8">' .$cantidadprest. '</font></td></tr>';
    }

    $vestuariosdisp = $pdf->ObjConsulta->vestuariosdisp($pdf->conect_sistemas_vtv, $id_vestuario_ini);
    $contvestuariosdisp=count($vestuariosdisp);
    if ($contvestuariosdisp=='0'){
        $vestfemdisp.='';

    }else{
        $id_vestuario_disp=$vestuariosdisp[1][1];
        $cantidaddisp=$vestuariosdisp[1][2];

        $tipo_vestuarios = $pdf->ObjConsulta->descvestuario($pdf->conect_sistemas_vtv, $id_vestuario_disp);
        $desc_vest_disp=$tipo_vestuarios[1][2];

        $vestfemdisp.='<tr nobr="true"><td align="center"><font size="8">' . $desc_vest_disp . '</font></td><td align="center"><font size="8">' .$cantidaddisp. '</font></td></tr>';
    } 


}*/
/*
$tipo_vestuario = $pdf->ObjConsulta->vestuario($pdf->conect_sistemas_vtv);
foreach ($tipo_vestuario as $llave => $valor) {
    $id_vestuario_ini= $valor[1];

    $vestuarios = $pdf->ObjConsulta->vestuarios($pdf->conect_sistemas_vtv, $id_vestuario_ini);
        foreach ($vestuarios as $llave2 => $valor2) {
            $id_vestuario_ex=$valor2[1];
            $cantidad=$valor2[2];

            $tipo_vestuario = $pdf->ObjConsulta->descvestuario($pdf->conect_sistemas_vtv, $id_vestuario_ex);
            $desc_vest=$tipo_vestuario[1][2];

            $vestfem.='<tr nobr="true"><td align="center"><font size="8">' . $desc_vest . '</font></td><td align="center"><font size="8">' .$cantidad. '</font></td></tr>';
        }
    $vestuariosprest = $pdf->ObjConsulta->vestuariosprest($pdf->conect_sistemas_vtv, $id_vestuario_ini);
        foreach ($vestuariosprest as $llave3 => $valor3) {
            $id_vestuario_prest=$valor3[1];
            $cantidad=$valor3[2];

            $tipo_vestuario = $pdf->ObjConsulta->descvestuario($pdf->conect_sistemas_vtv, $id_vestuario_prest);
            $desc_vest=$tipo_vestuario[1][2];

            $vestfemprest.='<tr nobr="true"><td align="center"><font size="8">' . $desc_vest . '</font></td><td align="center"><font size="8">' .$cantidad. '</font></td></tr>';
        } 

    $vestuariosdisp = $pdf->ObjConsulta->vestuariosdisp($pdf->conect_sistemas_vtv, $id_vestuario_ini);
        foreach ($vestuariosdisp as $llave4 => $valor4) {
            $id_vestuario_disp=$valor4[1];
            $cantidad=$valor4[2];

            $tipo_vestuario = $pdf->ObjConsulta->descvestuario($pdf->conect_sistemas_vtv, $id_vestuario_disp);
            $desc_vest=$tipo_vestuario[1][2];

            $vestfemdisp.='<tr nobr="true"><td align="center"><font size="8">' . $desc_vest . '</font></td><td align="center"><font size="8">' .$cantidad. '</font></td></tr>';
        }        
}*/

/*
$datosmateriales = $pdf->ObjConsulta->selectingreso_maquillaje($pdf->conect_sistemas_vtv, $fechadesde, $fechahasta);
$contador= (count($datosmateriales));
if($contador==0){
    $materiales ='<tr nobr="true"><th colspan="8"  color="red"><font size="10" ><b>No se encuentran ingresos registrados</b></font></th></tr>';
}else{
    foreach ($datosmateriales as $llave => $valor) {
        $id_ingreso= $valor[1];
        $descripcion= $valor[2];
        $marca= $valor[3];
        $color= $valor[4];
        $costo= $valor[5];
        $cantidad= $valor[6];
        $fecha_reg= $valor[7];
        $fecha_reg = $pdf->Objfechahora->flibInvertirInEs($fecha_reg);
        $user_reg= $valor[8];
        $desc_unidad_cant= $valor[9];

        $responsable=$pdf->ObjConsulta->selectpersonalresp($pdf->conect_sigesp, $user_reg);
        $nombres=$responsable[1][2];
        $apellidos=$responsable[1][3];
        $cargo=$responsable[1][4];
        
        $materiales.='
        <tr nobr="true"><td align="center"><font size="8">' . $id_ingreso . '</font></td>
        <td align="left"><font size="8">' . $descripcion . '</font></td>
        <td align="left"><font size="8">' . $marca . '</font></td>
        <td align="left"><font size="8">' . $color . '</font></td>
        <td align="center"><font size="8">' . $cantidad . ' ' . $desc_unidad_cant . '</font></td>
        <td align="center"><font size="8">' . $costo . '</font></td>
        <td align="center"><font size="8">' . $fecha_reg . '</font></td>
        <td align="center"><font size="8">' . $nombres . ' ' . $apellidos . '</font></td>
        '.$tipo.'
        </tr>';
    } 


}

$columnas='<tr nobr="true">
<th><div align="center"><font size="8"><b>N de Ingreso</b></font></div></th>
<th><div align="center"><font size="8"><b>Descripci&oacute;n</b></font></div></th>
<th><div align="center"><font size="8"><b>Marca</b></font></div></th>
<th><div align="center"><font size="8"><b>Color</b></font></div></th>
<th><div align="center"><font size="8"><b>Cantidad</b></font></div></th>
<th><div align="center"><font size="8"><b>Costo</b></font></div></th>
<th><div align="center"><font size="8"><b>Fecha de ingreso</b></font></div></th>
<th><div align="center"><font size="8"><b>Responsable</b></font></div></th>
</tr>'; 


$body = '
<table align="center" border="1">
        <tr nobr="true"><th colspan="2"  bgcolor="DarkGray"><font size="10" ><b>Art&iacute;culos Femeninos en Existencia</b></font></th></tr>
        <tr nobr="true"><th><div align="center"><font size="10"><b>Descripci&oacute;n</b></font></div></th><th><div align="center"><font size="10"><b>Cantidad</b></font></div></th></tr>
        ' . $vestfem . '
</table>
<table align="center" border="0">
        <tr nobr="true"><th></th></tr>
</table>
<table align="center" border="1">
        <tr nobr="true"><th colspan="2"  bgcolor="DarkGray"><font size="10" ><b>Art&iacute;culos Femeninos Disponibles</b></font></th></tr>
        <tr nobr="true"><th><div align="center"><font size="10"><b>Descripci&oacute;n</b></font></div></th><th><div align="center"><font size="10"><b>Cantidad</b></font></div></th></tr>
        ' . $vestfemdisp . '
</table>
<table align="center" border="0">
        <tr nobr="true"><th></th></tr>
</table>
<table align="center" border="1">
        <tr nobr="true"><th colspan="2"  bgcolor="DarkGray"><font size="10" ><b>Art&iacute;culos Femeninos Prestados</b></font></th></tr>
        <tr nobr="true"><th><div align="center"><font size="10"><b>Descripci&oacute;n</b></font></div></th><th><div align="center"><font size="10"><b>Cantidad</b></font></div></th></tr>
        ' . $vestfemprest . '
</table>


';
*/

/////////////////////////////////////////////////////////////////////////////////////////////////


$body = '
<table align="center" border="1">
		<tr nobr="true"><th colspan="4"  bgcolor="DarkGray"><font size="10" ><b>Art&iacute;culos Femeninos </b></font></th></tr>
        <tr nobr="true"><th><div align="center"><font size="10"><b>Descripci&oacute;n</b></font></div></th><th><div align="center"><font size="10"><b>Cantidad Total</b></font></div></th><th><div align="center"><font size="10"><b>Cantidad Prestada</b></font></div></th><th><div align="center"><font size="10"><b>Cantidad Disponible</b></font></div></th></tr>
        ' . $vestfem . '
</table>
<table align="center" border="0">
        <tr nobr="true"><th></th></tr>
</table>
<table align="center" border="1">
        <tr nobr="true"><th colspan="4"  bgcolor="DarkGray"><font size="10" ><b>Art&iacute;culos Masculinos </b></font></th></tr>
        <tr nobr="true"><th><div align="center"><font size="10"><b>Descripci&oacute;n</b></font></div></th><th><div align="center"><font size="10"><b>Cantidad Total</b></font></div></th><th><div align="center"><font size="10"><b>Cantidad Prestada</b></font></div></th><th><div align="center"><font size="10"><b>Cantidad Disponible</b></font></div></th></tr>
        ' . $vestmas . '
</table>
<table align="center" border="0">
        <tr nobr="true"><th></th></tr>
</table>
<table align="center" border="1">
        <tr nobr="true"><th colspan="4"  bgcolor="DarkGray"><font size="10" ><b>Art&iacute;culos Unisex </b></font></th></tr>
        <tr nobr="true"><th><div align="center"><font size="10"><b>Descripci&oacute;n</b></font></div></th><th><div align="center"><font size="10"><b>Cantidad Total</b></font></div></th><th><div align="center"><font size="10"><b>Cantidad Prestada</b></font></div></th><th><div align="center"><font size="10"><b>Cantidad Disponible</b></font></div></th></tr>
        ' . $vestunisex . '
</table>

';
//echo $_GET['idasignacion'];
//$regmat = $pdf->ObjConsulta->select_equiposasignacion($pdf->conect_sistemas_vtv, $_GET['idasignacion']);
//$regmat = array_merge($regmat);
//$regmat = array_map(array_merge, $regmat);
//echo $body;

$pdf->Cell(10);
$pdf->writeHTML(utf8_encode($body), true, 0, true, 0);
$pdf->Output("Reporte_de_clasificacion.pdf", 'I');
?>