<?php

header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
ini_set('memory_limit', '9999999999999999999M');
require_once('tcpdf/tcpdf.php');


require_once("../class/bd/classbdConsultas.php");
require_once("../../../librerias/classlibFecHor.php");

class reporte extends TCPDF {

    public $conect_sistemas_vtv;
    public $ObjConsulta;
    public $Objfechahora;
    public $registros;
    public $almacenista;
    public $receptor;

    function __construct($orientation='P', $unit='mm', $format='A4', $unicode=true, $encoding='UTF-8', $diskcache=false) {
        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache);
        $this->conect_sistemas_vtv = "../../../database/archi_conex/sistemas_vtv_5431";
        $this->conect_sigesp = "../../../database/archi_conex/sistema_sigesp";
        $this->ObjConsulta = new classbdConsultas();
        $this->Objfechahora = new classlibFecHor(); 
    }

    function header() {

        // $this->registros = $this->ObjConsulta->select_data_asignacionplani($this->conect_sistemas_vtv, $_GET['idasignacion']);
        //fix array
        //  $this->registros = array_merge($this->registros);
        //$this->registros = array_map("array_merge", $this->registros);

        if ($this->registros[0][1] == "") {
            $this->registros[0][1] = "NO EMPLEADO";
        }


        $this->SetFont('', '', 10);


        $this->almacenista = utf8_encode($this->registros[0][1]);
        $this->receptor = utf8_encode($this->registros[0][2]);

        $titulo1 = "RECEPCI&Oacute;N DE MATERIALES";

        $htmltable = '<table border="0" width="650px" cellspacing="4">
	  <tr>
	  	
		<td width="160px" rowspan="3"><img src="imagenes/logo_vtv.jpg" style="width: 173px; height: 72px;" alt="logo"/></td>
		<!--<div align="center"><font size="10">FECHA: <b>' . date("d/m/Y H:i:s") . '</b></font></div>-->
		<td ><br /><br /><div align="center"><font size="10"><b>' . $titulo1 . '</b></font></div></td>
	 </tr>
	 </table>
          
';
        $fechaimp = date("d/m/Y H:i:s");

        // echo $htmltable;
        $this->writeHTML($htmltable);

        //$this->Image('../imagenes/bandera.jpg','', $this->GetY()-3, 168);
    }

    function footer() {
        $this->SetFont('', '', 6);
        $this->Ln(3);
        $this->Cell(0, 0, 'FECHA:' . date("d/m/Y H:i:s") . '', 0, 0, 'L');
    }

    function renderizarimagetofile($url, $name, $path="imagenes/") {
        if (($f = fopen($url, 'r')) != false) {
            fclose($f);
            $res = join(file($url));
            if (($f = fopen($path . $name . ".png", "w")) != false) {
                fwrite($f, $res);
                fclose($f);
            }
        }
    }

}

//$pdf2=new MEM_IMAGE();
$pdf = new reporte(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, '', PDF_HEADER_STRING);//PDF_HEADER_TITLE
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, 'LISTADO CONSTANCIAS DE TRABAJO DEL '.$desde.' AL '.$hasta, PDF_HEADER_STRING);//PDF_HEADER_TITLE
// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(15, 38, 20);
$pdf->SetHeaderMargin(15);
$pdf->SetFooterMargin(20);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

$pdf->AddPage('P');
require_once('tcpdf/htmlcolors.php');
$pdf->Ln(2);







$id_entrada = $_GET['id_entrada'];
//$identrada=$_GET['identrada'];

$datossolicitud = $pdf->ObjConsulta->selectentradas($pdf->conect_sistemas_vtv, $id_entrada);
$contador= (count($datossolicitud));
if($contador==0){
    $materiales ='<tr nobr="true"><th colspan="2"  color="red"><font size="10" ><b>No se encuentran materiales para la solicitud indicada</b></font></th></tr>';
}else{

    $id_entrada= $datossolicitud[1][1];
    $id_solicitud_prestamo= $datossolicitud[1][2];
    $resp_entrada= $datossolicitud[1][3];
    $user_reg= $datossolicitud[1][4];
    $fecha_entrada= $datossolicitud[1][5];
    $fecha_entrada = $pdf->Objfechahora->flibInvertirInEs($fecha_entrada);
    $hora_entrada= $datossolicitud[1][6];
    $observacion= $datossolicitud[1][7];
    

    $datosprestamo = $pdf->ObjConsulta->selectprestamos($pdf->conect_sistemas_vtv, $id_solicitud_prestamo);
    $tipo_prestamo=$datosprestamo[1][9];
    $identrada=$tipo_prestamo;


    $responsableg=$pdf->ObjConsulta->selectpersonalresp($pdf->conect_sigesp, $user_reg);
    $nombres1=$responsableg[1][2];
    $apellidos1=$responsableg[1][3];
    $cargo1=$responsableg[1][4];


    $responsable=$pdf->ObjConsulta->selectpersonalresp($pdf->conect_sigesp, $resp_entrada);
    $nombres=$responsable[1][2];
    $apellidos=$responsable[1][3];
    $cargo=$responsable[1][4];

    if ($observacion==''){
        $observaciones = '<th align="left" bgcolor="DarkGray"><font size="10">&nbsp;&nbsp;<b>Observaciones:</b></font></th><td align="left" ><font size="8">&nbsp;&nbsp;Sin Observaciones</font></td>';

    }else{
        $observaciones = '<th align="left" bgcolor="DarkGray"><font size="10">&nbsp;&nbsp;<b>Observaciones:</b></font></th><td align="left" ><font size="8">&nbsp;&nbsp;'.$observacion.'</font></td>';

    } 
    $responsablesol = '<th align="left" bgcolor="DarkGray"><font size="10">&nbsp;&nbsp;<b>Responsable:</b></font></th><td align="left" ><font size="8">&nbsp;&nbsp;' . $nombres . '&nbsp;&nbsp;' . $apellidos . '</font></td>';

    $fecha_ent= '<th align="left" bgcolor="DarkGray"><font size="10">&nbsp;&nbsp;<b>Fecha de la entrada:</b></font></th><td align="left" ><font size="8">&nbsp;&nbsp;' . $fecha_entrada . '</font></td>';




    /*$fecha_condicion = '1';
    $datosmateriales = $pdf->ObjConsulta->selectgrupoentregados($pdf->conect_sistemas_vtv, $id_solicitud_prestamo, $fecha_condicion);
    */

    /*if($tipo_prestamo==1){//es de grupos
        $datosmaterialespres = $pdf->ObjConsulta->selectgrupoentregados($pdf->conect_sistemas_vtv, $id_solicitud_prestamo);

    }elseif($tipo_prestamo==2){//es de equipos
        $datosmaterialespres = $pdf->ObjConsulta->selectequipoentregados($pdf->conect_sistemas_vtv, $id_solicitud_prestamo);

    }elseif($tipo_prestamo==9){//es de equipos y grupos
        $datosmaterialespres = $pdf->ObjConsulta->selectequipoentregados($pdf->conect_sistemas_vtv, $id_solicitud_prestamo);
        $datosmaterialespresgrup = $pdf->ObjConsulta->selectgrupoentregados($pdf->conect_sistemas_vtv, $id_solicitud_prestamo);

    }else{//es camaras
        $datosmaterialespres = $pdf->ObjConsulta->selectcamaraentregadas($pdf->conect_sistemas_vtv, $id_solicitud_prestamo);

    }*/ 
    if($tipo_prestamo==3){
        $datosmaterialespres = $pdf->ObjConsulta->selectcamaraentregadas($pdf->conect_sistemas_vtv, $id_solicitud_prestamo);  
    }else{
        $datosmaterialespres = $pdf->ObjConsulta->selectequipoentregados($pdf->conect_sistemas_vtv, $id_solicitud_prestamo);
        $datosmaterialespresgrup = $pdf->ObjConsulta->selectgrupoentregados($pdf->conect_sistemas_vtv, $id_solicitud_prestamo);

    }

    $contador= (count($datosmaterialespres));
    $contador2= (count($datosmaterialespresgrup));
    if($contador==0 and $contador2==0){
        $titulo2= 'MATERIALES SOLICITADOS';

        $titulos='<tr nobr="true"><th colspan="1"  bgcolor="DarkGray"><font size="10"><b>' . $titulo2 . '</b></font></th></tr>';
                
        $materiales ='<tr nobr="true"><th colspan="3"  color="red"><font size="10" ><b>No existen materiales</b></font></th></tr>';
    }else{

        if($identrada==3){
            $datosmaterialespres = $pdf->ObjConsulta->selectcamaraentregadas($pdf->conect_sistemas_vtv, $id_solicitud_prestamo);
            foreach ($datosmaterialespres as $llave => $valor) {
                $datosgrupospres = $pdf->ObjConsulta->selectdatosgrupo($pdf->conect_sistemas_vtv, $valor[1]);
                $id_grupo=$datosgrupospres[1][1];
                $descripcion=$datosgrupospres[1][2];
                $imagen=$datosgrupospres[1][3];
                $titulo2= 'MATERIALES ENTREGADOS';
                $fechaexp = $pdf->ObjConsulta->selectfechacamaraentregado($pdf->conect_sistemas_vtv, $id_solicitud_prestamo, $valor[1]);
                $fecha_exp= $fechaexp[1][1];
                $fecha_exp = $pdf->Objfechahora->flibInvertirInEs($fecha_exp);
                
                $titulos='<tr nobr="true"><th colspan="2"  bgcolor="DarkGray"><font size="10"><b>' . $titulo2 . '</b></font></th></tr>
                <tr nobr="true"><th><div align="center"><font size="10"><b>Descripci&oacute;n</b></font></div></th><th><div align="center"><font size="10"><b>Fecha de entrega</b></font></div></th></tr>';
                
                $materiales.='<tr nobr="true"><td align="left"><font size="8">' . $descripcion . '</font></td><td align="center"><font size="8">' . $fecha_exp . '</font></td></tr>';
                        
            }
        }else{
            $datosmaterialespres = $pdf->ObjConsulta->selectequipoentregados($pdf->conect_sistemas_vtv, $id_solicitud_prestamo);
            //var_dump($datosmaterialespres);die();
            foreach ($datosmaterialespres as $llave => $valor) {
                $datosequipopres = $pdf->ObjConsulta->selectdatosequipo($pdf->conect_sistemas_vtv, $valor[1]);
                $id_articulo=$datosequipopres[1][1];
                $tipo_articulo=$datosequipopres[1][2];
                $descripcion=$datosequipopres[1][3];
                $marca=$datosequipopres[1][4];
                $modelo=$datosequipopres[1][5];
                $bien_nac=$datosequipopres[1][6];
                $serial=$datosequipopres[1][7];

                $marca="Marca: " . $marca . "";

                if($modelo==""){
                    $modelo=="";
                }else{
                    $modelo="Modelo: " . $modelo . "";
                }

                if($bien_nac==""){
                    $bien_nac=='';
                }else{
                    $bien_nac="Bien Nac.: " . $bien_nac . "";
                }

                if($serial==""){
                    $serial=="";
                }else{
                    $serial="Serial: " . $serial . "";
                }


                $desc_detalle= "" . $modelo . "<br/>" . $marca . "<br/>" . $bien_nac . "<br/>" . $serial . "";
                
                $fechaexp = $pdf->ObjConsulta->selectfechaequipoentregado($pdf->conect_sistemas_vtv, $id_solicitud_prestamo, $valor[1]);
                $fecha_exp= $fechaexp[1][1];
                $fecha_exp = $pdf->Objfechahora->flibInvertirInEs($fecha_exp);
                
                $titulo2= 'MATERIALES ENTREGADOS';

                $titulos='<tr nobr="true"><th colspan="3"  bgcolor="DarkGray"><font size="10"><b>' . $titulo2 . '</b></font></th></tr>
                <tr nobr="true"><th><div align="center"><font size="10">Descripci&oacute;n</font></div></th><th><div align="center"><font size="10">Detalle</font></div></th><th><div align="center"><font size="10">Fecha de entrega</font></div></th></tr>';
                
                $materiales.='<tr nobr="true"><td align="left"><font size="8">' . $descripcion . '</font></td><td align="left"><font size="8">' . $desc_detalle . '</font></td><td align="center"><font size="8">' . $fecha_exp . '</font></td></tr>';
                    
            }

            $datosmaterialespres = $pdf->ObjConsulta->selectgrupoentregados($pdf->conect_sistemas_vtv, $id_solicitud_prestamo);
            foreach ($datosmaterialespres as $llave => $valor) {
                $datosgrupospres = $pdf->ObjConsulta->selectdatosgrupo($pdf->conect_sistemas_vtv, $valor[1]);
                $id_grupo=$datosgrupospres[1][1];
                $descripcion=$datosgrupospres[1][2];
                $imagen=$datosgrupospres[1][3];
                $titulo2= 'MATERIALES ENTREGADOS';
                $fechaexp = $pdf->ObjConsulta->selectfechagrupoentregado($pdf->conect_sistemas_vtv, $id_solicitud_prestamo, $valor[1]);
                $fecha_exp= $fechaexp[1][1];
                $fecha_exp = $pdf->Objfechahora->flibInvertirInEs($fecha_exp);
                
                $titulos='<tr nobr="true"><th colspan="3"  bgcolor="DarkGray"><font size="10"><b>' . $titulo2 . '</b></font></th></tr>
                <tr nobr="true"><th colspan="2"><div align="center"><font size="10"><b>Descripci&oacute;n</b></font></div></th><th ><div align="center"><font size="10"><b>Fecha de entrega</b></font></div></th></tr>';
                
                $materiales.='<tr nobr="true"><td align="left" colspan="2"><font size="8">' . $descripcion . '</font></td><td align="center"><font size="8">' . $fecha_exp . '</font></td></tr>';
                        
            }
        }
        /*if($identrada==1){//es de grupos
            $datosmaterialespres = $pdf->ObjConsulta->selectgrupoentregados($pdf->conect_sistemas_vtv, $id_solicitud_prestamo);
            foreach ($datosmaterialespres as $llave => $valor) {
                $datosgrupospres = $pdf->ObjConsulta->selectdatosgrupo($pdf->conect_sistemas_vtv, $valor[1]);
                $id_grupo=$datosgrupospres[1][1];
                $descripcion=$datosgrupospres[1][2];
                $imagen=$datosgrupospres[1][3];
                $titulo2= 'MATERIALES ENTREGADOS';
                $fechaexp = $pdf->ObjConsulta->selectfechagrupoentregado($pdf->conect_sistemas_vtv, $id_solicitud_prestamo, $valor[1]);
                $fecha_exp= $fechaexp[1][1];
                $fecha_exp = $pdf->Objfechahora->flibInvertirInEs($fecha_exp);
                
                $titulos='<tr nobr="true"><th colspan="2"  bgcolor="DarkGray"><font size="10"><b>' . $titulo2 . '</b></font></th></tr>
                <tr nobr="true"><th><div align="center"><font size="10"><b>Descripci&oacute;n</b></font></div></th><th><div align="center"><font size="10"><b>Fecha de entrega</b></font></div></th></tr>';
                
                $materiales.='<tr nobr="true"><td align="left"><font size="8">' . $descripcion . '</font></td><td align="center"><font size="8">' . $fecha_exp . '</font></td></tr>';
                        
            }
            
        }elseif($identrada==2){//es de equipos
            $datosmaterialespres = $pdf->ObjConsulta->selectequipoentregados($pdf->conect_sistemas_vtv, $id_solicitud_prestamo);
            //var_dump($datosmaterialespres);die();
            foreach ($datosmaterialespres as $llave => $valor) {
                $datosequipopres = $pdf->ObjConsulta->selectdatosequipo($pdf->conect_sistemas_vtv, $valor[1]);
                $id_articulo=$datosequipopres[1][1];
                $tipo_articulo=$datosequipopres[1][2];
                $descripcion=$datosequipopres[1][3];
                $marca=$datosequipopres[1][4];
                $modelo=$datosequipopres[1][5];
                $bien_nac=$datosequipopres[1][6];
                $serial=$datosequipopres[1][7];

                $marca="Marca: " . $marca . "";

                if($modelo==""){
                    $modelo=="";
                }else{
                    $modelo="Modelo: " . $modelo . "";
                }

                if($bien_nac==""){
                    $bien_nac=='';
                }else{
                    $bien_nac="Bien Nac.: " . $bien_nac . "";
                }

                if($serial==""){
                    $serial=="";
                }else{
                    $serial="Serial: " . $serial . "";
                }


                $desc_detalle= "" . $modelo . "<br/>" . $marca . "<br/>" . $bien_nac . "<br/>" . $serial . "";
                
                $fechaexp = $pdf->ObjConsulta->selectfechaequipoentregado($pdf->conect_sistemas_vtv, $id_solicitud_prestamo, $valor[1]);
                $fecha_exp= $fechaexp[1][1];
                $fecha_exp = $pdf->Objfechahora->flibInvertirInEs($fecha_exp);
                
                $titulo2= 'MATERIALES ENTREGADOS';

                $titulos='<tr nobr="true"><th colspan="3"  bgcolor="DarkGray"><font size="10"><b>' . $titulo2 . '</b></font></th></tr>
                <tr nobr="true"><th><div align="center"><font size="10">Descripci&oacute;n</font></div></th><th><div align="center"><font size="10">Detalle</font></div></th><th><div align="center"><font size="10">Fecha de entrega</font></div></th></tr>';
                
                $materiales.='<tr nobr="true"><td align="left"><font size="8">' . $descripcion . '</font></td><td align="left"><font size="8">' . $desc_detalle . '</font></td><td align="center"><font size="8">' . $fecha_exp . '</font></td></tr>';
                    
            }
                    

        }elseif($identrada==9){//es de equipos
            $datosmaterialespres = $pdf->ObjConsulta->selectequipoentregados($pdf->conect_sistemas_vtv, $id_solicitud_prestamo);
            //var_dump($datosmaterialespres);die();
            foreach ($datosmaterialespres as $llave => $valor) {
                $datosequipopres = $pdf->ObjConsulta->selectdatosequipo($pdf->conect_sistemas_vtv, $valor[1]);
                $id_articulo=$datosequipopres[1][1];
                $tipo_articulo=$datosequipopres[1][2];
                $descripcion=$datosequipopres[1][3];
                $marca=$datosequipopres[1][4];
                $modelo=$datosequipopres[1][5];
                $bien_nac=$datosequipopres[1][6];
                $serial=$datosequipopres[1][7];

                $marca="Marca: " . $marca . "";

                if($modelo==""){
                    $modelo=="";
                }else{
                    $modelo="Modelo: " . $modelo . "";
                }

                if($bien_nac==""){
                    $bien_nac=='';
                }else{
                    $bien_nac="Bien Nac.: " . $bien_nac . "";
                }

                if($serial==""){
                    $serial=="";
                }else{
                    $serial="Serial: " . $serial . "";
                }


                $desc_detalle= "" . $modelo . "<br/>" . $marca . "<br/>" . $bien_nac . "<br/>" . $serial . "";
                
                $fechaexp = $pdf->ObjConsulta->selectfechaequipoentregado($pdf->conect_sistemas_vtv, $id_solicitud_prestamo, $valor[1]);
                $fecha_exp= $fechaexp[1][1];
                $fecha_exp = $pdf->Objfechahora->flibInvertirInEs($fecha_exp);
                
                $titulo2= 'MATERIALES ENTREGADOS';

                $titulos='<tr nobr="true"><th colspan="3"  bgcolor="DarkGray"><font size="10"><b>' . $titulo2 . '</b></font></th></tr>
                <tr nobr="true"><th><div align="center"><font size="10">Descripci&oacute;n</font></div></th><th><div align="center"><font size="10">Detalle</font></div></th><th><div align="center"><font size="10">Fecha de entrega</font></div></th></tr>';
                
                $materiales.='<tr nobr="true"><td align="left"><font size="8">' . $descripcion . '</font></td><td align="left"><font size="8">' . $desc_detalle . '</font></td><td align="center"><font size="8">' . $fecha_exp . '</font></td></tr>';
                    
            }

            $datosmaterialespres = $pdf->ObjConsulta->selectgrupoentregados($pdf->conect_sistemas_vtv, $id_solicitud_prestamo);
            foreach ($datosmaterialespres as $llave => $valor) {
                $datosgrupospres = $pdf->ObjConsulta->selectdatosgrupo($pdf->conect_sistemas_vtv, $valor[1]);
                $id_grupo=$datosgrupospres[1][1];
                $descripcion=$datosgrupospres[1][2];
                $imagen=$datosgrupospres[1][3];
                $titulo2= 'MATERIALES ENTREGADOS';
                $fechaexp = $pdf->ObjConsulta->selectfechagrupoentregado($pdf->conect_sistemas_vtv, $id_solicitud_prestamo, $valor[1]);
                $fecha_exp= $fechaexp[1][1];
                $fecha_exp = $pdf->Objfechahora->flibInvertirInEs($fecha_exp);
                
                $titulos='<tr nobr="true"><th colspan="3"  bgcolor="DarkGray"><font size="10"><b>' . $titulo2 . '</b></font></th></tr>
                <tr nobr="true"><th colspan="2"><div align="center"><font size="10"><b>Descripci&oacute;n</b></font></div></th><th ><div align="center"><font size="10"><b>Fecha de entrega</b></font></div></th></tr>';
                
                $materiales.='<tr nobr="true"><td align="left" colspan="2"><font size="8">' . $descripcion . '</font></td><td align="center"><font size="8">' . $fecha_exp . '</font></td></tr>';
                        
            }
                    

        }else{//es camaras

            $datosmaterialespres = $pdf->ObjConsulta->selectcamaraentregadas($pdf->conect_sistemas_vtv, $id_solicitud_prestamo);
            foreach ($datosmaterialespres as $llave => $valor) {
                $datosgrupospres = $pdf->ObjConsulta->selectdatosgrupo($pdf->conect_sistemas_vtv, $valor[1]);
                $id_grupo=$datosgrupospres[1][1];
                $descripcion=$datosgrupospres[1][2];
                $imagen=$datosgrupospres[1][3];
                $titulo2= 'MATERIALES ENTREGADOS';
                $fechaexp = $pdf->ObjConsulta->selectfechacamaraentregado($pdf->conect_sistemas_vtv, $id_solicitud_prestamo, $valor[1]);
                $fecha_exp= $fechaexp[1][1];
                $fecha_exp = $pdf->Objfechahora->flibInvertirInEs($fecha_exp);
                
                $titulos='<tr nobr="true"><th colspan="2"  bgcolor="DarkGray"><font size="10"><b>' . $titulo2 . '</b></font></th></tr>
                <tr nobr="true"><th><div align="center"><font size="10"><b>Descripci&oacute;n</b></font></div></th><th><div align="center"><font size="10"><b>Fecha de entrega</b></font></div></th></tr>';
                
                $materiales.='<tr nobr="true"><td align="left"><font size="8">' . $descripcion . '</font></td><td align="center"><font size="8">' . $fecha_exp . '</font></td></tr>';
                        
            }
                 
        }*/ 
    }
    ////////////////////////////////************Pendientes por entregar**************/////////////////////////
    /*if($tipo_prestamo==1){//es de grupos
        $datosmaterialesprespen = $pdf->ObjConsulta->selectgrupoporentregar($pdf->conect_sistemas_vtv, $id_solicitud_prestamo);

    }elseif($tipo_prestamo==2){//es de equipos
        $datosmaterialesprespen = $pdf->ObjConsulta->selectequipoporentregar($pdf->conect_sistemas_vtv, $id_solicitud_prestamo);

    }elseif($tipo_prestamo==9){//es de equipos y grupos
        $datosmaterialesprespen = $pdf->ObjConsulta->selectequipoporentregar($pdf->conect_sistemas_vtv, $id_solicitud_prestamo);
        $datosmaterialesprespengrup = $pdf->ObjConsulta->selectgrupoporentregar($pdf->conect_sistemas_vtv, $id_solicitud_prestamo);

    }else{//es camaras
        $datosmaterialesprespen = $pdf->ObjConsulta->selectcamaraporentregar($pdf->conect_sistemas_vtv, $id_solicitud_prestamo);

    } */

    if($tipo_prestamo==3){
        $datosmaterialesprespen = $pdf->ObjConsulta->selectcamaraporentregar($pdf->conect_sistemas_vtv, $id_solicitud_prestamo);

    }else{
        $datosmaterialesprespen = $pdf->ObjConsulta->selectequipoporentregar($pdf->conect_sistemas_vtv, $id_solicitud_prestamo);
        $datosmaterialesprespengrup = $pdf->ObjConsulta->selectgrupoporentregar($pdf->conect_sistemas_vtv, $id_solicitud_prestamo);
 
    }

    $contador3= (count($datosmaterialesprespen));
    $contador4= (count($datosmaterialesprespengrup));
    
    if($contador3==0 and $contador4==0){
        $titulo3= 'MATERIALES PENDIENTES';

        $titulospen='<tr nobr="true"><th colspan="1"  bgcolor="DarkGray"><font size="10"><b>' . $titulo3 . '</b></font></th></tr>';
                
        $materialespen ='<tr nobr="true"><th colspan="3"  color="red"><font size="10" ><b>No existen equipos pendientes por entregar</b></font></th></tr>';
    }else{

        if($identrada==3){//Son camaras
            $datosmaterialesprespen = $pdf->ObjConsulta->selectcamaraporentregar($pdf->conect_sistemas_vtv, $id_solicitud_prestamo);
            foreach ($datosmaterialesprespen as $llave => $valor) {
                $datosgrupospres = $pdf->ObjConsulta->selectdatosgrupo($pdf->conect_sistemas_vtv, $valor[1]);
                $id_grupo=$datosgrupospres[1][1];
                $descripcion=$datosgrupospres[1][2];
                $imagen=$datosgrupospres[1][3];
                $titulo3= 'MATERIALES PENDIENTES';
                
                $titulospen='<tr nobr="true"><th colspan="1"  bgcolor="DarkGray"><font size="10"><b>' . $titulo3 . '</b></font></th></tr>
                <tr nobr="true"><th><div align="center"><font size="10"><b>Descripci&oacute;n</b></font></div></th></tr>';
                
                $materialespen.='<tr nobr="true"><td align="left"><font size="8">' . $descripcion . '</font></td></tr>';
                        
            }
        }else{//son equipos y/o grupos
            $datosmaterialesprespen = $pdf->ObjConsulta->selectequipoporentregar($pdf->conect_sistemas_vtv, $id_solicitud_prestamo);
            //var_dump($datosmaterialespres);die();
            foreach ($datosmaterialesprespen as $llave => $valor) {
                $datosequipopres = $pdf->ObjConsulta->selectdatosequipo($pdf->conect_sistemas_vtv, $valor[1]);
                $id_articulo=$datosequipopres[1][1];
                $tipo_articulo=$datosequipopres[1][2];
                $descripcion=$datosequipopres[1][3];
                $marca=$datosequipopres[1][4];
                $modelo=$datosequipopres[1][5];
                $bien_nac=$datosequipopres[1][6];
                $serial=$datosequipopres[1][7];

                $marca="Marca: " . $marca . "";

                if($modelo==""){
                    $modelo=="";
                }else{
                    $modelo="Modelo: " . $modelo . "";
                }

                if($bien_nac==""){
                    $bien_nac=='';
                }else{
                    $bien_nac="Bien Nac.: " . $bien_nac . "";
                }

                if($serial==""){
                    $serial=="";
                }else{
                    $serial="Serial: " . $serial . "";
                }


                $desc_detalle= "" . $modelo . "<br/>" . $marca . "<br/>" . $bien_nac . "<br/>" . $serial . "";
                
                
                $titulo3= 'MATERIALES PENDIENTES';

                $titulospen='<tr nobr="true"><th colspan="2"  bgcolor="DarkGray"><font size="10"><b>' . $titulo3 . '</b></font></th></tr>
                <tr nobr="true"><th><div align="center"><font size="10">Descripci&oacute;n</font></div></th><th><div align="center"><font size="10">Detalle</font></div></th></tr>';
                
                $materialespen.='<tr nobr="true"><td align="left"><font size="8">' . $descripcion . '</font></td><td align="left"><font size="8">' . $desc_detalle . '</font></td></tr>';
                    
            }

            $datosmaterialesprespen = $pdf->ObjConsulta->selectgrupoporentregar($pdf->conect_sistemas_vtv, $id_solicitud_prestamo);
            foreach ($datosmaterialesprespen as $llave => $valor) {
                $datosgrupospres = $pdf->ObjConsulta->selectdatosgrupo($pdf->conect_sistemas_vtv, $valor[1]);
                $id_grupo=$datosgrupospres[1][1];
                $descripcion=$datosgrupospres[1][2];
                $imagen=$datosgrupospres[1][3];
                $titulo3= 'MATERIALES PENDIENTES';
                
                $titulospen='<tr nobr="true"><th colspan="1"  bgcolor="DarkGray"><font size="10"><b>' . $titulo3 . '</b></font></th></tr>
                <tr nobr="true"><th><div align="center"><font size="10"><b>Descripci&oacute;n</b></font></div></th></tr>';
                
                $materialespen.='<tr nobr="true"><td align="left" colspan="2"><font size="8">' . $descripcion . '</font></td></tr>';
                        
            }
        }   
        /*if($identrada==1){//es de grupos
            $datosmaterialesprespen = $pdf->ObjConsulta->selectgrupoporentregar($pdf->conect_sistemas_vtv, $id_solicitud_prestamo);
            foreach ($datosmaterialesprespen as $llave => $valor) {
                $datosgrupospres = $pdf->ObjConsulta->selectdatosgrupo($pdf->conect_sistemas_vtv, $valor[1]);
                $id_grupo=$datosgrupospres[1][1];
                $descripcion=$datosgrupospres[1][2];
                $imagen=$datosgrupospres[1][3];
                $titulo3= 'MATERIALES PENDIENTES';
                
                $titulospen='<tr nobr="true"><th colspan="1"  bgcolor="DarkGray"><font size="10"><b>' . $titulo3 . '</b></font></th></tr>
                <tr nobr="true"><th><div align="center"><font size="10"><b>Descripci&oacute;n</b></font></div></th></tr>';
                
                $materialespen.='<tr nobr="true"><td align="left"><font size="8">' . $descripcion . '</font></td></tr>';
                        
            }
            
        }elseif($identrada==2){//es de equipos
            $datosmaterialesprespen = $pdf->ObjConsulta->selectequipoporentregar($pdf->conect_sistemas_vtv, $id_solicitud_prestamo);
            //var_dump($datosmaterialespres);die();
            foreach ($datosmaterialesprespen as $llave => $valor) {
                $datosequipopres = $pdf->ObjConsulta->selectdatosequipo($pdf->conect_sistemas_vtv, $valor[1]);
                $id_articulo=$datosequipopres[1][1];
                $tipo_articulo=$datosequipopres[1][2];
                $descripcion=$datosequipopres[1][3];
                $marca=$datosequipopres[1][4];
                $modelo=$datosequipopres[1][5];
                $bien_nac=$datosequipopres[1][6];
                $serial=$datosequipopres[1][7];

                $marca="Marca: " . $marca . "";

                if($modelo==""){
                    $modelo=="";
                }else{
                    $modelo="Modelo: " . $modelo . "";
                }

                if($bien_nac==""){
                    $bien_nac=='';
                }else{
                    $bien_nac="Bien Nac.: " . $bien_nac . "";
                }

                if($serial==""){
                    $serial=="";
                }else{
                    $serial="Serial: " . $serial . "";
                }


                $desc_detalle= "" . $modelo . "<br/>" . $marca . "<br/>" . $bien_nac . "<br/>" . $serial . "";
                
                
                $titulo3= 'MATERIALES PENDIENTES';

                $titulospen='<tr nobr="true"><th colspan="2"  bgcolor="DarkGray"><font size="10"><b>' . $titulo3 . '</b></font></th></tr>
                <tr nobr="true"><th><div align="center"><font size="10">Descripci&oacute;n</font></div></th><th><div align="center"><font size="10">Detalle</font></div></th></tr>';
                
                $materialespen.='<tr nobr="true"><td align="left"><font size="8">' . $descripcion . '</font></td><td align="left"><font size="8">' . $desc_detalle . '</font></td></tr>';
                    
            }
                    

        }elseif($identrada==9){//es de equipos
            $datosmaterialesprespen = $pdf->ObjConsulta->selectequipoporentregar($pdf->conect_sistemas_vtv, $id_solicitud_prestamo);
            //var_dump($datosmaterialespres);die();
            foreach ($datosmaterialesprespen as $llave => $valor) {
                $datosequipopres = $pdf->ObjConsulta->selectdatosequipo($pdf->conect_sistemas_vtv, $valor[1]);
                $id_articulo=$datosequipopres[1][1];
                $tipo_articulo=$datosequipopres[1][2];
                $descripcion=$datosequipopres[1][3];
                $marca=$datosequipopres[1][4];
                $modelo=$datosequipopres[1][5];
                $bien_nac=$datosequipopres[1][6];
                $serial=$datosequipopres[1][7];

                $marca="Marca: " . $marca . "";

                if($modelo==""){
                    $modelo=="";
                }else{
                    $modelo="Modelo: " . $modelo . "";
                }

                if($bien_nac==""){
                    $bien_nac=='';
                }else{
                    $bien_nac="Bien Nac.: " . $bien_nac . "";
                }

                if($serial==""){
                    $serial=="";
                }else{
                    $serial="Serial: " . $serial . "";
                }


                $desc_detalle= "" . $modelo . "<br/>" . $marca . "<br/>" . $bien_nac . "<br/>" . $serial . "";
                
                
                $titulo3= 'MATERIALES PENDIENTES';

                $titulospen='<tr nobr="true"><th colspan="2"  bgcolor="DarkGray"><font size="10"><b>' . $titulo3 . '</b></font></th></tr>
                <tr nobr="true"><th><div align="center"><font size="10">Descripci&oacute;n</font></div></th><th><div align="center"><font size="10">Detalle</font></div></th></tr>';
                
                $materialespen.='<tr nobr="true"><td align="left"><font size="8">' . $descripcion . '</font></td><td align="left"><font size="8">' . $desc_detalle . '</font></td></tr>';
                    
            }

            $datosmaterialesprespen = $pdf->ObjConsulta->selectgrupoporentregar($pdf->conect_sistemas_vtv, $id_solicitud_prestamo);
            foreach ($datosmaterialesprespen as $llave => $valor) {
                $datosgrupospres = $pdf->ObjConsulta->selectdatosgrupo($pdf->conect_sistemas_vtv, $valor[1]);
                $id_grupo=$datosgrupospres[1][1];
                $descripcion=$datosgrupospres[1][2];
                $imagen=$datosgrupospres[1][3];
                $titulo3= 'MATERIALES PENDIENTES';
                
                $titulospen='<tr nobr="true"><th colspan="1"  bgcolor="DarkGray"><font size="10"><b>' . $titulo3 . '</b></font></th></tr>
                <tr nobr="true"><th><div align="center"><font size="10"><b>Descripci&oacute;n</b></font></div></th></tr>';
                
                $materialespen.='<tr nobr="true"><td align="left"><font size="8">' . $descripcion . '</font></td></tr>';
                        
            }
                    

        }else{//es camaras

            $datosmaterialesprespen = $pdf->ObjConsulta->selectcamaraporentregar($pdf->conect_sistemas_vtv, $id_solicitud_prestamo);
            foreach ($datosmaterialesprespen as $llave => $valor) {
                $datosgrupospres = $pdf->ObjConsulta->selectdatosgrupo($pdf->conect_sistemas_vtv, $valor[1]);
                $id_grupo=$datosgrupospres[1][1];
                $descripcion=$datosgrupospres[1][2];
                $imagen=$datosgrupospres[1][3];
                $titulo3= 'MATERIALES PENDIENTES';
                
                $titulospen='<tr nobr="true"><th colspan="1"  bgcolor="DarkGray"><font size="10"><b>' . $titulo3 . '</b></font></th></tr>
                <tr nobr="true"><th><div align="center"><font size="10"><b>Descripci&oacute;n</b></font></div></th></tr>';
                
                $materialespen.='<tr nobr="true"><td align="left"><font size="8">' . $descripcion . '</font></td></tr>';
                        
            }
                 
        } */
    }

}

$resp= '<td align="center" >_____________________<br/>
        Quien entrega:<br/>
        ' . $nombres . '&nbsp;' . $apellidos . '<br/>
        ' . $cargo . '<br/>
        C.I:' . $resp_entrada . '<br/>
        </td>';


$entr= '<td align="center" >_____________________<br/>
        Quien recibe:<br/>
        ' . $nombres1 . '&nbsp;' . $apellidos1 . '<br/>
        ' . $cargo1 . '<br/>
        C.I:' . $user_reg . '<br/>
        </td>';

/////////////////////////////////////////////////////////////////////////////////////////////////

$titulo = 'DATOS GENERALES';

$body = '

<table align="center" border="1">
        <tr nobr="true"><th colspan="2"  bgcolor="DarkGray"><font size="10" ><b>' . $titulo . '</b></font></th></tr>
        <tr nobr="true"><th align="left" width="150px" bgcolor="DarkGray"><font size="10">&nbsp;&nbsp;<b>N&deg; de solicitud:</b></font></th><td align="left" width="435px"><font size="8">&nbsp;&nbsp;' . $id_solicitud_prestamo . '</font></td></tr>
        <tr nobr="true">' . $fecha_ent . '</tr>
        <tr nobr="true">' . $responsablesol . '</tr>
        <tr nobr="true">' . $observaciones . '</tr>
</table>
<table align="center" border="0">
        <tr nobr="true" ><th colspan="2"  ></th></tr>
</table>
<table align="center" border="1">
        ' . $titulos . '
        ' . $materiales . '
</table>
<table align="center" border="0">
        <tr nobr="true" ><th colspan="2"  ></th></tr>
</table>
<table align="center" border="1">
        ' . $titulospen . '
        ' . $materialespen . '
</table>
<br />
<br />
<br />
<br />
<table  align="center" border="0" >
        <tr nobr="true" >'.$resp.' '.$entr.'</tr>
</table>';


//echo $_GET['idasignacion'];
//$regmat = $pdf->ObjConsulta->select_equiposasignacion($pdf->conect_sistemas_vtv, $_GET['idasignacion']);
//$regmat = array_merge($regmat);
//$regmat = array_map(array_merge, $regmat);
//echo $body;

$pdf->Cell(10);
$pdf->writeHTML(utf8_encode($body), true, 0, true, 0);
$pdf->Output("entrada".$id_entrada.".pdf", 'I');
?>