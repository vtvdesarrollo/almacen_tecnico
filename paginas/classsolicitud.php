<?php
//ini_set("error_reporting","E_ALL & ~E_NOTICE");
require("classDirectorioFunciones.php");
class solicitud{
    public $html;
    function __construct(){
        $this->ObjclasslibSession = new classlibSession();
        $this->ObjCabPie=new classlibCabPie("Solicitud ","");
        $this->ObjOther=new classOtherMenu();
        $this->ObjMensaje=new classMensaje("","mostrar");
        $this->classDirectorioFunciones = new classDirectorioFunciones(true);
        $ficherosjs = "
        <script type='text/javascript' src='../class/other/ms-Dropdown/msdropdown/js/uncompressed.jquery.dd.js'></script>
        <script type='text/javascript' src='../class/other/classjavascript.js'></script>
        <script type='text/javascript' src='../../../librerias/datepick/jquery.datepick.pack.js'></script>
        <script type='text/javascript' src='../../../librerias/datepick/jquery.datepick-es.js'></script>
        <link rel='stylesheet' type='text/css' href='../class/other/ms-Dropdown/msdropdown/dd.css' />
        <link rel='stylesheet' type='text/css' href='../class/other/ms-Dropdown/msdropdown/dd.css' />
        <link rel='stylesheet' href='../../../librerias/datepick/jquery.datepick.css' type='text/css' media='screen' charset='utf-8' />

        ";

        $administrador=$_SESSION['id_tipo_usuario'];
        if(isset($_SESSION['cedula'])){    
            $this->htm = $this->ObjCabPie->flibHtmCab(0, $ficherosjs, '', $this->ObjOther->fomArregloAsocia2($administrador), 0, "");
        }else{
            echo"<script>var pagina='classRegistro.php';                        
            alert('Disculpa la session ha expirado, debe iniciar sesion nuevamente.');
            function redireccionar() { 
                location.href=pagina;
            } 
            setTimeout ('redireccionar()', 0);
            </script>";
        }
    }

    function solicitud(){

        $id_tipo_sol=$_GET['id_tipo_sol'];

        //pasar $id_tipo_sol por solicitud()
        //dependiendo del tipo de solicitud mostrar los combos

        if($id_tipo_sol==1){

            $titulo="Seleccione el tipo de pr&eacute;stamo";
            $radios="<div align='center'>
                    <th>Grupos</th><td><input type='radio' name='identrada' id='identrada1' value='1' /></td>
                    <th>Equipos</th><td><input type='radio'  name='identrada' id='identrada2' value='2' /></td>
                    <th>Grupos y Equipos</th><td><input type='radio'  name='identrada' id='identrada3' value='9' /></td>
                    <th>Camaras Asignadas</th><td><input type='radio' name='identrada' id='identrada4' value='3' /></td>
                    </div>";
        }else{
            $titulo="Seleccione el tipo de despacho";
            $radios="<div align='center'>
                    <th>Audio</th><td><input type='radio' name='identrada' id='identrada1' value='4' /></td>
                    <th>Video</th><td><input type='radio'  name='identrada' id='identrada2' value='5' /></td>
                    <th>Iluminacion</th><td><input type='radio' name='identrada' id='identrada3' value='6' /></td>
                    <!--<th>Electricidad</th><td><input type='radio'  name='identrada' id='identrada4' value='7' /></td>-->
                    <th>Todos</th><td><input type='radio'  name='identrada' id='identrada6' value='8' /></td>
                    </div>";
        }

        $botonA = "<input type=\"button\" class='boton' value=\"Aceptar\" OnClick=solicitud('".$id_tipo_sol."');>";
        $botonC = "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('classbienvenida.php');>";

        $this->htm.="<div id='datosp' align='center'><table class='tabla' align='center' style='width:650px;' >
        <tr><th colspan='12' class='titulo' >".$titulo."</th></tr>
        <tr>".$radios."</tr>
        </table>
        <table class='tabla' style='width:650px;'>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table></div>";
    }


    function __destruct(){
        if(isset($_SESSION['cedula'])){
            $this->htm.=$this->ObjCabPie->flibCerrarHtm("");
            echo $this->htm;
        }
    }
}


$solicitud = new solicitud();
$solicitud->solicitud();
?>