<?php
ini_set("error_reporting","E_ALL & ~E_NOTICE");
$classlistadeusuarios = new classlistadeusuarios();
class classlistadeusuarios
{
	function classlistadeusuarios()
	{
		//Librerias comunes
		require("../../../librerias/classlibCabPie.php");
		// Libreria de bd
		require("../class/bd/classbdConsultas.php");
		require("../class/bd/classbdConsultasSqlServer.php");
		// Clase Other
		require("../class/other/classOtherMenu.php");
		// Clase Interfaz
		require("../../../librerias/classlibSession.php");
		require("../class/interfaz/classMensaje.php");
		////////////////////////////////////////////////////
		$this->ObjclasslibSession = new classlibSession();

		if(isset($_SESSION['cedula']))
		{		
			$this->cargarPagina();	
		}
		else{
			echo"<script>var pagina='classRegistro.php';						
			alert('Disculpa la session ha expirado, debe iniciar sesion nuevamente.');
			function redireccionar() { 
			location.href=pagina;
			} 
			setTimeout ('redireccionar()', 0);
			</script>
			";  		
		}
	}

	function cargarPagina()
	{
		$ficherosjs="
			<script type='text/javascript' src='../class/other/classjavascript.js'></script>
			<script>
			$(document).ready(function (){
			buscarusuarios();
			});
			</script>
			";
					
		$this->ObjCabPie=new classlibCabPie("LISTA DE PAUTAS","");
		$this->ObjOther=new classOtherMenu();
		$this->ObjMensaje=new classMensaje("","mostrar");
		$this->ObjclasslibSession = new classlibSession();
		$this->ObjConsulta=new classbdConsultas();
		$this->ObjConsultaSqlServer=new classbdConsultasSqlServer();
		$cedula=$_SESSION['cedula'];
		$administrador=$_SESSION['id_tipo_usuario'];
		$nombres=$_SESSION['nombres'];
		$apellidos=$_SESSION['apellidos'];
		$gerencia=$_SESSION['gerencia'];
		if(($administrador==20) or ($administrador==27)){

		$campo_busqueda="<input type='text' name='busqueda' id='busqueda' class='campo' onkeypress='return enter(event,buscarusuarios)' size='70'>";
			
		$botonB="<input type=\"button\" class='boton' value=\"Buscar\" OnClick=buscarusuarios();>";

		$htm =$this->ObjCabPie->flibHtmCab(0,$ficherosjs,'',$this->ObjOther->fomArregloAsocia2($administrador),0);
		$htm.=$this->ObjMensaje->InterfazListaPersona($campo_busqueda,$botonB);		
		$htm.=$this->ObjCabPie->flibCerrarHtm("");
		echo $htm;
		}
		else{
		echo"<script>var pagina='classRegistro.php';						
			alert('Disculpa no tiene permitido el acceso a esta pagina.');
			function redireccionar() { 
			location.href=pagina;
			} 
			setTimeout ('redireccionar()', 0);
			</script>
			";  
		}
	}

}

?>

