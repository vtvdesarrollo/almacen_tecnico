<?php
/**
 * carga de librerias y css
 */
class htmlCargador{
	private $lib;
	private $tipolibs;

	function __construct($tipo){
		$this->tipolibs=$tipo;
	}

	public function add_Arc($path){
		$this->lib[]=$path;
	}

	public function outputHTML(){
		for ($i=0;$i<count($this->lib);$i++){
			switch ($this->tipolibs){
				case "javascript":
					$html.="<script type=\"text/javascript\" src=\"".$this->lib[$i]."\"></script> \n\r";
					break;
				case "css":
					$html.="<link href=\"".$this->lib[$i]."\" rel=\"stylesheet\"> \n\r";
					break;
			}
		}
		return $html;
	}

	function __destruct(){
		//borrando datos
		unset($this->lib);
		unset($this->tipolibs);
	}
}

/**
 * Funcion que carga archivos (javascript, css) al HTML
 *
 * @param Array $Arc
 */
function cargarArchivos($Arc){
	for ($i=0;$i<count($Arc);$i++){
		$archivo=new htmlCargador(key($Arc));
		for ($c=0;$c<count($Arc[key($Arc)]);$c++){
			$archivo->add_Arc($Arc[key($Arc)][$c]);
		}
		echo $archivo->outputHTML();
		next($Arc);
	}
}

?>