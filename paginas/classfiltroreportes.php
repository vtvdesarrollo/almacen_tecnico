<?php

//ini_set("error_reporting","E_ALL & ~E_NOTICE");
require("classDirectorioFunciones.php");
////////////////////////////////////////////////////

class classfiltroreportes {

    function __construct() {
        $this->ObjclasslibSession = new classlibSession();
        $this->conect_sistemas_vtv = "../../../database/archi_conex/sistemas_vtv_5431";
        $this->ObjCabPie = new classlibCabPie("REPORTES", "");
        $this->ObjOther = new classOtherMenu();
        $this->ObjMensaje = new classMensaje("", "mostrar");
        $this->classDirectorioFunciones = new classDirectorioFunciones(true);
        
        $ficherosjs = "
            <script type='text/javascript' src='../class/other/classjavascript.js'></script>";

        $administrador=$_SESSION['id_tipo_usuario'];
        if (isset($_SESSION['cedula'])) {

            $this->htm = $this->ObjCabPie->flibHtmCab(0, $ficherosjs, '', $this->ObjOther->fomArregloAsocia2($administrador), 0, "");

        } else {
            echo"<script>var pagina='classRegistro.php';                        
            alert('Disculpa la session ha expirado, debe iniciar sesion nuevamente.');
            function redireccionar() { 
            location.href=pagina;
            } 
            setTimeout ('redireccionar()', 0);
            </script>
            ";   
        }
    }    

    function filtroreportes($metodo,$tipo){

        $this->htm.= $this->classDirectorioFunciones->$metodo($tipo);
       
    }  

    function __destruct(){
        if(isset($_SESSION['cedula'])){
            $this->htm.=$this->ObjCabPie->flibCerrarHtm("");
            echo $this->htm;
        }
    }

    
}
$classfiltroreportes = new classfiltroreportes();
$classfiltroreportes->filtroreportes('filtroreportes',$_GET['tipo']);

?>