<?php

//ini_set("error_reporting","E_ALL & ~E_NOTICE");
require("classDirectorioFunciones.php");
////////////////////////////////////////////////////

class classagregar {

    function __construct() {
        $this->ObjclasslibSession = new classlibSession();
        $this->conect_sistemas_vtv = "../../../database/archi_conex/sistemas_vtv_5431";
        $this->ObjCabPie = new classlibCabPie("AGREGAR", "");
        $this->ObjOther = new classOtherMenu();
        $this->ObjMensaje = new classMensaje("", "mostrar");
        $this->classDirectorioFunciones = new classDirectorioFunciones(true);
        
        $ficherosjs = "
            <script type='text/javascript' src='../class/other/classjavascript.js'></script>";

        $administrador=$_SESSION['id_tipo_usuario'];
        if (isset($_SESSION['cedula'])) {

            $this->htm = $this->ObjCabPie->flibHtmCab(0, $ficherosjs, '', $this->ObjOther->fomArregloAsocia2($administrador), 0, "");

        } else {
            echo"<script>var pagina='classRegistro.php';                        
            alert('Disculpa la session ha expirado, debe iniciar sesion nuevamente.');
            function redireccionar() { 
            location.href=pagina;
            } 
            setTimeout ('redireccionar()', 0);
            </script>
            ";   
        }
    }    

    function agregar($metodo,$tipo){

        $administrador = $_SESSION['id_tipo_usuario'];
        if ($administrador == 31 or $administrador == 32) {

        $this->htm.= $this->classDirectorioFunciones->$metodo($tipo);
        
        } else {
            echo"<script>var pagina='classbienvenida.php';                        
            alert('Disculpa no tiene permitido el acceso a esta pagina.');
            function redireccionar() { 
            location.href=pagina;
            } 
            setTimeout ('redireccionar()', 0);
            </script>
            ";
        }
    }  

    function __destruct(){
        if(isset($_SESSION['cedula'])){
            $this->htm.=$this->ObjCabPie->flibCerrarHtm("");
            echo $this->htm;
        }
    }

    
}
$classagregar = new classagregar();
$classagregar->agregar('guardar',$_GET['tipo']);

?>