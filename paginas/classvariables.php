<?php
//ini_set("error_reporting","E_ALL & ~E_NOTICE");
require("classDirectorioFunciones.php");
class variables{
    public $html;
    function __construct(){
        $this->ObjclasslibSession = new classlibSession();
        $this->ObjCabPie=new classlibCabPie("Solicitud ","");
        $this->ObjOther=new classOtherMenu();
        $this->ObjMensaje=new classMensaje("","mostrar");
        $this->classDirectorioFunciones = new classDirectorioFunciones(true);
        $ficherosjs = "
        <script type='text/javascript' src='../class/other/ms-Dropdown/msdropdown/js/uncompressed.jquery.dd.js'></script>
        <script type='text/javascript' src='../class/other/classjavascript.js'></script>
        <script type='text/javascript' src='../librerias/datepick/jquery.datepick.pack.js'></script>
        <script type='text/javascript' src='../librerias/datepick/jquery.datepick-es.js'></script>
        <link rel='stylesheet' type='text/css' href='../class/other/ms-Dropdown/msdropdown/dd.css' />
        <link rel='stylesheet' href='../librerias/datepick/jquery.datepick.css' type='text/css' media='screen' charset='utf-8' />

        ";

        $administrador=$_SESSION['id_tipo_usuario'];
        if(isset($_SESSION['cedula'])){    
            $this->htm = $this->ObjCabPie->flibHtmCab(0, $ficherosjs, '', $this->ObjOther->fomArregloAsocia2($administrador), 0, "");
        }else{
            echo"<script>var pagina='classRegistro.php';                        
            alert('Disculpa la session ha expirado, debe iniciar sesion nuevamente.');
            function redireccionar() { 
                location.href=pagina;
            } 
            setTimeout ('redireccionar()', 0);
            </script>";
        }
    }

    function variables(){

        $id_tipo_var=$_GET['id_tipo_var'];

        //pasar $id_tipo_sol por solicitud()
        //dependiendo del tipo de solicitud mostrar los combos

        if($id_tipo_var==1){
            $titulo="Nuevo Estado";
        }elseif($id_tipo_var==2){
            $titulo="Nueva Unidad de medida";
        }elseif($id_tipo_var==3){
            $titulo="Nueva Categoria";
        }elseif($id_tipo_var==4){
            $titulo="Nuevo Programa";
        }else{
            $titulo="Nuevo Tipo Descripcion";
            $tipoart ="<select id='tipoart' style='width:180px;'>
                    <option value='0' selected='selected' >Seleccione</option>
                    <option value='1'>Prestamo</option>
                    <option value='2'>Consumible</option>
                    </select>";
            $tipo_art="<th>Tipo de articulo:</th><td>" . $tipoart . "</td>";
        }

        $variables="<tr><th colspan='8'><div align='center'>Descripci&oacute;n: <input type='text' name='variable' id='variable' class='campo' size='30' /></div></th></tr></div></tr>
                    <tr>".$tipo_art."</tr>";
        
        $botonA = "<input type=\"button\" class='boton' value=\"Aceptar\" OnClick=nuevavariable(".$id_tipo_var.");>";
        $botonC = "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('classbienvenida.php');>";

        $this->htm.="<div id='datosp' align='center'><table class='tabla' align='center' style='width:500px;' >
        <tr><th colspan='8' class='titulo' >".$titulo."</th></tr>
        <tr>".$variables."</tr>
        </table>
        <table class='tabla' style='width:500px;'>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table></div>";
    }


    function __destruct(){
        if(isset($_SESSION['cedula'])){
            $this->htm.=$this->ObjCabPie->flibCerrarHtm("");
            echo $this->htm;
        }
    }
}


$variables = new variables();
$variables->variables();
?>