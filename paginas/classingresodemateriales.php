<?php
//ini_set("error_reporting","E_ALL & ~E_NOTICE");
require("classDirectorioFunciones.php");
class ingresodemateriales{
    public $html;
    function __construct(){
        $this->ObjclasslibSession = new classlibSession();
        $this->ObjCabPie=new classlibCabPie("Entrada ","");
        $this->ObjOther=new classOtherMenu();
        $this->ObjMensaje=new classMensaje("","mostrar");
        $this->classDirectorioFunciones = new classDirectorioFunciones(true);
        $ficherosjs = "
        <script type='text/javascript' src='../class/other/ms-Dropdown/msdropdown/js/uncompressed.jquery.dd.js'></script>
        <script type='text/javascript' src='../class/other/classjavascript.js'></script>
        <script type='text/javascript' src='../../../librerias/datepick/jquery.datepick.pack.js'></script>
        <script type='text/javascript' src='../../../librerias/datepick/jquery.datepick-es.js'></script>
        <link rel='stylesheet' type='text/css' href='../class/other/ms-Dropdown/msdropdown/dd.css' />
        <link rel='stylesheet' href='../../../librerias/datepick/jquery.datepick.css' type='text/css' media='screen' charset='utf-8' />

        <script type=\"text/javascript\">
            function customRange(input) { 
            return {minDate: 'today'};  
            }
        $(document).ready(function(){
            $('#fecha').datepick({beforeShow: customRange, showOn: 'both', buttonImageOnly: true, buttonImage: '../../../estilos/imagenes/estatus/calendar.png'}); 
            })
        </script>";

        $administrador=$_SESSION['id_tipo_usuario'];

        if(isset($_SESSION['cedula'])){    
            $this->htm = $this->ObjCabPie->flibHtmCab(0, $ficherosjs, '', $this->ObjOther->fomArregloAsocia2($administrador), 0, "");
        }else{
            echo"<script>var pagina='classRegistro.php';                        
            alert('Disculpa la session ha expirado, debe iniciar sesion nuevamente.');
            function redireccionar() { 
                location.href=pagina;
            } 
            setTimeout ('redireccionar()', 0);
            </script>";
        }
    }

    function modulo($metodo, $arg){

        $this->htm.= $this->classDirectorioFunciones->$metodo($arg);
    }

    function __destruct(){
        if(isset($_SESSION['cedula'])){
            $this->htm.=$this->ObjCabPie->flibCerrarHtm("");
            echo $this->htm;
        }
    }
}

$ingresodemateriales = new ingresodemateriales();
$ingresodemateriales->modulo("ingresodemateriales", $_GET['idsolicitud']);
?>