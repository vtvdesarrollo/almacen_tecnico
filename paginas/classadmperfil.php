<?php

//ini_set("error_reporting","E_ALL & ~E_NOTICE");
$classadmperfil = new classadmperfil();

class classadmperfil {

    function classadmperfil() {
        //Librerias comunes
        require("../../../librerias/classlibCabPie.php");
        // Libreria de bd
        require("../class/bd/classbdConsultas.php");
        require("../class/bd/classbdConsultasSqlServer.php");
        // Clase Other
        require("../class/other/classOtherMenu.php");
        // Clase Interfaz
        require("../../../librerias/classlibSession.php");
        require("../class/interfaz/classMensaje.php");
        ////////////////////////////////////////////////////
        $this->ObjclasslibSession = new classlibSession();
        $this->conect_sistemas_vtv = "../../../database/archi_conex/sistemas_vtv_5431";


        /* if(isset($_SESSION['cedula']))
          { */
        $this->cargarPagina();
        /* }
          else{
          echo"<script>var pagina='classRegistro.php';
          alert('Disculpa la session ha expirado, debe iniciar sesion nuevamente.');
          function redireccionar() {
          location.href=pagina;
          }
          setTimeout ('redireccionar()', 0);
          </script>
          ";
          } */
    }

    function cargarPagina() {
        $ficherosjs = "
			<script type='text/javascript' src='../class/other/classjavascript.js'></script>";

        $this->ObjCabPie = new classlibCabPie("CAMBIO DE PERFIL DE USUARIOS", "");
        $this->ObjOther = new classOtherMenu();
        $this->ObjMensaje = new classMensaje("", "mostrar");
        $this->ObjclasslibSession = new classlibSession();
        $this->ObjConsulta = new classbdConsultas();
        $this->ObjConsultaSqlServer = new classbdConsultasSqlServer();
        $cedula = $_SESSION['cedula'];
        $administrador = $_SESSION['id_tipo_usuario'];
        $nombres = $_SESSION['nombres'];
        $apellidos = $_SESSION['apellidos'];
        $ced = base64_decode($_GET['cedula']);

        if (($administrador == 31) or ($administrador == 32)) {
            $datosusuario = $this->ObjConsulta->selectusuario($this->conect_sistemas_vtv, $ced);
            $nombre = $datosusuario[1][1];
            $apellido = $datosusuario[1][2];

            $idtipousuario = $this->ObjConsulta->selectidtipousuario($this->conect_sistemas_vtv, $ced);
            $id_tipousuario = $idtipousuario[1][1];

            $datostipousuario = $this->ObjConsulta->selecttipousuario($this->conect_sistemas_vtv, $id_tipousuario);
            $tipousuario = $datostipousuario[1][1];

            $cedula = "<input type='text' value='$ced' name='cedula' id='cedula' class='campo' size='8' maxlegth='8'/>";

            $id_aplicacion=15;
            
            if ($administrador == 31 or $administrador == 32) {

              $tipo_usuarios = $this->ObjConsulta->selecttipousuarios($this->conect_sistemas_vtv, $id_aplicacion);
              $perfilmod = "<select id='perfilmod' name='perfilmod' style='width:105px;'>";
              $perfilmod.="<option value='0' selected >Seleccione </option>";
              foreach ($tipo_usuarios as $llave => $valor) {
              $perfilmod.="<option value='" . $valor[1] . "' $selected >" . $valor[2] . "</option>";
              }
              $perfilmod.="</SELECT> ";

            }else{

              $tipo_usuarios = $this->ObjConsulta->selecttipousuariosadmin($this->conect_sistemas_vtv, $id_aplicacion);
              $perfilmod = "<select id='perfilmod' name='perfilmod' style='width:105px;'>";
              $perfilmod.="<option value='0' selected >Seleccione </option>";
              foreach ($tipo_usuarios as $llave => $valor) {
                $perfilmod.="<option value='" . $valor[1] . "' $selected >" . $valor[2] . "</option>";
              }
              $perfilmod.="</SELECT> ";

            }

            $botonA = "<input type=\"button\" class='boton' value=\"Cambiar\" OnClick=cambioperfil($ced,$id_tipousuario);>";
            $botonC = "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('classlista.php?modulo=listadeusuarios');>";
            $botonB = "<input type=\"button\" class='boton' value=\"Buscar\" OnClick=buscarpersona();>";
            $titulo1 = "DATOS DEL USUARIO";
            $titulo2 = "CAMBIO DE PERFIL";


            $htm = $this->ObjCabPie->flibHtmCab(0, $ficherosjs, '', $this->ObjOther->fomArregloAsocia2($administrador), 0, "");
            $htm.=$this->ObjMensaje->interfazadmperfil($ced, $nombre, $apellido, $tipousuario, $perfilmod, $titulo1, $titulo2, $botonA, $botonC);
            $htm.=$this->ObjCabPie->flibCerrarHtm("");
            echo $htm;
        } else {
            echo"<script>var pagina='classbienvenida.php';						
			alert('Disculpa no tiene acceso a esta pagina');
			function redireccionar() { 
			location.href=pagina;
			} 
			setTimeout ('redireccionar()', 0);
			</script>
			";
        }
    }

}

?>
