<?php
/**
* librerias comunes...
*/
include("../librerias/classlibSession.php");
include("../class/bd/classbdConsultas.php");
include("../class/bd/classbdConsultasSqlServer.php");
include("../class/interfaz/classMensaje.php");
include("class.phpmailer.php");
include("../librerias/classlibCabPie.php");
include("../librerias/classlibFecHor.php");
include("../class/other/classOtherMenu.php");

function modhb_implode($relleno, $array) {
	while ($data = current($array)) {
		$string[] = str_replace("_", " ", (key($array))) . ': ' . $data;
		next($array);
	}
	return (implode($relleno, $string));
}

class classDirectorioFunciones {
	
	function __construct($nocab=false){
		$this->ObjConsulta = new classbdConsultas();
		$this->ObjMensaje = new classMensaje("", "");
		$this->enviomail = new PHPMailer();
		$this->Objfechahora = new classlibFecHor();

		$this->conect_sistemas_vtv = "../database/archi_conex/sistemas_vtv_5431";
		//sigesp para datos de personas.
		$this->conect_sigesp = "../database/archi_conex/sistema_sigesp";
		/**
		*configuracion de correo..
		**/
		$this->enviomail->From = "almacentecnico@vtv.gob.ve";
		$this->enviomail->Host = '172.16.8.8';
		$this->enviomail->Mailer = "smtp";
		$this->enviomail->FromName = "Sistema de Almacen Tecnico";
		$this->enviomail->AltBody = "EL SERVICIO DE MENSAJERIA REQUERE UN MANEJADOR DE CORREO MODERNO";
		if($nocab==false){
			$this->ObjCabPie = new classlibCabPie("", "");
			$this->ObjSesion = new classlibSession();
			$this->Objfechahora = new classlibFecHor();
			if (isset($_GET['obtenerFuncion'])) {
				$this->$_GET['obtenerFuncion']();
			}else if (isset($_POST['obtenerFuncion'])) {
				$this->$_POST['obtenerFuncion']();
			}
		}

	}
	

	/////////////////////////////////////Funciones de correo///////////////////////////////////////////////////////////
	/**
	* Esta funcion se encarga de enviar correo de forma general.
	*/
	function enviarcorreo($correoaenviar, $datospauta, $asunto) {
		$this->enviomail->Subject = $asunto;
		$htmltable = $datospauta;
		$this->enviomail->Body = $htmltable;
		$this->enviomail->AddAddress($correoaenviar, "");
		if ($this->enviomail->Send()) {
			$this->enviomail->ClearAddresses();
		}
		$this->enviomail->ClearAttachments();
	}

	/**
	*esta funcion se encarga de manejar las peticiones de correo en ajax
	*/
	function correoenviado() {
		$pin = $_GET['pin'];
		$correo = $_GET['correo'];
		$tope = $_GET['tope'];
		$this->enviarcorreo($correo, $pin, $tope);
		$mensaje = "<div style='color: #009900;font-weight: bold;' align='center'><br>Correo Enviado Satisfactoriamente<div><br>";
		$tabla = "
		<table class='tabla'>
		<tr><td>" . $mensaje . "</td></tr>
		</table>";
		echo $tabla;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//Esta funcion valida que el usuario tenga acceso al sistema 
	function validaringresousuario() {
		$cedula = $_GET['login'];
		$clave = $_GET['clave'];
		$id_aplicacion = 15;
		$id_modulo = "";
		$id_tipo_usuario = "";
		$id_submodulo = "";
		$datosusuario = $this->ObjConsulta->usuario($this->conect_sistemas_vtv, $cedula, $clave, $id_aplicacion, $id_modulo, $id_tipo_usuario, $id_submodulo);
		if (count($datosusuario) > 0) {// crea sesion
			/*$datospersonalesusuario = $this->ObjConsulta->datospersonalesusuario($this->conect_sistemas_vtv, $cedula);*/
			$campos['id_acceso'] = $datosusuario[1][1];
			$campos['cedula'] = $datosusuario[1][2];
			$campos['clave'] = $datosusuario[1][3];
			$campos['id_modulo'] = $datosusuario[1][4];
			$campos['id_aplicacion'] = $datosusuario[1][5];
			$campos['niv_con'] = $datosusuario[1][6];
			$campos['niv_eli'] = $datosusuario[1][7];
			$campos['niv_inc'] = $datosusuario[1][8];
			$campos['niv_mod'] = $datosusuario[1][9];
			$campos['ced_trans'] = $datosusuario[1][10];
			$campos['fecha_trans'] = $datosusuario[1][11];
			$campos['fecha_exp'] = $datosusuario[1][12];
			$campos["id_tipo_usuario"] = $datosusuario[1][13];
			$campos["id_submodulo"] = $datosusuario[1][14];
			$campos["nombres"] = $datospersonalesusuario[1][2];
			$campos["apellidos"] = $datospersonalesusuario[1][3];
			
			$this->ObjSesion->flibCrearVariable($campos, 1);
			$cedula = $_SESSION['cedula'];
			$id_tipo_usuario = $_SESSION['id_tipo_usuario'];
			$nombres = $_SESSION['nombres'];
			$apellidos = $_SESSION['apellidos'];
			echo "<script type='text/javascript'>CancelarRegresar('classbienvenida.php');</script>";
		} else {

			echo "<script type='text/javascript'>CancelarRegresar('classRegistro.php?error=si');</script>";
		}
	}
	
	//funcion destinada a crear la ventana de bienvenida de las paginas.
	function bienvenida(){
		
		$datausuario = $this->ObjConsulta->selectusuario($this->conect_sistemas_vtv, $_SESSION['cedula']);
		$t_acceso = $this->ObjConsulta->selectexisteacceso($this->conect_sistemas_vtv, $_SESSION['cedula'], 15);
		$desc_t_acceso = $this->ObjConsulta->selecttipousuario($this->conect_sistemas_vtv,  $t_acceso[1][2]);
		$htm=" <table border='0' class='tabla' style='width: 40%; height:50px; float: center'>
        <thead>
        <tr>
        <th colspan='2'>Bienvenido al sistema</th>
        </tr>
        </thead>
        <tbody>
        <tr>
        <td rowspan='4' colspan='1' style='text-align: center;''><img width='70px' height='70px' src='http://intranet/sistemas/directorio/paginas/download_foto.php?id=".base64_encode($_SESSION['cedula'])."' /></td>
        <td><b>  " . strtoupper($datausuario[1][1])." ".strtoupper($datausuario[1][2]) . "</b> </td>
        <tr><td><b>Su Perfil es:</b> " . $desc_t_acceso[1][1] . "</td></tr>
        </div>
        </td>
        </tr>
        </tbody>
        </table>";
		//$htm=$this->ObjMensaje->interfazbienvenida($datausuario[1][1]." ".$datausuario[1][2]);
		return $htm;	
	}

	//esta funcion permite a los usuarios recordar la clave al correo electronico...
	function olvidocontrasena($cedula=null, $imprimemensaje=null, $noboton=null) {
		if($cedula==null){
			$cedula = $_GET['cedula'];
		}
		
		$claveusuario = $this->ObjConsulta->selectclave($this->conect_sistemas_vtv, $cedula);
		$clave = $claveusuario[1][1];

		if ($clave == "") {
			$javascript="<script>var pagina='classRegistro.php';                        
			alert('Disculpe la cedula ingresada no tiene acceso al sistema.');
			function redireccionar() { 
				location.href=pagina;
			} 
			setTimeout ('redireccionar()', 0);
			</script>
			";
			if($imprimemensaje==null){
				echo $javascript;
			}else{
				return $javascript;
			}
		} else {
			$datocorreo = $this->ObjConsulta->selectdatocorreo($this->conect_sistemas_vtv, $cedula);
			$correo = $datocorreo[1][1];
			if ($correo == "") {
				$javascript="<script>var pagina='classRegistro.php';                        
				alert('Disculpe no tiene asociada una direccion de correo.');
				function redireccionar() { 
					location.href=pagina;
				} 
				setTimeout ('redireccionar()', 0);
				</script>
				";
				if($imprimemensaje==null){
					echo $javascript;
				}else{
					return $javascript;
				}
			} else {
				$datoscorreo = "Su clave usuario es: " . $clave . " , se le recomienda por seguridad cambiarla al ingresar al sistema.<br /> <br />Gerencia de Tecnolog&iacute;a de la Informaci&oacute;n y la Comunicaci&oacute;n.<br />
				";
				$this->enviarcorreo($correo, $datoscorreo, "SU CLAVE AL CORREO");
				if($noboton==null){
					$botonA = "<input type=\"button\" class='boton' value=\"Aceptar\" OnClick=CancelarRegresar('classRegistro.php');>";
				}
				$mensaje = "<div style='color: #009900;font-weight: bold;'><br>Su clave usuario se a enviado con exito a su e_mail: " . $correo . "<div><br>" . $botonA . "<br>";
				
				if($imprimemensaje==null){
					echo $this->ObjMensaje->InterfazExitosamente($mensaje);
				}else{
					return $this->ObjMensaje->InterfazExitosamente($mensaje);
				}
			}
		}
	}


	/////////////////////////////////////Funciones de administracion de usuarios/////////////////////////////////////////
	
	//Esta funcion es para verificar si un usuario ya existe y si tiene permiso en la aplicacion, si no existe: se crea el usuario y se
	//le da acceso, si ya el usuario existe: se verifica si tiene acceso, si no lo tiene se le da.
	function guardarusuario() {
		$tipousuario = $_GET['tipousuario'];
		$nombre = strtoupper(utf8_encode($_GET['nombre']));
		$apellido = strtoupper(utf8_encode($_GET['apellido']));
		$cedula = $_GET['cedula'];
		$idgerencia = 1;
		$iddivision = 1;
		$telefono1 = $_GET['telefono1'];
		$telefono2 = $_GET['telefono2'];
		$clave1 = $_GET['clave1'];
		$correo = strtolower($_GET['correo']);
		$ced_sesion = $_SESSION['cedula'];
		$id_aplicacion = 15; //idaplicacion que le damos.
		$id_modulo = 110; //idmodulo que le damos en sistema.

		$t_datospersonales = $this->ObjConsulta->selectexiste($this->conect_sistemas_vtv, $cedula);
		if ((count($t_datospersonales) > 0)) {
			if ($correo != "") {
				$updatecorreo = $this->ObjConsulta->updatecorreo($this->conect_sistemas_vtv, $cedula, $correo);
			}
			if ($telefono1 != "") {
				$updatetlf1bd = $this->ObjConsulta->updatetlf1($this->conect_sistemas_vtv, $cedula, $telefono1);
			}

			if ($telefono2 != "" and $telefono2 != "undefined") {
				$updatetlf2bd = $this->ObjConsulta->updatetlf2($this->conect_sistemas_vtv, $cedula, $telefono2);
			}
			$t_acceso = $this->ObjConsulta->selectexisteacceso($this->conect_sistemas_vtv, $cedula, $id_aplicacion);
			if ((count($t_acceso) == 0)) {
				$insertclaveusuarios1 = $this->ObjConsulta->insertclaveusuarios($this->conect_sistemas_vtv, $cedula, $clave1, $id_modulo, $ced_sesion, $tipousuario, $idgerencia, $iddivision, $id_aplicacion);
			}

			//$olvidoclave=$this->olvidocontrasena($cedula, $imprimemensaje="imprime","sinboton");
			$mensaje = "<div style='color: #009900;font-weight: bold;'><br>El usuario " . $nombre . "&nbsp;" . $apellido . " fue actualizado con exito<div><br>";
			echo $this->ObjMensaje->InterfazExitosamente($mensaje);
			echo"<script>var pagina='classlista.php?modulo=listadeusuarios';                      
			function redireccionar() { 
				location.href=pagina;
			} 
			setTimeout ('redireccionar()', 3000);
			</script>";
		} else {
			//ingresar datos personales nuevos y agregar acceso.
			$insertusuarios1 = $this->ObjConsulta->insertusuarios($this->conect_sistemas_vtv, $nombre, $apellido, $cedula, $correo, $telefono1, $telefono2);
			//revisar acceso..
			$t_acceso = $this->ObjConsulta->selectexisteacceso($this->conect_sistemas_vtv, $cedula, $id_aplicacion);
			if ((count($t_acceso) == 0)) {
				$insertclaveusuarios1 = $this->ObjConsulta->insertclaveusuarios($this->conect_sistemas_vtv, $cedula, $clave1, $id_modulo, $ced_sesion, $tipousuario, $idgerencia, $iddivision, $id_aplicacion);
			}

			//$olvidoclave=$this->olvidocontrasena($cedula, $imprimemensaje="imprime","sinboton");
			$mensaje = "<div style='color: #009900;font-weight: bold;'><br>El usuario " . $nombre . "&nbsp;" . $apellido . " fue ingresado con exito<div><br>";

			echo $this->ObjMensaje->InterfazExitosamente($mensaje);
			echo"<script>var pagina='classlista.php?modulo=listadeusuarios';                      
			function redireccionar() { 
				location.href=pagina;
			} 
			setTimeout ('redireccionar()', 3000);
			</script>";
		}
	}

	//Esta funcion es para eliminar el acceso a la aplicacion del usuario $_GET['cedula'] realizando un 
	//update en la fecha de expiracion, no se elimina el usuario.
	function eliminarusuario() {
		$cedula = $_GET['cedula'];
		$id_aplicacion = 15;
		$datosusuario = $this->ObjConsulta->selectusuario($this->conect_sistemas_vtv, $cedula);
		$nombre = $datosusuario[1][1];
		$apellido = $datosusuario[1][2];
		$correo = $datosusuario[1][3];
		$elimusuario = $this->ObjConsulta->updateusuario($this->conect_sistemas_vtv, $cedula, $id_aplicacion);
		$mensaje = "<div style='color: #009900;font-weight: bold;'><br>El usuario " . $nombre . "&nbsp;" . $apellido . " fue eliminado con exito<div><br>";
		//$DatosCorreo = "Aviso de eliminacion de usuario del sistema de facilidades tecnicas, comuniquese con su administrador local.";
		//$this->enviarcorreo($correo, $DatosCorreo, "Su usuario ha sido eliminado del sistema");
		echo $this->ObjMensaje->InterfazExitosamente($mensaje);

		echo"<script>var pagina='classlista.php?modulo=listadeusuarios';                     
		function redireccionar() { 
			location.href=pagina;
		} 
		setTimeout ('redireccionar()', 3000);
		</script>";
	}

	//Esa funcion realiza el cambio de perfil de los usuarios mediante un update en la tabla de acceso al sistema
	function cambioperfil() {
		$cedula = $_GET['cedula'];
		$perfilmod = $_GET['perfilmod'];
		$perfilact = $_GET['perfilact'];
		$datosusuario = $this->ObjConsulta->selectusuario($this->conect_sistemas_vtv, $cedula);
		$nombre = utf8_encode($datosusuario[1][1]);
		$apellido = utf8_encode($datosusuario[1][2]);
		$correo = $datosusuario[1][3];
		$cambiarperfil = $this->ObjConsulta->updatecambiarperfil($this->conect_sistemas_vtv, $cedula, $perfilact, $perfilmod);
		$descperfil = $this->ObjConsulta->selectdescperfil($this->conect_sistemas_vtv, $perfilmod);
		$desc = $descperfil[1][1];

		//$DatosCorreo = "Aviso de cambio de perfil en el sistema, comuniquese con el administrador local y consulte el manual de sistema sobre las acciones que podra realizar sobre el perfil ". ucwords($desc);
		//$this->enviarcorreo($correo, $DatosCorreo, "Aviso de cambio de perfil");
		$mensaje = "<div style='color: #009900;font-weight: bold;'><br>El nuevo perfil del usuario  " . $nombre . "&nbsp;" . $apellido . " es: " . ucwords($desc) . "<div><br>";
		echo $this->ObjMensaje->InterfazExitosamente($mensaje);
		echo"<script>var pagina='classlista.php?modulo=listadeusuarios';                     
		function redireccionar() { 
			location.href=pagina;
		} 
		setTimeout ('redireccionar()', 3000);
		</script>
		";
	}
	//Esta funcion realiza el cambio de clave del usuario $cedula = $_SESSION['cedula']; con un update en la
	// tabla de acceso del sistema
	function cambioclave() {
		if (isset($_SESSION['cedula'])) {
			$claveactual = $_GET['claveactual'];
			$clavenueva = $_GET['clavenueva'];
			$cedula = $_SESSION['cedula'];
			$datosusuario = $this->ObjConsulta->usuario($this->conect_sistemas_vtv, $cedula, $claveactual, 15, 110, "", "");
			if (count($datosusuario) > 0) {
				$id_acceso = $datosusuario[1][1];
				$cedula = $datosusuario[1][2];
				$clave = $datosusuario[1][3];
				$id_modulo = $datosusuario[1][4];
				$id_aplicacion = $datosusuario[1][5];
				$niv_con = $datosusuario[1][6];
				$niv_eli = $datosusuario[1][7];
				$niv_inc = $datosusuario[1][8];
				$niv_mod = $datosusuario[1][9];
				$ced_trans = $datosusuario[1][10];
				$fecha_trans = $datosusuario[1][11];
				$fecha_exp = $datosusuario[1][12];
				$id_tipo_usuario = $datosusuario[1][13];
				$id_submodulo = $datosusuario[1][14];
			
				$updateexpirausuario = $this->ObjConsulta->updateexpirausuario($this->conect_sistemas_vtv, $cedula, $clave, $id_aplicacion, $id_modulo, $id_tipo_usuario, $id_submodulo);
				$insertusuario = $this->ObjConsulta->insertusuario($this->conect_sistemas_vtv, $cedula, $clavenueva, $id_aplicacion, $id_modulo, $id_tipo_usuario, "");
				//$olvidoclave=$this->olvidocontrasena($cedula, $imprimemensaje="imprime","sinboton");
				$mensaje = "<div style='color: #009900;font-weight: bold;'><br>Cambio de Clave Satisfactoria<div><br>".$olvidoclave;
				echo $this->ObjMensaje->InterfazExitosamente($mensaje);
				echo"<script>var pagina='classbienvenida.php';                      
				function redireccionar() { 
					location.href=pagina;
				} 
				setTimeout ('redireccionar()', 3000);
				</script>
				";
			} else {
				echo"<script>var pagina='classcambioclave.php';                     
				jAlert('Disculpa la clave actual es incorrecta');
				function redireccionar() { 
					location.href=pagina;
				} 
				setTimeout ('redireccionar()', 3000);
				</script>
				";
			}
		} else {
			echo"<script>var pagina='classRegistro.php';                        
			alert('Disculpa la session ha expirado, debe iniciar session nuevamente.');
			function redireccionar() { 
				location.href=pagina;
			} 
			setTimeout ('redireccionar()', 0);
			</script>
			";
		}
	}

	//esta funcion permite reiniciar la clave a los usuarios y a su vez envia un 
	//correo usando los metodos de olvido de contraseña.
	function reiniciar_clave() {
		$cedula = $_GET['cedula'];
		$datosusuario = $this->ObjConsulta->selectusuario($this->conect_sistemas_vtv, $cedula);
		$nombre = $datosusuario[1][1];
		$apellido = $datosusuario[1][2];
		$reiniciarclave = $this->ObjConsulta->updateclave($this->conect_sistemas_vtv, $cedula);
		$mensaje = "<div style='color: #009900;font-weight: bold;'><br>La clave del usuario " . $nombre . "&nbsp;" . $apellido . " fue reiniciada con exito<div><br>";
		echo $this->ObjMensaje->InterfazExitosamente($mensaje);
		echo "<script>var pagina='classlista.php?modulo=listadeusuarios';                        
		function redireccionar() { 
			location.href=pagina;
		} 
		setTimeout ('redireccionar()', 3500);
		</script>";
	}


	//////////////////////////////Funciones de administracion de los articulos///////////////////////////////////////////
	//Esta funcion se encarga de mostrar los formularios para la recoleccion de datos necesarios para registrar el articulo
	function guardar() {
		$tipo = $_GET['tipo'];
		$cedula = $_SESSION['cedula'];
        $administrador = $_SESSION['id_tipo_usuario'];
        $nombres = $_SESSION['nombres'];
        $apellidos = $_SESSION['apellidos'];
 
        $descripcion_grupo = $this->ObjConsulta->grupos($this->conect_sistemas_vtv);
        $grupos = "<select id='grupo' name='grupo' style='width:105px;'>";
		$grupos.="<option value='0' selected >Seleccione </option>";
       	foreach ($descripcion_grupo as $llave => $valor) {
        		$grupos.="<option value='" . $valor[1] . "' $selected >" . $valor[2] . "</option>";
        	}
        $grupos.="</SELECT> "; 

        $descripcion_categoria = $this->ObjConsulta->categoria($this->conect_sistemas_vtv);
        $categoria = "<select id='categoria' name='categoria' style='width:105px;'>";
		$categoria.="<option value='0' selected >Seleccione </option>";
       	foreach ($descripcion_categoria as $llave => $valor) {
        		$categoria.="<option value='" . $valor[1] . "' $selected >" . $valor[2] . "</option>";
        	}
        $categoria.="</SELECT> "; 

        $descripcion_estado = $this->ObjConsulta->estado($this->conect_sistemas_vtv);
        $estado = "<select id='estado' name='estado' style='width:105px;'>";
		$estado.="<option value='0' selected >Seleccione </option>";
       	foreach ($descripcion_estado as $llave => $valor) {
        		$estado.="<option value='" . $valor[1] . "' $selected >" . $valor[2] . "</option>";
        	}
        $estado.="</SELECT> ";

        $desc_unidad_medidacant = $this->ObjConsulta->unidad_medida($this->conect_sistemas_vtv);
        $unidad_medidascant = "<select id='unidad_medida' name='unidad_medida' style='width:105px;'>";
		$unidad_medidascant.="<option value='0' selected >Seleccione </option>";
       	foreach ($desc_unidad_medidacant as $llave => $valor) {
        		$unidad_medidascant.="<option value='" . $valor[1] . "' $selected >" . $valor[2] . "</option>";
        	}
        $unidad_medidascant.="</SELECT> ";		

        
        if($tipo == 1){
        	$titulo = "AGREGAR USUARIO";
        	$id_aplicacion=15;
        	if ($administrador == 31) {
        		$tipo_usuarios = $this->ObjConsulta->selecttipousuarios($this->conect_sistemas_vtv, $id_aplicacion);
        		$tipo_usuario = "<select id='tipousuario' name='tipousuario' style='width:105px;'>";
				$tipo_usuario.="<option value='0' selected >Seleccione </option>";
       			foreach ($tipo_usuarios as $llave => $valor) {
        			$tipo_usuario.="<option value='" . $valor[1] . "' $selected >" . $valor[2] . "</option>";
        		}
        		$tipo_usuario.="</SELECT> ";

            }else{

            	$tipo_usuarios = $this->ObjConsulta->selecttipousuariosadmin($this->conect_sistemas_vtv, $id_aplicacion);
        		$tipo_usuario = "<select id='tipousuario' name='tipousuario' style='width:105px;'>";
				$tipo_usuario.="<option value='0' selected >Seleccione </option>";
       			foreach ($tipo_usuarios as $llave => $valor) {
        			$tipo_usuario.="<option value='" . $valor[1] . "' $selected >" . $valor[2] . "</option>";
        		}
        		$tipo_usuario.="</SELECT> ";

            }

        	$telefono1 = "<input type='text' name='telefono1' id='telefono1' class='campo' onkeypress='return validaN(event)' size='11' maxlength='11'/>";
        	$telefono2 = "<input type='text' name='telefono2' id='telefono2' class='campo' onkeypress='return validaN(event)' size='11' maxlength='11'/>";
        	$datos= "<div id='datosp' align='center'><table class='tabla' align='center' style='width:500px;' >
        	<tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        	<tr><th>C&eacute;dula:</th><td><input type='text' name='cedula' id='cedula' class='campo' size='8' maxlength='8' onkeypress='return validaN(event)' /></td></tr>
        	<tr><th>Nombre:</th><td><input type='text' name='nombre' id='nombre' class='campo' size='25'/></td></tr>
        	<tr><th>Apellido:</th><td><input type='text' name='apellido' id='apellido' class='campo' size='25'/></td></tr>
        	<tr><th>Tipo de usuario:</th><td>" . $tipo_usuario . "</td></tr>
        	<tr><th>Telefono celular:</th><td>" . $telefono1 . "</td></tr>
        	<tr><th>Telefono opcional:</th><td>" . $telefono2 . "</td></tr>
        	<tr><th>Correo:</th><td><input type='text' name='correo' id='correo' class='campo' size='25' maxlength='40'/></td></tr>
        	<tr><th>Clave:</th><td><input type='password' name='clave1' id='clave1' class='campo' size='25' maxlength='6'/></td></tr>
        	<tr><th>Confirmar clave:</th><td><input type='password' name='clave2' id='clave2' class='campo' size='25' maxlength='6'/></td></tr>
   			</table>";
   			$botonA = "<input type=\"button\" class='boton' value=\"Agregar\" OnClick=guardarusuario();>";
   			$lista = 'classlista.php?modulo=listadeusuarios';

        }elseif($tipo == 2){

        	$titulo = "AGREGAR MATERIAL DE ALMACEN";
        	

            $tipoart ="<select id='tipoart' >
                    <option value='0' selected='selected' >Seleccione</option>
			        <option value='1'>Prestamo</option>
                    <option value='2'>Consumible</option>
                    </select>";      

            $tipodescripcion = $this->ObjConsulta->selecttipodescripcion($this->conect_sistemas_vtv);
        		$tipo_desc = "<select id='tipodescripcion' name='tipodescripcion' style='width:105px;'>";
				$tipo_desc.="<option value='0' selected >Seleccione </option>";
       			foreach ($tipodescripcion as $llave => $valor) {
        			$tipo_desc.="<option value='" . $valor[1] . "' $selected >" . $valor[2] . "</option>";
        		}
        		$tipo_desc.="</SELECT> ";

			//$boton_Ag =  "<button style=\"width:12px;height:12px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' title=\"Agregar tipo\" name='button' id='button' OnClick=CancelarRegresar('classvariables.php?id_tipo_var=5');><img src='../estilos/imagenes/estatus/nuevo.gif' style=\"width:15px;height:15px;border-width:none;border-style:none;\" ></button>";

   			$datos= "<div id='datosp' align='center'><table class='tabla' align='center' style='width:500px;' >
        	<tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        	<tr><th>Tipo:</th><td>" . $tipoart . "</td></tr>
        	<tr><th>Categoria:</th><td>" . $categoria . "</td></tr>
        	<tr><th>Tipo Descripci&oacute;n:</th><td>" . $tipo_desc . " " . $boton_Ag . "</td></tr>
        	<tr><th>Descripci&oacute;n:</th><td><input type='textarea' name='descripcion' id='descripcion' class='campo' size='20'/></td></tr>
        	<tr><th>Marca:</th><td><input type='text' name='marca' id='marca' class='campo' /></td></tr>
        	<tr><th>Modelo:</th><td><input type='text' name='modelo' id='modelo' class='campo' /></td></tr>
        	<tr><th>Serial:</th><td><input type='text' name='serial' id='serial' class='campo' /></td></tr>
        	<tr><th>Bien Nac.:</th><td><input type='text' name='biennac' id='biennac' class='campo' /></td></tr>
        	<tr><th>Siglas:</th><td><input type='text' name='siglas' id='siglas' class='campo' /></td></tr>
        	<tr><th>Grupo:</th><td>" . $grupos . "</td></tr>
        	<tr><th>Estante:</th><td><input type='text' name='estante' id='estante' class='campo' /></td></tr>
        	<tr><th>Pelda&ntilde;o:</th><td><input type='text' name='peldano' id='peldano' class='campo' /></td></tr>
        	<tr><th>Cantidad:</th><td><input type='text' name='cantidad' id='cantidad' class='campo' onkeypress='return validaN(event)' size='5' />" . $unidad_medidascant . "</td></tr>
        	<tr><th>Cantidad Minima en Exixtencia:</th><td><input type='text' name='cantidadmin' id='cantidadmin' class='campo' onkeypress='return validaN(event)' size='5' /></td></tr>
        	<tr><th>Costo (Ej:12345.67):</th><td><input type='text' name='costo' id='costo' class='campo' size='5' /></td></tr>
        	<tr><th>Estado:</th><td>" . $estado . "</td></tr>
        	<tr><th>Observaci&oacute;n:</th><td><textarea name='observacion' rows='5' cols='50' id='observacion' class='campo' ></textarea></td></tr>
        	</table>";
   			$botonA = "<input type=\"button\" class='boton' value=\"Agregar\" OnClick=guardamaterialal();>";
   			$lista = 'classlista.php?modulo=listadealmacen';
        	
        }elseif($tipo == 3){

        	$titulo = "AGREGAR GRUPO";
        	$asig ="<select id='asig' >
                    <option value='0' selected='selected' >Seleccione</option>
			        <option value='1'>Asignable</option>
                    <option value='2'>NO Asignable</option>
                    </select>"; 

   			$datos= "<div id='datosp' align='center'><table class='tabla' align='center' style='width:500px;' >
        	<tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        	<tr><th>Categoria:</th><td>" . $categoria . "</td></tr>
        	<tr><th>Asignaci&oacute;n:</th><td>" . $asig . "</td></tr>
        	<tr><th>Estado:</th><td>" . $estado . "</td></tr>
        	<tr><th>Descripci&oacute;n:</th><td><input type='textarea' name='descripcion' id='descripcion' class='campo' size='20'/></td></tr>
        	</table>";
   			$botonA = "<input type=\"button\" class='boton' value=\"Agregar\" OnClick=guardagrupo();>";
   			$lista = 'classlista.php?modulo=listadegrupos';
        	
        }else{

        	$titulo = "ASIGNAR RESPONSABLE";
        	
        	$solicitantes=$this->ObjConsulta->selectpersonalC($this->conect_sigesp);
      		$responsable = "<select id='responsable' name='responsable' style='width:375px;'>";
			$responsable.="<option value='0' selected >Seleccione </option>";
       		foreach ($solicitantes as $llave => $valor) {

       			$valor[2]=utf8_decode($valor[2]);
       			$valor[3]=utf8_decode($valor[3]);
       			$img="http://intranet/sistemas/directorio/paginas/download_foto.php?id=".base64_encode($valor[1])."'";
       			$responsable.="<option value='" . $valor[1] . "' $selected title= '".$img."' >" . $valor[2] . " " . $valor[3] . "</option>";
           	}
        	$responsable.="</SELECT> ";

   			$datos= "<div id='datosp' align='center'><table class='tabla' align='center' style='width:850px;' >
        	<tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        	<tr><th>Responsable:</th><td>" . $responsable . "</td></tr>
			</table>";
   			$botonA = "<input type=\"button\" class='boton' value=\"Agregar\" OnClick=asigrupo('$idgrupo');>";
   			$lista = 'classlista.php?modulo=listadegrupos';
        	
        }

        $botonC = "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('classbienvenida.php');>";

   		$datos.="<table class='tabla' style='width:500px;'>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table></div>";

        return $datos; 
    }

    //Esta funcion realiza los insert de los articulos en la base de datos
    function guardararticulo(){
    	//tipodescripcion
    	$tipoart =$_GET['tipoart'];
    	$tipodesc =$_GET['tipodescripcion'];
    	$descripcion =strtoupper($_GET['descripcion']);
    	$marca =strtoupper($_GET['marca']);
    	$modelo =strtoupper($_GET['modelo']);
    	$biennac =$_GET['biennac'];
    	$serial =$_GET['serial'];
    	$siglas =$_GET['siglas'];
    	$id_grupo =$_GET['grupo'];
    	$id_categoria =$_GET['categoria'];
    	$peldano =$_GET['peldano'];
    	$estante =$_GET['estante'];
    	$costo =$_GET['costo'];
    	$cantidad =$_GET['cantidad'];
    	$cantidadmin=$_GET['cantidadmin'];
    	$observacion =$_GET['observacion'];
    	$unidad_medida =$_GET['unidad_medida'];
    	$id_estado=$_GET['estado'];
    	$id_estatus=1;
    	$user_reg=$_SESSION['cedula'];

    	if ($peldano==''){
    		$peldano=0;
    	}
    	if ($estante==''){
    		$estante=0;
    	}
    	if ($cantidadmin==''){
    		$cantidadmin=0;
    	}


    	$insertarticulo = $this->ObjConsulta->insertarticulo($this->conect_sistemas_vtv, $tipoart, $descripcion, $marca, $modelo, $biennac, $serial, $siglas, $id_grupo, $id_categoria, $peldano, $estante, $id_estado, $observacion, $unidad_medida, $costo, $cantidad, $cantidadmin, $id_estatus, $user_reg, $tipodesc);
        	

    	$lista='classlista.php?modulo=listadealmacen';

    	$botonA = "<input type=\"button\" class='boton' value=\"Aceptar\" OnClick=cargarimagen('" . $lista . "');>";
        $botonC = "<input type=\"button\" class='boton' value=\"Continuar\" OnClick=nocargarimagen('" . $lista . "');>";

        $imagen="<form action='classDirectorioFunciones.php?obtenerFuncion=cargarimagen&lista=" . $lista . "' method='post' enctype='multipart/form-data'>
        <div id='datosp' align='center'><table class='tabla' align='center' style='width:500px;' >
        <tr><th colspan='7' class='titulo' >Imagen del art&iacute;culo</th></tr>
        <tr><th>Imagen:</th><td><input type='file' size='50' name='upload'><input type='hidden' name='id_articulo' value='".$insertarticulo[0]."'></td><td><input type='hidden' name='tipo' value=1></td></tr>
        </table>
        <table class='tabla' style='width:500px;'>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
       	</table></div>
       	";
        echo $imagen;
    }

    function guardargrupo(){

    	$descripcion =strtoupper($_GET['descripcion']);
    	$id_categoria =$_GET['categoria'];
    	$id_asig =$_GET['asig'];
    	$id_estado =$_GET['estado'];
    	$id_estatus=1;
    	$user_reg=$_SESSION['cedula'];
    	if($id_asig==1){
    		$asig=1;
    	}else {
    		$asig=0;
    	}
    	$insertgrupo = $this->ObjConsulta->insertgrupo($this->conect_sistemas_vtv, $id_estatus, $descripcion, $id_categoria,$user_reg, $id_asig, $id_estado, $asig);
    	$id_del_grupo=$insertgrupo[0];
    	$id_grupo='GP'.$id_del_grupo;
    	$updategrupo = $this->ObjConsulta->updateidgrupo($this->conect_sistemas_vtv, $id_grupo, $id_del_grupo);

   		$lista = 'classlista.php?modulo=listadegrupos';

    	$botonA = "<input type=\"button\" class='boton' value=\"Aceptar\" OnClick=cargarimagen('" . $lista . "');>";
        $botonC = "<input type=\"button\" class='boton' value=\"Continuar\" OnClick=nocargarimagen('" . $lista . "');>";

        $imagen="<form action='classDirectorioFunciones.php?obtenerFuncion=cargarimagen&lista=" . $lista . "' method='post' enctype='multipart/form-data'>
        <div id='datosp' align='center'><table class='tabla' align='center' style='width:500px;' >
        <tr><th colspan='7' class='titulo' >Imagen del art&iacute;culo</th></tr>
        <tr><th>Imagen:</th><td><input type='file' size='50' name='upload'><input type='hidden' name='id_articulo' value='".$insertgrupo[0]."'></td><td><input type='hidden' name='tipo' value=2></td></tr>
        </table>
        <table class='tabla' style='width:500px;'>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
       	</table></div>
       	";
        echo $imagen;
    }
    //Esta funcion es para mostrar la informacion del articulo que se desea modificar y los muestra en los campos 
    //para ser modificados de una vez.
    function modificararticulo() {

		$id = $_GET['id'];
		$tipo = $_GET['tipo'];
		               
        if($tipo==1){
        	$datosgrupo = $this->ObjConsulta->selectgrupo($this->conect_sistemas_vtv, $id);
        	$descripcion=$datosgrupo[1][1];
	        $id_categoria=$datosgrupo[1][2];
	        $id_asig=$datosgrupo[1][3];
	        $id_desc_estado=$datosgrupo[1][4];

	        $desc_categoria = $this->ObjConsulta->desc_categoria($this->conect_sistemas_vtv, $id_categoria);
			$desc_categoria= $desc_categoria[1][1];

			$descripcion_categoria = $this->ObjConsulta->categoria($this->conect_sistemas_vtv);
	        $categoria = "<select id='categoria' name='categoria' style='width:105px;'>";
			$categoria.="<option value='".$id_categoria."' selected >".$desc_categoria."</option>";
	       	foreach ($descripcion_categoria as $llave => $valor) {
	        		$categoria.="<option value='" . $valor[1] . "' $selected >" . $valor[2] . "</option>";
	        	}
	        $categoria.="</SELECT> ";
		
			$desc_estado = $this->ObjConsulta->desc_estado($this->conect_sistemas_vtv, $id_desc_estado);
			$desc_estado= $desc_estado[1][1];

			$descripcion_estado = $this->ObjConsulta->estado($this->conect_sistemas_vtv);
        	$estado = "<select id='estado' name='estado' style='width:105px;'>";
			$estado.="<option value='".$id_desc_estado."' selected >".$desc_estado." </option>";
       		foreach ($descripcion_estado as $llave => $valor) {
        		$estado.="<option value='" . $valor[1] . "' $selected >" . $valor[2] . "</option>";
        	}
        	$estado.="</SELECT> ";

			if($id_asig==1){
				$desc_asig='Asignable';
			}else {
				$desc_asig='NO Asignable';
				
			}
			$asig ="<select id='asig' >
                    <option value='".$id_asig."' selected='selected' >".$desc_asig."</option>
			        <option value='1'>Asignable</option>
                    <option value='2'>NO Asignable</option>
                    </select>"; 

	        
        	$titulo = "AGREGAR GRUPO";
  		

   			$datos= "<div id='datosp' align='center'><table class='tabla' align='center' style='width:500px;' >
        	<tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        	<tr><th>Categoria:</th><td>" . $categoria . "</td></tr>
        	<tr><th>Asignaci&oacute;n:</th><td>" . $asig . "</td></tr>
        	<tr><th>Estado:</th><td>" . $estado . "</td></tr>
        	<tr><th>Descripci&oacute;n:</th><td><input type='textarea' name='descripcion' id='descripcion' value='".$descripcion."' class='campo' size='20'/></td></tr>
        	</table>";
   			$botonA = "<input type=\"button\" class='boton' value=\"Agregar\" OnClick=modificargrupo('$id');>";
   			$lista = 'classlista.php?modulo=listadegrupos';

        }else{


        	$datosarticulo = $this->ObjConsulta->selectarticulo($this->conect_sistemas_vtv, $id);
		
			$tipo_articulo=$datosarticulo[1][1];
	        $descripcion=$datosarticulo[1][2];
	        $marca=$datosarticulo[1][3];
	        $modelo=$datosarticulo[1][4];
	        $bien_nac=$datosarticulo[1][5];
	        $serial=$datosarticulo[1][6];
	        $siglas=$datosarticulo[1][7];
	        $id_grupo=$datosarticulo[1][8];
	        $peldano=$datosarticulo[1][9];
	        $estante=$datosarticulo[1][10];
	        $cantidad=$datosarticulo[1][11];
	        $costo=$datosarticulo[1][12];
	        $id_estatus=$datosarticulo[1][13];
	        $id_estado=$datosarticulo[1][14];
	        $id_categoria=$datosarticulo[1][15];
	        $id_unidad_medida=$datosarticulo[1][16];
	        $fecha_exp=$datosarticulo[1][17];
	        $fecha_reg=$datosarticulo[1][18];
	        $user_reg=$datosarticulo[1][19];
	        $id_descripcion=$datosarticulo[1][22];
	        
	        $desc_unidad_medida = $this->ObjConsulta->desc_unidad_medida($this->conect_sistemas_vtv, $id_unidad_medida);
			$descripcion_med= $desc_unidad_medida[1][1];

			$desc_desc = $this->ObjConsulta->desc_desc($this->conect_sistemas_vtv, $id_descripcion);
			$descrip= $desc_desc[1][1];

			$desc_grupo = $this->ObjConsulta->desc_grupo($this->conect_sistemas_vtv, $id_grupo);
			$desc_grupo= $desc_grupo[1][1];

			$desc_categoria = $this->ObjConsulta->desc_categoria($this->conect_sistemas_vtv, $id_categoria);
			$desc_categoria= $desc_categoria[1][1];

			$desc_estado = $this->ObjConsulta->desc_estado($this->conect_sistemas_vtv, $id_estado);
			$desc_estado= $desc_estado[1][1];

			if ($tipo_articulo == 1){

				$des_tipo_articulo= 'Prestamo';
			}else{

				$des_tipo_articulo= 'Consumible';
			}

			$descripcion_grupo = $this->ObjConsulta->grupos($this->conect_sistemas_vtv);
	        $grupos = "<select id='grupo' name='grupo' style='width:105px;'>";
			$grupos.="<option value='".$id_grupo."' selected >".$desc_grupo."</option>";
	       	foreach ($descripcion_grupo as $llave => $valor) {
	        		$grupos.="<option value='" . $valor[1] . "' $selected >" . $valor[2] . "</option>";
	        	}
	        $grupos.="</SELECT> "; 

	        $descripcion_categoria = $this->ObjConsulta->categoria($this->conect_sistemas_vtv);
	        $categoria = "<select id='categoria' name='categoria' style='width:105px;'>";
			$categoria.="<option value='".$id_categoria."' selected >".$desc_categoria."</option>";
	       	foreach ($descripcion_categoria as $llave => $valor) {
	        		$categoria.="<option value='" . $valor[1] . "' $selected >" . $valor[2] . "</option>";
	        	}
	        $categoria.="</SELECT> "; 

	        $descripcion_estado = $this->ObjConsulta->estado($this->conect_sistemas_vtv);
	        $estado = "<select id='estado' name='estado' style='width:105px;'>";
			$estado.="<option value='".$id_estado."' selected >".$desc_estado."</option>";
	       	foreach ($descripcion_estado as $llave => $valor) {
	        		$estado.="<option value='" . $valor[1] . "' $selected >" . $valor[2] . "</option>";
	        	}
	        $estado.="</SELECT> ";

	        $desc_unidad_medidacant = $this->ObjConsulta->unidad_medida($this->conect_sistemas_vtv);
	        $unidad_medidascant = "<select id='unidad_medida' name='unidad_medida' style='width:105px;'>";
			$unidad_medidascant.="<option value='".$id_unidad_medida."' selected >".$descripcion_med." </option>";
	       	foreach ($desc_unidad_medidacant as $llave => $valor) {
	        		$unidad_medidascant.="<option value='" . $valor[1] . "' $selected >" . $valor[2] . "</option>";
	        	}
	        $unidad_medidascant.="</SELECT> ";	

	        $tipodescripcion = $this->ObjConsulta->selecttipodescripcion($this->conect_sistemas_vtv);
        	$tipo_desc = "<select id='tipodescripcion' name='tipodescripcion' style='width:105px;'>";
			$tipo_desc.="<option value='".$id_descripcion."' selected >".$descrip." </option>";
       		foreach ($tipodescripcion as $llave => $valor) {
        		$tipo_desc.="<option value='" . $valor[1] . "' $selected >" . $valor[2] . "</option>";
        	}
        	$tipo_desc.="</SELECT> ";	

	        $tipoart ="<select id='tipoart' >
	        <option value=".$tipo_articulo." selected='selected'>".$des_tipo_articulo."</option>
	        <option value='1'>Prestamo</option>
	        <option value='2'>Consumible</option>
	        </select>"; 

        	$titulo = "MODIFICAR MATERIAL DE ALMACEN";

	   		$datos= "<div id='datosp' align='center'><table class='tabla' align='center' style='width:850px;' >
	        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
	        <tr><th>Tipo:</th><td>" . $tipoart . "</td></tr>
	        <tr><th>Categoria:</th><td>" . $categoria . "</td></tr>
	        <tr><th>Tipo Descripci&oacute;n:</th><td>" . $tipo_desc . "</td></tr>
	        <tr><th>Descripci&oacute;n:</th><td><input type='textarea' name='descripcion' id='descripcion' value='".$descripcion."' class='campo' size='20'/></td></tr>
	        <tr><th>Marca:</th><td><input type='text' name='marca' id='marca' class='campo' value='".$marca."' /></td></tr>
	        <tr><th>Modelo:</th><td><input type='text' name='modelo' id='modelo' class='campo' value='".$modelo."' /></td></tr>
	        <tr><th>Serial:</th><td><input type='text' name='serial' id='serial' class='campo' value='".$serial."' /></td></tr>
	        <tr><th>Bien Nac.:</th><td><input type='text' name='biennac' id='biennac' class='campo' value='".$bien_nac." ' /></td></tr>
	        <tr><th>Siglas:</th><td><input type='text' name='siglas' id='siglas' class='campo' value='".$siglas." ' /></td></tr>
	        <tr><th>Grupo:</th><td>" . $grupos . "</td></tr>
	        <tr><th>Estante:</th><td><input type='text' name='estante' id='estante' class='campo' value='".$estante."'/></td></tr>
	        <tr><th>Pelda&ntilde;o:</th><td><input type='text' name='peldano' id='peldano' class='campo' value='".$peldano."' /></td></tr>
	        <tr><th>Cantidad:</th><td><input type='text' name='cantidad' id='cantidad' value='".$cantidad." ' class='campo' onkeypress='return validaN(event)' size='5' />".$unidad_medidascant."</td></tr>
	        <tr><th>Costo:</th><td><input type='text' name='costo' id='costo' class='campo' value='".$costo."'  /></td></tr>
	        <tr><th>Estado:</th><td>" . $estado . "</td></tr>
	        <tr><th>Observaci&oacute;n:</th><td><textarea name='observacion' rows='5' cols='50' id='observacion' class='campo' >".$observacion."</textarea></td></tr>
	        </table>";
	   		$botonA = "<input type=\"button\" class='boton' value=\"Agregar\" OnClick=modificarmaterialal($id);>";
	   		$lista = 'classlista.php?modulo=listadealmacen';
	        
        }
        	
        

        
		$botonC = "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('".$lista."');>";

   		$datos.="<table class='tabla' style='width:850px;'>
        <tr><th colspan='2'><div align='center'>" . $botonB . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table></div>";
        
        echo $datos;
	}

	function modificardatosgrupo(){
		$id=$_GET['id'];
		$descripcion=$_GET['descripcion'];
        $id_categoria=$_GET['categoria'];
        $id_asig=$_GET['asig'];
        $id_desc_estado=$_GET['estado'];


        $datosarticulo = $this->ObjConsulta->updategrupos($this->conect_sistemas_vtv, $id_categoria, $descripcion, $id, $id_asig, $id_desc_estado);

        $mensaje = "<div style='color: #009900;font-weight: bold;'><br>El grupo fue modificado con exito<div><br>";
		echo $this->ObjMensaje->InterfazExitosamente($mensaje);
		echo"<script>var pagina='".$lista."';                     
		function redireccionar() { 
			location.href=pagina;
		} 
		setTimeout ('redireccionar()', 3000);
		</script>
		";
	}
	//Esta funcion es para guardar los datos modificados de los articulos
	function modificardatosarticulo() {

		$id=$_GET['id'];
		$tipoart= $_GET['tipoart'];
		$descripcion= strtoupper($_GET['descripcion']);
		$marca= strtoupper($_GET['marca']);
		$modelo= strtoupper($_GET['modelo']);
		$talla_medida= $_GET['talla_medida'];
		$unidad_medida= $_GET['unidad_medida'];
		$biennac= $_GET['biennac'];
		$serial= $_GET['serial'];
		$costo= $_GET['costo'];
		$id_estado=$_GET['estado'];
		$cantidad=$_GET['cantidad'];
		$observacion= strtoupper($_GET['observacion']);
		$id_categoria=$_GET['categoria'];
		$id_grupo=$_GET['grupo'];
		$siglas=$_GET['siglas'];
		$peldano=$_GET['peldano'];
		$estante=$_GET['estante']; 
		$tipodesc=$_GET['tipodescripcion']; 
	
        $lista = 'classlista.php?modulo=listadealmacen';
      

		$datosarticulo = $this->ObjConsulta->updatearticulos($this->conect_sistemas_vtv, 
			$tipoart, $descripcion, $marca, $modelo, $biennac, $serial, $siglas, $id_grupo, 
			$id_categoria, $peldano, $estante, $id_estado, $observacion, $unidad_medida, $costo, 
			$cantidad,$id,$tipodesc);

		$mensaje = "<div style='color: #009900;font-weight: bold;'><br>El articulo fue modificado con exito<div><br>";
		echo $this->ObjMensaje->InterfazExitosamente($mensaje);
		echo"<script>var pagina='".$lista."';                     
		function redireccionar() { 
			location.href=pagina;
		} 
		setTimeout ('redireccionar()', 3000);
		</script>
		";
    }

    function ingreso_stock(){



    	$id = $_GET['idarticulo'];
    	$lista = $_GET['lista'];
		$datosarticulo = $this->ObjConsulta->selectarticulo($this->conect_sistemas_vtv, $id);
		$tipo_articulo=$datosarticulo[1][1];
	    $descripcion=$datosarticulo[1][2];
	    $marca=$datosarticulo[1][3];
	    $modelo=$datosarticulo[1][4];
	    $bien_nac=$datosarticulo[1][5];
	    $serial=$datosarticulo[1][6];
	    $siglas=$datosarticulo[1][7];
	    $id_grupo=$datosarticulo[1][8];
	    $peldano=$datosarticulo[1][9];
	    $estante=$datosarticulo[1][10];
	    $cantidad=$datosarticulo[1][11];
	    $costo=$datosarticulo[1][12];
	    $id_estatus=$datosarticulo[1][13];
	    $id_estado=$datosarticulo[1][14];
	    $id_categoria=$datosarticulo[1][15];
	    $id_unidad_medida=$datosarticulo[1][16];
	    $fecha_exp=$datosarticulo[1][17];
	    $fecha_reg=$datosarticulo[1][18];
	    $user_reg=$datosarticulo[1][19];
	    $imagen=$datosarticulo[1][20];
	    
	    if($lista==1){
	    	$lista = 'classlista.php?modulo=listadealmacen';

	    }elseif($lista==2){
	    	$lista = 'classlista.php?modulo=listadeexistenciamin';
	    	
	    }else{
	    	$lista = 'classlista.php?modulo=listadeubicacion';
	    	
	    }

	    if($tipo_articulo == 1){
			$mensaje = "<div style='color: #009900;font-weight: bold;'><br>El articulo es de tipo prestamo.<div><br>";
			$ficherosjs="<script>var pagina='".$lista."';                     
			function redireccionar() { 
			location.href=pagina;
			} 
			setTimeout ('redireccionar()', 3000);
			</script>";

		}else{

			$desc_grupo = $this->ObjConsulta->desc_grupo($this->conect_sistemas_vtv, $id_grupo);
			$desc_grupo= $desc_grupo[1][1];

			$desc_categoria = $this->ObjConsulta->desc_categoria($this->conect_sistemas_vtv, $id_categoria);
			$desc_categoria= $desc_categoria[1][1];

			$desc_estado = $this->ObjConsulta->desc_estado($this->conect_sistemas_vtv, $id_estado);
			$desc_estado= $desc_estado[1][1];

			$descripcion_categoria = $this->ObjConsulta->desc_categoria($this->conect_sistemas_vtv, $id_categoria);
			$categoria=$descripcion_categoria[1][1];


	        if ($tipo_articulo == 1){
				$des_tipo_articulo= 'Prestamo';
			}else{
				$des_tipo_articulo= 'Consumible';
			}



			if($observacion == ''){
				$observacion ='Sin Observaciones';
			}


			if($unidad_medida == ''){
				$unidad_medida= '';
			}else{
				$desc_unidad_medida = $this->ObjConsulta->desc_unidad_medida($this->conect_sistemas_vtv, $id_unidad_medida);
				$descripcion_med= $desc_unidad_medida[1][1];
			}


			if ($imagen==''){
				$img= "<img src='../imagenes/inventario/sinfoto.jpg' style='width:75px;height:75px;border-width:none;border-style:none;'>";
			}else{
				$img= "<a href='../imagenes/inventario/".$imagen."' target='new'><img src='../imagenes/inventario/".$imagen."' style='width:75px;height:75px;border-width:none;border-style:none;'></a>";
			}
			
	        	$titulo = "ALMACEN";           


	   			$datos= "<div id='datosp' align='center'><table class='tabla' align='center' style='width:500px;' >
	        	<tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
	        	<tr><th>Imagen:</th><td>" . $img . "</td></tr>
	        	<tr><th>Tipo:</th><td>" . $des_tipo_articulo . "</td></tr>
	        	<tr><th>Categoria:</th><td>" . $categoria . "</td></tr>
	        	<tr><th>Descripci&oacute;n:</th><td>".$descripcion."</td></tr>
	        	<tr><th>Marca:</th><td>".$marca."</td></tr>
	        	<tr><th>Modelo:</th><td>".$modelo."</td></tr>
	        	<tr><th>Serial:</th><td>".$serial."</td></tr>
	        	<tr><th>Bien Nac.:</th><td>".$bien_nac."</td></tr>
	        	<tr><th>Siglas:</th><td>".$siglas."</td></tr>
	        	<tr><th>Grupo:</th><td>".$desc_grupo."</td></tr>
	        	<tr><th>Estante:</th><td>".$estante."</td></tr>
	        	<tr><th>Pelda&ntilde;o:</th><td>".$peldano."</td></tr>
	        	<tr><th>Cantidad:</th><td>".$cantidad." " .$descripcion_med. "</td></tr>
	        	<tr><th>Costo:</th><td>".$costo."</td></tr>
	        	<tr><th>Estado:</th><td>" .$desc_estado . "</td></tr>
	        	<tr><th>Observaci&oacute;n:</th><td>".$observacion."</td></tr>
	        	</table>";
	   				

			
		        $datos.= "<div id='datosp' align='center'><table class='tabla' align='center' style='width:500px;' >
		        <tr><th class='titulo' colspan='2'>Nuevo Ingreso</th></tr>
		        <tr><th>Costo:</th><td><input type='text' name='costo' id='costo' class='campo' onkeypress='return validaN(event)' size='5' /></td></tr>
		        <tr><th>Cantidad:</th><td><input type='text' name='cantidad' id='cantidad' class='campo' onkeypress='return validaN(event)' size='5' /></td></tr>
		    	</table>";

		    	$botonA = "<input type=\"button\" class='boton' value=\"Agregar\" OnClick=ingresarstock($id,'".$lista."');>";
		    	$botonC = "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('".$lista."');>";

		   		$botones="<table class='tabla' style='width:500px;'>
		        <tr><th colspan='2'><div align='center'>" . $botonB . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
		        </table></div>";
		        
		        
		        echo $datos;
		        echo $botones;
		}
    }
    
   	function nuevoingreso(){
   		$id=$_GET['idarticulo'];
   		$costo=$_GET['costo'];
   		$cantidad=$_GET['cantidad'];
   		$user_reg=$_SESSION['cedula'];
 
   		$lista=$_GET['lista'];


   	    $nuevoingreso=$this->ObjConsulta->insertstock($this->conect_sistemas_vtv, $id, $costo, $cantidad, $user_reg);
   		
   		$datosarticulo = $this->ObjConsulta->selectarticulo($this->conect_sistemas_vtv, $id);
   		$costoant= $datosarticulo[1][12];
   		$cantidadant= $datosarticulo[1][11];

   		$costo = ($costo+$costoant)/2;
   	    $cantidad= $cantidad+$cantidadant;

   		$updatestock=$this->ObjConsulta->updatestock($this->conect_sistemas_vtv, $id, $costo, $cantidad); 

   		$mensaje = "<div style='color: #009900;font-weight: bold;'><br>El articulo fue modificado con exito<div><br>";
		echo $this->ObjMensaje->InterfazExitosamente($mensaje);
		echo"<script>var pagina='".$lista."';                     
		function redireccionar() { 
			location.href=pagina;
		} 
		setTimeout ('redireccionar()', 3000);
		</script>
		";
   	}

 	//Esta funcion es para realizar el insert de la imagen del articulo en la base de datos.
    function cargarimagen(){
    	$tipo=$_POST['tipo'];
    	echo $id=$_POST['id_articulo'];

    	$output_dir = "../imagenes/inventario/";
		$name="upload";

		$fileCount =count($_FILES[$name]["name"]);
		for($i=0; $i < $fileCount; $i++)
		{
		  $fileName = str_replace(" ", "_", $_FILES[$name]["name"]);
		  //para guardar el archivo solo se necesita descomentar la linea de abajo.
		  move_uploaded_file($_FILES[$name]["tmp_name"],$output_dir.$fileName );
		}

		if($tipo == 1){
			$datosfile = $this->ObjConsulta->updatefile1($this->conect_sistemas_vtv, $fileName, $id);

		}else{
			$datosfile = $this->ObjConsulta->updatefile2($this->conect_sistemas_vtv, $fileName, $id);
		}	
    	$lista=$_GET['lista'];
    	$mensaje = "<div style='color: #009900;font-weight: bold;'><br>El art&iacute;culo fue registrado con exito<div><br>";
		$ficherosjs="<script>var pagina='".$lista."';                     
			function redireccionar() { 
			location.href=pagina;
			} 
			setTimeout ('redireccionar()', 3000);
			</script>";
		echo $this->ObjCabPie->flibHtmCab(0,$ficherosjs,'','',0,'');

		echo $this->ObjMensaje->InterfazExitosamente($mensaje);
		echo $this->ObjCabPie->flibCerrarHtm("");
    }

    //Esta funcion es para realizar el insert de la imagen nueva del articulo en la base de datos.
    function modificarimagen(){
    	$id=$_GET['id'];
    	$tipo=$_GET['tipo'];
    	if ($tipo==1){

    		$lista= 'classlista.php?modulo=listadealmacen';
    	}else{

   			$lista = 'classlista.php?modulo=listadegrupos';
    	}
    	
    	
    	$botonA = "<input type=\"button\" class='boton' value=\"Aceptar\" OnClick=cargarimagen('" . $lista . "');>";
        $botonC = "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('" . $lista . "');>";

        $imagen="<form action='classDirectorioFunciones.php?obtenerFuncion=cargarimagen&lista=" . $lista . "' method='post' enctype='multipart/form-data'>
        <div id='datosp' align='center'><table class='tabla' align='center' style='width:500px;' >
        <tr><th colspan='7' class='titulo' >Imagen del art&iacute;culo</th></tr>
        <tr><th>Imagen:</th><td><input type='file' size='50' name='upload'><input type='hidden' name='id_articulo' value='".$id."'><td></td><input type='hidden' name='tipo' value='".$tipo."'></td></tr>
        </table>
        <table class='tabla' style='width:500px;'>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
       	</table></div>
       	";
        echo $imagen;
    }

    //Esta funcion muestra un mensaje indicando el ingreso exitoso del articulo en la base de datos.
    function nocargarimagen(){
     	$mensaje = "<div style='color: #009900;font-weight: bold;'><br>El art&iacute;culo fue registrado con exito<div><br>";
		echo $this->ObjMensaje->InterfazExitosamente($mensaje);
		echo"<script>var pagina='".$lista."';                     
		function redireccionar() { 
			location.href=pagina;
		} 
		setTimeout ('redireccionar()', 3000);
		</script>
		";
    }

    //Esta funcion realiza una consulta de los datos del articulo y los muestra.
    function mostrararticulo() {

		$id = $_GET['id'];
		$datosarticulo = $this->ObjConsulta->selectarticulo($this->conect_sistemas_vtv, $id);
		$tipo_articulo=$datosarticulo[1][1];
	    $descripcion=$datosarticulo[1][2];
	    $marca=$datosarticulo[1][3];
	    $modelo=$datosarticulo[1][4];
	    $bien_nac=$datosarticulo[1][5];
	    $serial=$datosarticulo[1][6];
	    $siglas=$datosarticulo[1][7];
	    $id_grupo=$datosarticulo[1][8];
	    $peldano=$datosarticulo[1][9];
	    $estante=$datosarticulo[1][10];
	    $cantidad=$datosarticulo[1][11];
	    $costo=$datosarticulo[1][12];
	    $id_estatus=$datosarticulo[1][13];
	    $id_estado=$datosarticulo[1][14];
	    $id_categoria=$datosarticulo[1][15];
	    $id_unidad_medida=$datosarticulo[1][16];
	    $fecha_exp=$datosarticulo[1][17];
	    $fecha_reg=$datosarticulo[1][18];
	    $user_reg=$datosarticulo[1][19];
	    $imagen=$datosarticulo[1][20];
	    

		$desc_grupo = $this->ObjConsulta->desc_grupo($this->conect_sistemas_vtv, $id_grupo);
		$desc_grupo= $desc_grupo[1][1];

		$desc_categoria = $this->ObjConsulta->desc_categoria($this->conect_sistemas_vtv, $id_categoria);
		$desc_categoria= $desc_categoria[1][1];

		$desc_estado = $this->ObjConsulta->desc_estado($this->conect_sistemas_vtv, $id_estado);
		$desc_estado= $desc_estado[1][1];

		$descripcion_categoria = $this->ObjConsulta->desc_categoria($this->conect_sistemas_vtv, $id_categoria);
		$categoria=$descripcion_categoria[1][1];


        if ($tipo_articulo == 1){
			$des_tipo_articulo= 'Prestamo';
		}else{
			$des_tipo_articulo= 'Consumible';
		}



		if($observacion == ''){
			$observacion ='Sin Observaciones';
		}


		if($unidad_medida == ''){
			$unidad_medida= '';
		}else{
			$desc_unidad_medida = $this->ObjConsulta->desc_unidad_medida($this->conect_sistemas_vtv, $id_unidad_medida);
			$descripcion_med= $desc_unidad_medida[1][1];
		}


		if ($imagen==''){
			$img= "<img src='../imagenes/inventario/sinfoto.jpg' style='width:75px;height:75px;border-width:none;border-style:none;'>";
		}else{
			$img= "<a href='../imagenes/inventario/".$imagen."' target='new'><img src='../imagenes/inventario/".$imagen."' style='width:75px;height:75px;border-width:none;border-style:none;'></a>";
		}
		
        	$titulo = "ALMACEN";           


   			$datos= "<div id='datosp' align='center'><table class='tabla' align='center' style='width:500px;' >
        	<tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        	<tr><th>Imagen:</th><td>" . $img . "</td></tr>
        	<tr><th>Tipo:</th><td>" . $des_tipo_articulo . "</td></tr>
        	<tr><th>Categoria:</th><td>" . $categoria . "</td></tr>
        	<tr><th>Descripci&oacute;n:</th><td>".$descripcion."</td></tr>
        	<tr><th>Marca:</th><td>".$marca."</td></tr>
        	<tr><th>Modelo:</th><td>".$modelo."</td></tr>
        	<tr><th>Serial:</th><td>".$serial."</td></tr>
        	<tr><th>Bien Nac.:</th><td>".$bien_nac."</td></tr>
        	<tr><th>Siglas:</th><td>".$siglas."</td></tr>
        	<tr><th>Grupo:</th><td>".$desc_grupo."</td></tr>
        	<tr><th>Estante:</th><td>".$estante."</td></tr>
        	<tr><th>Pelda&ntilde;o:</th><td>".$peldano."</td></tr>
        	<tr><th>Cantidad:</th><td>".$cantidad." " .$descripcion_med. "</td></tr>
        	<tr><th>Costo:</th><td>".$costo."</td></tr>
        	<tr><th>Estado:</th><td>" .$desc_estado . "</td></tr>
        	<tr><th>Observaci&oacute;n:</th><td>".$observacion."</td></tr>
        	</table>";
   			$lista = 'classlista.php?modulo=listadealmacen';
        	
        

        $botonC = "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('".$lista."');>";

   		$datos.="<table class='tabla' style='width:500px;'>
        <tr><th colspan='2'><div align='center'>" . $botonC . "</div></tr>
        </table></div>";
        echo $datos;
    }

    //Esta funcion se encarga de mostrar los equipos que tiene asignado ese grupo

    function mostrararticulodegrupo() {

		$id = $_GET['idgrupo'];
		$desc_grupo = $this->ObjConsulta->desc_grupo($this->conect_sistemas_vtv, $id);
		$desc_grupo= $desc_grupo[1][1];
		$articulos_grupo = $this->ObjConsulta->selectarticulodegrupo($this->conect_sistemas_vtv, $id);
		$cont=count($articulos_grupo);
		if($cont==0){
	        
	        $articulos.="<tr><td  colspan='6' align='center'><font color='RED'><b>No se encotraron articulos asociados a este grupo</b></font></td>";

		}else{
			foreach ($articulos_grupo as $llave => $valor) {
				$datosarticulo = $this->ObjConsulta->selectarticulo($this->conect_sistemas_vtv, $valor[1]);
				$tipo_articulo=$datosarticulo[1][1];
			    $descripcion=$datosarticulo[1][2];
			    $marca=$datosarticulo[1][3];
			    $modelo=$datosarticulo[1][4];
			    $bien_nac=$datosarticulo[1][5];
			    $serial=$datosarticulo[1][6];
			    $siglas=$datosarticulo[1][7];
			    $id_grupo=$datosarticulo[1][8];
			    $peldano=$datosarticulo[1][9];
			    $estante=$datosarticulo[1][10];
			    $cantidad=$datosarticulo[1][11];
			    $costo=$datosarticulo[1][12];
			    $id_estatus=$datosarticulo[1][13];
			    $id_estado=$datosarticulo[1][14];
			    $id_categoria=$datosarticulo[1][15];
			    $id_unidad_medida=$datosarticulo[1][16];
			    $fecha_exp=$datosarticulo[1][17];
			    $fecha_reg=$datosarticulo[1][18];
			    $user_reg=$datosarticulo[1][19];
			    $imagen=$datosarticulo[1][20];
			    

				$desc_grupo = $this->ObjConsulta->desc_grupo($this->conect_sistemas_vtv, $id_grupo);
				$desc_grupo= $desc_grupo[1][1];

				$desc_categoria = $this->ObjConsulta->desc_categoria($this->conect_sistemas_vtv, $id_categoria);
				$desc_categoria= $desc_categoria[1][1];

				$desc_estado = $this->ObjConsulta->desc_estado($this->conect_sistemas_vtv, $id_estado);
				$desc_estado= $desc_estado[1][1];

				$descripcion_categoria = $this->ObjConsulta->desc_categoria($this->conect_sistemas_vtv, $id_categoria);
				$categoria=$descripcion_categoria[1][1];

				if ($imagen==''){
					$img= "<img src='../imagenes/inventario/sinfoto.jpg' style='width:60px;height:60px;border-width:none;border-style:none;'>";
				}else{
					$img= "<a href='../imagenes/inventario/".$imagen."' target='new'><img src='../imagenes/inventario/".$imagen."' style='width:60px;height:60px;border-width:none;border-style:none;'></a>";
				}

	        	$articulos.="<tr><td  align='left'>".strtoupper($descripcion)."</td><td  align='left'>".$marca."</td><td  align='left'>".$modelo."</td><td  align='left'>".$bien_nac."</td><td  align='left'>".$serial."</td><td  align='left'>".$desc_estado."</td><td  align='left'>".$img."</td>";
	        }
		}
		

		
			
        	$titulo = "ALMACEN";           


   			$datos= "<div id='datosp' align='center'><table class='tabla' align='center' style='width:850px;' >
        	<tr><th class='titulo' colspan='7'>" . $desc_grupo . "</th></tr>
        	<tr><th><div align='left'>Descripci&oacute;n</div></th><th><div align='left'>Marca</div></th><th><div align='left'>Modelo</div></th><th><div align='left'>Bien Nac.</div></th><th><div align='left'>Serial</div></th><th><div align='left'>Estado</div></th><th><div align='left'>Imagen</div></th></tr>
        	".$articulos."
        	</table>";
   			$lista = 'classlista.php?modulo=listadegrupos';
        	
        

        $botonC = "<input type=\"button\" class='boton' value=\"Aceptar\" OnClick=CancelarRegresar('".$lista."');>";

   		$datos.="<table class='tabla' style='width:850px;'>
        <tr><th colspan='2'><div align='center'>" . $botonC . "</div></tr>
        </table></div>";
        echo $datos;
    }	

    //Esta funcion es para realizar un update en la fecha de expiracion del articulo, no se elimina con delete
    function eliminararticulo() {
		$id = $_GET['id'];
		$user_exp = $_SESSION['cedula'];
		$datosarticulo=$this->ObjConsulta->selectarticulo($this->conect_sistemas_vtv, $id);
		$datosarticulo[1][1];


		
		$lista='classlista.php?modulo=listadealmacen';
		

		$elimarticulo = $this->ObjConsulta->updatearticulo($this->conect_sistemas_vtv, $id, $user_exp);
		$mensaje = "<div style='color: #009900;font-weight: bold;'><br>EL articulo fue eliminado con exito<div><br>";
		echo $this->ObjMensaje->InterfazExitosamente($mensaje);

		echo"<script>var pagina='".$lista."';                     
		function redireccionar() { 
			location.href=pagina;
		} 
		setTimeout ('redireccionar()', 3000);
		</script>";
	}

	//Esta funcion es para realizar un update en la fecha de expiracion del articulo, no se elimina con delete
    function eliminargrupo() {
		$id = $_GET['id'];
		$user_exp = $_SESSION['cedula'];
		$id_grupo= 'GP1';
		$lista='classlista.php?modulo=listadegrupos';
		
		$elimarticulo = $this->ObjConsulta->updategrupo($this->conect_sistemas_vtv, $id, $user_exp);

		$articulos_grupo = $this->ObjConsulta->selectarticulodegrupo($this->conect_sistemas_vtv, $id);
		foreach ($articulos_grupo as $llave => $valor) {
				$updateartgrupo = $this->ObjConsulta->updeteartgrupo($this->conect_sistemas_vtv, $valor[1], $id_grupo);
		}
		$mensaje = "<div style='color: #009900;font-weight: bold;'><br>El grupo fue eliminado con exito<div><br>";
		echo $this->ObjMensaje->InterfazExitosamente($mensaje);

		echo"<script>var pagina='".$lista."';                     
		function redireccionar() { 
			location.href=pagina;
		} 
		setTimeout ('redireccionar()', 3000);
		</script>";
	}

	//Esta funcion muestra los radio buttom, para la seleccion del destino del prestamo, si es una gerencia, un 
    //programa o un remoto
    function destino (){
    	$id_tipo_sol=$_GET['id_tipo_sol'];
    	$identrada=$_GET['identrada'];


    	 	$id_tipo_sol=$_GET['id_tipo_sol'];
	    	$identrada=$_GET['identrada'];
	        $titulo="Destino de la solicitud";
	    	
	    	if($identrada==3){
	    		$radios="<div align='center'>
	            		<th>Gerencia</th><td><input type='radio' name='iddestino' id='iddestino1' value='1' /></td>
	                    <th>Programa</th><td><input type='radio'  name='iddestino' id='iddestino2' value='2' /></td>
	                    <th>Remoto</th><td><input type='radio'  name='iddestino' id='iddestino3' value='3' /></td></div>
	                    <th>Ente extreno</th><td><input type='radio'  name='iddestino' id='iddestino3' value='5' /></td></div>
	                    <th>Responsable</th><td><input type='radio'  name='iddestino' id='iddestino4' value='4' /></td></div>";
	    	}else{
	    		$radios="<div align='center'>
	            		<th>Gerencia</th><td><input type='radio' name='iddestino' id='iddestino1' value='1' /></td>
	                	<th>Programa</th><td><input type='radio'  name='iddestino' id='iddestino2' value='2' /></td>
	                	<th>Remoto</th><td><input type='radio'  name='iddestino' id='iddestino3' value='3' /></td></div>";
	  	
	    	}
	           
	        $botonA = "<input type=\"button\" class='boton' value=\"Aceptar\" OnClick=destino('".$id_tipo_sol."','".$identrada."');>";
	        $botonC = "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('classbienvenida.php');>";

	        $datos="<div id='datosp' align='center'><table class='tabla' align='center' style='width:600px;' >
	        <tr><th colspan='10' class='titulo' >".$titulo."</th></tr>
	        <tr>".$radios."</tr>
	        </table>
	        <table class='tabla' style='width:600px;'>
	        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
	        </table></div>";

	        echo $datos;
    	
    }
    //En el caso de que el destino sea un remoto, Esta funcion muestra los radio button para indicar si es oficial o programa
    function tiporemoto (){
    	$id_tipo_sol=$_GET['id_tipo_sol'];
    	$identrada=$_GET['identrada'];
    	$iddestino=$_GET['iddestino'];


    	 $titulo="Tipo de Remoto";
	    	
	    	
	    $radios="<div align='center'>
	            <th>Oficial</th><td><input type='radio' name='idremoto' id='idremoto' value='1' /></td>
	            <th>Programa</th><td><input type='radio'  name='idremoto' id='idremoto' value='2' /></td>
	            </div>";
	  	
	    	
	    $botonA = "<input type=\"button\" class='boton' value=\"Aceptar\" OnClick=tiporemoto('".$id_tipo_sol."','".$identrada."','".$iddestino."');>";
	    $botonC = "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('classbienvenida.php');>";

	    $datos="<div id='datosp' align='center'><table class='tabla' align='center' style='width:500px;' >
	    <tr><th colspan='9' class='titulo' >".$titulo."</th></tr>
	    <tr>".$radios."</tr>
	    </table>
	    <table class='tabla' style='width:500px;'>
	    <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
	    </table></div>";

	    echo $datos;
    }

	//Esta funcion muestra un formulario para el registro de la solicitud de prestamo
    function datossolicitudes(){

    	$id_estatus=1;//Estatus DISPONIBLE
    	$id_desc_estado=1;//Estado del equipo (BUENO)
    	$identrada=$_GET['identrada'];//Si es 1 es grupo, si es 2 es equipos, si es 9 son gruposy equipos y si es 3 son camaras.
    	$iddestino=$_GET['iddestino'];//Si es gerencia 1 , programa 2 ,  remoto 3 o responsable de camara 4
		$idremoto=$_GET['idremoto'];//Oficial o programas
		if ($idremoto==''){
			$idremoto=0;
		}else{
			$idremoto=$idremoto;
		}
    	/////////////////////////////////DEFINIR DESTINO////////////////////////////////////////////
    	//Dependiendo del iddestino, consulta en la base de datos las gerencias  o los programas///
    	//en el caso de que sea un remoto, si es oficial, se coloca un campo de texto para que  ///
    	//indiquen el nombre del remoto y si es un remoto programa se coloca el combo de programas/
    	//o si es un ente externo se coloca un campo de texto para que coloque el destino si el ///
    	//iddestino es de camaras asignadas, se coloca un campo de texto para que indiquen ////////
    	//el destino del remoto////////////////////////////////////////////////////////////////////
    	if($iddestino== 1 ){
    		////*********$iddestino 1 son Gerencias********///////////////
    		$gerencias=$this->ObjConsulta->selectgerencia($this->conect_sigesp);
    		$destino = "<select id='destino' name='destino' style='width:375px;'>";
			$destino.="<option value='0' selected >Gerencia </option>";
       		foreach ($gerencias as $llave => $valor) {
       			$valor[2]=utf8_encode($valor[2]);
       			$destino.="<option value='" . $valor[1] . "' >" . $valor[2] . "</option>";
       		}
       		$destino.="</SELECT> ";
       		$destinos="<tr><th>Destino:</th><td>" . $destino . "</td></tr>";

    	}elseif($iddestino== 2 ){
    		////*********$iddestino 2 son Programas********///////////////
    		$datosprogramas = $this->ObjConsulta->programas($this->conect_sistemas_vtv);
    		$destino = "<select id='destino' name='destino' style='width:105px;'>";
			$destino.="<option value='0' selected >Selecione</option>";
       		foreach ($datosprogramas as $llave => $valor) {
       			$destino.="<option value='" . $valor[1] . "' >" . $valor[2] . "</option>";
       		}
       		$destino.="</SELECT> ";
       		$destinos="<tr><th>Destino:</th><td>" . $destino . "</td></tr>";
    	}elseif($iddestino== 3 ){
    		/////*********$iddestino 3 es remoto************//////////////////
    		if ($idremoto==1) {///***** Es un remoto tipo OFICIAL*****//////
    			$destinos="<tr><th>Destino:</th><td><input type='text' name='destino' id='destino' class='campo' size='46' /></td></tr>
    					   <tr><th>Placa:</th><td><input type='text' name='placa' id='placa' class='campo' size='20' /></td></tr>";
    		}else{///***** Es un remoto tipo PROGRAMA*****//////
    			$datosprogramas = $this->ObjConsulta->programas($this->conect_sistemas_vtv);
	    		$destino = "<select id='destino' name='destino' style='width:105px;'>";
				$destino.="<option value='0' selected >Selecione</option>";
	       		foreach ($datosprogramas as $llave => $valor) {
	       			$destino.="<option value='" . $valor[1] . "' >" . $valor[2] . "</option>";
	       		}
	       		$destino.="</SELECT> ";
	       		$destinos="<tr><th>Destino:</th><td>" . $destino . "</td></tr>
    					   <tr><th>Placa:</th><td><input type='text' name='placa' id='placa' class='campo' size='20' /></td></tr>";

	    	}

    	}else{
    		/////*********$iddestino 4 es responsable de la camara $iddestino 5 es ente externo************//////////////////
    		$destinos="<tr><th>Destino:</th><td><input type='text' name='destino' id='destino' class='campo' size='46' /></td></tr>";
    	}
    	/////////////////////////////////////////////////////////////////////////////////////////////////

    	//////////////////////////////DIFERENCIAR SI ES PRESTAMO O DESPACHO////////////////////////////////////
    	//Con el id_tipo_sol se identifica si es un prestamo o un despacho, en el caso de que sea un prestamo//
    	//se colocan dos calendarios, uno para la fecha de la olicitud y otro para indicar la fecha en la cual/
    	//va a entregar los equipos; si es un despacho se coloca solo un calendario para indicar la fecha//////

    	$id_tipo_sol = $_GET['id_tipo_sol'];

    	if($id_tipo_sol== 1){
    		$tipo_articulo = 1;
    		$titulo="Solicitud de pr&eacute;stamo";
    		$fechas="<tr><th>Fecha de Solicitud:</th><td><input type='text' name='fecha' id='fecha' class='campo' size='8' maxlength='8'/></td></tr>
    		        <!--<tr><th>Fecha de Entrega:</th><td><input type='text' name='fecha2' id='fecha2' class='campo' size='8' maxlength='8'/></td></tr>-->
    		        ";

    	}else{
    		$tipo_articulo = 2;
    		$titulo="Despacho";
    		$fechas="<tr><th>Fecha de Solicitud:</th><td><input type='text' name='fecha' id='fecha' class='campo' size='8' maxlength='8'/></td></tr>
    		        ";
    	}
    	/////////////////////////////////////////////////////////////////////////////////////////////////////////

    	/////////////////////////////*********CAMPOS DEL FORMULARIO*******///////////////////////////////////////
    	/////////////////////////////////////////////////////////////////////////////////////////////////////////

    	/////////////HORA///////////////////
    	$hora = "<select name='hora' id='hora' class='campo_vl'>";
        $hora.="<option value='24' selected='selected'>Hora</option>";
        $hora.= $this->opccion_com_pauta(0, 23);
        $hora.="</select>";
        $min = "<select name='min' id='min' class='campo_vl'>";
        $min.="<option value='60' selected='selected'>Min</option>";
        $min.=$this->opccion_com_pauta(0, 59);
        $min.="</select>";  


        /////////////////////////////////////QUIEN SOLICITA/////////////////////////////////////////////////////
        //Selecciona los trabajadores de las gerencias de  Serv. a la prod y programas//////////////////////////

       	if($identrada==1 or $identrada== 2 or $identrada== 9){//Grupos, equipos o grupos y equipos
       		$solicitantes=$this->ObjConsulta->selectpersonalA($this->conect_sigesp);
       	}elseif($identrada==3){//camaras
       		if($iddestino==5){
       			$solicitantes=$this->ObjConsulta->selectpersonalaut($this->conect_sigesp);
       		}else{
       			$solicitantes=$this->ObjConsulta->selectpersonalC($this->conect_sigesp);
       		}
       	}else{
       		$solicitantes=$this->ObjConsulta->selectpersonalaut($this->conect_sigesp);
       	}
      	
        $quiensolicita = "<select id='quiensolicita' name='quiensolicita' style='width:375px;'>";
		$quiensolicita.="<option value='0' selected >Seleccione </option>";
       		foreach ($solicitantes as $llave => $valor) {

       			$valor[2]=utf8_decode($valor[2]);
       			$valor[3]=utf8_decode($valor[3]);
       			$img="http://intranet/sistemas/directorio/paginas/download_foto.php?id=".base64_encode($valor[1])."'";
       			$quiensolicita.="<option value='" . $valor[1] . "' $selected title= '".$img."' >" . $valor[2] . " " . $valor[3] . "</option>";
           	}
        $quiensolicita.="</SELECT> ";
        
        if($iddestino==4){//Responsable de la Camara asignada

        	$solicitante="<tr><th>Quien Solicita:</th><td><b>El responsable</b></td></tr>";

        }else{//Gerencia, programa o remoto 	
        	$solicitante="<tr><th>Quien Solicita:</th><td>" . $quiensolicita . "</td></tr>";
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////


		///////////////////////////////////////FORMULARIO//////////////////////////////////////////////////////////
    	$datossol.= "<table class='tabla' align='center' style='width:650px;' >
    	<tr><th class='titulo' colspan='4'>" . $titulo . "</th></tr>
    	".$fechas."
        <tr><th>Hora de Solicitud:</th><td>".$hora." ".$min."</td></tr>
        ".$solicitante."
       	".$destinos."
        <!--<tr><th>Gerencia/Programa:</th><td><input type='text' name='destino' id='destino' class='campo'/></td></tr>-->
        <tr><th>Observaci&oacute;n:</th><td><textarea name='observacion' rows='5' cols='50' id='observacion' class='campo' ></textarea></td></tr>
        </table>";

        ////////////////////////////////////////////Botones/////////////////////////////////////////////////////////

        $botonC = "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('classbienvenida.php');>";

        //////////////////////////////*///////*****COMBO DE ARTICULOS DISPONIBLES******////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //$descripcion_categoria = $this->ObjConsulta->selectcategoria($this->conect_sistemas_vtv);
		

        if($identrada==1){//Son grupos

        	$datosmateriales = $this->ObjConsulta->gruposdisp($this->conect_sistemas_vtv, $id_estatus, $id_desc_estado);

        }elseif($identrada==2){//Son equipos tipo prestamo

        	$id_grupo="GP1";
        	$datosmateriales = $this->ObjConsulta->equiposdisp($this->conect_sistemas_vtv, $tipo_articulo, $id_estatus, $id_desc_estado, $id_grupo);
        	$cantdis=$datosmateriales[1][8];//SE VERIFICA LA CANTIDAD DISPONIBLE PARA QUE EL USUARIO NO SOLICITE MAS DE LO QUE HAY EN EXISTENCIA

        }elseif($identrada==3){//Son las camaras asignadas
        	if ( $iddestino==4){//responsable(Asignadas)
        		$datosmateriales = $this->ObjConsulta->camarasdisp($this->conect_sistemas_vtv, $id_estatus, $id_desc_estado);

        	}else{//no asignadas
        		$datosmateriales = $this->ObjConsulta->camarasdispsa($this->conect_sistemas_vtv, $id_estatus, $id_desc_estado);
        	}

		}elseif($identrada==8){// son todos los equipos de tipo consumible
				
			$id_grupo="GP1";
			$datosmateriales = $this->ObjConsulta->equiposdispcon($this->conect_sistemas_vtv, $tipo_articulo, $id_estatus, $id_desc_estado, $id_grupo);
	        $cantdis=$datosmateriales[1][8];//SE VERIFICA LA CANTIDAD DISPONIBLE PARA QUE EL USUARIO NO SOLICITE MAS DE LO QUE HAY EN EXISTENCIA
	        $cantmin=$datosmateriales[1][10];// SE VERIFICA LA CANTIDAD MINIMA PARA INDICARLE AL USUARIO SI EL ARTICULO ESTA EN SU NIVEL MINIMO
	        
	        if($cantdis>=$cantmin){
	        	$cant_min=$cantmin;
	        }else{
	        	$cant_min=0;
	        }
	    
	    }elseif($identrada==9){// son todos los equipos y grupos
				
			$id_grupo="GP1";
			$datosmateriales = $this->ObjConsulta->equiposdisp($this->conect_sistemas_vtv, $tipo_articulo, $id_estatus, $id_desc_estado, $id_grupo);
        	$datosmaterialesgrupo = $this->ObjConsulta->gruposdisp($this->conect_sistemas_vtv, $id_estatus, $id_desc_estado);
        	
	    
	    }else{//son equipos tipo consumibles por categoria

			$id_grupo="GP1";

			if($identrada==4){
				$id_categoria=1;
			}elseif($identrada==5){
				$id_categoria=2;
			}else{
				$id_categoria=3;
			}

        	$datosmateriales = $this->ObjConsulta->equiposdisppc($this->conect_sistemas_vtv, $tipo_articulo, $id_estatus, $id_desc_estado, $id_grupo, $id_categoria);
	        $cantdis=$datosmateriales[1][8];//SE VERIFICA LA CANTIDAD DISPONIBLE PARA QUE EL USUARIO NO SOLICITE MAS DE LO QUE HAY EN EXISTENCIA
	        $cantmin=$datosmateriales[1][10];// SE VERIFICA LA CANTIDAD MINIMA PARA INDICARLE AL USUARIO SI EL ARTICULO ESTA EN SU NIVEL MINIMO

	        
		}
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
       	//$datosmateriales = $this->ObjConsulta->materialesdisp($this->conect_sistemas_vtv, $tipo_articulo, $tipo, $id_estatus, $id_desc_estado);
        ////////////////////////////*******************Combo de materiales*********************//////////////////////////////
        if(count ($datosmateriales)==0){
	        $material = "No se encontraron art&iacute;culos disponibles";

	        $botonA = "<input type=\"button\" class='boton' value=\"Aceptar\" OnClick=CancelarRegresar('classbienvenida.php');>";
	        $materiales= "<table class='tabla' align='center' style='width:650px;' >
	        <tr><th class='titulo' colspan='5'>Materiales</th></tr>
	        <tr><th>Materiales:</th><td>" . $material . "</td>
	        </tr>
	        </table>
	        <table class='tabla' style='width:650px;'>
	        <tr><th colspan='2'><div align='center'>" . $botonA . "</div></tr>
	        </table></div>";

        }else{

        	$material = "<select id='materiales' name='materiales' class='combofoto' style='width:430px;'>";
			$material.="<option value='0' selected >Seleccione </option>";
       		foreach ($datosmateriales as $llave => $valor) {
       			//Si son grupos o camaras
       			if (($identrada==1)  or ($identrada==3)){
       				if($identrada==3){//Son camaras
       					$imagen=$valor[4];
		       			if ($imagen == ''){
							$img= "../imagenes/inventario/sinfoto.jpg";
						}else{
							$img= "../imagenes/inventario/".$imagen;
						}
	       				
	       				$material.="<option value='" . $valor[1] . "' $selected  title= '".$img."'>" . $valor[2] . " -- " . $valor[5] . "</option>";

       				}else{//Son Grupos
       					$imagen=$valor[4];
		       			if ($imagen == ''){
							$img= "../imagenes/inventario/sinfoto.jpg";
						}else{
							$img= "../imagenes/inventario/".$imagen;
						}
	       				
	       				$material.="<option value='" . $valor[1] . "' $selected  title= '".$img."'>" . $valor[2] . " -- " . $valor[3] . "</option>";

       				}
       				
       			}else{//Si son equipos y consumibles
       				
       				$descripcion_categoria = $this->ObjConsulta->desc_categoria($this->conect_sistemas_vtv, $valor[11]);
					$categoria=$descripcion_categoria[1][1];

	       			$des_unidad_medida = $this->ObjConsulta->desc_unidad_medida($this->conect_sistemas_vtv, $valor[9]);
	       			$unidad_medida=$des_unidad_medida[1][1];

	       			$imagen=$valor[7];
		       		if ($imagen == ''){
						$img= "../imagenes/inventario/sinfoto.jpg";
					}else{
						$img= "../imagenes/inventario/".$imagen;
					}

	       			$material.="<option value='" . $valor[1] . "' cantdis='".$valor[8]."' cantmin='".$valor[10]."'  $selected  title= '".$img."'>" . $valor[2] . " " . $valor[3] . " " . $valor[4] . " Bien Nac.:" . $valor[5] . " Serial: " . $valor[6] . " -- " . $valor[3] . "</option>";

       				
       				
       			}
       			/*if($identrada==2){
       				
       				$des_unidad_medida = $this->ObjConsulta->desc_unidad_medida($this->conect_sistemas_vtv, $valor[9]);
       				$unidad_medida=$des_unidad_medida[1][1];

       				$imagen=$valor[7];
	       			if ($imagen == ''){
						$img= "../imagenes/inventario/sinfoto.jpg";
					}else{
						$img= "../imagenes/inventario/".$imagen;
					}

       				$material.="<option value='" . $valor[1] . "' cantdis='".$valor[11]."'  $selected  title= '".$img."'>" . $valor[2] . " " . $valor[3] . " " . $valor[4] . " Bien Nac.:" . $valor[5] . " Serial: " . $valor[6] . "</option>";
       			}elseif ($identrada==1) {
       				$imagen=$valor[4];
	       			if ($imagen == ''){
						$img= "../imagenes/inventario/sinfoto.jpg";
					}else{
						$img= "../imagenes/inventario/".$imagen;
					}
       				
       				$material.="<option value='" . $valor[1] . "' $selected  title= '".$img."'>" . $valor[2] . "</option>";
       			}else{
       				$imagen=$valor[4];
	       			if ($imagen == ''){
						$img= "../imagenes/inventario/sinfoto.jpg";
					}else{
						$img= "../imagenes/inventario/".$imagen;
					}

       				$material.="<option value='" . $valor[1] . "' $selected  title= '".$img."'>" . $valor[2] . "</option>";
       			}*/
			
           }
           	if($identrada==9){//Si son equipos y grupos
           		//Primero organiza arriba en el else los combos de $datosmateriales que son los equipos//// 
           		//en este caso y luego los concatena con los de $datosmaterialesgrupo que son los grupos///
           		foreach ($datosmaterialesgrupo as $llave => $valor) {
	           		$imagen=$valor[4];
		       		if ($imagen == ''){
						$img= "../imagenes/inventario/sinfoto.jpg";
					}else{
						$img= "../imagenes/inventario/".$imagen;
					}
	       				
       				$material.="<option value='" . $valor[1] . "' $selected  title= '".$img."'>" . $valor[2] . " -- " . $valor[3] . "</option>";

           		}	
           	}
           	

        	$material.="</SELECT> ";

        	/////////////////////////////**************Lista de lo solicitado******************///////////////////
        	//////////////////////////////////////////////////////////////////////////////////////////////////////
        	if ($id_tipo_sol== 1){//Son prestamos (Despachos)

        		$botonA = "<input type=\"button\" class='boton' value=\"Guardar\" OnClick=datossolicitud($identrada,$iddestino,$idremoto);>";
       			$cantidad="<td><input type=\"button\" class='boton' value=\"+\" OnClick=agregarmaterialalistapres();></td>";

       			$botones="
        		<div id='livetable'>
        		<table class='tabla' align='center' style='display:none; width:650px;' >
        			<tbody>
        				<tr>
        					<th class='titulo' colspan='3'>Materiales Seleccionados</th>
        				</tr>
        				<tr>
        					<th>NOMBRE</th>
        					<th>ACCION</th>
        				</tr>
        			</tbody>
       			</table>
        		</div>

        		<table class='tabla' style='width:650px;'>
        		<tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        		</table></div>";

       		}else{//Son despachos de suministros

       			//<input type='hidden' name='cantdisp' id='cantdisp' value='".$cantdis."' class='campo'/>
       			$botonA = "<input type=\"button\" class='boton' value=\"Guardar\" OnClick=datossolicituddesp($identrada,$iddestino,$idremoto);>";
       			$cantidad="<th>Cantidad:</th><td><input type='text' name='cantidad' id='cantidad' class='campo' size='3'/>
       			           </td>
       					   <td><input type=\"button\" class='boton' value=\"+\" OnClick=agregarmaterialesalista();></td>";
       		
	       		$botones="
	        		<div id='livetable'>
	        		<table class='tabla' align='center' style='display:none; width:650px;' >
	        			<tbody>
	        				<tr>
	        					<th class='titulo' colspan='3'>Materiales Seleccionados</th>
	        				</tr>
	        				<tr>
	        					<th>NOMBRE</th>
	        					<th>CANTIDAD</th>
	        					<th>ACCION</th>
	        				</tr>
	        			</tbody>
	       			</table>
	        		</div>

	        	<table class='tabla' style='width:650px;'>
	        	<tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
	        	</table></div>";
       	}
       	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        	

        $materiales= "<table class='tabla' align='center' style='width:650px;' >
        <tr><th class='titulo' colspan='5'>Materiales</th></tr>
        <tr><td>" . $material . "</td>
        ".$cantidad."
        </tr>
        </table>";
        }
       
        echo $datossol;
        echo $materiales;
        echo $botones;
    }

    //Estas funcion es para generar incidencias de una solicitud de despacho ya generada
    function datosincidencias(){
    	$idsolicitud=$_GET['idsolicitud'];//Solicitud principal a la cual se le va a generar una incidencia
    	$datossolcitud=$this->ObjConsulta->selectdatossol($this->conect_sistemas_vtv, $idsolicitud);
		$identrada=$datossolcitud[1][12];//Tipo_prestamo, Si es 1 es grupo, si es 2 es equipos, si es 9 son grupos y equipos y si es 3 son camaras.
		$iddestino=$datossolcitud[1][1];//Si es gerencia 1 , programa 2 ,  remoto 3 o responsable de camara 4
		$id_estatus=1;//Estatus DISPONIBLE
    	$id_desc_estado=1;//Estado del equipo (BUENO)
    	$idremoto=$_GET['idremoto'];//Oficial o programas

    	
    	$tipo_articulo = 1;
    		$titulo="Incidencia";
    		$fechas="<tr><th>Fecha de Solicitud:</th><td><input type='text' name='fecha' id='fecha' class='campo' size='8' maxlength='8'/></td></tr>
    		        ";
    
    	/////////////////////////////*********CAMPOS DEL FORMULARIO*******///////////////////////////////////////
    	/////////////////////////////////////////////////////////////////////////////////////////////////////////

    	/////////////HORA///////////////////
    	$hora = "<select name='hora' id='hora' class='campo_vl'>";
        $hora.="<option value='24' selected='selected'>Hora</option>";
        $hora.= $this->opccion_com_pauta(0, 23);
        $hora.="</select>";
        $min = "<select name='min' id='min' class='campo_vl'>";
        $min.="<option value='60' selected='selected'>Min</option>";
        $min.=$this->opccion_com_pauta(0, 59);
        $min.="</select>";  


        /////////////////////////////////////QUIEN SOLICITA/////////////////////////////////////////////////////
        //selecciona los trabajadores de las gerencias de  Serv. a la prod y programas//////////////////////////

       	if($identrada==1 or $identrada== 2 or $identrada== 9){//Grupos, equipos o grupos y equipos
       		$solicitantes=$this->ObjConsulta->selectpersonalA($this->conect_sigesp);
       	}else{//camaras
       		$solicitantes=$this->ObjConsulta->selectpersonalC($this->conect_sigesp);
       	}
      	
        $quiensolicita = "<select id='quiensolicita' name='quiensolicita' style='width:375px;'>";
		$quiensolicita.="<option value='0' selected >Seleccione </option>";
       		foreach ($solicitantes as $llave => $valor) {

       			$valor[2]=utf8_decode($valor[2]);
       			$valor[3]=utf8_decode($valor[3]);
       			$img="http://intranet/sistemas/directorio/paginas/download_foto.php?id=".base64_encode($valor[1])."'";
       			$quiensolicita.="<option value='" . $valor[1] . "' $selected title= '".$img."' >" . $valor[2] . " " . $valor[3] . "</option>";
           	}
        $quiensolicita.="</SELECT> ";
        
        if($iddestino==4){//Responsable de la Camara asignada

        	$solicitante="<tr><th>Quien Solicita:</th><td><b>El responsable</b></td></tr>";

        }else{//Gerencia, programa o remoto 	
        	$solicitante="<tr><th>Quien Solicita:</th><td>" . $quiensolicita . "</td></tr>";
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////


		///////////////////////////////////////FORMULARIO//////////////////////////////////////////////////////////
    	$datossol.= "<table class='tabla' align='center' style='width:650px;' >
    	<tr><th class='titulo' colspan='4'>" . $titulo . "</th></tr>
    	<tr><th>N° Solicitud:</th><td><b>".$idsolicitud."</b></td></tr>
    	".$fechas."
        <tr><th>Hora de Solicitud:</th><td>".$hora." ".$min."</td></tr>
        ".$solicitante."
       	<tr><th>Observaci&oacute;n:</th><td><textarea name='observacion' rows='5' cols='50' id='observacion' class='campo' ></textarea></td></tr>
        </table>";

        ////////////////////////////////////////////Botones/////////////////////////////////////////////////////////

        $botonC = "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('classbienvenida.php');>";

        //////////////////////////////*///////*****COMBO DE ARTICULOS DISPONIBLES******////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    

		$id_grupo="GP1";
		$datosmateriales = $this->ObjConsulta->equiposdisp($this->conect_sistemas_vtv, $tipo_articulo, $id_estatus, $id_desc_estado, $id_grupo);
        $datosmaterialesgrupo = $this->ObjConsulta->gruposdisp($this->conect_sistemas_vtv, $id_estatus, $id_desc_estado);
        	
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
       	////////////////////////////*******************Combo de materiales*********************//////////////////////////////
        if(count ($datosmateriales)==0 and $datosmaterialesgrupo==0){
	        $material = "No se encontraron art&iacute;culos disponibles";

	        $botonA = "<input type=\"button\" class='boton' value=\"Aceptar\" OnClick=CancelarRegresar('classbienvenida.php');>";
	        $materiales= "<table class='tabla' align='center' style='width:650px;' >
	        <tr><th class='titulo' colspan='5'>Materiales</th></tr>
	        <tr><th>Materiales:</th><td>" . $material . "</td>
	        </tr>
	        </table>
	        <table class='tabla' style='width:650px;'>
	        <tr><th colspan='2'><div align='center'>" . $botonA . "</div></tr>
	        </table></div>";

        }else{
        	$material = "<select id='materiales' name='materiales' class='combofoto' style='width:430px;'>";
			$material.="<option value='0' selected >Seleccione </option>";
       		foreach ($datosmateriales as $llave => $valor) {
       			$descripcion_categoria = $this->ObjConsulta->desc_categoria($this->conect_sistemas_vtv, $valor[11]);
				$categoria=$descripcion_categoria[1][1];

       			$des_unidad_medida = $this->ObjConsulta->desc_unidad_medida($this->conect_sistemas_vtv, $valor[9]);
       			$unidad_medida=$des_unidad_medida[1][1];

       			$imagen=$valor[7];
	       		if ($imagen == ''){
					$img= "../imagenes/inventario/sinfoto.jpg";
				}else{
					$img= "../imagenes/inventario/".$imagen;
				}

       			$material.="<option value='" . $valor[1] . "' cantdis='".$valor[8]."' cantmin='".$valor[10]."'  $selected  title= '".$img."'>" . $categoria . "--" . $valor[2] . " " . $valor[3] . " " . $valor[4] . " Bien Nac.:" . $valor[5] . " Serial: " . $valor[6] . "</option>";
			}

			foreach ($datosmaterialesgrupo as $llave => $valor) {
	           		$imagen=$valor[4];
		       		if ($imagen == ''){
						$img= "../imagenes/inventario/sinfoto.jpg";
					}else{
						$img= "../imagenes/inventario/".$imagen;
					}
	       				
       				$material.="<option value='" . $valor[1] . "' $selected  title= '".$img."'>" . $valor[3] . " --" . $valor[2] . "</option>";

           	}
           	

        	$material.="</SELECT> ";

        	/////////////////////////////**************Lista de lo solicitado******************///////////////////
        	//////////////////////////////////////////////////////////////////////////////////////////////////////
        	

        		$botonA = "<input type=\"button\" class='boton' value=\"Guardar\" OnClick=datosincidencia($identrada,$idsolicitud);>";
       			$cantidad="<td><input type=\"button\" class='boton' value=\"+\" OnClick=agregarmaterialalistapres();></td>";

       			$botones="
        		<div id='livetable'>
        		<table class='tabla' align='center' style='display:none; width:650px;' >
        			<tbody>
        				<tr>
        					<th class='titulo' colspan='3'>Materiales Seleccionados</th>
        				</tr>
        				<tr>
        					<th>NOMBRE</th>
        					<th>ACCION</th>
        				</tr>
        			</tbody>
       			</table>
        		</div>

        		<table class='tabla' style='width:650px;'>
        		<tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        		</table></div>";

       	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        	

        $materiales= "<table class='tabla' align='center' style='width:650px;' >
        <tr><th class='titulo' colspan='5'>Materiales</th></tr>
        <tr><td>" . $material . "</td>
        ".$cantidad."
        </tr>
        </table>";
        }
       
        echo $datossol;
        echo $materiales;
        echo $botones;
    }
    //Esta funcion realiza los insert de los datos de la solicitud de prestamo e indica que se realizo con exito
    // el registro
    function guardararticulossol(){
    	//var_dump($_GET);
    	$identrada=$_GET['identrada'];//Si es 1 es grupo, si es 2 es equipos, si es 9 son gruposy equipos y si es 3 son camaras.
    	$iddestino=$_GET['iddestino'];//Si es gerencia 1 , programa 2 ,  remoto 3 o responsable de camara 4
    	$idremoto=$_GET['idremoto'];//Si es 1 es oficial o es 2 es programa
    	$id=$_GET['id'];//equipos y7o grupos solicitados
    	$valores=$_GET['valores'];//las cantidades(solo despachos de suministros)
    	$fecha_sol=$_GET['fecha'];
    	//$fecha_exp=$_GET['fecha2'];
    	$fecha_reg= date("Y-m-d");
    	$hora=$_GET['hora'];
    	$min=$_GET['min'];
    	$quiensol=$_GET['quiensolicita'];
    	$quienent=$_SESSION['cedula'];//User_reg
    	$destino=$_GET['destino'];//Id de la gerencia o del programa, o la descripcion del remoto
    	$cantidad=$_GET['cantidad'];
    	$observacion=$_GET['observacion'];
    	$placa=$_GET['placa'];//En caso de que sea un remoto
    	$id_estatus_prestamo= 4; //Solicitud en proceso
    	$id_estatus_articulo=2;//Estatus SOLICITADO
    	$ids=explode(',',$id);//Convertir en array
    	//print_r($ids);
    	$cont=count($ids);

    	if($placa=='' or $placa == "undefined"){
    		$placa='';

    	}else{
    		$placa=$placa;
    	}

    	if ($hora <= 9){
			$hora="0".$hora;
		}
		if ($min <= 9){
			$min="0".$min;
		}
		$hora=$hora.":".$min;

		if($iddestino== 4 ){//Son camaras asignadas
			$art=trim($ids[0]);
			$resp_cam=$this->ObjConsulta->respasig($this->conect_sistemas_vtv, $art);
			$resp=$resp_cam[1][1];
			$conresp=count($resp);
			if ($conresp==0){
				$mensaje = "<div style='color: #FF0000;font-weight: bold;'><br>La camara seleccionada no tiene responsable asignado, la solicitud no pudo ser registrada!<div><br>";
				echo $this->ObjMensaje->InterfazExitosamente($mensaje);
				echo"<script>
				var pagina='classlista.php?modulo=listadegrupos';                    
				function redireccionar() { 
					location.href=pagina;
				} 	
				setTimeout ('redireccionar()', 3000);
				</script>";
			}else{//Registro de la solicitud de la camara asignada
				$id_incidencia=0;
				$tipo_prestamo=3;
				$quiensol=$resp;
				$insertdatosprestamo = $this->ObjConsulta->insertsolprestamo($this->conect_sistemas_vtv, $iddestino, $destino, $quiensol, $quienent, $fecha_reg, $fecha_sol, $id_estatus_prestamo, $observacion, $hora, $placa, $tipo_prestamo, $idremoto); //fecha_exp se quito del query
	    		$idprestamo=$insertdatosprestamo[0];
	    		$art=trim($ids[0]);
	    		$insertarticulo = $this->ObjConsulta->insertcamarassol($this->conect_sistemas_vtv, $art, $idprestamo, $fecha_reg, $quienent, $id_incidencia);
	    		$art_camara = $this->ObjConsulta->selectartgrupo($this->conect_sistemas_vtv, $art);
	    		foreach ($art_camara as $llave => $valor) {
	    			$act_estatus = $this->ObjConsulta->updateestart($this->conect_sistemas_vtv, $valor[1], $id_estatus_articulo);
	    		}
	    		$act_estatus = $this->ObjConsulta->updateestgrup($this->conect_sistemas_vtv, $art, $id_estatus_articulo);
	    		$mensaje = "<div style='color: #009900;font-weight: bold;'><br>La solicitud fue procesada con exito, su solicitud es la: ".$idprestamo."<div><br>";
				echo $this->ObjMensaje->InterfazExitosamente($mensaje);
				echo"<script>
				var reporte='../reportes/prestamo.php?id_solicitud=".$idprestamo."&identrada".$identrada."';   
				var pagina='classbienvenida.php';                    
				function redireccionar() { 
					location.href=pagina;
					window.open(reporte)
				} 	
				setTimeout ('redireccionar()', 3000);
				</script>";
				}
			

		}else{
			/////////////////////////////////////Registro de los datos generales de la solicitud//////////////////////////
			$tipo_prestamo=$identrada;
			$insertdatosprestamo = $this->ObjConsulta->insertsolprestamo($this->conect_sistemas_vtv, $iddestino, $destino, $quiensol, $quienent, $fecha_reg, $fecha_sol, $id_estatus_prestamo, $observacion, $hora, $placa, $tipo_prestamo, $idremoto);//fecha_exp se quito del query
	    	$idprestamo=$insertdatosprestamo[0];
	    	$id_incidencia=0;
	    	//////////////////////////////////Insertando los articulos solicitados ////////////////////////////////////////
	    	for ($i = 0; $i < $cont; $i++) { 
	    		$art=trim($ids[$i]);
	    		if ($identrada==1){//Son grupos

	    			$insertarticulo = $this->ObjConsulta->insertgrupossol($this->conect_sistemas_vtv, $art, $idprestamo, $fecha_reg, $quienent, $id_incidencia);
	    			$art_grupo = $this->ObjConsulta->selectartgrupo($this->conect_sistemas_vtv, $art);
	    			foreach ($art_grupo as $llave => $valor) {
	    				$act_estatus = $this->ObjConsulta->updateestart($this->conect_sistemas_vtv, $valor[1], $id_estatus_articulo);
	    			}
	    			$act_estatus = $this->ObjConsulta->updateestgrup($this->conect_sistemas_vtv, $art, $id_estatus_articulo);

	    		}elseif($identrada==2){//Son equipos
	    			$insertarticulo = $this->ObjConsulta->insertarticulosol($this->conect_sistemas_vtv, $art, $idprestamo, $fecha_reg, $quienent, $id_incidencia);
	    			$act_estatus = $this->ObjConsulta->updateestart($this->conect_sistemas_vtv, $art, $id_estatus_articulo);

	    		}elseif($identrada==9){//Son grupos y equipos

	    			$var=substr($art, 0, 2);
	    			if ($var=='GP'){//Grupos
	    				$insertarticulo = $this->ObjConsulta->insertgrupossol($this->conect_sistemas_vtv, $art, $idprestamo, $fecha_reg, $quienent, $id_incidencia);
		    			$art_grupo = $this->ObjConsulta->selectartgrupo($this->conect_sistemas_vtv, $art);
		    			foreach ($art_grupo as $llave => $valor) {
		    				$act_estatus = $this->ObjConsulta->updateestart($this->conect_sistemas_vtv, $valor[1], $id_estatus_articulo);
		    			}
		    			$act_estatus = $this->ObjConsulta->updateestgrup($this->conect_sistemas_vtv, $art, $id_estatus_articulo);
	    			}else{//Equipos
	    				$insertarticulo = $this->ObjConsulta->insertarticulosol($this->conect_sistemas_vtv, $art, $idprestamo, $fecha_reg, $quienent, $id_incidencia);
	    				$act_estatus = $this->ObjConsulta->updateestart($this->conect_sistemas_vtv, $art, $id_estatus_articulo);

	    			}
	    		}else{//Camaras
	    			$insertarticulo = $this->ObjConsulta->insertcamarassol($this->conect_sistemas_vtv, $art, $idprestamo, $fecha_reg, $quienent, $id_incidencia);
	    			$art_camara = $this->ObjConsulta->selectartgrupo($this->conect_sistemas_vtv, $art);
	    			foreach ($art_camara as $llave => $valor) {
	    				$act_estatus = $this->ObjConsulta->updateestart($this->conect_sistemas_vtv, $valor[1], $id_estatus_articulo);
	    			}
	    			$act_estatus = $this->ObjConsulta->updateestgrup($this->conect_sistemas_vtv, $art, $id_estatus_articulo);
	    		}

			} 
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	    	$mensaje = "<div style='color: #009900;font-weight: bold;'><br>La solicitud fue procesada con exito, su solicitud es la: ".$idprestamo."<div><br>";
			echo $this->ObjMensaje->InterfazExitosamente($mensaje);
			echo"<script>
			var reporte='../reportes/prestamo.php?id_solicitud=".$idprestamo."&identrada=".$identrada."';  
			var pagina='classbienvenida.php';                    
			function redireccionar() { 
				location.href=pagina;
				window.open(reporte)
			} 	
			setTimeout ('redireccionar()', 3000);
			</script>";
		}
    }
    //Esta funcion se encarga de registrar los datos generales de las incidencia y los articulos seleccionados
    function guardararticulosinc(){
    	//var_dump($_GET);
    	$idsolicitud=$_GET['idsolicitud'];//Solicitud principal
    	$identrada=$_GET['identrada'];//Si es 1 es grupo, si es 2 es equipos, si es 9 son gruposy equipos y si es 3 son camaras.
    	$id=$_GET['id'];//equipos y7o grupos solicitados
    	$valores=$_GET['valores'];//las cantidades(solo despachos de suministros)
    	$fecha_sol=$_GET['fecha'];
    	$fecha_reg= date("Y-m-d");
    	$hora=$_GET['hora'];
    	$min=$_GET['min'];
    	$quiensol=$_GET['quiensolicita'];
    	$quienent=$_SESSION['cedula'];//User_reg
    	$observacion=$_GET['observacion'];
    	$id_estatus_articulo=2;//Estatus SOLICITADO
    	$ids=explode(',',$id);//Convertir en array
    	//print_r($ids);
    	$cont=count($ids);

    	if ($hora <= 9){
			$hora="0".$hora;
		}
		if ($min <= 9){
			$min="0".$min;
		}
		$hora=$hora.":".$min;

			/////////////////////////////////////Registro de los datos generales de la solicitud//////////////////////////
			//$tipo_prestamo=$identrada;
			$existeninc=$this->ObjConsulta->selectincidencias($this->conect_sistemas_vtv, $idsolicitud);
			$ninc=count($existeninc);
			$numinc=$ninc+1;
			$id_incidencia=$numinc;
			$insertincidencia = $this->ObjConsulta->insertincidencia($this->conect_sistemas_vtv, $quiensol, $quienent, $fecha_reg, $fecha_sol, $observacion, $hora, $idsolicitud, $numinc);
	    	
	    	
	    	//////////////////////////////////Insertando los articulos solicitados ////////////////////////////////////////
	    	for ($i = 0; $i < $cont; $i++) { 
	    		$art=trim($ids[$i]);
	    		$var=substr($art, 0, 2);
	    		if ($var=='GP'){//Grupos
	    			$insertarticulo = $this->ObjConsulta->insertgrupossol($this->conect_sistemas_vtv, $art, $idsolicitud, $fecha_reg, $quienent, $id_incidencia);
		    		$art_grupo = $this->ObjConsulta->selectartgrupo($this->conect_sistemas_vtv, $art);
		    		foreach ($art_grupo as $llave => $valor) {
		    			$act_estatus = $this->ObjConsulta->updateestart($this->conect_sistemas_vtv, $valor[1], $id_estatus_articulo);
		    		}
		    		$act_estatus = $this->ObjConsulta->updateestgrup($this->conect_sistemas_vtv, $art, $id_estatus_articulo);
	    		}else{//Equipos
	    			$insertarticulo = $this->ObjConsulta->insertarticulosol($this->conect_sistemas_vtv, $art, $idsolicitud, $fecha_reg, $quienent, $id_incidencia);
	    			$act_estatus = $this->ObjConsulta->updateestart($this->conect_sistemas_vtv, $art, $id_estatus_articulo);

	    		}
	    		

			} 
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			$mensaje = "<div style='color: #009900;font-weight: bold;'><br>La solicitud fue procesada con exito, incidencia: ".$idsolicitud."-".$numinc."<div><br>";
			echo $this->ObjMensaje->InterfazExitosamente($mensaje);
			echo"<script>
			var reporte='../reportes/incidencia.php?id_solicitud=".$idsolicitud."&id_incidencia=".$id_incidencia."';  
			var pagina='classbienvenida.php';                    
			function redireccionar() { 
				location.href=pagina;
				window.open(reporte)
			} 	
			setTimeout ('redireccionar()', 3000);
			</script>";
	}

    //Esta funcion realiza los insert de los datos del despacho e indica que se realizo con exito el registro
    function guardararticulosdesp(){
    	//var_dump($_GET);
    	$identrada=$_GET['identrada'];
    	$iddestino=$_GET['iddestino'];
    	$idremoto=$_GET['idremoto'];
    	$id=$_GET['id'];
    	$valores=$_GET['valores'];
    	$fecha_sol=$_GET['fecha'];
    	$fecha_reg= date("Y-m-d");
    	$hora=$_GET['hora'];
    	$min=$_GET['min'];
    	$quiensol=$_GET['quiensolicita'];
    	$quienent=$_SESSION['cedula'];
    	$destino=$_GET['destino'];
    	$cantidad=$_GET['cantidad'];
    	$observacion=$_GET['observacion'];
    	$sol_ppal=1;
    	$id_estatus_despacho= 3;
    	$ids=explode(',',$id);
    	$cantidades=explode(',',$valores);
    	//print_r($ids);
    	$cont=count($ids);

    	if ($hora <= 9){
			$hora="0".$hora;
		}
		if ($min <= 9){
			$min="0".$min;
		}
		$hora=$hora.":".$min;

    	$insertdatosdespacho = $this->ObjConsulta->insertsoldespacho($this->conect_sistemas_vtv, $iddestino, $destino, $quiensol, $quienent, $fecha_reg, $fecha_sol, $id_estatus_despacho, $observacion, $hora, $idremoto, $sol_ppal);
    	$iddespacho=$insertdatosdespacho[0];
    	
    	for ($i = 0; $i < $cont; $i++) { 
    		$art=trim($ids[$i]);
    		$cant=trim($cantidades[$i]);
    		
    		$cantidaddisp = $this->ObjConsulta->selectcantdisp($this->conect_sistemas_vtv, $art);
    		$cantdisp=$cantidaddisp[1][1];
    		$cantidad=$cantdisp-$cant;
    		if($cantidad==0){
    			$id_estatus_articulo=6;
    		}else{
    			$id_estatus_articulo=1;
    		}

    		$insertarticulo = $this->ObjConsulta->insertarticulodesp($this->conect_sistemas_vtv, $art, $iddespacho, $fecha_reg, $quienent, $cant, $sol_ppal);
    		$act_estatus = $this->ObjConsulta->updateart($this->conect_sistemas_vtv, $art, $id_estatus_articulo, $cantidad);
		} 
		
    	$mensaje = "<div style='color: #009900;font-weight: bold;'><br>El despacho se realizo con exito<div><br>";
		echo $this->ObjMensaje->InterfazExitosamente($mensaje);
	    echo"<script>
		var reporte='../reportes/despacho.php?id_solicitud=".$iddespacho."&identrada=".$identrada."'; 
		var pagina='classbienvenida.php';                    
		function redireccionar() { 
			location.href=pagina;
			window.open(reporte)
		} 	
		setTimeout ('redireccionar()', 3000);
		</script>";
    }

    //Esta funcion se encarga de realizar los updates de los articulos que se van a devolver
    function devoluciones(){
    	//var_dump($_GET);
    	$iddespacho=$_GET['idsolicitud'];
    	$id=$_GET['id'];
    	$valores=$_GET['valores'];
    	$fecha_sol=$_GET['fecha'];
    	$fecha_reg= date("Y-m-d");
    	$hora=$_GET['hora'];
    	$min=$_GET['min'];
    	$resp_devol=$_GET['quiensolicita'];
    	$user_reg=$_SESSION['cedula'];
    	$user_exp=$_SESSION['cedula'];
    	$fecha_exp=date("Y-m-d");
    	$cantidad=$_GET['cantidad'];
    	$observacion=$_GET['observacion'];
    	$hora_dev=date("H:i:s");
    	$hora_exp=date("H:i:s");
    	$ids=explode(',',$id);
    	
    	$cantidades=explode(',',$valores);
    	//print_r($ids);
    	$cont=count($ids);

    	if ($hora <= 9){
			$hora="0".$hora;
		}
		if ($min <= 9){
			$min="0".$min;
		}
		$hora=$hora.":".$min;
		
    	for ($i = 0; $i < $cont; $i++) { 
    		$art=trim($ids[$i]);
    		$cant=trim($cantidades[$i]);
    		
    		$cantidaddisp = $this->ObjConsulta->selectcantdisp($this->conect_sistemas_vtv, $art);
    		$cantdisp=$cantidaddisp[1][1];
    		$cantidad=$cantdisp+$cant;
    		if($cantidad==0){
    			$id_estatus_articulo=6;
    		}else{
    			$id_estatus_articulo=1;
    		}

    		//Select de la cantidad original de la solicitud
	    	$cantanterior = $this->ObjConsulta->selectcantidad($this->conect_sistemas_vtv, $art, $iddespacho);
	    	$cantant=$cantanterior[1][1];
	    	//Expirar el articulo despachado
    		$updatearticulo = $this->ObjConsulta->updatearticulodesp($this->conect_sistemas_vtv, $art, $iddespacho, $user_exp, $fecha_exp, $resp_devol, $hora_exp, $hora_dev);
    		//Registrar el articulo despachado nuevamente con la nueva cantidad
    		$cantact=$cantant-$cant;
	    	$insertarticulo = $this->ObjConsulta->insertarticulodespdev($this->conect_sistemas_vtv, $art, $iddespacho, $user_reg, $fecha_reg, $cantact);
			//Actualizar la existencia del articulo en t_articulo
	    	$act_estatus = $this->ObjConsulta->updateart($this->conect_sistemas_vtv, $art, $id_estatus_articulo, $cantidad);
		} 
		
    	$mensaje = "<div style='color: #009900;font-weight: bold;'><br>La devolucion se realizo con exito<div><br>";
		echo $this->ObjMensaje->InterfazExitosamente($mensaje);
	    echo"<script>
		var reporte='../reportes/despacho.php?id_solicitud=".$iddespacho."'; 
	    var pagina='classlista.php?modulo=listadedespachos';                    
		function redireccionar() { 
			location.href=pagina;
			window.open(reporte)
		} 	
		setTimeout ('redireccionar()', 3000);
		</script>";
    }

    //Esta funcion mostrar un formulario para suspender el despacho
    function datossuspendersol(){
    	$idsolicitud=$_GET['idsolicitud'];
    	////////////////////////*****Quien Solicita*****/////////////////////////////
    	$solicitantes=$this->ObjConsulta->selectpersonalA($this->conect_sigesp);
       	
        $quiensolicita = "<select id='quiensolicita' name='quiensolicita' style='width:375px;'>";
		$quiensolicita.="<option value='0' selected >Seleccione </option>";
       		foreach ($solicitantes as $llave => $valor) {

       			$valor[2]=utf8_decode($valor[2]);
       			$valor[3]=utf8_decode($valor[3]);
       			$img="http://intranet/sistemas/directorio/paginas/download_foto.php?id=".base64_encode($valor[1])."'";
       			$quiensolicita.="<option value='" . $valor[1] . "' $selected title= '".$img."' >" . $valor[2] . " " . $valor[3] . "</option>";
           	}
        $quiensolicita.="</SELECT> ";
        
        
        $solicitante="<tr><th>Quien Solicita:</th><td>" . $quiensolicita . "</td></tr>";
        

        /////////////**********Formulario**********///////////////////
        $botonC = "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('classbienvenida.php');>";
	    $botonA = "<input type=\"button\" class='boton' value=\"Aceptar\" OnClick=datossuspendersol($idsolicitud);>";
       			   
	    $titulo="Suspender Solicitud";
    	$datossol.= "<table class='tabla' align='center' style='width:650px;' >
    	<tr><th class='titulo' colspan='4'>" . $titulo . "</th></tr>
    	<tr><th>N° Solicitud:</th><td><b>".$idsolicitud."</b></td></tr>
    	".$solicitante."
       	<tr><th>Observaci&oacute;n:</th><td><textarea name='observacion' rows='5' cols='50' id='observacion' class='campo' ></textarea></td></tr>
        </table>
        <table class='tabla' style='width:650px;'>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table></div>";
        echo $datossol;
    }

    //Esta funcion se encarga de expirar las solicitudes de despacho y cambiar el estatus a suspendido, 
    //expirando en t_prestamo la solicitud y en cada tabla los articulos solicitados.
    function suspendersol(){
    	$idsolicitud=$_GET['idsolicitud'];
    	$quiensol=$_GET['quiensolicita'];
    	$obs_susp=$_GET['observacion'];

    	//Hacer Update del estatus de la solicitud a 6 (suspendido) y expirar 
		$id_estatus_prestamo = 6;//Solicitud suspendida
	    $act_sol = $this->ObjConsulta->updatesolicitudsusp($this->conect_sistemas_vtv, $idsolicitud, $id_estatus_prestamo, $quiensol, $obs_susp);
	    $datosfechaentrada = $this->ObjConsulta->updatefechaentrada($this->conect_sistemas_vtv, $idsolicitud);

		////////*******Grupos*********////////////
    	$datosmaterialespres = $this->ObjConsulta->selectgruposprestados($this->conect_sistemas_vtv, $idsolicitud);
		$cont=count($datosmaterialespres);
		if($cont!=0){
		  	foreach ($datosmaterialespres as $llave => $valor){
		  	$art=$valor[1];
			//Cambiar fecha de exp en t_grupo_sol(para que ya no este solicitado)
	    	$act_estatus = $this->ObjConsulta->updategrupres($this->conect_sistemas_vtv, $art, $_SESSION['cedula']);
	    	//Cambiar estatus del articulo a 1 en t_articulo(para que este disponible)
	    	$id_estatus = 1;
	    	$act_disp = $this->ObjConsulta->updatedispgrupo($this->conect_sistemas_vtv, $art, $id_estatus);
	    	//Cambiar estatus a todos los equipos del grupo
	    	$act_disp = $this->ObjConsulta->updatedispartgrupo($this->conect_sistemas_vtv, $art, $id_estatus);
	    	}	
		}
		////////*******Equipos*********////////////
		$datosmaterialespres = $this->ObjConsulta->selectequiposprestados($this->conect_sistemas_vtv, $idsolicitud);
		$cont2=count($datosmaterialespres);
		if ($cont2!=0){
			foreach ($datosmaterialespres as $llave => $valor) {
			$art=$valor[1];
			//Cambiar fecha de exp en t_material_sol(para que ya no este solicitado)
	    	$act_estatus = $this->ObjConsulta->updateartpres($this->conect_sistemas_vtv, $art, $_SESSION['cedula']);
	    	//Cambiar estatus del articulo a 1 en t_articulo(para que este disponible)
	    	$id_estatus = 1;
	    	$act_disp = $this->ObjConsulta->updatedispart($this->conect_sistemas_vtv, $art, $id_estatus);
	    	}
		}
		//////////*****Camaras********//////////////////	
		$datosmaterialespres = $this->ObjConsulta->selectccamarasprestada($this->conect_sistemas_vtv, $idsolicitud);
		$cont3=count($datosmaterialespres);
		if ($cont3!=0){
			foreach ($datosmaterialespres as $llave => $valor) {
		   	$art=$valor[1];
			//Cambiar fecha de exp en t_camara_sol(para que ya no este solicitado)
		   	$act_estatus = $this->ObjConsulta->updatecampres($this->conect_sistemas_vtv, $art, $_SESSION['cedula']);
		   	//Cambiar estatus del articulo a 1 en t_articulo(para que este disponible)
		   	$id_estatus = 1;
		   	$act_disp = $this->ObjConsulta->updatedispgrupo($this->conect_sistemas_vtv, $art, $id_estatus);
			//Cambiar estatus a todos los equipos del grupo
	    	$act_disp = $this->ObjConsulta->updatedispartgrupo($this->conect_sistemas_vtv, $art, $id_estatus);
	    	}
		}

		$mensaje = "<div style='color: #009900;font-weight: bold;'><br>La solicitud se suspendio con exito!<div><br>";
		echo $this->ObjMensaje->InterfazExitosamente($mensaje);
	    echo"<script>
		var pagina='classbienvenida.php';                    
		function redireccionar() { 
			location.href=pagina;
		} 	
		setTimeout ('redireccionar()', 3000);
		</script>";
		//var reporte='../reportes/despacho.php?id_solicitud=".$iddespacho."'; 
	    //window.open(reporte)
    }

    //Esta funcion se encarga de asignar un responsable al grupo
    function asigarticulo(){
    	$idgrupo=$_GET['idgrupo'];
    	
    	$desc_grupo = $this->ObjConsulta->desc_grupo($this->conect_sistemas_vtv, $idgrupo);
		$desc_grupo= $desc_grupo[1][1];

    	$responsable=$this->ObjConsulta->respasig

    	($this->conect_sistemas_vtv, $idgrupo);
    	$resp=$responsable[1][1];

    	$cont=count($resp);

    	
    	if($cont!=0){

    		$datosresp=$this->ObjConsulta->selectpersonalresp($this->conect_sigesp, $resp);
    		$nombres=utf8_encode($datosresp[1][2]);
			$apellidos=utf8_encode($datosresp[1][3]);
			$img="<img style='width:60px; height:60px; 'src='http://intranet/sistemas/directorio/paginas/download_foto.php?id=".base64_encode($resp)."'>";

    		$datos= "<div id='datosp' align='center'>
			<table class='tabla' align='center' style='width:850px;' >
			<tbody> 
			<tr><th class='titulo' colspan='3'>" . $desc_grupo . "</th></tr>
			<tr><th>Responsable:</th><td>".$img." ".$nombres." ".$apellidos."</td></tr>
			</tbody>
			</table>";
   			$botonA = "<input type=\"button\" class='boton' value=\"Cambiar\" OnClick=cambiarasigrupo('$idgrupo');>";
			
    	}else{

    		$titulo = "ASIGNAR RESPONSABLE";
        	
        	$solicitantes=$this->ObjConsulta->selectpersonalC($this->conect_sigesp);
      		$responsable = "<select id='responsable' name='responsable' style='width:375px;'>";
			$responsable.="<option value='0' selected >Seleccione </option>";
       		foreach ($solicitantes as $llave => $valor) {

       			$valor[2]=utf8_decode($valor[2]);
       			$valor[3]=utf8_decode($valor[3]);
       			$img="http://intranet/sistemas/directorio/paginas/download_foto.php?id=".base64_encode($valor[1])."'";
       			$responsable.="<option value='" . $valor[1] . "' $selected title= '".$img."' >" . $valor[2] . " " . $valor[3] . "</option>";
           	}
        	$responsable.="</SELECT> ";

   			$datos= "<div id='datosp' align='center'><table class='tabla' align='center' style='width:850px;' >
        	<tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        	<tr><th>Responsable:</th><td>" . $responsable . "</td></tr>
			</table>";
   			$botonA = "<input type=\"button\" class='boton' value=\"Agregar\" OnClick=asigrupo('$idgrupo');>";
   			$lista = 'classlista.php?modulo=listadegrupos';

    	}	
        $botonC = "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('classlista.php?modulo=listadegrupos');>";

   		$datos.="<table class='tabla' style='width:850px;'>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table></div>";
    	
    	echo $datos;
    }

    function cambiarasigrupo(){
    	$idgrupo=$_GET['idgrupo'];

    	$titulo = "ASIGNAR RESPONSABLE";
        	
        $solicitantes=$this->ObjConsulta->selectpersonalC($this->conect_sigesp);
      	$responsable = "<select id='responsable' name='responsable' style='width:375px;'>";
		$responsable.="<option value='0' selected >Seleccione </option>";
       	foreach ($solicitantes as $llave => $valor) {

       		$valor[2]=utf8_decode($valor[2]);
       		$valor[3]=utf8_decode($valor[3]);
       		$img="http://intranet/sistemas/directorio/paginas/download_foto.php?id=".base64_encode($valor[1])."'";
       		$responsable.="<option value='" . $valor[1] . "' $selected title= '".$img."' >" . $valor[2] . " " . $valor[3] . "</option>";
        }
        $responsable.="</SELECT> ";

   		$datos= "<div id='datosp' align='center'><table class='tabla' align='center' style='width:850px;' >
        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
        <tr><th>Responsable:</th><td>" . $responsable . "</td></tr>
		</table>";
   		$botonA = "<input type=\"button\" class='boton' value=\"Agregar\" OnClick=asigrupo('$idgrupo');>";
   		$botonC = "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('classlista.php?modulo=listadegrupos');>";

   		$datos.="<table class='tabla' style='width:850px;'>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table></div>";
    	
    	echo $datos;

    }
    function asigrupo(){
    	$idgrupo=$_GET['idgrupo'];
    	$responsable=$_GET['responsable'];

	    $asignacion = $this->ObjConsulta->updateresponsable($this->conect_sistemas_vtv, $idgrupo, $responsable);

	    $mensaje = "<div style='color: #009900;font-weight: bold;'><br>La asignacion se realizo con exito<div><br>";
		echo $this->ObjMensaje->InterfazExitosamente($mensaje);

		echo"<script>var pagina='classlista.php?modulo=listadegrupos';                     
		function redireccionar() { 
			location.href=pagina;
		} 	
		setTimeout ('redireccionar()', 3000);
		</script>";
    }

	//Esta funcion muestra una consulta de los datos de la solicitud a la cual se le va a realizar la recepcion
	//previo al ingreso de los materiales.
    function entrada(){
    	$idarticulo=$_GET['idarticulo'];
		$identrada=$_GET['identrada'];//Si consulta de quien es el responsable del prestamo o si es una entrada
    	$idsolicitud=$_GET['idsolicitud'];
    	$idlista=$_GET['idlista'];
    	$titulo1="Datos de la solicitud";
    

    	if($identrada==50){//Es consulta

    		if($idlista==1){
    			$lista="classlista.php?modulo=listadegrupos";
    			
    			$datosasig=$this->ObjConsulta->selectidasig($this->conect_sistemas_vtv, $idarticulo);
    			$id_asig=$datosasig[1][1];

    			if($id_asig==2){//es un grupo no asignable
					$solicitudpres = $this->ObjConsulta->selectidprestamogrupo($this->conect_sistemas_vtv, $idarticulo);
	    			$idsolicitud=$solicitudpres[1][1];
	    		
    			}elseif($id_asig==1){//es un grupo asignable(camara)
	    			$solicitudpres = $this->ObjConsulta->selectidprestamocamara($this->conect_sistemas_vtv, $idarticulo);
	    			$idsolicitud=$solicitudpres[1][1];
	    		
    			} 	

    		}elseif($idlista==2){
    			$lista="classlista.php?modulo=listadealmacen";

    			//buscar en t_articulo el grupo al que corresponde el articulo y si es asinable  o no.
	    		$datosart = $this->ObjConsulta->selectdatosart($this->conect_sistemas_vtv, $idarticulo);
	    		$id_grupo=$datosart[1][1];

	    		if($id_grupo=='GP1'){//no tiene grupo, es un equipo, busco el id de la solicitud
	    			$solicitudpres = $this->ObjConsulta->selectidprestamoequipo($this->conect_sistemas_vtv, $idarticulo);
	    			$idsolicitud=$solicitudpres[1][1];
	    		}else{//es un grupo
	    			$datosasig=$this->ObjConsulta->selectidasig($this->conect_sistemas_vtv, $id_grupo);
	    			$id_asig=$datosasig[1][1];

	    			if($id_asig==2){//es un grupo no asignable
						$solicitudpres = $this->ObjConsulta->selectidprestamogrupo($this->conect_sistemas_vtv, $id_grupo);
		    			$idsolicitud=$solicitudpres[1][1];
		    		
	    			}elseif($id_asig==1){//es un grupo asignable(camara)
		    			$solicitudpres = $this->ObjConsulta->selectidprestamocamara($this->conect_sistemas_vtv, $id_grupo);
		    			$idsolicitud=$solicitudpres[1][1];
		    		
	    			}
	    		}
    		}else{
    			$lista="classlista.php?modulo=listadeequiposprestados";

    			//buscar en t_articulo el grupo al que corresponde el articulo y si es asinable  o no.
	    		$datosart = $this->ObjConsulta->selectdatosart($this->conect_sistemas_vtv, $idarticulo);
	    		$id_grupo=$datosart[1][1];

	    		if($id_grupo=='GP1'){//no tiene grupo, es un equipo, busco el id de la solicitud
	    			$solicitudpres = $this->ObjConsulta->selectidprestamoequipo($this->conect_sistemas_vtv, $idarticulo);
	    			$idsolicitud=$solicitudpres[1][1];
	    		}else{//es un grupo
	    			$datosasig=$this->ObjConsulta->selectidasig($this->conect_sistemas_vtv, $id_grupo);
	    			$id_asig=$datosasig[1][1];

	    			if($id_asig==2){//es un grupo no asignable
						$solicitudpres = $this->ObjConsulta->selectidprestamogrupo($this->conect_sistemas_vtv, $id_grupo);
		    			$idsolicitud=$solicitudpres[1][1];
		    		
	    			}elseif($id_asig==1){//es un grupo asignable(camara)
		    			$solicitudpres = $this->ObjConsulta->selectidprestamocamara($this->conect_sistemas_vtv, $id_grupo);
		    			$idsolicitud=$solicitudpres[1][1];
		    		
	    			}
	    		}

    		}
    		
    		count($solicitudpres);
    		
    		if (count($solicitudpres)==0){
    			$mensaje = "<div style='color: #009900;font-weight: bold;'><br>El articulo se encuentra disponible<div><br>";
				echo $this->ObjMensaje->InterfazExitosamente($mensaje);

				//deberia ir a la lista de las entradas
				echo"<script>var pagina='".$lista."';                     
				function redireccionar() { 
					location.href=pagina;
				} 	
				setTimeout ('redireccionar()', 3000);
				</script>";

    		}else{
    			$botonC = "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('".$lista."');>";

				$botones="<table class='tabla' style='width:650px;'>
				<tr><th colspan='1'><div align='center'>" . $botonC . "</div></tr>
				</table></div>";

    			$datossolicitud = $this->ObjConsulta->selectdatossol($this->conect_sistemas_vtv, $idsolicitud);
				$id_destino=$datossolicitud[1][1];
				$id_desc_dest=$datossolicitud[1][2];
				$resp_prestamo=$datossolicitud[1][3];
				$user_reg=$datossolicitud[1][4];
				$fecha_reg=$datossolicitud[1][5];
	        	$fecha_reg = $this->Objfechahora->flibInvertirInEs($fecha_reg);
		        $fecha_sol=$datossolicitud[1][6];
		        $fecha_sol = $this->Objfechahora->flibInvertirInEs($fecha_sol);
	        	$fecha_exp=$datossolicitud[1][7];
	        	$fecha_exp = $this->Objfechahora->flibInvertirInEs($fecha_exp);
	        	$id_estatus_prestamo=$datossolicitud[1][8];
	        	$observacion =$datossolicitud[1][9];
			        
		        if($id_destino==1){//es para un gerencia
		        	$tipo_destino="Gerencia:";
		        	$datosgerencia=$this->ObjConsulta->selectgerenciadesc($this->conect_sigesp, $id_desc_dest);
		        	$destino=utf8_encode($datosgerencia[1][2]);
		        	
		        }elseif($id_destino==2){//es para un programa
		        	$tipo_destino=" Programa:";
		        	$datosdestino = $this->ObjConsulta->selectprograma($this->conect_sistemas_vtv, $id_desc_dest);
		        	$destino=$datosdestino[1][2];

		        }elseif($id_destino==3){//es un remoto (oficial o programa)
				    	if(is_numeric ($id_desc_dest)==true){
				    		$tipo_destino=" Programa:";
			        		$datosdestino = $this->ObjConsulta->selectprograma($this->conect_sistemas_vtv, $id_desc_dest);
			        		$destino=$datosdestino[1][2];

				    	}else{
				    		$tipo_destino="Lugar:";
			        		$destino=$id_desc_dest;
				    	}
			   		
			    }else{//es un remoto o una camara asignada
		        	$tipo_destino=" Remoto:";
		        	$destino=$id_desc_dest;
		        }

				        
				if($observacion == ''){
					
					$observacion ='Sin Observaciones';
				}
				       	
				$responsable=$this->ObjConsulta->selectpersonalresp($this->conect_sigesp, $resp_prestamo);
				$nombres=utf8_encode($responsable[1][2]);
				$apellidos=utf8_encode($responsable[1][3]);
						
				$img="<img style='width:60px; height:60px; 'src='http://intranet/sistemas/directorio/paginas/download_foto.php?id=".base64_encode($resp_prestamo)."'>";
						//"http://intranet/sistemas/directorio/paginas/download_foto.php?id=".base64_encode($valor[1])."'";
		    		$datossolicitud= "<div id='datosp' align='center'>
		    		<table class='tabla' align='center' style='width:650px;' >
		    		<tbody> 
		        	<tr><th class='titulo' colspan='2'>" . $titulo1 . "</th></tr>
		        	<tr><th></th><td>".$img."</td></tr>
		        	<tr><th>Solicitud:</th><td>".$idsolicitud."</td></tr>
		        	<tr><th>Responsable:</th><td>".$nombres." ".$apellidos."</td></tr>
		        	<tr><th>".$tipo_destino."</th><td>".$destino."</td></tr>
		        	<tr><th>Fecha de solicitud:</th><td>".$fecha_sol."</td></tr>
		        	<!--<tr><th>Fecha de entrega:</th><td>".$fecha_exp."</td></tr>-->
		        	<tr><th>Observaci&oacute;n:</th><td>".$observacion."</td></tr>
		        	</tbody>
		       		</table>";

		       		$datossolicitud= $datossolicitud.$botones;
				       	
				echo $datossolicitud;
    		}
			
    	}else{//Es una entrada
    		$solicitud = $this->ObjConsulta->selectsolicitud($this->conect_sistemas_vtv, $idsolicitud);
    		$tipo_prestamo=$solicitud[1][2];
    		$existe= count ($solicitud);

	    	if ($existe==0){
	    		////***** Consultar si la solicitud existe, si no mostrar un mensaje*****////
	    		$mensaje = "<div style='color: #009900;font-weight: bold;'><br>La solicitud no exite, intente de nuevo<div><br>";
				echo $this->ObjMensaje->InterfazExitosamente($mensaje);

				//deberia ir a la lista de las entradas
				echo"<script>var pagina='classlista.php?modulo=listadeprestamos';                     
				function redireccionar() { 
					location.href=pagina;
				} 	
				setTimeout ('redireccionar()', 3000);
				</script>";

	    	}else{

	    		////***** Consultar si tienen articulos por ingresar, si no mostrar un mensaje*****////	
	    		/*if($tipo_prestamo==1){//es de grupos
	    			$datosmaterialespres = $this->ObjConsulta->selectgruposprestados($this->conect_sistemas_vtv, $idsolicitud);

	    		}elseif($tipo_prestamo==2){//es de equipos
	    			$datosmaterialespres = $this->ObjConsulta->selectequiposprestados($this->conect_sistemas_vtv, $idsolicitud);

	    		}elseif ($tipo_prestamo==9) {//Son grupos y equipos
	    			$datosmaterialespresgrup = $this->ObjConsulta->selectgruposprestados($this->conect_sistemas_vtv, $idsolicitud);
	    			$datosmaterialespres = $this->ObjConsulta->selectequiposprestados($this->conect_sistemas_vtv, $idsolicitud);

	    		}else{//es camaras
	    			$datosmaterialespres = $this->ObjConsulta->selectccamaraprestada($this->conect_sistemas_vtv, $idsolicitud);

	    		}*/
	    		if ($tipo_prestamo==3){//es camaras
	    			$datosmaterialespres = $this->ObjConsulta->selectccamaraprestada($this->conect_sistemas_vtv, $idsolicitud);

	    		}else{//Son materiales y equipos
	    			$datosmaterialespresgrup = $this->ObjConsulta->selectgruposprestados($this->conect_sistemas_vtv, $idsolicitud);
	    			$datosmaterialespres = $this->ObjConsulta->selectequiposprestados($this->conect_sistemas_vtv, $idsolicitud);

	    		}


	    		$artpres= count ($datosmaterialespres);
	    		$artpres2= count ($datosmaterialespresgrup);
	        	
	        	if($artpres==0 and $artpres2==0){
		        	$mensaje = "<div style='color: #009900;font-weight: bold;'><br>Todos los articulos de la solicitud ya fueron ingresado<div><br>";
					echo $this->ObjMensaje->InterfazExitosamente($mensaje);

					echo"<script>var pagina='classlista.php?modulo=listadeentradas';                     
					function redireccionar() { 
						location.href=pagina;
					} 	
					setTimeout ('redireccionar()', 3000);
					</script>";
	        	}else{
			        $botonA = "<input type=\"button\" class='boton' value=\"Aceptar\" OnClick=entrada('".$idsolicitud."','".$identrada."');>";
			   	    $botonC = "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('classbienvenida.php');>";

			   		$botones="<table class='tabla' style='width:650px;'>
			        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
			        </table></div>";
			       		           
			    	//Se encarga de consultar la informacion de la solicitud

			    	$datossolicitud = $this->ObjConsulta->selectdatossol($this->conect_sistemas_vtv, $idsolicitud);
			    	$id_destino=$datossolicitud[1][1];
			       	$id_desc_dest=$datossolicitud[1][2];
			        $resp_prestamo=$datossolicitud[1][3];
			       	$user_reg=$datossolicitud[1][4];
			        $fecha_reg=$datossolicitud[1][5];
			        $fecha_reg = $this->Objfechahora->flibInvertirInEs($fecha_reg);
			        $fecha_sol=$datossolicitud[1][6];
			        $fecha_sol = $this->Objfechahora->flibInvertirInEs($fecha_sol);
			        $fecha_exp=$datossolicitud[1][7];
			        $fecha_exp = $this->Objfechahora->flibInvertirInEs($fecha_exp);
			        $id_estatus_prestamo=$datossolicitud[1][8];
			        $observacion =$datossolicitud[1][9];
			        
			        if($id_destino==1){//es para un gerencia
			        	$tipo_destino="Gerencia:";
			        	$datosgerencia=$this->ObjConsulta->selectgerenciadesc($this->conect_sigesp, $id_desc_dest);
			        	$destino=utf8_encode($datosgerencia[1][2]);
			        	
			        }elseif($id_destino==2){//es para un programa
			        	$tipo_destino=" Programa:";
			        	$datosdestino = $this->ObjConsulta->selectprograma($this->conect_sistemas_vtv, $id_desc_dest);
			        	$destino=$datosdestino[1][2];

			        }elseif($id_destino==3){//es un remoto (oficial o programa)
				    	if(is_numeric ($id_desc_dest)==true){
				    		$tipo_destino=" Programa:";
			        		$datosdestino = $this->ObjConsulta->selectprograma($this->conect_sistemas_vtv, $id_desc_dest);
			        		$destino=$datosdestino[1][2];

				    	}else{
				    		$tipo_destino="Lugar:";
			        		$destino=$id_desc_dest;
				    	}
			   		
			    	}else{//es un remoto o una camara asignada
			        	$tipo_destino="Lugar:";
			        	$destino=$id_desc_dest;
			        }

			        if($observacion == ''){
						$observacion ='Sin Observaciones';
					}
			       	
					$responsable=$this->ObjConsulta->selectpersonalresp($this->conect_sigesp, $resp_prestamo);
					$nombres=utf8_encode($responsable[1][2]);
					$apellidos=utf8_encode($responsable[1][3]);
					
					$img="<img style='width:60px; height:60px; 'src='http://intranet/sistemas/directorio/paginas/download_foto.php?id=".base64_encode($resp_prestamo)."'>";
					//"http://intranet/sistemas/directorio/paginas/download_foto.php?id=".base64_encode($valor[1])."'";
			    		$datossolicitud= "<div id='datosp' align='center'>
			    		<table class='tabla' align='center' style='width:650px;' >
			    		<tbody> 
			        	<tr><th class='titulo' colspan='2'>" . $titulo1 . "</th></tr>
			        	<tr><th></th><td>".$img."</td></tr>
		        		<tr><th>Solicitud:</th><td>".$idsolicitud."</td></tr>
			        	<tr><th>Responsable:</th><td>".$nombres." ".$apellidos."</td></tr>
			        	<tr><th>".$tipo_destino."</th><td>".$destino."</td></tr>
			        	<tr><th>Fecha de solicitud:</th><td>".$fecha_sol."</td></tr>
			        	<!--<tr><th>Fecha de entrega:</th><td>".$fecha_exp."</td></tr>-->
			        	<tr><th>Observaci&oacute;n:</th><td>".$observacion."</td></tr>
			        	</tbody>
			       		</table>";

			       		$datossolicitud= $datossolicitud.$botones;
			       	
					echo $datossolicitud;
	    		}
	  
	        }
    	}

    	

    	//////////////////////////////////////////////////////////////////////////////////////
	}

	function consultardespacho(){
    	
    	$idsolicitud=$_GET['idsolicitud'];
    	$titulo1="Datos de la solicitud";
	    //Se encarga de consultar la informacion de la solicitud

		$datossolicitud = $this->ObjConsulta->selectdatossol($this->conect_sistemas_vtv, $idsolicitud);
		$id_destino=$datossolicitud[1][1];
		$id_desc_dest=$datossolicitud[1][2];
		$resp_prestamo=$datossolicitud[1][3];
		$user_reg=$datossolicitud[1][4];
		$fecha_reg=$datossolicitud[1][5];
		$fecha_reg = $this->Objfechahora->flibInvertirInEs($fecha_reg);
		$fecha_sol=$datossolicitud[1][6];
		$fecha_sol = $this->Objfechahora->flibInvertirInEs($fecha_sol);
		$fecha_exp=$datossolicitud[1][7];
		$fecha_exp = $this->Objfechahora->flibInvertirInEs($fecha_exp);
		$id_estatus_prestamo=$datossolicitud[1][8];
		$observacion =$datossolicitud[1][9];
		$resp_susp =$datossolicitud[1][13];
		$obs_susp=$datossolicitud[1][14];
			        
			        if($id_destino==1){//es para un gerencia
			        	$tipo_destino="Gerencia:";
			        	$datosgerencia=$this->ObjConsulta->selectgerenciadesc($this->conect_sigesp, $id_desc_dest);
			        	$destino=utf8_encode($datosgerencia[1][2]);
			        	
			        }elseif($id_destino==2){//es para un programa
			        	$tipo_destino=" Programa:";
			        	$datosdestino = $this->ObjConsulta->selectprograma($this->conect_sistemas_vtv, $id_desc_dest);
			        	$destino=$datosdestino[1][2];

			        }elseif($id_destino==3){//es un remoto (oficial o programa)
				    	if(is_numeric ($id_desc_dest)==true){
				    		$tipo_destino=" Programa:";
			        		$datosdestino = $this->ObjConsulta->selectprograma($this->conect_sistemas_vtv, $id_desc_dest);
			        		$destino=$datosdestino[1][2];

				    	}else{
				    		$tipo_destino="Lugar:";
			        		$destino=$id_desc_dest;
				    	}
			   		
			    	}else{//es un remoto o una camara asignada o un F5
			        	$tipo_destino="Lugar:";
			        	$destino=$id_desc_dest;
			        }

			        if($observacion == ''){
						$observacion ='Sin Observaciones';
					}
			       	
					$responsable=$this->ObjConsulta->selectpersonalresp($this->conect_sigesp, $resp_prestamo);
					$nombres=utf8_encode($responsable[1][2]);
					$apellidos=utf8_encode($responsable[1][3]);

					$responsable=$this->ObjConsulta->selectpersonalresp($this->conect_sigesp, $resp_susp);
					$nombressusp=utf8_encode($responsable[1][2]);
					$apellidossusp=utf8_encode($responsable[1][3]);

					if($id_estatus_prestamo==6){
						$susp="<tr><th>Suspendido por:</th><td>".$nombressusp." ".$apellidossusp."</td></tr>
			        	       <tr><th>Observaci&oacute;n :</th><td>".$obs_susp."</td></tr>
			        	";
					}else{
						$susp=" ";	
					}
					

					$botonC = "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('classlista.php?modulo=listadeprestamos');>";
					$botones="<table class='tabla' style='width:650px;'>
                        <tr><th colspan='2'><div align='center'>" . $botonC . "</div></tr>
                        </table></div>";

					$img="<img style='width:60px; height:60px; 'src='http://intranet/sistemas/directorio/paginas/download_foto.php?id=".base64_encode($resp_prestamo)."'>";
					//"http://intranet/sistemas/directorio/paginas/download_foto.php?id=".base64_encode($valor[1])."'";
			    		$datossolicitud= "<div id='datosp' align='center'>
			    		<table class='tabla' align='center' style='width:650px;' >
			    		<tbody> 
			        	<tr><th class='titulo' colspan='2'>" . $titulo1 . "</th></tr>
			        	<tr><th></th><td>".$img."</td></tr>
		        		<tr><th>Solicitud:</th><td>".$idsolicitud."</td></tr>
			        	<tr><th>Responsable:</th><td>".$nombres." ".$apellidos."</td></tr>
			        	<tr><th>".$tipo_destino."</th><td>".$destino."</td></tr>
			        	<tr><th>Fecha de solicitud:</th><td>".$fecha_sol."</td></tr>
			        	<!--<tr><th>Fecha de entrega:</th><td>".$fecha_exp."</td></tr>-->
			        	<tr><th>Observaci&oacute;n:</th><td>".$observacion."</td></tr>
			        	".$susp."
			        	</tbody>
			       		</table>
			       		";

			       		$datossolicitud= $datossolicitud.$botones;
			       	
					echo $datossolicitud;
	}

    //Esta funcion muestra un formulario para ingresar los datos de//
    // la recepcion, y muestra los articulos que van a devolver/////
    //Esto es solo la recepcion de la entrada///////////////////////
    function ingresodemateriales (){
    	///verificar estatus de la solicitud, si es 4 hacer todo, sino, mostrar un mensaje de que ya fue entregado

    	$idsolicitud=  $_GET['idsolicitud'];
    	$identrada=  $_GET['identrada'];
    	$tipo_despacho= $_GET['tipo_despacho'];
    	$titulo="Entrada";

        if($tipo_despacho==1){//es una entrada
        	$solicitantes=$this->ObjConsulta->selectpersonalaut($this->conect_sigesp);
	        $quienentrega = "<select id='quienentrega' name='quienentrega' style='width:375px;'>";
			$quienentrega.="<option value='0' selected >Seleccione </option>";
	       		foreach ($solicitantes as $llave => $valor) {

	       			$valor[2]=utf8_decode($valor[2]);
	       			$valor[3]=utf8_decode($valor[3]);
	       			$img="http://intranet/sistemas/directorio/paginas/download_foto.php?id=".base64_encode($valor[1])."'";
	       			$quienentrega.="<option value='" . $valor[1] . "' $selected title= '".$img."' >" . $valor[2] . " " . $valor[3] . "</option>";
	           	}
	        $quienentrega.="</SELECT> ";

	        //////////    //Combo de hora y minutos////////////////
	        $hora = "<select name='hora' id='hora' class='campo_vl'>";
	        $hora.="<option value='24' selected='selected'>Hora</option>";
	        $hora.= $this->opccion_com_pauta(0, 23);
	        $hora.="</select>";
	        $min = "<select name='min' id='min' class='campo_vl'>";
	        $min.="<option value='60' selected='selected'>Min</option>";
	        $min.=$this->opccion_com_pauta(0, 59);
	        $min.="</select>";  

			
	        $material = "<select id='materiales' name='materiales' style='width:375px;'>";
			$material.="<option value='0' selected >Seleccione </option>";
			//////////*********Grupos**********///////
			$datosmaterialespres = $this->ObjConsulta->selectgruposprestados($this->conect_sistemas_vtv, $idsolicitud);
		    $cont=count($datosmaterialespres);
		    if($cont!=0){
		    	foreach ($datosmaterialespres as $llave => $valor){
		    		$datosgrupospres = $this->ObjConsulta->selectdatosgrupo($this->conect_sistemas_vtv, $valor[1]);
		    		$id_grupo=$datosgrupospres[1][1];
		    		$descripcion=$datosgrupospres[1][2];
		    		$imagen=$datosgrupospres[1][3];
		    		
		       		if ($imagen == ''){
						$img= "../imagenes/inventario/sinfoto.jpg";
					}else{
						$img= "../imagenes/inventario/".$imagen;
					}
		       	
		       		$material.="<option value='" . $id_grupo . "' $selected  title= '".$img."'>" . $descripcion . "</option>";
		       		
		       	}	
		    }
		    ////////*******Equipos*********////////////
		    $datosmaterialespres = $this->ObjConsulta->selectequiposprestados($this->conect_sistemas_vtv, $idsolicitud);
		    $cont2=count($datosmaterialespres);
		    if ($cont2!=0){
		    	foreach ($datosmaterialespres as $llave => $valor) {
		    		$datosequipopres = $this->ObjConsulta->selectdatosequipo($this->conect_sistemas_vtv, $valor[1]);
		    		$id_articulo=$datosequipopres[1][1];
			        $tipo_articulo=$datosequipopres[1][2];
			        $descripcion=$datosequipopres[1][3];
			        $marca=$datosequipopres[1][4];
			        $modelo=$datosequipopres[1][5];
			       	$bien_nac=$datosequipopres[1][6];
			        $serial=$datosequipopres[1][7];
			        $imagen=$datosequipopres[1][17];
			        
		       		if ($imagen == ''){
						$img= "../imagenes/inventario/sinfoto.jpg";
					}else{
						$img= "../imagenes/inventario/".$imagen;
					}

	       			$material.="<option value='" . $id_articulo . "' $selected  title= '".$img."'>" . $descripcion . " " . $marca . " " . $modelo . " Bien Nac.:" . $bien_nac . " Serial: " . $serial . "</option>";

		       	}
		    }
		    //////////*****Camaras********//////////////////	
		    $datosmaterialespres = $this->ObjConsulta->selectccamarasprestada($this->conect_sistemas_vtv, $idsolicitud);
		    $cont3=count($datosmaterialespres);
		    if ($cont3!=0){
		    	$datosmaterialespres = $this->ObjConsulta->selectccamarasprestada($this->conect_sistemas_vtv, $idsolicitud);
		    	foreach ($datosmaterialespres as $llave => $valor) {
		    		$datosgrupospres = $this->ObjConsulta->selectdatosgrupo($this->conect_sistemas_vtv, $valor[1]);
		    		$id_grupo=$datosgrupospres[1][1];
		    		$descripcion=$datosgrupospres[1][2];
		    		$imagen=$datosgrupospres[1][3];
		    		
		       		if ($imagen == ''){
						$img= "../imagenes/inventario/sinfoto.jpg";
					}else{
						$img= "../imagenes/inventario/".$imagen;
					}
		       	
		       		$material.="<option value='" . $id_grupo . "' $selected  title= '".$img."'>" . $descripcion . "</option>";
		       		
		       	}
		    }
		    $material.="</SELECT> ";	
		    ///////////////////////////////////////////////////////////////////////////////////////////////
			/*
	        if($identrada==1){//es de grupos
		    	$datosmaterialespres = $this->ObjConsulta->selectgrupoprestado($this->conect_sistemas_vtv, $idsolicitud);
		    	$material = "<select id='materiales' name='materiales' style='width:375px;'>";
				$material.="<option value='0' selected >Seleccione </option>";
		       	foreach ($datosmaterialespres as $llave => $valor) {
		    		$datosgrupospres = $this->ObjConsulta->selectdatosgrupo($this->conect_sistemas_vtv, $valor[1]);
		    		$id_grupo=$datosgrupospres[1][1];
		    		$descripcion=$datosgrupospres[1][2];
		    		$imagen=$datosgrupospres[1][3];
		    		
		       		if ($imagen == ''){
						$img= "../imagenes/inventario/sinfoto.jpg";
					}else{
						$img= "../imagenes/inventario/".$imagen;
					}
		       	
		       		$material.="<option value='" . $id_grupo . "' $selected  title= '".$img."'>" . $descripcion . "</option>";
		       		
		       	}
		        
		       	
		        $material.="</SELECT> ";
		    }elseif($identrada==2){//es de equipos

		    	$datosmaterialespres = $this->ObjConsulta->selectequipoprestados($this->conect_sistemas_vtv, $idsolicitud);
		    	$material = "<select id='materiales' name='materiales' style='width:375px;'>";
				$material.="<option value='0' selected >Seleccione </option>";
		       	foreach ($datosmaterialespres as $llave => $valor) {
		    		$datosequipopres = $this->ObjConsulta->selectdatosequipo($this->conect_sistemas_vtv, $valor[1]);
		    		$id_articulo=$datosequipopres[1][1];
			        $tipo_articulo=$datosequipopres[1][2];
			        $descripcion=$datosequipopres[1][3];
			        $marca=$datosequipopres[1][4];
			        $modelo=$datosequipopres[1][5];
			       	$bien_nac=$datosequipopres[1][6];
			        $serial=$datosequipopres[1][7];
			        $imagen=$datosequipopres[1][17];
			        
		       		if ($imagen == ''){
						$img= "../imagenes/inventario/sinfoto.jpg";
					}else{
						$img= "../imagenes/inventario/".$imagen;
					}

	       			$material.="<option value='" . $id_articulo . "' $selected  title= '".$img."'>" . $descripcion . " " . $marca . " " . $modelo . " Bien Nac.:" . $bien_nac . " Serial: " . $serial . "</option>";

		       	}
		        
		       	
		        $material.="</SELECT> ";
		    }else{//es camaras

		    	$datosmaterialespres = $this->ObjConsulta->selectccamaraprestada($this->conect_sistemas_vtv, $idsolicitud);
		    	$material = "<select id='materiales' name='materiales' style='width:375px;'>";
				$material.="<option value='0' selected >Seleccione </option>";
		       	foreach ($datosmaterialespres as $llave => $valor) {
		    		$datosgrupospres = $this->ObjConsulta->selectdatosgrupo($this->conect_sistemas_vtv, $valor[1]);
		    		$id_grupo=$datosgrupospres[1][1];
		    		$descripcion=$datosgrupospres[1][2];
		    		$imagen=$datosgrupospres[1][3];
		    		
		       		if ($imagen == ''){
						$img= "../imagenes/inventario/sinfoto.jpg";
					}else{
						$img= "../imagenes/inventario/".$imagen;
					}
		       	
		       		$material.="<option value='" . $id_grupo . "' $selected  title= '".$img."'>" . $descripcion . "</option>";
		       		
		       	}
		        
		       	
		        $material.="</SELECT> ";
		    }
			*/
	        ////////////////////////////ESTADO //////////////////////////
	        $descripcion_estado = $this->ObjConsulta->estado($this->conect_sistemas_vtv);
	        $estado = "<select id='estado' name='estado' style='width:105px;'>";
			$estado.="<option value='0' selected >Seleccione </option>";
	       	foreach ($descripcion_estado as $llave => $valor) {

	        		$estado.="<option value='" . $valor[1] . "' >" . $valor[2] . "</option>";
	        }
	        $estado.="</SELECT> ";

	                                
	        //Formulario para ingresar los datos generales de la entrada.
	        $datosrecepcion= "<div id='datosp' align='center'><table class='tabla' align='center' style='width:650px;' >
	        <tr><th class='titulo' colspan='2'>" . $titulo . "</th></tr>
	        <tr><th>Fecha de entrega:</th><td><input type='text' name='fecha' id='fecha' class='campo' size='8' maxlength='8'/></td></tr>
	        <tr><th>Hora de entrega:</th><td>".$hora." ".$min."</td></tr>
	        <tr><th>Quien Entrega:</th><td>" . $quienentrega . "</td></tr>
	        <!--<tr><th>Estado:</th><td>" . $estado . "</td></tr>-->
	        <tr><th>Observaci&oacute;n:</th><td><textarea name='observacion' rows='5' cols='50' id='observacion' class='campo' ></textarea></td></tr>
	        </table>";

	        $botonA = "<input type=\"button\" class='boton' value=\"Guardar\" OnClick=ingreso('".$idsolicitud."','".$identrada."');>";
	        $botonC = "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('classbienvenida.php');>";

	        $entrar="<td><input type=\"button\" class='boton' value=\"Ingresar\" OnClick=agregarmaterialalista();></td>";

	       	$botones="
	        	<div id='livetable'>
	        	<table class='tabla' align='center' style='display:none; width:650px;' >
	        		<tbody>
	        			<tr>
	        				<th class='titulo' colspan='3'>Materiales Seleccionados</th>
	        			</tr>
	        			<tr>
	        				<th>NOMBRE</th>
	        				<th>ESTADO</th>
	        				<th>ACCION</th>
	        			</tr>
	        		</tbody>
	       		</table>
	        	</div>

	        	<table class='tabla' style='width:650px;'>
	        	<tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
	        	</table></div>";


	        $datosrecepcion.= "<div id='datosp' align='center'><table class='tabla' align='center' style='width:650px;' >
	            <tr><th class='titulo' colspan='5'>Materiales</th></tr>
	            <tr><td>" . $material . "</td><td>" . $estado . "</td><td>".$entrar."</tr>
	        	</table>";
	    


        }else{////////////////**************despacho de suministros*******************////////////////////////

        	 ///////////////***********************Devoluciones*********************/////////////////////

        	$solicitantes=$this->ObjConsulta->selectpersonalaut($this->conect_sigesp);
	        $quienentrega = "<select id='quiensolicita' name='quiensolicita' style='width:375px;'>";
			$quienentrega.="<option value='0' selected >Seleccione </option>";
	       		foreach ($solicitantes as $llave => $valor) {

	       			$valor[2]=utf8_decode($valor[2]);
	       			$valor[3]=utf8_decode($valor[3]);
	       			$img="http://intranet/sistemas/directorio/paginas/download_foto.php?id=".base64_encode($valor[1])."'";
	       			$quienentrega.="<option value='" . $valor[1] . "' $selected title= '".$img."' >" . $valor[2] . " " . $valor[3] . "</option>";
	           	}
	        $quienentrega.="</SELECT> ";

	        //////////    //Combo de hora y minutos////////////////
	        $hora = "<select name='hora' id='hora' class='campo_vl'>";
	        $hora.="<option value='24' selected='selected'>Hora</option>";
	        $hora.= $this->opccion_com_pauta(0, 23);
	        $hora.="</select>";
	        $min = "<select name='min' id='min' class='campo_vl'>";
	        $min.="<option value='60' selected='selected'>Min</option>";
	        $min.=$this->opccion_com_pauta(0, 59);
	        $min.="</select>";  

			
	        $material = "<select id='materiales' name='materiales' style='width:375px;'>";
			$material.="<option value='0' selected >Seleccione </option>";
			//////////*********Grupos**********///////
			$datosmaterialesdes = $this->ObjConsulta->selectsumdesp($this->conect_sistemas_vtv, $idsolicitud);
		    $cont2=count($datosmaterialesdes);
		    if ($cont2!=0){
		    	foreach ($datosmaterialesdes as $llave => $valor) {
		    		$datosequipo = $this->ObjConsulta->selectdatosequipo($this->conect_sistemas_vtv, $valor[1]);
		    		$id_articulo=$datosequipo[1][1];
			        $descripcion=$datosequipo[1][3];
			        $marca=$datosequipo[1][4];
			        $modelo=$datosequipo[1][5];
			       	$imagen=$datosequipo[1][17];

		       		if ($imagen == ''){
						$img= "../imagenes/inventario/sinfoto.jpg";
					}else{
						$img= "../imagenes/inventario/".$imagen;
					}

	       			$material.="<option value='" . $id_articulo . "' $selected  cantsol='".$valor[2]."' title= '".$img."'>" . $descripcion . " " . $marca . " " . $modelo . " </option>";

		       	}
		    }

	                                
	        //Formulario para ingresar los datos generales de la entrada.
	        $datosrecepcion= "<div id='datosp' align='center'><table class='tabla' align='center' style='width:650px;' >
	        <tr><th class='titulo' colspan='2'>Devoluci&oacute;n</th></tr>
	        <tr><th>Fecha de entrega:</th><td><input type='text' name='fecha' id='fecha' class='campo' size='8' maxlength='8'/></td></tr>
	        <tr><th>Hora de entrega:</th><td>".$hora." ".$min."</td></tr>
	        <tr><th>Quien Entrega:</th><td>" . $quienentrega . "</td></tr>
	        <tr><th>Observaci&oacute;n:</th><td><textarea name='observacion' rows='5' cols='50' id='observacion' class='campo' ></textarea></td></tr>
	        </table>";

	        $botonC = "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('classlista.php?modulo=listadedespachos');>";
			$botonA = "<input type=\"button\" class='boton' value=\"Guardar\" OnClick=devoluciones($idsolicitud);>";
       		$cantidad="<th>Cantidad:</th><td><input type='text' name='cantidad' id='cantidad' class='campo' size='3'/></td>
       				   <td><input type=\"button\" class='boton' value=\"+\" OnClick=agregarmaterialalistadev();></td>";
       		
	       		$botones="
	        		<div id='livetable'>
	        		<table class='tabla' align='center' style='display:none; width:650px;' >
	        			<tbody>
	        				<tr>
	        					<th class='titulo' colspan='3'>Materiales Seleccionados</th>
	        				</tr>
	        				<tr>
	        					<th>NOMBRE</th>
	        					<th>CANTIDAD</th>
	        					<th>ACCION</th>
	        				</tr>
	        			</tbody>
	       			</table>
	        		</div>

	        	<table class='tabla' style='width:650px;'>
	        	<tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
	        	</table></div>";

	    	$datosrecepcion.= "<table class='tabla' align='center' style='width:650px;' >
	        <tr><th class='titulo' colspan='5'>Materiales</th></tr>
	        <tr><td>" . $material . "</td>
	        ".$cantidad."
	        </tr>
	        </table>";
        
        }
        ////////////////////Combo que muestra el Personal/////////////////
    	/*if($identrada==1 or $identrada== 2){
       		$solicitantes=$this->ObjConsulta->selectpersonalA($this->conect_sigesp);
       	}else{
       		$solicitantes=$this->ObjConsulta->selectpersonalC($this->conect_sigesp);
       	}*/
       	    
        echo $datosrecepcion;
        echo $botones;
    }

    //En esta funcion valida si quedaron pendientes materiales por ingresar
    // y si es asi muestra un mensaje indicandolo, de lo contrario indica que el ingreso fue exitoso
    function ingreso(){

    	$fecha=$_GET['fecha'];
    	$hora=$_GET['hora'];
    	$min=$_GET['min'];
    	$id=$_GET['id'];
    	$valores=$_GET['valores'];
		$identrada=$_GET['identrada'];
		$idsolicitud=$_GET['idsolicitud'];
		$observacion=$_GET['observacion'];
		$id_estado=$_GET['estado'];
		$resp_entrada=$_GET['quienentrega'];
		$user_reg=$_SESSION['cedula'];
		$ids=explode(',',$id);
		$estado=explode(',',$valores);
		
		$cont=count($ids); 
		

		if ($hora <= 9){
			$hora="0".$hora;
		}
		if ($min <= 9){
			$min="0".$min;
		}
		$hora=$hora.":".$min;

		//Consultar los articulos solicitados y compararlos con los que se entregaron

			///Si es grupo//////
        	$datosgrupospres = $this->ObjConsulta->selectgruposprestados($this->conect_sistemas_vtv, $idsolicitud);
        	$grupres= count ($datosgrupospres);
			//Si es equipo//////
        	$datosequipospres = $this->ObjConsulta->selectequiposprestados($this->conect_sistemas_vtv, $idsolicitud);
        	$equpres= count ($datosequipospres);
			//Si es camara/////
        	$datoscamaraspres = $this->ObjConsulta->selectcamaraprestada($this->conect_sistemas_vtv, $idsolicitud);
        	$campres= count ($datoscamaraspres);
			
			$artpres=$grupres+$equpres+$campres;

        	///////////////////////////////////////MATERIALES///////////////////////////////////////////////
        	for ($i = 0; $i < $cont; $i++) { 
	    		$art=trim($ids[$i]);
	    		$est=trim($estado[$i]);
	    		$var=substr($art, 0, 2);
	    		/////////////Cambio de estatus de los articulos dependiendo del tipo de entrada/////////////////
	    		if ($var=='GP'){//Es un Grupos o una camara
		    		$tipo_grupo = $this->ObjConsulta->selectdatosgrupo($this->conect_sistemas_vtv, $art);
		    		$id_asig=$tipo_grupo[1][4];
		    		if($id_asig==1){//Es camara
		    			//Cambiar fecha de exp en t_camara_sol(para que ya no este solicitado)
			    		$act_estatus = $this->ObjConsulta->updatecampres($this->conect_sistemas_vtv, $art, $_SESSION['cedula']);
			    		//Cambiar estatus del articulo a 1 en t_articulo(para que este disponible)
			    		$id_estatus = 1;
			    		$act_disp = $this->ObjConsulta->updatedispgrupo($this->conect_sistemas_vtv, $art, $id_estatus);
						//Cambiar estatus a todos los equipos del grupo
			    		$act_disp = $this->ObjConsulta->updatedispartgrupo($this->conect_sistemas_vtv, $art, $id_estatus);
			    		//Cambiar estado al grupo
			    		$act_disp = $this->ObjConsulta->updateestadopgrupo($this->conect_sistemas_vtv, $art, $est);

		    		}else{//es un Grupo
		    			//Cambiar fecha de exp en t_grupo_sol(para que ya no este solicitado)
			    		$act_estatus = $this->ObjConsulta->updategrupres($this->conect_sistemas_vtv, $art, $_SESSION['cedula']);
			    		//Cambiar estatus del articulo a 1 en t_articulo(para que este disponible)
			    		$id_estatus = 1;
			    		$act_disp = $this->ObjConsulta->updatedispgrupo($this->conect_sistemas_vtv, $art, $id_estatus);
			    		//Cambiar estatus a todos los equipos del grupo
			    		$act_disp = $this->ObjConsulta->updatedispartgrupo($this->conect_sistemas_vtv, $art, $id_estatus);
			    		//Cambiar estado al grupo
			    		$act_disp = $this->ObjConsulta->updateestadopgrupo($this->conect_sistemas_vtv, $art, $est);

		    		}
	    			
	    		}else{//es un equipo
	    			//Cambiar fecha de exp en t_material_sol(para que ya no este solicitado)
		    		$act_estatus = $this->ObjConsulta->updateartpres($this->conect_sistemas_vtv, $art, $_SESSION['cedula']);
		    		//Cambiar estatus del articulo a 1 en t_articulo(para que este disponible)
		    		$id_estatus = 1;
		    		$act_disp = $this->ObjConsulta->updatedispart($this->conect_sistemas_vtv, $art, $id_estatus);
		    		//Cambiar el estado del grupo
		    		$act_disp = $this->ObjConsulta->updateestadopart($this->conect_sistemas_vtv, $art, $est);

	    		}

	    		/*//Cambiar fecha de exp en t_material_sol(para que ya no este solicitado)
	    		$act_estatus = $this->ObjConsulta->updateartpres($this->conect_sistemas_vtv, $art);
	    		//Cambiar estatus del articulo a 1 en t_articulo(para que este disponible)
	    		$id_estatus = 1;
	    		$act_disp = $this->ObjConsulta->updatedispart($this->conect_sistemas_vtv, $art, $id_estatus);

	    		$act_disp = $this->ObjConsulta->updateestadopart($this->conect_sistemas_vtv, $art, $est);*/
	    	}

        if ($cont==$artpres){
        	
        	//*********Esto cuando se entrega todo*********///
	    	//Cambiar estatus a 5 en t_prestamo(para que no este pendiente)
	    	$id_estatus_prestamo = 5;//Solicitud procesada
	    	$act_sol = $this->ObjConsulta->updatesolicitud($this->conect_sistemas_vtv, $idsolicitud, $id_estatus_prestamo);
	    	//Registrar la entrada en t_entrada(por control)
	    	$datosentrada = $this->ObjConsulta->insertentrada($this->conect_sistemas_vtv, $idsolicitud, $resp_entrada, $user_reg, $fecha, $observacion, $hora);
	   		$id_entrada_sol=$datosentrada[0];
			$datosfechaentrada = $this->ObjConsulta->updatefechaentrada($this->conect_sistemas_vtv, $idsolicitud);

	    	//Mostrar mensaje indicando que no quedaron articulos pendientes
		  	$mensaje = "<div style='color: #009900;font-weight: bold;'><br>El ingreso de los materiales se realizo con exito<div><br>";
			echo $this->ObjMensaje->InterfazExitosamente($mensaje);
			echo"<script>
			var reporte='../reportes/entrada.php?id_entrada=".$id_entrada_sol."';   
			var pagina='classbienvenida.php';                    
			function redireccionar() { 
				location.href=pagina;
				window.open(reporte)
			} 	
			setTimeout ('redireccionar()', 3000);
			</script>";	
        }else{

        	//*********Esto cuando faltan por entragar********/////
        	/*for ($i = 0; $i < $cont; $i++) { 
	    		$art=trim($ids[$i]);
	    		$est=trim($estado[$i]);
	    		/////////////Cambio de estatus de los articulos dependiendo del tipo de entrada/////////////////
	    		if ($identrada==1){//es un grupo
	    			//Cambiar fecha de exp en t_grupo_sol(para que ya no este solicitado)
		    		$act_estatus = $this->ObjConsulta->updategrupres($this->conect_sistemas_vtv, $art, $_SESSION['cedula']);
		    		//Cambiar estatus del articulo a 1 en t_articulo(para que este disponible)
		    		$id_estatus = 1;
		    		$act_disp = $this->ObjConsulta->updatedispgrupo($this->conect_sistemas_vtv, $art, $id_estatus);
		    		//Cambiar estatus a todos los equipos del grupo
		    		$act_disp = $this->ObjConsulta->updatedispartgrupo($this->conect_sistemas_vtv, $art, $id_estatus);
		    		//Cambiar estado al grupo
		    		$act_disp = $this->ObjConsulta->updateestadopgrupo($this->conect_sistemas_vtv, $art, $est);

	    		}elseif($identrada==2){//es un equipo
	    			//Cambiar fecha de exp en t_material_sol(para que ya no este solicitado)
		    		$act_estatus = $this->ObjConsulta->updateartpres($this->conect_sistemas_vtv, $art, $_SESSION['cedula']);
		    		//Cambiar estatus del articulo a 1 en t_articulo(para que este disponible)
		    		$id_estatus = 1;
		    		$act_disp = $this->ObjConsulta->updatedispart($this->conect_sistemas_vtv, $art, $id_estatus);
		    		//Cambiar el estado del grupo
		    		$act_disp = $this->ObjConsulta->updateestadopart($this->conect_sistemas_vtv, $art, $est);

	    		}else{//es una camara
	    			//Cambiar fecha de exp en t_camara_sol(para que ya no este solicitado)
		    		$act_estatus = $this->ObjConsulta->updatecampres($this->conect_sistemas_vtv, $art, $_SESSION['cedula']);
		    		//Cambiar estatus del articulo a 1 en t_articulo(para que este disponible)
		    		$id_estatus = 1;
		    		$act_disp = $this->ObjConsulta->updatedispgrupo($this->conect_sistemas_vtv, $art, $id_estatus);
					//Cambiar estatus a todos los equipos del grupo
		    		$act_disp = $this->ObjConsulta->updatedispartgrupo($this->conect_sistemas_vtv, $art, $id_estatus);
		    		//Cambiar estado al grupo
		    		$act_disp = $this->ObjConsulta->updateestadopgrupo($this->conect_sistemas_vtv, $art, $est);

	    		}*/	




	    		/*//Cambiar fecha de exp en t_material_sol del articulo que entrego(para que ya no este solicitado)
	    		$act_estatus = $this->ObjConsulta->updateartpres($this->conect_sistemas_vtv, $art);
	    		//Cambiar estatus del articulo que entrego a 1 en t_articulo(para que este disponible)
	    		$id_estatus = 1;
	    		$act_disp = $this->ObjConsulta->updatedispart($this->conect_sistemas_vtv, $art, $id_estatus);

	    		$act_disp = $this->ObjConsulta->updateestadopart($this->conect_sistemas_vtv, $art, $id_estado);*/
	    	//}

	    	//Registrar la entrada en t_entrada(por control)
	    	$datosentrada = $this->ObjConsulta->insertentrada($this->conect_sistemas_vtv, $idsolicitud, $resp_entrada, $user_reg, $fecha, $observacion, $hora);
			$id_entrada_sol=$datosentrada[0];
			//Dejar el estatus en 4 en t_prestamo(para saber que esta pendiente)
	    	//Mostrar mensaje indicando que quedaron articulos pendientes
	    	$mensaje = "<div style='color: #009900;font-weight: bold;'><br>El ingreso fue exitoso, quedan aun articulos pendientes<div><br>";
			echo $this->ObjMensaje->InterfazExitosamente($mensaje);
			echo"<script>
			var reporte='../reportes/entrada.php?id_entrada=".$id_entrada_sol."'; 
			var pagina='classbienvenida.php';                    
			function redireccionar() { 
				location.href=pagina;
				window.open(reporte)
			} 	
			setTimeout ('redireccionar()', 3000);
			</script>";	
        }	
    }


    function ingresarvariable(){

        $variable=strtoupper ($_GET['variable']);
        $id_tipo_var=$_GET['id_tipo_var'];
        $id_tipo_art=$_GET['tipoart'];
        $user_reg=$_SESSION['cedula'];
        if($id_tipo_var==1){
            $titulo="Nuevo Estado";
	    	$nuevoestado = $this->ObjConsulta->insertestado($this->conect_sistemas_vtv, $variable, $user_reg);

        }elseif($id_tipo_var==2){
            $titulo="Nueva Unidad de medida";
	    	$nuevamedida = $this->ObjConsulta->insertmedida($this->conect_sistemas_vtv, $variable, $user_reg);


        }elseif($id_tipo_var==3){
            $titulo="Nueva Categoria";
	    	$nuevacat = $this->ObjConsulta->insertcategoria($this->conect_sistemas_vtv, $variable, $user_reg);

        }elseif($id_tipo_var==4){
        	$titulo="Nuevo Programa";
	    	$nuevoprog = $this->ObjConsulta->insertprograma($this->conect_sistemas_vtv, $variable, $user_reg);

        }else{
        	$titulo="Nuevo Tipo de Descripcion";
	    	$nuevotipo = $this->ObjConsulta->inserttipodesc($this->conect_sistemas_vtv, $variable, $user_reg, $id_tipo_art);

        }

        $mensaje = "<div style='color: #009900;font-weight: bold;'><br>El ingreso se realizo con exito<div><br>";
		echo $this->ObjMensaje->InterfazExitosamente($mensaje);
		echo"<script> 
		var pagina='classbienvenida.php';                    
		function redireccionar() { 
			location.href=pagina;
			
		} 	
		setTimeout ('redireccionar()', 3000);
		</script>";	

    }

    function eliminarvariable(){

        $variable=$_GET['variable'];
        $id_tipo_var=$_GET['id_tipo_var'];
        $user_exp=$_SESSION['cedula'];

        if($id_tipo_var==1){
        	$nom_var="El estado";
            $nuevoestado = $this->ObjConsulta->updateestado($this->conect_sistemas_vtv, $variable, $user_exp);

        }elseif($id_tipo_var==2){
        	$nom_var="La unidad de medida";
            $nuevamedida = $this->ObjConsulta->updateunidadmedida($this->conect_sistemas_vtv, $variable, $user_exp);

        }elseif($id_tipo_var==3){
        	$nom_var="La categoria";
            $nuevacat = $this->ObjConsulta->updatecategoria($this->conect_sistemas_vtv, $variable, $user_exp);

        }elseif($id_tipo_var==4){
        	$nom_var="El programa";
        	$nuevaprog = $this->ObjConsulta->updateprograma($this->conect_sistemas_vtv, $variable, $user_exp);
		}else{
        	$nom_var="El tipo de descripci&oacute;n";
        	$nuevadesc = $this->ObjConsulta->updatetipodesc($this->conect_sistemas_vtv, $variable, $user_exp);
		}

        $mensaje = "<div style='color: #009900;font-weight: bold;'><br>".$nom_var." se elimino con exito <div><br>";
		echo $this->ObjMensaje->InterfazExitosamente($mensaje);
		echo"<script> 
		var pagina='classbienvenida.php';                    
		function redireccionar() { 
			location.href=pagina;
			
		} 	
		setTimeout ('redireccionar()', 3000);
		</script>";	

    }

    ///////////////////////////////////////////////////////Listas//////////////////////////////////////////////////////
	function listadeusuarios() {
		//print_r($_GET);   
		if (isset($_GET['parametrobusqueda'])) {
			$busquedaget = $_GET['parametrobusqueda'];
		} else {
			$busquedaget = $_GET['busqueda'];
		}

		$pagina_ajax = "'listadeusuarios&ajax_request=true'";
		
		$nombre_fn = "0";
		if (isset($_GET['minimo'])) {
			$minimo = $_GET['minimo'];
			$count = $_GET['count'];
			if ($count > 1) {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button' onClick=\"anterior(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Anterior'><img src='../estilos/imagenes/estatus/p_2.gif' style=\"border-width:none;border-style:none;\"></button>";
			} else {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button'><img src='../estilos/imagenes/estatus/p_2_2.gif'style=\"border-width:none;border-style:none;\"></button>";
			}
		} else {
			$minimo = 0;
			$count = 1;
			$boton_Ant = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name=\"button'  id=\"button\"><img src=\"../estilos/imagenes/estatus/p_2_2.gif\" style=\"border-width:none;border-style:none;\"></button>";
		}

			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = "usuario.usuarios_almacen_tecnico", $campo_rn = "cedula", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = "usuario.usuarios_almacen_tecnico", $campo_rn = "cedula,nombre_usuario,apellido_usuario,desc_usuario", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "", $minimo);
		

		$num_pag = ceil(count($todas) / 10);
		for ($i = 1; $i <= $num_pag; $i++) {
			$cantidad_paginas[] = $i;
		}
		if (count($cantidad_paginas) > 0) {
			$combo = "<SELECT name=\"cantidad_paginas\" id=\"cantidad_paginas\" class='lista' style='width:50px;' onchange=\"cambiopagina(" . $pagina_ajax . "," . $nombre_fn . ");\">";
			foreach ($cantidad_paginas as $llave => $valor) {
				if ($valor == $count) {
					$selected = 'selected';
				} else {
					$selected = "";
				}
				$combo.="<option value='" . $llave . "' $selected >" . $valor . "</option>";
			}
			$combo.="</SELECT> ";
		}

		if($_GET['ajax_request']!="true"){
			$campo_busqueda="<input type='text' name='busqueda' id='busqueda' class='campo' onkeypress='return enter(event,buscar_listadeusuarios)' size='70'>";
			$botonB="<input type=\"button\" class='boton' value=\"Buscar\" OnClick=buscar_listadeusuarios();>";

			$buscador="<table class='tabla'>
        <tr><th class='titulo' colspan='2'>BUSQUEDA</th></tr>
        <tr><td colspan='2'>" . $campo_busqueda . "&nbsp;&nbsp;&nbsp;" . $botonB . "</td></tr>
        </table>";
			$boton_agreg = "<input type=\"button\" class='boton' value=\"Agregar Usuario\" OnClick='ir_a(\"classagregar.php?tipo=1\")'>";
			$lista=$buscador."
			<div id='form' ><table class='tabla'>
			<tr><th><div align=\"center\">".$boton_agreg."</div></th>
			</table>";
		}

		$lista.="
		<div id='datosp'>
		<input name='minimo' id='minimo' type='hidden' value=" . $minimo . " />
		<input name='count' id='count' type='hidden' value=" . $count . " />    
		<input name='parametrobusqueda' id='parametrobusqueda' type='hidden' value='" . $busquedaget . "'>  
		<table  class='tabla' id='tabla_corres'>
		<tr><th class='titulo' colspan='5'>USUARIOS</th></tr>
		<tr>    
		<th style='200'><div align=\"center\">CEDULA</div></th>
		<th style='200'><div align=\"center\">NOMBRE Y APELLIDO</div></th>
		<th><div align=\"center\">TIPO DE USUARIO</div></th>  
		<th><div align=\"center\">ACCIONES</div></th>     
		</tr>";
		if (count($ObjListado) > 0) {
			foreach ($ObjListado as $llave2 => $valor2) {
				$resto = $llave2 % 2;
				if (($resto == 0) && ($llave2 != 0)) {
					$color = "style='background:#f1f1f1;'"; //par
				} else {
					$color = "style='background:#ffffff;'"; //impar f9f9f9
				}
				$ced_cod = base64_encode($valor2[1]);
				$boton_E =  "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' title=\"Eliminar\" name='button' id='button' OnClick=eliminarusuario($valor2[1]); ><img src='../estilos/imagenes/estatus/eliminado.gif' style=\"width:20px;height:20px;border-width:none;border-style:none;\" ></button>";
				$boton_P = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' title=\"Cambio de perfil\" name='button' id='button' OnClick=CancelarRegresar('classadmperfil.php?cedula=" . $ced_cod . "'); ><img src='../estilos/imagenes/usuario.jpeg' style=\"width:25px;height:25px;border-width:none;border-style:none;\" ></button>";
				$boton_R = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' title=\"Reiniciar clave\" name='button' id='button' OnClick=reiniciar_clave($valor2[1]); ><img src='../estilos/imagenes/llave.jpeg' style=\"width:25px;height:25px;border-width:none;border-style:none;\" ></button>";
				$lista.="<tr id='" . $valor2[1] . "'>
				<td $color><div align=\"left\">" . $valor2[1] . "</div></td>
				<td $color><div align=\"left\">" . strtoupper(ucwords(utf8_decode($valor2[2]))) . "<BR>" . strtoupper(ucwords(utf8_decode($valor2[3]))) . "</div></td>
				<td $color><div align=\"center\">" . ucwords($valor2[4]) . "</div></td>
				<td $color><div align=\"center\">" . $boton_E . "&nbsp;" . $boton_P . "&nbsp;" . $boton_R . "</div></td>
				</tr>";
			}
		} else {
			$mensaje = "<div style='color: #FF0000;font-weight: bold;'><br>Error: Disculpe su busqueda NO arrojo ningun resultado.<div><br>";
			$lista.="<td colspan='8'><div align=\"center\">" . $mensaje . "</div></td>";
		}
		$lista.="</table>";
		if ($num_pag > $count) {
			$boton_Sig = " <button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name='button'  id=\"button\" onClick=\"siguiente(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Siguiente'><img src=\"../estilos/imagenes/estatus/p_3.gif\"  style=\"border-width:none;border-style:none;\"></button>";
		} else {
			$boton_Sig = "
			<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' name='button' id='button'><img src='../estilos/imagenes/estatus/p_3_2.gif' style=\"border-width:none;border-style:none;\"></button>";
		}
		$lista.=$this->leyenda($tipo = 0);
		$lista.="
		<table class='tabla'>
		<tr><th><div align=\"center\">" . $num_pag . "&nbsp;&nbsp;p&aacute;ginas,&nbsp;" . count($todas) . "&nbsp;&nbsp;usuario(s)</div></th>
		<th>" . $combo . "&nbsp;&nbsp;" . $boton_Ant . " &nbsp; " . $count . " / " . $num_pag . "&nbsp;" . $boton_Sig . "&nbsp;</th>
		</tr></table></div></div>";


		if($_GET['ajax_request']=="true"){
			echo $lista;
		}else{
			return $lista;	
		}	
	}

	function listadealmacen() {
		//print_r($_GET);   
		if (isset($_GET['parametrobusqueda'])) {
			$busquedaget = $_GET['parametrobusqueda'];
		} else {
			$busquedaget = $_GET['busqueda'];
		}

		$pagina_ajax = "'listadealmacen&ajax_request=true'";
		$nombre_fn = "0";
		if (isset($_GET['minimo'])) {
			$minimo = $_GET['minimo'];
			$count = $_GET['count'];
			if ($count > 1) {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button' onClick=\"anterior(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Anterior'><img src='../estilos/imagenes/estatus/p_2.gif' style=\"border-width:none;border-style:none;\"></button>";
			} else {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button'><img src='../estilos/imagenes/estatus/p_2_2.gif'style=\"border-width:none;border-style:none;\"></button>";
			}
		} else {
			$minimo = 0;
			$count = 1;
			$boton_Ant = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name=\"button'  id=\"button\"><img src=\"../estilos/imagenes/estatus/p_2_2.gif\" style=\"border-width:none;border-style:none;\"></button>";
		}

			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = "almacen_tecnico.articulos_almacen", $campo_rn = "id_articulo", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = "almacen_tecnico.articulos_almacen", $campo_rn = "id_articulo,descripcion,marca,modelo,bien_nac,serial,siglas,costo,cantidad,desc_unidad_medida,estado,tipo_articulo,estatus", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "", $minimo);
		

		$num_pag = ceil(count($todas) / 10);
		for ($i = 1; $i <= $num_pag; $i++) {
			$cantidad_paginas[] = $i;
		}
		if (count($cantidad_paginas) > 0) {
			$combo = "<SELECT name=\"cantidad_paginas\" id=\"cantidad_paginas\" class='lista' style='width:50px;' onchange=\"cambiopagina(" . $pagina_ajax . "," . $nombre_fn . ");\">";
			foreach ($cantidad_paginas as $llave => $valor) {
				if ($valor == $count) {
					$selected = 'selected';
				} else {
					$selected = "";
				}
				$combo.="<option value='" . $llave . "' $selected >" . $valor . "</option>";
			}
			$combo.="</SELECT> ";
		}

		if($_GET['ajax_request']!="true"){
			$campo_busqueda="<input type='text' name='busqueda' id='busqueda' class='campo' onkeypress='return enter(event,buscar_listadealmacen)' size='70'>";
			$botonB="<input type=\"button\" class='boton' value=\"Buscar\" OnClick=buscar_listadealmacen();>";

			$buscador="<table class='tabla'>
        	<tr><th class='titulo' colspan='2'>BUSQUEDA</th></tr>
        	<tr><td colspan='2'>" . $campo_busqueda . "&nbsp;&nbsp;&nbsp;" . $botonB . "</td></tr>
        	</table>";
			$boton_agreg = "<input type=\"button\" class='boton' value=\"Agregar\" OnClick='ir_a(\"classagregar.php?tipo=2\")'>";
			$lista=$buscador."
			<div id='form' ><table class='tabla'>
			<tr><th><div align=\"center\">".$boton_agreg."</div></th>
			</table>";
		}

		$lista.="
		<div id='datosp'>
		<input name='minimo' id='minimo' type='hidden' value=" . $minimo . " />
		<input name='count' id='count' type='hidden' value=" . $count . " />    
		<input name='parametrobusqueda' id='parametrobusqueda' type='hidden' value='" . $busquedaget . "'>  
		<table  class='tabla' id='tabla_corres'>
		<tr><th class='titulo' colspan='14'>ALMAC&Eacute;N</th></tr>
		<tr>
		<th style='200'><div align=\"center\">DESCRIPCION</div></th>
		<th><div align=\"center\">MARCA</div></th>
		<th><div align=\"center\">MODELO</div></th>
		<th><div align=\"center\">BIEN NACIONAL</div></th>     
		<th><div align=\"center\">SERIAL</div></th>
		<th><div align=\"center\">COSTO</div></th>
		<th><div align=\"center\">ESTADO</div></th>
		<th><div align=\"center\">CANTIDAD</div></th>
		<th><div align=\"center\">ACCIONES</div></th> 
		</tr>";
		if (count($ObjListado) > 0) {
			foreach ($ObjListado as $llave2 => $valor2) {
				$resto = $llave2 % 2;
				if (($resto == 0) && ($llave2 != 0)) {

					$color = "style='background:#f1f1f1;'"; //par
				} else {

					$color = "style='background:#ffffff;'"; //impar f9f9f9
				}

				if($valor2[13]=='PRESTADO'){

					$icono= "rojo.gif";
				}else{

					$icono= "verde.gif";
				}

				if($valor2[9]==0){

					$cantidad="<font color='RED'>AGOTADO</font>";
				}else{

					$cantidad=$valor2[9]." ".$valor2[10];
				} 

				if($_SESSION['id_tipo_usuario']==31 or $_SESSION['id_tipo_usuario']==3){

				}
				$boton_E =  "<button style=\"width:12px;height:12px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' title=\"Eliminar\" name='button' id='button' OnClick=eliminararticulo($valor2[1]); ><img src='../estilos/imagenes/estatus/eliminado.gif' style=\"width:15px;height:15px;border-width:none;border-style:none;\" ></button>";
				$boton_M = "<button style=\"width:12px;height:12px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' title=\"Modificar Art&iacute;culo\" name='button' id='button' OnClick=modificararticulo($valor2[1],2); ><img src='../estilos/imagenes/folder_edit.png' style=\"width:20px;height:20px;border-width:none;border-style:none;\" ></button>";
				$boton_I = "<button style=\"width:12px;height:12px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' title=\"Modificar Imagen\" name='button' id='button' OnClick=modificarimagenes($valor2[1],1); ><img src='../estilos/imagenes/estatus/camera_picture.png' style=\"width:20px;height:20px;border-width:none;border-style:none;\" ></button>";
				$boton_B = "<button style=\"width:12px;height:12px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' title=\"Mostrar Art&iacute;culo\" name='button' id='button' OnClick=mostrararticulo($valor2[1],2); ><img src='../estilos/imagenes/estatus/buscar.gif' style=\"width:20px;height:20px;border-width:none;border-style:none;\" ></button>";
				$boton_R = "<button style=\"width:12px;height:12px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' title=\"$valor2[13]\" name='button' id='button' OnClick=responsable($valor2[1],50,2); ><img src='../estilos/imagenes/".$icono."' style=\"width:25px;height:25px;border-width:none;border-style:none;\" ></button>";
				$boton_S = "<button style=\"width:12px;height:12px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' title=\"Ingreso\" name='button' id='button' OnClick=ingreso_stock($valor2[1],1); ><img src='../estilos/imagenes/ing_alm.jpeg' style=\"width:25px;height:25px;border-width:none;border-style:none;\" ></button>";

				if($valor2[12] == 2){//consumible
					if($_SESSION['id_tipo_usuario']==31 or $_SESSION['id_tipo_usuario']==32){//administradores
						$botones="<td $color><div align=\"left\">" . $boton_E . "&nbsp;" . $boton_M . "&nbsp;" . $boton_I. "&nbsp;" . $boton_B. "&nbsp;&nbsp;" . $boton_S. "</div></td>";

					}elseif($_SESSION['id_tipo_usuario']==33){//almacenista
						$botones="<td $color><div align=\"center\">" . $boton_B. "&nbsp;&nbsp;" . $boton_S. "</div></td>";	
					}else{
						$botones="<td $color><div align=\"center\">" . $boton_B. "</div></td>";
					}

				}else{//prestamo
					
					if($_SESSION['id_tipo_usuario']==31 or $_SESSION['id_tipo_usuario']==32){//administradores
						$botones="<td $color><div align=\"left\">" . $boton_E . "&nbsp;" . $boton_M . "&nbsp;" . $boton_I. "&nbsp;" . $boton_B. "&nbsp;" . $boton_R. "</div></td>";

					}elseif($_SESSION['id_tipo_usuario']==33){//almacenista
						$botones="<td $color><div align=\"center\">" . $boton_B. "&nbsp;" . $boton_R. "</div></td>";
					}else{
						$botones="<td $color><div align=\"center\">" . $boton_B. "</div></td>";
					}




					//$botones="<td $color><div align=\"left\">" . $boton_E . "&nbsp;" . $boton_M . "&nbsp;" . $boton_I. "&nbsp;" . $boton_B. "&nbsp;" . $boton_R. "</div></td>";
				}

				$lista.="<tr id='" . $valor2[1] . "'>
				<td $color><div align=\"left\">" . strtoupper(ucwords(utf8_encode($valor2[2]))) . "</div></td>
				<td $color><div align=\"center\">" . $valor2[3] . "</div></td>
				<td $color><div align=\"center\">" . $valor2[4] . "</div></td>
				<td $color><div align=\"center\">" . $valor2[5] . "</div></td>
				<td $color><div align=\"center\">" . $valor2[7] . "</div></td>
				<td $color><div align=\"center\">" . $valor2[8] . "</div></td>
				<td $color><div align=\"center\">" . $valor2[11] . "</div></td>
				<td $color><div align=\"center\">" . $cantidad . " </div></td>			
				".$botones."
				</tr>";

				//<td $color><div align=\"center\">" . $boton_E . "&nbsp;" . $boton_P . "&nbsp;" . $boton_R . "</div></td>
			}
		} else {
			$mensaje = "<div style='color: #FF0000;font-weight: bold;'><br>Error: Disculpe su busqueda NO arrojo ningun resultado.<div><br>";
			$lista.="<td colspan='8'><div align=\"center\">" . $mensaje . "</div></td>";
		}
		$lista.="</table>";
		if ($num_pag > $count) {
			$boton_Sig = " <button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name='button'  id=\"button\" onClick=\"siguiente(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Siguiente'><img src=\"../estilos/imagenes/estatus/p_3.gif\"  style=\"border-width:none;border-style:none;\"></button>";
		} else {
			$boton_Sig = "
			<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' name='button' id='button'><img src='../estilos/imagenes/estatus/p_3_2.gif' style=\"border-width:none;border-style:none;\"></button>";
		}
		$lista.=$this->leyenda($tipo = 0);
		$lista.="
		<table class='tabla'>
		<tr><th><div align=\"center\">" . $num_pag . "&nbsp;&nbsp;p&aacute;ginas,&nbsp;" . count($todas) . "&nbsp;&nbsp;articulo(s)</div></th>
		<th>" . $combo . "&nbsp;&nbsp;" . $boton_Ant . " &nbsp; " . $count . " / " . $num_pag . "&nbsp;" . $boton_Sig . "&nbsp;</th>
		</tr></table></div></div>";

		if($_GET['ajax_request']=="true"){
			echo $lista;
		}else{
			return $lista;	
		}
	}

	function listadeequiposprestados() {
		//print_r($_GET);   
		if (isset($_GET['parametrobusqueda'])) {
			$busquedaget = $_GET['parametrobusqueda'];
		} else {
			$busquedaget = $_GET['busqueda'];
		}

		$pagina_ajax = "'listadeequiposprestados&ajax_request=true'";
		$nombre_fn = "0";
		if (isset($_GET['minimo'])) {
			$minimo = $_GET['minimo'];
			$count = $_GET['count'];
			if ($count > 1) {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button' onClick=\"anterior(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Anterior'><img src='../estilos/imagenes/estatus/p_2.gif' style=\"border-width:none;border-style:none;\"></button>";
			} else {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button'><img src='../estilos/imagenes/estatus/p_2_2.gif'style=\"border-width:none;border-style:none;\"></button>";
			}
		} else {
			$minimo = 0;
			$count = 1;
			$boton_Ant = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name=\"button'  id=\"button\"><img src=\"../estilos/imagenes/estatus/p_2_2.gif\" style=\"border-width:none;border-style:none;\"></button>";
		}

			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = "almacen_tecnico.articulos_almacen_prestados", $campo_rn = "id_articulo", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = "almacen_tecnico.articulos_almacen_prestados", $campo_rn = "id_articulo,descripcion,marca,modelo,bien_nac,serial,siglas,costo,cantidad,desc_unidad_medida,estado,tipo_articulo,estatus", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "", $minimo);
		

		$num_pag = ceil(count($todas) / 10);
		for ($i = 1; $i <= $num_pag; $i++) {
			$cantidad_paginas[] = $i;
		}
		if (count($cantidad_paginas) > 0) {
			$combo = "<SELECT name=\"cantidad_paginas\" id=\"cantidad_paginas\" class='lista' style='width:50px;' onchange=\"cambiopagina(" . $pagina_ajax . "," . $nombre_fn . ");\">";
			foreach ($cantidad_paginas as $llave => $valor) {
				if ($valor == $count) {
					$selected = 'selected';
				} else {
					$selected = "";
				}
				$combo.="<option value='" . $llave . "' $selected >" . $valor . "</option>";
			}
			$combo.="</SELECT> ";
		}

		if($_GET['ajax_request']!="true"){
			$campo_busqueda="<input type='text' name='busqueda' id='busqueda' class='campo' onkeypress='return enter(event,buscar_listadeequiposprestados)' size='70'>";
			$botonB="<input type=\"button\" class='boton' value=\"Buscar\" OnClick=buscar_listadeequiposprestados();>";

			$buscador="<table class='tabla'>
        	<tr><th class='titulo' colspan='2'>BUSQUEDA</th></tr>
        	<tr><td colspan='2'>" . $campo_busqueda . "&nbsp;&nbsp;&nbsp;" . $botonB . "</td></tr>
        	</table>";
			//$boton_agreg = "<input type=\"button\" class='boton' value=\"Agregar\" OnClick='ir_a(\"classagregar.php?tipo=2\")'>";
			$lista=$buscador."
			<div id='form' ><table class='tabla'>
			<tr><th><div align=\"center\">".$boton_agreg."</div></th>
			</table>";
		}

		$lista.="
		<div id='datosp'>
		<input name='minimo' id='minimo' type='hidden' value=" . $minimo . " />
		<input name='count' id='count' type='hidden' value=" . $count . " />    
		<input name='parametrobusqueda' id='parametrobusqueda' type='hidden' value='" . $busquedaget . "'>  
		<table  class='tabla' id='tabla_corres'>
		<tr><th class='titulo' colspan='14'>ALMAC&Eacute;N</th></tr>
		<tr>
		<th style='200'><div align=\"center\">DESCRIPCION</div></th>
		<th><div align=\"center\">MARCA</div></th>
		<th><div align=\"center\">MODELO</div></th>
		<th><div align=\"center\">BIEN NACIONAL</div></th>     
		<th><div align=\"center\">SERIAL</div></th>
		<th><div align=\"center\">COSTO</div></th>
		<th><div align=\"center\">ESTADO</div></th>
		<th><div align=\"center\">CANTIDAD</div></th>
		<th><div align=\"center\">ACCIONES</div></th> 
		</tr>";
		if (count($ObjListado) > 0) {
			foreach ($ObjListado as $llave2 => $valor2) {
				$resto = $llave2 % 2;
				if (($resto == 0) && ($llave2 != 0)) {

					$color = "style='background:#f1f1f1;'"; //par
				} else {

					$color = "style='background:#ffffff;'"; //impar f9f9f9
				}

				if($valor2[13]=='PRESTADO'){

					$icono= "rojo.gif";
				}else{

					$icono= "verde.gif";
				}

				if($valor2[9]==0){

					$cantidad="<font color='RED'>AGOTADO</font>";
				}else{

					$cantidad=$valor2[9]." ".$valor2[10];
				} 

				
				$boton_E =  "<button style=\"width:12px;height:12px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' title=\"Eliminar\" name='button' id='button' OnClick=eliminararticulo($valor2[1]); ><img src='../estilos/imagenes/estatus/eliminado.gif' style=\"width:15px;height:15px;border-width:none;border-style:none;\" ></button>";
				$boton_M = "<button style=\"width:12px;height:12px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' title=\"Modificar Art&iacute;culo\" name='button' id='button' OnClick=modificararticulo($valor2[1],2); ><img src='../estilos/imagenes/folder_edit.png' style=\"width:20px;height:20px;border-width:none;border-style:none;\" ></button>";
				$boton_I = "<button style=\"width:12px;height:12px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' title=\"Modificar Imagen\" name='button' id='button' OnClick=modificarimagenes($valor2[1],1); ><img src='../estilos/imagenes/estatus/camera_picture.png' style=\"width:20px;height:20px;border-width:none;border-style:none;\" ></button>";
				$boton_B = "<button style=\"width:12px;height:12px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' title=\"Mostrar Art&iacute;culo\" name='button' id='button' OnClick=mostrararticulo($valor2[1],2); ><img src='../estilos/imagenes/estatus/buscar.gif' style=\"width:20px;height:20px;border-width:none;border-style:none;\" ></button>";
				$boton_R = "<button style=\"width:12px;height:12px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' title=\"$valor2[13]\" name='button' id='button' OnClick=responsable($valor2[1],50,3); ><img src='../estilos/imagenes/".$icono."' style=\"width:25px;height:25px;border-width:none;border-style:none;\" ></button>";
				$boton_S = "<button style=\"width:12px;height:12px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' title=\"Ingreso\" name='button' id='button' OnClick=ingreso_stock($valor2[1],1); ><img src='../estilos/imagenes/ing_alm.jpeg' style=\"width:25px;height:25px;border-width:none;border-style:none;\" ></button>";

				$botones="<td $color><div align=\"center\">" . $boton_R. "</div></td>";


				$lista.="<tr id='" . $valor2[1] . "'>
				<td $color><div align=\"left\">" . strtoupper(ucwords(utf8_encode($valor2[2]))) . "</div></td>
				<td $color><div align=\"center\">" . $valor2[3] . "</div></td>
				<td $color><div align=\"center\">" . $valor2[4] . "</div></td>
				<td $color><div align=\"center\">" . $valor2[5] . "</div></td>
				<td $color><div align=\"center\">" . $valor2[7] . "</div></td>
				<td $color><div align=\"center\">" . $valor2[8] . "</div></td>
				<td $color><div align=\"center\">" . $valor2[11] . "</div></td>
				<td $color><div align=\"center\">" . $cantidad . " </div></td>			
				".$botones."
				</tr>";

				//<td $color><div align=\"center\">" . $boton_E . "&nbsp;" . $boton_P . "&nbsp;" . $boton_R . "</div></td>
			}
		} else {
			$mensaje = "<div style='color: #FF0000;font-weight: bold;'><br>Error: Disculpe su busqueda NO arrojo ningun resultado.<div><br>";
			$lista.="<td colspan='8'><div align=\"center\">" . $mensaje . "</div></td>";
		}
		$lista.="</table>";
		if ($num_pag > $count) {
			$boton_Sig = " <button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name='button'  id=\"button\" onClick=\"siguiente(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Siguiente'><img src=\"../estilos/imagenes/estatus/p_3.gif\"  style=\"border-width:none;border-style:none;\"></button>";
		} else {
			$boton_Sig = "
			<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' name='button' id='button'><img src='../estilos/imagenes/estatus/p_3_2.gif' style=\"border-width:none;border-style:none;\"></button>";
		}
		$lista.=$this->leyenda($tipo = 0);
		$lista.="
		<table class='tabla'>
		<tr><th><div align=\"center\">" . $num_pag . "&nbsp;&nbsp;p&aacute;ginas,&nbsp;" . count($todas) . "&nbsp;&nbsp;articulo(s)</div></th>
		<th>" . $combo . "&nbsp;&nbsp;" . $boton_Ant . " &nbsp; " . $count . " / " . $num_pag . "&nbsp;" . $boton_Sig . "&nbsp;</th>
		</tr></table></div></div>";

		if($_GET['ajax_request']=="true"){
			echo $lista;
		}else{
			return $lista;	
		}
	}

	function listadegrupos() {
		//print_r($_GET);   
		if (isset($_GET['parametrobusqueda'])) {
			$busquedaget = $_GET['parametrobusqueda'];
		} else {
			$busquedaget = $_GET['busqueda'];
		}

		$pagina_ajax = "'listadegrupos&ajax_request=true'";
		$nombre_fn = "0";
		if (isset($_GET['minimo'])) {
			$minimo = $_GET['minimo'];
			$count = $_GET['count'];
			if ($count > 1) {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button' onClick=\"anterior(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Anterior'><img src='../estilos/imagenes/estatus/p_2.gif' style=\"border-width:none;border-style:none;\"></button>";
			} else {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button'><img src='../estilos/imagenes/estatus/p_2_2.gif'style=\"border-width:none;border-style:none;\"></button>";
			}
		} else {
			$minimo = 0;
			$count = 1;
			$boton_Ant = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name=\"button'  id=\"button\"><img src=\"../estilos/imagenes/estatus/p_2_2.gif\" style=\"border-width:none;border-style:none;\"></button>";
		}

			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = "almacen_tecnico.grupos", $campo_rn = "id_grupo", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = "almacen_tecnico.grupos", $campo_rn = "id_grupo,descripcion,estatus,categoria,id_asig", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "", $minimo);
		

		$num_pag = ceil(count($todas) / 10);
		for ($i = 1; $i <= $num_pag; $i++) {
			$cantidad_paginas[] = $i;
		}
		if (count($cantidad_paginas) > 0) {
			$combo = "<SELECT name=\"cantidad_paginas\" id=\"cantidad_paginas\" class='lista' style='width:50px;' onchange=\"cambiopagina(" . $pagina_ajax . "," . $nombre_fn . ");\">";
			foreach ($cantidad_paginas as $llave => $valor) {
				if ($valor == $count) {
					$selected = 'selected';
				} else {
					$selected = "";
				}
				$combo.="<option value='" . $llave . "' $selected >" . $valor . "</option>";
			}
			$combo.="</SELECT> ";
		}

		if($_GET['ajax_request']!="true"){
			$campo_busqueda="<input type='text' name='busqueda' id='busqueda' class='campo' onkeypress='return enter(event,buscar_listadegrupos)' size='70'>";
			$botonB="<input type=\"button\" class='boton' value=\"Buscar\" OnClick=buscar_listadegrupos();>";

			$buscador="<table class='tabla'>
        	<tr><th class='titulo' colspan='2'>BUSQUEDA</th></tr>
        	<tr><td colspan='2'>" . $campo_busqueda . "&nbsp;&nbsp;&nbsp;" . $botonB . "</td></tr>
        	</table>";
			$boton_agreg = "<input type=\"button\" class='boton' value=\"Agregar\" OnClick='ir_a(\"classagregar.php?tipo=3\")'>";
			$lista=$buscador."
			<div id='form' ><table class='tabla'>
			<tr><th><div align=\"center\">".$boton_agreg."</div></th>
			</table>";
		}

		$lista.="
		<div id='datosp'>
		<input name='minimo' id='minimo' type='hidden' value=" . $minimo . " />
		<input name='count' id='count' type='hidden' value=" . $count . " />    
		<input name='parametrobusqueda' id='parametrobusqueda' type='hidden' value='" . $busquedaget . "'>  
		<table  class='tabla' id='tabla_corres'>
		<tr><th class='titulo' colspan='14'>ALMAC&Eacute;N</th></tr>
		<tr>    
		<th><div align=\"center\">DESCRIPCION</div></th>
		<th><div align=\"center\">ESTATUS</div></th>
		<th><div align=\"center\">CATEGORIA</div></th>
		<th><div align=\"center\">ACCIONES</div></th> 
		</tr>";
		if (count($ObjListado) > 0) {
			foreach ($ObjListado as $llave2 => $valor2) {
				$resto = $llave2 % 2;
				if (($resto == 0) && ($llave2 != 0)) {

					$color = "style='background:#f1f1f1;'"; //par
				} else {

					$color = "style='background:#ffffff;'"; //impar f9f9f9
				}

				if($valor2[3]=='DISPONIBLE'){

					$icono= "verde.gif";
				}else{

					$icono= "rojo.gif";
				}

				if($valor2[5]==1){

					$icono2= "images.jpeg";
					$icono3= "pdf.gif";
				}else{

					$icono2= "";
					$icono3= "";
				}


				$boton_E =  "<button style=\"width:12px;height:12px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' title=\"Eliminar\" name='button' id='button' OnClick=eliminargrupo('$valor2[1]''); ><img src='../estilos/imagenes/estatus/eliminado.gif' style=\"width:15px;height:15px;border-width:none;border-style:none;\" ></button>";
				$boton_M = "<button style=\"width:12px;height:12px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' title=\"Modificar Art&iacute;culo\" name='button' id='button' OnClick=modificararticulo('$valor2[1]',1); ><img src='../estilos/imagenes/folder_edit.png' style=\"width:20px;height:20px;border-width:none;border-style:none;\" ></button>";
				$boton_B = "<button style=\"width:12px;height:12px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' title=\"Mostrar Art&iacute;culo\" name='button' id='button' OnClick=mostrararticulodegrupo('$valor2[1]'); ><img src='../estilos/imagenes/estatus/buscar.gif' style=\"width:20px;height:20px;border-width:none;border-style:none;\" ></button>";
				$boton_R = "<button style=\"width:12px;height:12px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' title=\"$valor2[3]\" name='button' id='button' OnClick=responsable('$valor2[1]',50,1); ><img src='../estilos/imagenes/".$icono."' style=\"width:25px;height:25px;border-width:none;border-style:none;\" ></button>";
				$boton_T = "<button style=\"width:10px;height:10px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' title=\"Responsable\" name='button' id='button' OnClick=asignacion('$valor2[1]',1); ><img src='../estilos/imagenes/".$icono2."' style=\"width:25px;height:25px;border-width:none;border-style:none;\" ></button>";
				$boton_I = "<button style=\"width:12px;height:12px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' title=\"Modificar Imagen\" name='button' id='button' OnClick=modificarimagenes('$valor2[1]',2); ><img src='../estilos/imagenes/estatus/camera_picture.png' style=\"width:20px;height:20px;border-width:none;border-style:none;\" ></button>";
				$boton_pdf = "<button style=\"width:12px;height:12px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' title=\"Imprimir Reporte\" name='button' id='button' OnClick=imprimirpdf('$valor2[1]',1); ><img src='../estilos/imagenes/".$icono3."' style=\"width:20px;height:20px;border-width:none;border-style:none;\" ></button>";

				
				$botones="<td $color><div align=\"center\">" . $boton_E . "&nbsp;" . $boton_M . "&nbsp;&nbsp;" . $boton_B. "&nbsp;" . $boton_R. "&nbsp;&nbsp;&nbsp;" . $boton_I. "&nbsp;&nbsp;&nbsp;" . $boton_T. "&nbsp;&nbsp;&nbsp;&nbsp;" . $boton_pdf. "</div></td>";
				

				$lista.="<tr id='" . $valor2[1] . "'>
				<td $color><div align=\"left\">" . strtoupper(ucwords(utf8_encode($valor2[2]))) . "</div></td>
				<td $color><div align=\"left\">" . $valor2[3] . "</div></td>
				<td $color><div align=\"center\">" .$valor2[4] . "</div></td>

			    ".$botones."
				</tr>";

				//<td $color><div align=\"center\">" . $boton_E . "&nbsp;" . $boton_P . "&nbsp;" . $boton_R . "</div></td>
			}
		} else {
			$mensaje = "<div style='color: #FF0000;font-weight: bold;'><br>Error: Disculpe su busqueda NO arrojo ningun resultado.<div><br>";
			$lista.="<td colspan='8'><div align=\"center\">" . $mensaje . "</div></td>";
		}
		$lista.="</table>";
		if ($num_pag > $count) {
			$boton_Sig = " <button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name='button'  id=\"button\" onClick=\"siguiente(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Siguiente'><img src=\"../estilos/imagenes/estatus/p_3.gif\"  style=\"border-width:none;border-style:none;\"></button>";
		} else {
			$boton_Sig = "
			<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' name='button' id='button'><img src='../estilos/imagenes/estatus/p_3_2.gif' style=\"border-width:none;border-style:none;\"></button>";
		}
		$lista.=$this->leyenda($tipo = 0);
		$lista.="
		<table class='tabla'>
		<tr><th><div align=\"center\">" . $num_pag . "&nbsp;&nbsp;p&aacute;ginas,&nbsp;" . count($todas) . "&nbsp;&nbsp;articulo(s)</div></th>
		<th>" . $combo . "&nbsp;&nbsp;" . $boton_Ant . " &nbsp; " . $count . " / " . $num_pag . "&nbsp;" . $boton_Sig . "&nbsp;</th>
		</tr></table></div></div>";

		if($_GET['ajax_request']=="true"){
			echo $lista;
		}else{
			return $lista;	
		}
	}

	function listadeubicacion() {
		//print_r($_GET);   
		if (isset($_GET['parametrobusqueda'])) {
			$busquedaget = $_GET['parametrobusqueda'];
		} else {
			$busquedaget = $_GET['busqueda'];
		}

		$pagina_ajax = "'listadeubicacion&ajax_request=true'";
		$nombre_fn = "0";
		if (isset($_GET['minimo'])) {
			$minimo = $_GET['minimo'];
			$count = $_GET['count'];
			if ($count > 1) {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button' onClick=\"anterior(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Anterior'><img src='../estilos/imagenes/estatus/p_2.gif' style=\"border-width:none;border-style:none;\"></button>";
			} else {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button'><img src='../estilos/imagenes/estatus/p_2_2.gif'style=\"border-width:none;border-style:none;\"></button>";
			}
		} else {
			$minimo = 0;
			$count = 1;
			$boton_Ant = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name=\"button'  id=\"button\"><img src=\"../estilos/imagenes/estatus/p_2_2.gif\" style=\"border-width:none;border-style:none;\"></button>";
		}

			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = "almacen_tecnico.ubicacion_consumibles", $campo_rn = "id_articulo", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = "almacen_tecnico.ubicacion_consumibles", $campo_rn = "id_articulo,descripcion,marca,modelo,bien_nac,serial,costo,cantidad,desc_unidad_medida,estado,tipo_articulo,estatus,peldano,estante", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "", $minimo);
		

		$num_pag = ceil(count($todas) / 10);
		for ($i = 1; $i <= $num_pag; $i++) {
			$cantidad_paginas[] = $i;
		}
		if (count($cantidad_paginas) > 0) {
			$combo = "<SELECT name=\"cantidad_paginas\" id=\"cantidad_paginas\" class='lista' style='width:50px;' onchange=\"cambiopagina(" . $pagina_ajax . "," . $nombre_fn . ");\">";
			foreach ($cantidad_paginas as $llave => $valor) {
				if ($valor == $count) {
					$selected = 'selected';
				} else {
					$selected = "";
				}
				$combo.="<option value='" . $llave . "' $selected >" . $valor . "</option>";
			}
			$combo.="</SELECT> ";
		}

		if($_GET['ajax_request']!="true"){
			$campo_busqueda="<input type='text' name='busqueda' id='busqueda' class='campo' onkeypress='return enter(event,buscar_listadeubicacion)' size='70'>";
			$botonB="<input type=\"button\" class='boton' value=\"Buscar\" OnClick=buscar_listadeubicacion();>";

			$buscador="<table class='tabla'>
        	<tr><th class='titulo' colspan='2'>BUSQUEDA</th></tr>
        	<tr><td colspan='2'>" . $campo_busqueda . "&nbsp;&nbsp;&nbsp;" . $botonB . "</td></tr>
        	</table>";
			//$boton_agreg = "<input type=\"button\" class='boton' value=\"Agregar\" OnClick='ir_a(\"classagregar.php?tipo=2\")'>";
			$lista=$buscador."
			<div id='form' ><table class='tabla'>
			<tr><th><div align=\"center\">".$boton_agreg."</div></th>
			</table>";
		}

		$lista.="
		<div id='datosp'>
		<input name='minimo' id='minimo' type='hidden' value=" . $minimo . " />
		<input name='count' id='count' type='hidden' value=" . $count . " />    
		<input name='parametrobusqueda' id='parametrobusqueda' type='hidden' value='" . $busquedaget . "'>  
		<table  class='tabla' id='tabla_corres'>
		<tr><th class='titulo' colspan='14'>ALMAC&Eacute;N</th></tr>
		<tr>
		<th style='200'><div align=\"center\">DESCRIPCION</div></th>
		<th><div align=\"center\">MARCA</div></th>
		<th><div align=\"center\">MODELO</div></th>
		<th><div align=\"center\">BIEN NAC</div></th>     
		<th><div align=\"center\">SERIAL</div></th>
		<th><div align=\"center\">COSTO</div></th>
		<th><div align=\"center\">CANTIDAD</div></th>
		<th><div align=\"center\">PELDA&Ntilde;O</div></th>
		<th><div align=\"center\">ESTANTE</div></th>
		<!--<th><div align=\"center\">ACCIONES</div></th>-->
		</tr>";
		if (count($ObjListado) > 0) {
			foreach ($ObjListado as $llave2 => $valor2) {
				$resto = $llave2 % 2;
				if (($resto == 0) && ($llave2 != 0)) {

					$color = "style='background:#f1f1f1;'"; //par
				} else {

					$color = "style='background:#ffffff;'"; //impar f9f9f9
				}


				if($valor2[8]==0){

					$cantidad="<font color='RED'>AGOTADO</font>";
				}else{

					$cantidad=$valor2[8]." ".$valor2[9];
				} 

				$boton_E =  "<button style=\"width:12px;height:12px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' title=\"Eliminar\" name='button' id='button' OnClick=eliminararticulo($valor2[1]); ><img src='../estilos/imagenes/estatus/eliminado.gif' style=\"width:15px;height:15px;border-width:none;border-style:none;\" ></button>";
				$boton_M = "<button style=\"width:12px;height:12px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' title=\"Modificar Art&iacute;culo\" name='button' id='button' OnClick=modificararticulo($valor2[1],2); ><img src='../estilos/imagenes/folder_edit.png' style=\"width:20px;height:20px;border-width:none;border-style:none;\" ></button>";
				$boton_I = "<button style=\"width:12px;height:12px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' title=\"Modificar Imagen\" name='button' id='button' OnClick=modificarimagenes($valor2[1],1); ><img src='../estilos/imagenes/estatus/camera_picture.png' style=\"width:20px;height:20px;border-width:none;border-style:none;\" ></button>";
				$boton_B = "<button style=\"width:12px;height:12px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' title=\"Mostrar Art&iacute;culo\" name='button' id='button' OnClick=mostrararticulo($valor2[1],2); ><img src='../estilos/imagenes/estatus/buscar.gif' style=\"width:20px;height:20px;border-width:none;border-style:none;\" ></button>";
				//$boton_R = "<button style=\"width:12px;height:12px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' title=\"$valor2[13]\" name='button' id='button' OnClick=responsable($valor2[1],50,2); ><img src='../estilos/imagenes/".$icono."' style=\"width:25px;height:25px;border-width:none;border-style:none;\" ></button>";
				$boton_S = "<button style=\"width:12px;height:12px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' title=\"Ingreso\" name='button' id='button' OnClick=ingreso_stock($valor2[1],3); ><img src='../estilos/imagenes/ing_alm.jpeg' style=\"width:25px;height:25px;border-width:none;border-style:none;\" ></button>";

				if($valor2[12] == 2){
					//consumible
					$botones="<td $color><div align=\"left\">" . $boton_E . "&nbsp;" . $boton_M . "&nbsp;" . $boton_I. "&nbsp;" . $boton_B. "&nbsp;&nbsp;" . $boton_S. "</div></td>";
				}else{
					//prestamo
					$botones="<td $color><div align=\"left\">" . $boton_E . "&nbsp;" . $boton_M . "&nbsp;" . $boton_I. "&nbsp;" . $boton_B. "&nbsp;" . $boton_R. "</div></td>";
				}

				$lista.="<tr id='" . $valor2[1] . "'>
				<td $color><div align=\"left\">" . strtoupper(ucwords(utf8_encode($valor2[2]))) . "</div></td>
				<td $color><div align=\"center\">" . $valor2[3] . "</div></td>
				<td $color><div align=\"center\">" . $valor2[4] . "</div></td>
				<td $color><div align=\"center\">" . $valor2[5] . "</div></td>
				<td $color><div align=\"center\">" . $valor2[6] . "</div></td>
				<td $color><div align=\"center\">" . $valor2[10] . "</div></td>
				<td $color><div align=\"center\">" . $cantidad . " </div></td>			
				<td $color><div align=\"center\">" . $valor2[13] . " </div></td>			
				<td $color><div align=\"center\">" . $valor2[14]. " </div></td>			
				<!--".$botones."-->
				</tr>";

				//<td $color><div align=\"center\">" . $boton_E . "&nbsp;" . $boton_P . "&nbsp;" . $boton_R . "</div></td>
			}
		} else {
			$mensaje = "<div style='color: #FF0000;font-weight: bold;'><br>Error: Disculpe su busqueda NO arrojo ningun resultado.<div><br>";
			$lista.="<td colspan='8'><div align=\"center\">" . $mensaje . "</div></td>";
		}
		$lista.="</table>";
		if ($num_pag > $count) {
			$boton_Sig = " <button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name='button'  id=\"button\" onClick=\"siguiente(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Siguiente'><img src=\"../estilos/imagenes/estatus/p_3.gif\"  style=\"border-width:none;border-style:none;\"></button>";
		} else {
			$boton_Sig = "
			<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' name='button' id='button'><img src='../estilos/imagenes/estatus/p_3_2.gif' style=\"border-width:none;border-style:none;\"></button>";
		}
		$lista.=$this->leyenda($tipo = 0);
		$lista.="
		<table class='tabla'>
		<tr><th><div align=\"center\">" . $num_pag . "&nbsp;&nbsp;p&aacute;ginas,&nbsp;" . count($todas) . "&nbsp;&nbsp;articulo(s)</div></th>
		<th>" . $combo . "&nbsp;&nbsp;" . $boton_Ant . " &nbsp; " . $count . " / " . $num_pag . "&nbsp;" . $boton_Sig . "&nbsp;</th>
		</tr></table></div></div>";

		if($_GET['ajax_request']=="true"){
			echo $lista;
		}else{
			return $lista;	
		}
	}

	function listadeprestamos() {
		//print_r($_GET);   
		if (isset($_GET['parametrobusqueda'])) {
			$busquedaget = $_GET['parametrobusqueda'];
		} else {
			$busquedaget = $_GET['busqueda'];
		}

		$pagina_ajax = "'listadeprestamos&ajax_request=true'";
		$nombre_fn = "0";
		if (isset($_GET['minimo'])) {
			$minimo = $_GET['minimo'];
			$count = $_GET['count'];
			if ($count > 1) {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button' onClick=\"anterior(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Anterior'><img src='../estilos/imagenes/estatus/p_2.gif' style=\"border-width:none;border-style:none;\"></button>";
			} else {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button'><img src='../estilos/imagenes/estatus/p_2_2.gif'style=\"border-width:none;border-style:none;\"></button>";
			}
		} else {
			$minimo = 0;
			$count = 1;
			$boton_Ant = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name=\"button'  id=\"button\"><img src=\"../estilos/imagenes/estatus/p_2_2.gif\" style=\"border-width:none;border-style:none;\"></button>";
		}

			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = "almacen_tecnico.prestamos", $campo_rn = "id_prestamo", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = "almacen_tecnico.prestamos", $campo_rn = "id_prestamo,id_destino,id_desc_dest,resp_prestamo,estatus,tipo_prestamo", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "", $minimo);
		

		$num_pag = ceil(count($todas) / 10);
		for ($i = 1; $i <= $num_pag; $i++) {
			$cantidad_paginas[] = $i;
		}
		if (count($cantidad_paginas) > 0) {
			$combo = "<SELECT name=\"cantidad_paginas\" id=\"cantidad_paginas\" class='lista' style='width:50px;' onchange=\"cambiopagina(" . $pagina_ajax . "," . $nombre_fn . ");\">";
			foreach ($cantidad_paginas as $llave => $valor) {
				if ($valor == $count) {
					$selected = 'selected';
				} else {
					$selected = "";
				}
				$combo.="<option value='" . $llave . "' $selected >" . $valor . "</option>";
			}
			$combo.="</SELECT> ";
		}

		if($_GET['ajax_request']!="true"){
			$campo_busqueda="<input type='text' name='busqueda' id='busqueda' class='campo' onkeypress='return enter(event,buscar_listadeprestamos)' size='70'>";
			$botonB="<input type=\"button\" class='boton' value=\"Buscar\" OnClick=buscar_listadeprestamos();>";

			$buscador="<table class='tabla'>
        	<tr><th class='titulo' colspan='2'>BUSQUEDA</th></tr>
        	<tr><td colspan='2'>" . $campo_busqueda . "&nbsp;&nbsp;&nbsp;" . $botonB . "</td></tr>
        	</table>";
			$boton_agreg = "<input type=\"button\" class='boton' value=\"Generar Reporte\" OnClick='ir_a(\"../reportes/usuarios.php\")'>";
        	$lista=$buscador."
			<div id='form' ><table class='tabla'>
			</table>";
		}

		$lista.="
		<div id='datosp'>
		<input name='minimo' id='minimo' type='hidden' value=" . $minimo . " />
		<input name='count' id='count' type='hidden' value=" . $count . " />    
		<input name='parametrobusqueda' id='parametrobusqueda' type='hidden' value='" . $busquedaget . "'>  
		<table  class='tabla' id='tabla_corres'>
		<tr><th class='titulo' colspan='6'>DESPACHOS</th></tr>
		<tr>    
		<th><div align=\"center\">N° PRESTAMO</div></th>
		<th><div align=\"center\">RESPONSABLE</div></th>
		<th width=25%;><div align=\"center\">GERENCIA/PROGRAMA</div></th>
		<th><div align=\"center\">ESTATUS</div></th>
		<th><div align=\"center\">ACCIONES</div></th> 
		</tr>";
		if (count($ObjListado) > 0) {
			foreach ($ObjListado as $llave2 => $valor2) {
				$resto = $llave2 % 2;
				if (($resto == 0) && ($llave2 != 0)) {
					$color = "style='background:#f1f1f1;'"; //par
				} else {
					$color = "style='background:#ffffff;'"; //impar f9f9f9
				}
				

				$responsable=$this->ObjConsulta->selectpersonalresp($this->conect_sigesp, $valor2[4]);
				$nombres=utf8_encode($responsable[1][2]);
				$apellidos=utf8_encode($responsable[1][3]);

				if($valor2[2]==1){
					$tipo_destino="Gerencia:";
			        $datosgerencia=$this->ObjConsulta->selectgerenciadesc($this->conect_sigesp, $valor2[3]);
			        $destino=utf8_encode($datosgerencia[1][2]);
			   }elseif($valor2[2]==2){
			   		$tipo_destino=" Programa:";
			        $datosdestino = $this->ObjConsulta->selectprograma($this->conect_sistemas_vtv, $valor2[3]);
			        $destino=$datosdestino[1][2];
			    }elseif($valor2[2]==3){
			    	if(is_numeric ($valor2[3])==true){
			    		$tipo_destino=" Programa:";
			        	$datosdestino = $this->ObjConsulta->selectprograma($this->conect_sistemas_vtv, $valor2[3]);
			        	$destino=$datosdestino[1][2];
			    	}else{
			    		$destino=strtoupper($valor2[3]);
			    	}
			   		
			    }else{
			    	$destino=strtoupper($valor2[3]);
			    }

	

			    $boton_R = "<a href='../reportes/prestamo.php?id_solicitud=" . $valor2[1] . "&tipo_prestamo=" . $valor2[6] . "' target='_blank' title=\"Imprimir\"><img src=\"../estilos/imagenes/estatus/printer.png\" style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\"></a>";
				$boton_I = "<button style=\"width:12px;height:12px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' title=\"Generar Incidencia\" name='button' id='button' OnClick='ir_a(\"classincidencias.php?idsolicitud=$valor2[1]\")'; ><img src='../estilos/imagenes/estatus/attach.png' style=\"width:20px;height:20px;border-width:none;border-style:none;\" ></button>";
				//$boton_C = "<button style=\"width:12px;height:12px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' title=\"Consultar\" name='button' id='button' OnClick=modificarimagenes($valor2[1],2);><img src='../estilos/imagenes/estatus/buscar.gif' style=\"width:20px;height:20px;border-width:none;border-style:none;\" ></button>";
				$boton_E = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' title=\"Dar Entrada\" name='button' id='button' OnClick='ir_a(\"classentradas.php?idsolicitud=$valor2[1]&tipo_despacho=1\")'; ><img src='../estilos/imagenes/estatus/flecha.jpeg' style=\"width:20px;height:20px;border-width:none;border-style:none;\" ></button>";
				$boton_S = "<button style=\"width:18px;height:18px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' title=\"Suspender\" name='button' id='button' OnClick='ir_a(\"classentradas.php?idsolicitud=$valor2[1]&tipo_despacho=1&susp=1\")'; ><img src='../estilos/imagenes/estatus/error.png' style=\"width:20px;height:20px;border-width:none;border-style:none;\" ></button>";
				$boton_O = "<button style=\"width:18px;height:18px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' title=\"Consultar\" name='button' id='button' OnClick=consultardespacho($valor2[1]); ><img src='../estilos/imagenes/estatus/buscar.gif' style=\"width:20px;height:20px;border-width:none;border-style:none;\" ></button>";
				
				if ($valor2[5]=="SOLICITUD PROCESADA" or $valor2[5]=="SUSPENDIDA" ){
					//si ya fue procesada la solicitud o fue suspendida no se podran generar incidencias ni dar entrada.
					$botones="<td $color><div align=\"center\">" . $boton_R. "&nbsp;&nbsp;" . $boton_O . "</div></td>";
				}else{
					//Si esta en proceso la solicitud
					if($_SESSION['id_tipo_usuario']==31 or $_SESSION['id_tipo_usuario']==32){//Solo administradores pueden suspender
						if($valor2[6]==3){//si es un despacho de camaras(no debe tener incidencias)
							$botones="<td $color><div align=\"left\">" . $boton_R. "&nbsp;&nbsp;&nbsp;" . $boton_E . "&nbsp;&nbsp;" . $boton_S . "</div></td>";
						}else{//en casos de los demas despachos se puede imprimir, dar entrada o suspender
							$botones="<td $color><div align=\"left\">" . $boton_R. "&nbsp;" . $boton_I . "&nbsp;&nbsp;&nbsp;" . $boton_E . "&nbsp;&nbsp;" . $boton_S . "</div></td>";
						}
					}else{
						if($valor2[6]==3){//si es un despacho de camaras(no debe tener incidencias)
							$botones="<td $color><div align=\"left\">" . $boton_R. "&nbsp;&nbsp;&nbsp;" . $boton_E . "</div></td>";
						}else{//en casos de los demas despachos se puede imprimir, dar entrada o suspender
							$botones="<td $color><div align=\"left\">" . $boton_R. "&nbsp;" . $boton_I . "&nbsp;&nbsp;&nbsp;" . $boton_E . "</div></td>";
						}
					}
				}


				$lista.="<tr id='" . $valor2[1] . "'>
				<td $color><div align=\"center\">" . $valor2[1] . "</div></td>
				<td $color><div align=\"left\">" . strtoupper($nombres) . " " . strtoupper($apellidos) . "</div></td>
				<td $color><div align=\"left\">" . $destino . "</div></td>
				<td $color><div align=\"center\">" . $valor2[5] . "</div></td>
				".$botones."
				</tr>";

				//<td $color><div align=\"center\">" . $boton_E . "&nbsp;" . $boton_P . "&nbsp;" . $boton_R . "</div></td>
			}
		} else {
			$mensaje = "<div style='color: #FF0000;font-weight: bold;'><br>Error: Disculpe su busqueda NO arrojo ningun resultado.<div><br>";
			$lista.="<td colspan='8'><div align=\"center\">" . $mensaje . "</div></td>";
		}
		$lista.="</table>";
		if ($num_pag > $count) {
			$boton_Sig = " <button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name='button'  id=\"button\" onClick=\"siguiente(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Siguiente'><img src=\"../estilos/imagenes/estatus/p_3.gif\"  style=\"border-width:none;border-style:none;\"></button>";
		} else {
			$boton_Sig = "
			<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' name='button' id='button'><img src='../estilos/imagenes/estatus/p_3_2.gif' style=\"border-width:none;border-style:none;\"></button>";
		}
		$lista.=$this->leyenda($tipo = 0);
		$lista.="
		<table class='tabla'>
		<tr><th><div align=\"center\">" . $num_pag . "&nbsp;&nbsp;p&aacute;ginas,&nbsp;" . count($todas) . "&nbsp;&nbsp;articulo(s)</div></th>
		<th>" . $combo . "&nbsp;&nbsp;" . $boton_Ant . " &nbsp; " . $count . " / " . $num_pag . "&nbsp;" . $boton_Sig . "&nbsp;</th>
		</tr></table></div></div>";

		if($_GET['ajax_request']=="true"){
			echo $lista;
		}else{
			return $lista;	
		}
	}

	function listadedespachos() {
		//print_r($_GET);   
		if (isset($_GET['parametrobusqueda'])) {
			$busquedaget = $_GET['parametrobusqueda'];
		} else {
			$busquedaget = $_GET['busqueda'];
		}

		$pagina_ajax = "'listadedespachos&ajax_request=true'";
		$nombre_fn = "0";
		if (isset($_GET['minimo'])) {
			$minimo = $_GET['minimo'];
			$count = $_GET['count'];
			if ($count > 1) {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button' onClick=\"anterior(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Anterior'><img src='../estilos/imagenes/estatus/p_2.gif' style=\"border-width:none;border-style:none;\"></button>";
			} else {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button'><img src='../estilos/imagenes/estatus/p_2_2.gif'style=\"border-width:none;border-style:none;\"></button>";
			}
		} else {
			$minimo = 0;
			$count = 1;
			$boton_Ant = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name=\"button'  id=\"button\"><img src=\"../estilos/imagenes/estatus/p_2_2.gif\" style=\"border-width:none;border-style:none;\"></button>";
		}

			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = "almacen_tecnico.despachos", $campo_rn = "id_despacho", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = "almacen_tecnico.despachos", $campo_rn = "id_despacho,id_destino,id_desc_dest,resp_despacho,fecha_sol", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "", $minimo);
		

		$num_pag = ceil(count($todas) / 10);
		for ($i = 1; $i <= $num_pag; $i++) {
			$cantidad_paginas[] = $i;
		}
		if (count($cantidad_paginas) > 0) {
			$combo = "<SELECT name=\"cantidad_paginas\" id=\"cantidad_paginas\" class='lista' style='width:50px;' onchange=\"cambiopagina(" . $pagina_ajax . "," . $nombre_fn . ");\">";
			foreach ($cantidad_paginas as $llave => $valor) {
				if ($valor == $count) {
					$selected = 'selected';
				} else {
					$selected = "";
				}
				$combo.="<option value='" . $llave . "' $selected >" . $valor . "</option>";
			}
			$combo.="</SELECT> ";
		}

		if($_GET['ajax_request']!="true"){
			$campo_busqueda="<input type='text' name='busqueda' id='busqueda' class='campo' onkeypress='return enter(event,buscar_listadedespachos)' size='70'>";
			$botonB="<input type=\"button\" class='boton' value=\"Buscar\" OnClick=buscar_listadedespachos();>";

			$buscador="<table class='tabla'>
        	<tr><th class='titulo' colspan='2'>BUSQUEDA</th></tr>
        	<tr><td colspan='2'>" . $campo_busqueda . "&nbsp;&nbsp;&nbsp;" . $botonB . "</td></tr>
        	</table>";
			$lista=$buscador."
			<div id='form' ><table class='tabla'>
			</table>";
		}

		$lista.="
		<div id='datosp'>
		<input name='minimo' id='minimo' type='hidden' value=" . $minimo . " />
		<input name='count' id='count' type='hidden' value=" . $count . " />    
		<input name='parametrobusqueda' id='parametrobusqueda' type='hidden' value='" . $busquedaget . "'>  
		<table  class='tabla' id='tabla_corres'>
		<tr><th class='titulo' colspan='6'>DESPACHOS</th></tr>
		<tr>    
		<th><div align=\"center\">N° DESPACHO</div></th>
		<th><div align=\"center\">RESPONSABLE</div></th>
		<th width=25%;><div align=\"center\">DESTINO</div></th>
		<th><div align=\"center\">FECHA DE SOLICITUD</div></th>
		<th><div align=\"center\">ACCIONES</div></th> 
		</tr>";
		if (count($ObjListado) > 0) {
			foreach ($ObjListado as $llave2 => $valor2) {
				$resto = $llave2 % 2;
				if (($resto == 0) && ($llave2 != 0)) {
					$color = "style='background:#f1f1f1;'"; //par
				} else {
					$color = "style='background:#ffffff;'"; //impar f9f9f9
				}
				

				$responsable=$this->ObjConsulta->selectpersonalresp($this->conect_sigesp, $valor2[4]);
				$nombres=utf8_encode($responsable[1][2]);
				$apellidos=utf8_encode($responsable[1][3]);

				if($valor2[2]==1){
					$tipo_destino="Gerencia:";
			        $datosgerencia=$this->ObjConsulta->selectgerenciadesc($this->conect_sigesp, $valor2[3]);
			        $destino=utf8_encode($datosgerencia[1][2]);
			   }elseif($valor2[2]==2){
			   		$tipo_destino=" Programa:";
			        $datosdestino = $this->ObjConsulta->selectprograma($this->conect_sistemas_vtv, $valor2[3]);
			        $destino=$datosdestino[1][2];
			    }elseif($valor2[2]==3){//es un remoto (oficial o programa)
				    	if(is_numeric ($valor2[3])==true){
				    		$tipo_destino=" Programa:";
			        		$datosdestino = $this->ObjConsulta->selectprograma($this->conect_sistemas_vtv, $valor2[3]);
			        		$destino=$datosdestino[1][2];

				    	}else{
				    		$destino=strtoupper($valor2[3]);
				    	}
			    }else{
			    	$destino=strtoupper($valor2[3]);
			    }

				$fecha_sol = $this->Objfechahora->flibInvertirInEs($valor2[5]);


			    $boton_R = "<a href='../reportes/despacho.php?id_solicitud=" . $valor2[1] . " ' target='_blank' title=\"Imprimir\">
				<img src=\"../estilos/imagenes/estatus/printer.png\" style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\"></a>";
				$boton_E = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' title=\"Devoluciones\" name='button' id='button' OnClick='ir_a(\"classentradas.php?idsolicitud=$valor2[1]&tipo_despacho=2\")'; ><img src='../estilos/imagenes/estatus/flechaverde.jpeg' style=\"width:20px;height:20px;border-width:none;border-style:none;\" ></button>";
				

				$lista.="<tr id='" . $valor2[1] . "'>
				<td $color><div align=\"center\">" . $valor2[1] . "</div></td>
				<td $color><div align=\"left\">" . strtoupper($nombres) . " " . strtoupper($apellidos) . "</div></td>
				<td $color><div align=\"left\">" . $destino . "</div></td>
				<td $color><div align=\"center\">" . $fecha_sol . "</div></td>
				<td $color><div align=\"center\">" . $boton_R. "&nbsp;&nbsp;" . $boton_E. "</div></td>
				</tr>";

				//<td $color><div align=\"center\">" . $boton_E . "&nbsp;" . $boton_P . "&nbsp;" . $boton_R . "</div></td>
			}
		} else {
			$mensaje = "<div style='color: #FF0000;font-weight: bold;'><br>Error: Disculpe su busqueda NO arrojo ningun resultado.<div><br>";
			$lista.="<td colspan='8'><div align=\"center\">" . $mensaje . "</div></td>";
		}
		$lista.="</table>";
		if ($num_pag > $count) {
			$boton_Sig = " <button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name='button'  id=\"button\" onClick=\"siguiente(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Siguiente'><img src=\"../estilos/imagenes/estatus/p_3.gif\"  style=\"border-width:none;border-style:none;\"></button>";
		} else {
			$boton_Sig = "
			<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' name='button' id='button'><img src='../estilos/imagenes/estatus/p_3_2.gif' style=\"border-width:none;border-style:none;\"></button>";
		}
		$lista.=$this->leyenda($tipo = 0);
		$lista.="
		<table class='tabla'>
		<tr><th><div align=\"center\">" . $num_pag . "&nbsp;&nbsp;p&aacute;ginas,&nbsp;" . count($todas) . "&nbsp;&nbsp;articulo(s)</div></th>
		<th>" . $combo . "&nbsp;&nbsp;" . $boton_Ant . " &nbsp; " . $count . " / " . $num_pag . "&nbsp;" . $boton_Sig . "&nbsp;</th>
		</tr></table></div></div>";

		if($_GET['ajax_request']=="true"){
			echo $lista;
		}else{
			return $lista;	
		}
	}

	function listadeentradas() {
		//print_r($_GET);   
		if (isset($_GET['parametrobusqueda'])) {
			$busquedaget = $_GET['parametrobusqueda'];
		} else {
			$busquedaget = $_GET['busqueda'];
		}

		$pagina_ajax = "'listadeentradas&ajax_request=true'";
		$nombre_fn = "0";
		if (isset($_GET['minimo'])) {
			$minimo = $_GET['minimo'];
			$count = $_GET['count'];
			if ($count > 1) {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button' onClick=\"anterior(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Anterior'><img src='../estilos/imagenes/estatus/p_2.gif' style=\"border-width:none;border-style:none;\"></button>";
			} else {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button'><img src='../estilos/imagenes/estatus/p_2_2.gif'style=\"border-width:none;border-style:none;\"></button>";
			}
		} else {
			$minimo = 0;
			$count = 1;
			$boton_Ant = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name=\"button'  id=\"button\"><img src=\"../estilos/imagenes/estatus/p_2_2.gif\" style=\"border-width:none;border-style:none;\"></button>";
		}

			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = "almacen_tecnico.entradas", $campo_rn = "id_entrada", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = "almacen_tecnico.entradas", $campo_rn = "id_entrada,id_solicitud,resp_entrada,fecha_entrada", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "", $minimo);
		

		$num_pag = ceil(count($todas) / 10);
		for ($i = 1; $i <= $num_pag; $i++) {
			$cantidad_paginas[] = $i;
		}
		if (count($cantidad_paginas) > 0) {
			$combo = "<SELECT name=\"cantidad_paginas\" id=\"cantidad_paginas\" class='lista' style='width:50px;' onchange=\"cambiopagina(" . $pagina_ajax . "," . $nombre_fn . ");\">";
			foreach ($cantidad_paginas as $llave => $valor) {
				if ($valor == $count) {
					$selected = 'selected';
				} else {
					$selected = "";
				}
				$combo.="<option value='" . $llave . "' $selected >" . $valor . "</option>";
			}
			$combo.="</SELECT> ";
		}

		if($_GET['ajax_request']!="true"){
			$campo_busqueda="<input type='text' name='busqueda' id='busqueda' class='campo' onkeypress='return enter(event,buscar_listadeentradas)' size='70'>";
			$botonB="<input type=\"button\" class='boton' value=\"Buscar\" OnClick=buscar_listadeentradas();>";

			$buscador="<table class='tabla'>
        	<tr><th class='titulo' colspan='2'>BUSQUEDA</th></tr>
        	<tr><td colspan='2'>" . $campo_busqueda . "&nbsp;&nbsp;&nbsp;" . $botonB . "</td></tr>
        	</table>";
			$lista=$buscador."
			<div id='form' ><table class='tabla'>
			</table>";
		}

		$lista.="
		<div id='datosp'>
		<input name='minimo' id='minimo' type='hidden' value=" . $minimo . " />
		<input name='count' id='count' type='hidden' value=" . $count . " />    
		<input name='parametrobusqueda' id='parametrobusqueda' type='hidden' value='" . $busquedaget . "'>  
		<table  class='tabla' id='tabla_corres'>
		<tr><th class='titulo' colspan='6'>ENTRADAS</th></tr>
		<tr>    
		<th><div align=\"center\">N° ENTRADAS</div></th>
		<th><div align=\"center\">N° SOLICITUD</div></th>
		<th><div align=\"center\">RESPONSABLE</div></th>
		<th><div align=\"center\">FECHA DE ENTRADA</div></th>
		<th><div align=\"center\">ACCIONES</div></th> 
		</tr>";
		if (count($ObjListado) > 0) {
			foreach ($ObjListado as $llave2 => $valor2) {
				$resto = $llave2 % 2;
				if (($resto == 0) && ($llave2 != 0)) {
					$color = "style='background:#f1f1f1;'"; //par
				} else {
					$color = "style='background:#ffffff;'"; //impar f9f9f9
				}
				

				$responsable=$this->ObjConsulta->selectpersonalresp($this->conect_sigesp, $valor2[3]);
				$nombres=utf8_encode($responsable[1][2]);
				$apellidos=utf8_encode($responsable[1][3]);

				
				$fecha_entrada = $this->Objfechahora->flibInvertirInEs($valor2[4]);


			    $boton_R = "<a href='../reportes/entrada.php?id_entrada=" . $valor2[1] . "' target='_blank' title=\"Imprimir\">
				<img src=\"../estilos/imagenes/estatus/printer.png\" style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\"></a>";
				

				$lista.="<tr id='" . $valor2[1] . "'>
				<td $color><div align=\"center\">" . $valor2[1] . "</div></td>
				<td $color><div align=\"center\">" . $valor2[2] . "</div></td>
				<td $color><div align=\"left\">" . strtoupper($nombres) . " " . strtoupper($apellidos) . "</div></td>
				<td $color><div align=\"center\">" . $fecha_entrada . "</div></td>
				<td $color><div align=\"center\">" . $boton_R. "</div></td>
				</tr>";

				//<td $color><div align=\"center\">" . $boton_E . "&nbsp;" . $boton_P . "&nbsp;" . $boton_R . "</div></td>
			}
		} else {
			$mensaje = "<div style='color: #FF0000;font-weight: bold;'><br>Error: Disculpe su busqueda NO arrojo ningun resultado.<div><br>";
			$lista.="<td colspan='8'><div align=\"center\">" . $mensaje . "</div></td>";
		}
		$lista.="</table>";
		if ($num_pag > $count) {
			$boton_Sig = " <button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name='button'  id=\"button\" onClick=\"siguiente(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Siguiente'><img src=\"../estilos/imagenes/estatus/p_3.gif\"  style=\"border-width:none;border-style:none;\"></button>";
		} else {
			$boton_Sig = "
			<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' name='button' id='button'><img src='../estilos/imagenes/estatus/p_3_2.gif' style=\"border-width:none;border-style:none;\"></button>";
		}
		$lista.=$this->leyenda($tipo = 0);
		$lista.="
		<table class='tabla'>
		<tr><th><div align=\"center\">" . $num_pag . "&nbsp;&nbsp;p&aacute;ginas,&nbsp;" . count($todas) . "&nbsp;&nbsp;articulo(s)</div></th>
		<th>" . $combo . "&nbsp;&nbsp;" . $boton_Ant . " &nbsp; " . $count . " / " . $num_pag . "&nbsp;" . $boton_Sig . "&nbsp;</th>
		</tr></table></div></div>";

		if($_GET['ajax_request']=="true"){
			echo $lista;
		}else{
			return $lista;	
		}
	}

	function listadeexistenciamin() {
		//print_r($_GET);   
		if (isset($_GET['parametrobusqueda'])) {
			$busquedaget = $_GET['parametrobusqueda'];
		} else {
			$busquedaget = $_GET['busqueda'];
		}

		$pagina_ajax = "'listadeexistenciamin&ajax_request=true'";
		$nombre_fn = "0";
		if (isset($_GET['minimo'])) {
			$minimo = $_GET['minimo'];
			$count = $_GET['count'];
			if ($count > 1) {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button' onClick=\"anterior(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Anterior'><img src='../estilos/imagenes/estatus/p_2.gif' style=\"border-width:none;border-style:none;\"></button>";
			} else {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button'><img src='../estilos/imagenes/estatus/p_2_2.gif'style=\"border-width:none;border-style:none;\"></button>";
			}
		} else {
			$minimo = 0;
			$count = 1;
			$boton_Ant = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name=\"button'  id=\"button\"><img src=\"../estilos/imagenes/estatus/p_2_2.gif\" style=\"border-width:none;border-style:none;\"></button>";
		}

			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = "almacen_tecnico.articulos_existencia_min", $campo_rn = "id_articulo", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = "almacen_tecnico.articulos_existencia_min", $campo_rn = "id_articulo,descripcion,modelo,marca,estante,peldano,cantidad,cant_min", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "", $minimo);
		

		$num_pag = ceil(count($todas) / 10);
		for ($i = 1; $i <= $num_pag; $i++) {
			$cantidad_paginas[] = $i;
		}
		if (count($cantidad_paginas) > 0) {
			$combo = "<SELECT name=\"cantidad_paginas\" id=\"cantidad_paginas\" class='lista' style='width:50px;' onchange=\"cambiopagina(" . $pagina_ajax . "," . $nombre_fn . ");\">";
			foreach ($cantidad_paginas as $llave => $valor) {
				if ($valor == $count) {
					$selected = 'selected';
				} else {
					$selected = "";
				}
				$combo.="<option value='" . $llave . "' $selected >" . $valor . "</option>";
			}
			$combo.="</SELECT> ";
		}

		if($_GET['ajax_request']!="true"){
			$campo_busqueda="<input type='text' name='busqueda' id='busqueda' class='campo' onkeypress='return enter(event,buscar_listadeexistenciamin)' size='70'>";
			$botonB="<input type=\"button\" class='boton' value=\"Buscar\" OnClick=buscar_listadeexistenciamin();>";

			$buscador="<table class='tabla'>
        	<tr><th class='titulo' colspan='2'>BUSQUEDA</th></tr>
        	<tr><td colspan='2'>" . $campo_busqueda . "&nbsp;&nbsp;&nbsp;" . $botonB . "</td></tr>
        	</table>";
			
			$boton_agreg = "<input type=\"button\" class='boton' value=\"Generar Reporte\" OnClick='imprimir(\"../reportes/existencia_minima.php\")'>";
        	$lista=$buscador."
			<div id='form' ><table class='tabla'>
			<tr><th><div align=\"center\">".$boton_agreg."</div></th>
			</table>";
		}

		$lista.="
		<div id='datosp'>
		<input name='minimo' id='minimo' type='hidden' value=" . $minimo . " />
		<input name='count' id='count' type='hidden' value=" . $count . " />    
		<input name='parametrobusqueda' id='parametrobusqueda' type='hidden' value='" . $busquedaget . "'>  
		<table  class='tabla' id='tabla_corres'>
		<th><div align=\"center\">DESCRIPCIÓN</div></th>
		<th><div align=\"center\">MARCA</div></th>
		<th><div align=\"center\">MODELO</div></th>
		<th><div align=\"center\">ESTANTE</div></th> 
		<th><div align=\"center\">PELDAÑO</div></th> 
		<th><div align=\"center\">CANTIDAD MINIMA</div></th> 
		<th><div align=\"center\">CANTIDAD ACTUAL</div></th> 
		<th><div align=\"center\">ACCIONES</div></th> 
		</tr>";
		if (count($ObjListado) > 0) {
			foreach ($ObjListado as $llave2 => $valor2) {
				$resto = $llave2 % 2;
				if (($resto == 0) && ($llave2 != 0)) {
					$color = "style='background:#f1f1f1;'"; //par
				} else {
					$color = "style='background:#ffffff;'"; //impar f9f9f9
				}
				
			    if($valor2[7]==0){
					$valor2[7]="<font color='RED'>AGOTADO</font>";
				}else{
					$valor2[7]=$valor2[7];
				}

			    $boton_R = "<a href='../reportes/prestamo.php?id_solicitud=" . $valor2[1] . "&tipo_prestamo=" . $valor2[6] . "' target='_blank' title=\"Imprimir\">
				<img src=\"../estilos/imagenes/estatus/printer.png\" style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\"></a>";
				$boton_C = "<button style=\"width:12px;height:12px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' title=\"Consultar\" name='button' id='button' OnClick=modificarimagenes($valor2[1],2); >
				<img src='../estilos/imagenes/estatus/buscar.gif' style=\"width:20px;height:20px;border-width:none;border-style:none;\" ></button>";
				$boton_E = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' title=\"Dar Entrada\" name='button' id='button' OnClick=modificarimagenes($valor2[1],2); >
				<img src='../estilos/imagenes/estatus/flecha.jpeg' style=\"width:20px;height:20px;border-width:none;border-style:none;\" ></button>";
				$boton_S = "<button style=\"width:12px;height:12px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' title=\"Ingreso\" name='button' id='button' OnClick=ingreso_stock($valor2[1],2); ><img src='../estilos/imagenes/ing_alm.jpeg' style=\"width:25px;height:25px;border-width:none;border-style:none;\" ></button>";
				


				$lista.="<tr id='" . $valor2[1] . "'>
				<td $color><div align=\"center\">" . $valor2[2] . "</div></td>
				<td $color><div align=\"left\">" . $valor2[3] . "</div></td>
				<td $color><div align=\"left\">" . $valor2[4] . "</div></td>
				<td $color><div align=\"center\">" . $valor2[5] . "</div></td>
				<td $color><div align=\"center\">" . $valor2[6] . "</div></td>
				<td $color><div align=\"center\">" . $valor2[8] . "</div></td>
				<td $color><div align=\"center\">" . $valor2[7] . "</div></td>
				<td $color><div align=\"center\">" . $boton_S. "</div></td>
				</tr>";

				//<td $color><div align=\"center\">" . $boton_E . "&nbsp;" . $boton_P . "&nbsp;" . $boton_R . "</div></td>
			}
		} else {
			$mensaje = "<div style='color: #FF0000;font-weight: bold;'><br>Error: Disculpe su busqueda NO arrojo ningun resultado.<div><br>";
			$lista.="<td colspan='8'><div align=\"center\">" . $mensaje . "</div></td>";
		}
		$lista.="</table>";
		if ($num_pag > $count) {
			$boton_Sig = " <button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name='button'  id=\"button\" onClick=\"siguiente(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Siguiente'><img src=\"../estilos/imagenes/estatus/p_3.gif\"  style=\"border-width:none;border-style:none;\"></button>";
		} else {
			$boton_Sig = "
			<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' name='button' id='button'><img src='../estilos/imagenes/estatus/p_3_2.gif' style=\"border-width:none;border-style:none;\"></button>";
		}
		$lista.=$this->leyenda($tipo = 0);
		$lista.="
		<table class='tabla'>
		<tr><th><div align=\"center\">" . $num_pag . "&nbsp;&nbsp;p&aacute;ginas,&nbsp;" . count($todas) . "&nbsp;&nbsp;articulo(s)</div></th>
		<th>" . $combo . "&nbsp;&nbsp;" . $boton_Ant . " &nbsp; " . $count . " / " . $num_pag . "&nbsp;" . $boton_Sig . "&nbsp;</th>
		</tr></table></div></div>";

		if($_GET['ajax_request']=="true"){
			echo $lista;
		}else{
			return $lista;	
		}
	}

	function filtro_reportes(){
		
		$fechadesde = $_GET['fecha'];
		$fechahasta = $_GET['fecha2'];
		$id_grupo=$_GET['id_grupo'];


		echo "<script>
			var reporte='../reportes/control_i_o_cam.php?desde=".$fechadesde."&hasta=".$fechahasta."&id_grupo=".$id_grupo."';  
			var pagina='classbienvenida.php';                    
			function redireccionar() { 
				location.href=pagina;
				window.open(reporte)
			} 	
			setTimeout ('redireccionar()', 0);
			</script>";	
	}

	function imprimirpdf(){
		
		$id_grupo=$_GET['id_grupo'];

		echo "<script> 
			var pagina='classfiltro.php?id_grupo=".$id_grupo."';                    
			function redireccionar() { 
				location.href=pagina;
			} 	
			setTimeout ('redireccionar()', 0);
			</script>";	
	}

	function filtroreportes(){
		$tipo=$_GET['tipo'];
		$tipoart=$tipo;
		$titulo="Reportes";
		$tipoart ="<select id='tipoart' >
                    <option value='0' selected='selected' >Seleccione</option>
			        <option value='1'>Prestamo</option>
                    <option value='2'>Consumible</option>
                    </select>";  

        $descripcion_grupo = $this->ObjConsulta->grupos($this->conect_sistemas_vtv);
        $grupos = "<select id='grupo' name='grupo' style='width:105px;'>";
		$grupos.="<option value='0' selected >Seleccione </option>";
       	foreach ($descripcion_grupo as $llave => $valor) {
        		$grupos.="<option value='" . $valor[1] . "' $selected >" . $valor[2] . "</option>";
        	}
        $grupos.="</SELECT> "; 

        $descripcion_categoria = $this->ObjConsulta->categoria($this->conect_sistemas_vtv);
        $categoria = "<select id='categoria' name='categoria' style='width:105px;'>";
		$categoria.="<option value='0' selected >Seleccione </option>";
       	foreach ($descripcion_categoria as $llave => $valor) {
        		$categoria.="<option value='" . $valor[1] . "' $selected >" . $valor[2] . "</option>";
        	}
        $categoria.="</SELECT> "; 

        $descripcion_estado = $this->ObjConsulta->estado($this->conect_sistemas_vtv);
        $estado = "<select id='estado' name='estado' style='width:105px;'>";
		$estado.="<option value='0' selected >Seleccione </option>";
       	foreach ($descripcion_estado as $llave => $valor) {
        		$estado.="<option value='" . $valor[1] . "' $selected >" . $valor[2] . "</option>";
        	}
        $estado.="</SELECT> ";
        if($tipo==1){//Almacen
        	$datos= "<div id='datosp' align='center'><table class='tabla' align='center' style='width:850px;' >
        	<tr><th class='titulo' colspan='10'>" . $titulo . "</th></tr>
        	<tr><th>Grupo:</th><td>" . $grupos . "</td>
        	<th>Categoria:</th><td>" . $categoria . "</td>
        	<th>Estado:</th><td>" . $estado . "</td>
        	<th>Marca:</th><td><input type='text' name='marca' id='marca' class='campo' size='20'/></td></tr>
        	</table>";
        	$tipoart=$tipo;
        }elseif($tipo==2){//Consumibles
        	$datos= "<div id='datosp' align='center'><table class='tabla' align='center' style='width:850px;' >
        	<tr><th class='titulo' colspan='10'>" . $titulo . "</th></tr>
        	<tr><th>Categoria:</th><td>" . $categoria . "</td>
        	<th>Marca:</th><td><input type='text' name='marca' id='marca' class='campo' size='20'/></td>
        	<th>Estante:</th><td><input type='text' name='estante' id='estante' onkeypress='return validaN(event)' class='campo' size='3'/></td>
        	<th>Pelda&ntilde;o:</th><td><input type='text' name='peldano' id='peldano'onkeypress='return validaN(event)'  class='campo' size='3'/></td></tr>
        	</table>";
        	$tipoart=$tipo;
        }
   			$botonA = "<input type=\"button\" class='boton' value=\"Generar\" OnClick=gererarreporte($tipo);>";

			$datos.="<table class='tabla' style='width:850px;'>
        	<tr><th colspan='2'><div align='center'>" . $botonA . "</tr>
       		</table></div>";
   			
   			return $datos;
	}

	function reportes(){
		$grupo=$_GET['grupo'];
		$categoria=$_GET['categoria'];
		$estado=$_GET['estado'];
		$tipo_art=$_GET['tipoart'];
		$marca=$_GET['marca'];
		$estante=$_GET['estante'];
		$peldano=$_GET['peldano'];
		$tipo=$_GET['tipo'];
		if($tipo==1){
			echo "<script>
			var reporte='../reportes/inventario_almacen.php?gr=".$grupo."&cat=".$categoria."&est=".$estado."&ta=".$tipo_art."&mar=".$marca."&es=".$estante."&pel=".$peldano."';  
			var pagina='classbienvenida.php';                    
			function redireccionar() { 
				location.href=pagina;
				window.open(reporte)
			} 	
			setTimeout ('redireccionar()', 0);
			</script>";	
		}else{
			echo "<script>
			var reporte='../reportes/ubicacion_consumible.php?gr=".$grupo."&cat=".$categoria."&est=".$estado."&ta=".$tipo_art."&mar=".$marca."&es=".$estante."&pel=".$peldano."';  
			var pagina='classbienvenida.php';                    
			function redireccionar() { 
				location.href=pagina;
				window.open(reporte)
			} 	
			setTimeout ('redireccionar()', 0);
			</script>";	
		}
		
	}

	function imprimir(){
		
		$pagina=$_GET['pagina'];

		echo "<script> 
			var pagina='".$pagina."';
			var lista='classlista.php?modulo=listadeexistenciamin';                    
			function redireccionar() { 
				
				location.href=lista;
				window.open(pagina);
				
			} 	
			setTimeout ('redireccionar()', 0);
			</script>";	
	}

	function tarea_programada(){

    	$prueba=$this->ObjConsulta->selectprestamosvencidos($this->conect_sistemas_vtv);
    	foreach ($prueba as $llave => $valor) {

	    	$id_prestamo=$_GET['id_prestamo'];
	        $id_estatus_prestamo=$_GET['id_estatus_prestamo'];
	        $fecha_exp=$_GET['fecha_exp'];


	        $descestatus = $this->ObjConsulta->selectdescestatus($this->conect_sistemas_vtv, $id_estatus_prestamo);
			$descripestatus = $descestatus[1][1];

			$prestamos.= $id_prestamo."|".$fecha_exp."|".$descripestatus."\n";
			//$mensaje = "Pauta caducada por el sistema ";
			
			$nuevo_estatus_prestamo=8;
			$expestatus=$this->ObjConsulta->updateestatus($this->conect_sistemas_vtv, $nuevo_estatus_prestamo, $id_prestamo);
    	}

    	@print_r($prestamos);


    	
    	/*$prueba=$this->ObjConsulta->selectpautascaducas($this->conect_sistemas_vtv);
    	foreach ($prueba as $llave => $valor) {
	        $nom_pauta = $valor[1]; 
	        $descripcion = $valor[2]; 
	        $fecha_citacion = $valor[3]; 
    		$hora_citacion = $valor[4]; 
    		$id_estatus = $valor[5]; 
    		$id_pauta = $valor[6];

    		if ($id_estatus == 36){
    			$pautas.= "";

    		}else{
	    		if($descripcion!= ""){
		       	$nom_evento=$descripcion;
		    	}else{
		       	$nom_evento=$nom_pauta;
		    	}

		  		$descestatus = $this->ObjConsulta->selectdescestatus($this->conect_sistemas_vtv, $id_estatus);
				$descripestatus = $descestatus[1][1];

				$pautas.= $id_pauta."|".$nom_evento."|".$fecha_citacion."|".$hora_citacion."|".$descripestatus."\n";

				$mensaje = "Pauta caducada por el sistema ";
				$estatus=36;
				$user_exp ="El sistema";
				$expestatus=$this->ObjConsulta->updateestatus($this->conect_sistemas_vtv, $id_pauta, $mensaje);
				$cambiarestatus=$this->ObjConsulta->insertestatusnuevo($this->conect_sistemas_vtv, $id_pauta, $estatus, $user_exp, null);
    		}

        }

    	@print_r($pautas);*/
    }

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
	function enviarsolicitud() {
		$id_pauta = $_GET['id_pauta'];
		$estatus = $_GET['estatus_pt'];
		$mod = $_GET['mod'];
		$id_gerencia = $_SESSION['gerencia'];
		$cedula_sesion = $_SESSION['cedula'];
		$administrador = $_SESSION['id_tipo_usuario'];
		if (($administrador == 26) || ($administrador == 22)) {
			if ($administrador == 26) {
				$id_estatus = 13; //solicitado por productor
			} else {
				$id_estatus = 20; //enviado a jefe y retificado por el jefe de area
			}

			$verificarestatuspauta = $this->ObjConsulta->verificarestatuspauta($this->conect_sistemas_vtv, $id_pauta);
			$estatusactual = $verificarestatuspauta[1][1];
			if ($estatusactual == 2) {
				echo"<script>var pagina='classlistadepautas.php';                       
				alert('Disculpa la pauta ya fue enviada a la Gerencia');
				function redireccionar() { 
					location.href=pagina;
				} 
				setTimeout ('redireccionar()', 2000);
				</script>
				";
			} else {
				$datosobs=$this->ObjConsulta->selectobsrech($this->conect_sistemas_vtv, $id_pauta);
				$updateexpestatus = $this->ObjConsulta->updateexpestatus($this->conect_sistemas_vtv, $id_pauta);
				$insertestatus = $this->ObjConsulta->insertestatus($this->conect_sistemas_vtv, $id_pauta, $estatus, $cedula_sesion, $datosobs[1][1]);
				if ($administrador == 26) {
					//quitando envio a las listas de gerencia y division...
					//$correoger = $this->ObjConsulta->selectcorreosger($this->conect_sistemas_vtv, $id_gerencia); //gerencia se informan del trabajo.
					//$correodiv = $this->ObjConsulta->selectcorreosdiv($this->conect_sistemas_vtv, $id_gerencia, $_SESSION['division']); //gerencia se informan del trabajo.
					$correo_personal = $this->ObjConsulta->selectcorreousuario($this->conect_sistemas_vtv, $id_gerencia, $_SESSION['division'], '22'); //jefe inmediato
					$correo = @array_merge((array) $correoger, (array) $correodiv, (array) $correo_personal);
				} else if ($administrador == 22) {
					$correo = $this->ObjConsulta->selectcorreousuario($this->conect_sistemas_vtv, null, null, '24'); //jefe inmediato
				}

				if ($correo == "") {
					$mensaje = "<div style='color: #FF3300;font-weight: bold;'><br>Disculpe la gerencia no tiene una direccion de correo asociada<div><br>";
					echo $this->ObjMensaje->InterfazExitosamente($mensaje);
					if ($administrador == 26) {
						$pg_red = 'classlistadepautas.php';
					}
					if ($administrador == 22) {
						$pg_red = 'classlistadepautasjarea.php';
					}
					echo"<script>var pagina='" . $pg_red . "';                      
					function redireccionar() { 
						location.href=pagina;
					} 
					setTimeout ('redireccionar()', 2500);
					</script>
					";
				} else {
					$datoscorreo = $this->get_datos_generales_pau($id_pauta); //funcion que consulta y crea html de la pauta.
					$asunto = "Solicitud de facilidades tecnicas de la pauta " . $id_pauta . " ";
					foreach ($correo as $llavec => $valorc) {
						$this->enviarcorreo($valorc[1], $datoscorreo, $asunto); //enviando correo...
					}

					if ($mod == 1){
						$pagina = '6';	
					}else{
						$pagina = '3';
					}
					$mensaje="<div style='color: #009900;font-weight: bold;'><br>Su pauta se envio con exito</div>";
					echo $this->ObjMensaje->InterfazExitosamente($mensaje,($this->pasosdelproceso($pagina)));
					if ($administrador == 26) {
						$pg_red = 'classlistadepautas.php';
					}
					if ($administrador == 22) {
						$pg_red = 'classlistadepautasjarea.php';
					}
					echo"<script>var pagina='" . $pg_red . "';                      
					function redireccionar() { 
						location.href=pagina;
					} 
					setTimeout ('redireccionar()', 3000);
					</script>
					";
				}
			}
		} else {
			echo"<script>var pagina='classRegistro.php';                        
			alert('Disculpa no tiene permitido el acceso a esta pagina.');
			function redireccionar() { 
				location.href=pagina;
			} 
			setTimeout ('redireccionar()', 0);
			</script>
			";
		}
	}

	function enviarsolicitud2() {
		$id_pauta = $_GET['id_pauta'];
		$estatus = 10; // estatus=10 es: enviado a las areas operativas
		$administrador = $_SESSION['id_tipo_usuario'];
		$cedula_sesion = $_SESSION['cedula'];
		if ($administrador == 24) {
			$verificarestatuspauta = $this->ObjConsulta->verificarestatuspauta($this->conect_sistemas_vtv, $id_pauta);
			$estatusactual = $verificarestatuspauta[1][1];
			if ($estatusactual == 10) {
				echo"<script>var pagina='classlistadepautasual.php';                        
				alert('Disculpa la pauta ya fue enviada a las Areas Operativas');
				function redireccionar() { 
					location.href=pagina;
				} 
				setTimeout ('redireccionar()', 2000);
				</script>
				";
			} else {
				$datosobs=$this->ObjConsulta->selectobsrech($this->conect_sistemas_vtv, $id_pauta);
				$updateexpestatus = $this->ObjConsulta->updateexpestatus($this->conect_sistemas_vtv, $id_pauta);
				$insertestatus = $this->ObjConsulta->insertestatus($this->conect_sistemas_vtv, $id_pauta, $estatus, $cedula_sesion, $datosobs[1][1]);
				$datospauta = $this->ObjConsulta->selectdatospauta($this->conect_sistemas_vtv, $id_pauta);
				$tipo_pauta = $datospauta[1][1];
				$nombre_programa = $datospauta[1][2];
				$nombre_evento = $datospauta[1][3];
				$productor = $datospauta[1][4];
				$pauta_locacion = $datospauta[1][5];
				$tipo_evento = $datospauta[1][6];
				$tipo_traje = $datospauta[1][7];
				$citacion_lugar = $datospauta[1][17];
				$fecha_citacion = $datospauta[1][9];
				$fecha_citacion = $this->Objfechahora->flibInvertirInEs($fecha_citacion);
				$hora_citacion = $datospauta[1][10];
				$montaje_lugar = $datospauta[1][17];
				$fecha_montaje = $datospauta[1][11];
				$fecha_montaje = $this->Objfechahora->flibInvertirInEs($fecha_montaje);
				$hora_montaje = $datospauta[1][12];
				$emision_lugar = $datospauta[1][17];
				$fecha_emision = $datospauta[1][13];
				$fecha_emision = $this->Objfechahora->flibInvertirInEs($fecha_emision);
				$hora_emision = $datospauta[1][14];
				$retorno_lugar = $datospauta[1][17];
				$fecha_retorno = $datospauta[1][15];
				$fecha_retorno = $this->Objfechahora->flibInvertirInEs($fecha_retorno);
				$hora_retorno = $datospauta[1][16];
				if ($retorno_lugar == '0') {
					$retorno_lugar = "";
				} else {
					$retorno_lugar = "<tr><th align='left' style='background:#f1f1f1'>Retorno:</th><td align='left'>" . ucwords($retorno_lugar) . "</td><th align='center'>Fecha:</th><td  align='center'>" . $fecha_retorno . "</td><th align='center'>Hora:</th><td  align='center'>" . $hora_retorno . "</td></tr>";
				}
				if ($nombre_programa != "") {
					$evento = "<th align='left' style='background:#f1f1f1'>Programa:</th><td align='left' colspan='7'>" . ucwords($nombre_programa) . "</td>";
				} else {
					$evento = "<th align='left' style='background:#f1f1f1'>Evento:</th><td align='left' colspan='7'>" . ucwords($nombre_evento) . "</td>";
				}
				$descproductor = $this->ObjConsulta->selectdescproductor($this->conect_sistemas_vtv, $productor);
				$descripcionprod = strtolower($descproductor[1][1]);
				$descripcionprod = ucwords($descripcionprod);
				$datoslista = $this->ObjConsulta->selectmostrarrecursos($this->conect_sistemas_vtv, $id_pauta);
				foreach ($datoslista as $llave => $valor) {
					$id = $valor[1];
					$cant = $valor[2];
					$datosmaterial = $this->ObjConsulta->selectlistmatdesc($this->conect_sistemas_vtv, $id);
					$desc_id_recurso = $datosmaterial[1][1];
					$resto = $llave % 2;
					if (($resto == 0) && ($llave != 0)) {
						$color = "style='background:#f1f1f1;'"; //par
					} else {
						$color = "style='background:#ffffff;'"; //impar f9f9f9
					}
					$materiales2.="<tr id='$id'  $color><td  align='left'>" . ucwords(strtoupper($desc_id_recurso)) . "</td><td  align='center'>" . $cant . "</td>";
				}
				$titulo1 = "DATOS GENERALES";
				$titulo2 = "LISTA DE RECURSOS ASIGNADOS";
				$datoslista = $this->ObjConsulta->selectlistmatasig($this->conect_sistemas_vtv, $id_pauta);
				foreach ($datoslista as $llave3 => $valor3) {
					$id_recurso_asignado = $valor3[1];
					$id_recurso = $valor3[2];
					$datosmaterial = $this->ObjConsulta->selectlistmatdesc($this->conect_sistemas_vtv, $id_recurso);
					$desc_id_recurso = $datosmaterial[1][1];
					$datoscaract = $this->ObjConsulta->selectdatoscedula($this->conect_sistemas_vtv, $id_recurso_asignado);
					$primero = $datoscaract[1][1];
					$id_foto = $primero;
					if ($primero == "") {
						$datoscaract4 = $this->ObjConsulta->selectdatosserial($this->conect_sistemas_vtv, $id_recurso_asignado);
						$primero = $datoscaract4[1][1];
						$datoscaract5 = $this->ObjConsulta->selectdatosmarca($this->conect_sistemas_vtv, $id_recurso_asignado);
						$segundo = $datoscaract5[1][1];
						$datoscaract6 = $this->ObjConsulta->selectdatosagregados($this->conect_sistemas_vtv, $id_recurso_asignado);
						$tercero = $datoscaract6[1][1];
						if ($primero == "") {
							$datoscaract7 = $this->ObjConsulta->selectdatosbiennac($this->conect_sistemas_vtv, $id_recurso_asignado);
							$primero = $datoscaract7[1][1];
						}
						if ($primero != "") {
							$primero = "Serial: " . $primero . "";
						}
						if ($segundo != "") {
							$segundo = "Marca: " . $segundo . "";
						}
						if ($tercero != "") {
							$tercero = "Agregado: " . $tercero . "";
						}
						$id_foto = base64_encode($id_foto);
						$foto = "<img style='width:60px; height:60px; 'src='http://intranet/sistemas/directorio/paginas/download_foto.php?id=" . $id_foto . "=' >";
						$desc_caracteristicas = "" . $foto . "&nbsp;" . $primero . "&nbsp;" . $segundo . "&nbsp;" . $tercero . " ";
					} else {
						$datoscaract2 = $this->ObjConsulta->selectdatospnombre($this->conect_sistemas_vtv, $id_recurso_asignado);
						$segundo = $datoscaract2[1][1];
						$datoscaract3 = $this->ObjConsulta->selectdatospapellido($this->conect_sistemas_vtv, $id_recurso_asignado);
						$tercero = $datoscaract3[1][1];
						$id_foto = base64_encode($id_foto);
						$foto = "<img style='width:60px; height:60px; 'src='http://intranet/sistemas/directorio/paginas/download_foto.php?id=" . $id_foto . "=' >";
						$primero = "Cedula: " . $primero . "";
						$segundo = "Nombre: " . $segundo . "";
						$tercero = "Apellido: " . $tercero . "";
						$desc_caracteristicas = "" . $foto . "&nbsp;" . $primero . "&nbsp;" . $segundo . "&nbsp;" . $tercero . " ";
					}
					$resto = $llave3 % 2;
					if (($resto == 0) && ($llave3 != 0)) {
						$color = "style='background:#f1f1f1;'"; //par
					} else {
						$color = "style='background:#ffffff;'"; //impar f9f9f9
					}
					$asignados2.="<tr id='$id' $color><td  align='left'>" . ucwords($desc_id_recurso) . "</td><th><div align='center'>&nbsp;&nbsp;</div></th><td  align='left'>" . $desc_caracteristicas . "</td></tr><tr><td><br /></td></tr>";
				}
				$asignados = "<div id='datosp' align='center' style='border: thin solid #d2d2d2; margin-top: 10px; margin-right: 5px; margin-left: 5px;width:850px;'>
				<table style='width:850px';>
				<tr><th colspan='8' style='background-color:#8B0000; color:#FFFFFF' align='center'>" . $titulo2 . "</th></tr>
				<tr><th><div align='center'>Descripci&oacute;n</div></th><th><div align='center'>&nbsp;&nbsp;</div></th><th><div align='center'>Detalle</div></th></tr>
				<tr><td>" . $asignados2 . "</td></tr>
				</table></div>";
				$datoscorreo = "<div id='datosp' align='center' style='border: thin solid #d2d2d2; margin-top: 10px; margin-right: 5px; margin-left: 5px;width:850px;'>
				<table style='width:850px';>
				<tr><th colspan='8' style='background-color:#8B0000; color:#FFFFFF' align='center'>" . $titulo1 . "</th></tr>
				<tr>
				<th align='left' width='125' style='background:#f1f1f1'>N&deg; de pauta:</th><td>" . $id_pauta . "</td></tr>
				<tr>" . $evento . "</tr>
				<tr><th align='left' style='background:#f1f1f1'>Productor:</th><td colspan='7' align='left'>" . strtoupper($descripcionprod) . "</td></tr>
				<tr><th align='left' style='background:#f1f1f1'>Locaci&oacute;n:</th><td colspan='7' align='left'>" . ucwords($pauta_locacion) . "</td></tr>
				<tr><th align='left' style='background:#f1f1f1'>Tipo evento:</th><td colspan='7' align='left'>" . ucwords($tipo_evento) . "</td></tr>
				<tr><th align='left' style='background:#f1f1f1'>Tipo traje:</th><td colspan='7' align='left'>" . ucwords($tipo_traje) . "</td></tr>
				<tr>
				<th align='left' style='background:#f1f1f1'> Citaci&oacute;n:</th>
				<td align='left'>" . ucwords($citacion_lugar) . "</td>
				<th align='center'>Fecha:</th><td align='center'>" . $fecha_citacion . "</td>
				<th align='center'>Hora:</th><td align='center'>" . $hora_citacion . "</td>
				</tr>
				<tr>
				<th align='left' style='background:#f1f1f1'>Montaje:</th>
				<td align='left'>" . ucwords($montaje_lugar) . "</td>
				<th align='center'>Fecha:</th><td align='center'>" . $fecha_montaje . "</td>
				<th align='center'>Hora:</th><td align='center'>" . $hora_montaje . "</td>
				</tr>
				<tr>
				<th align='left' style='background:#f1f1f1'>Emisi&oacute;n:</th>
				<td align='left'>" . ucwords($emision_lugar) . "</td>
				<th align='center'>Fecha:</th><td align='center'>" . $fecha_emision . "</td>
				<th align='center'>Hora:</th><td align='center'>" . $hora_emision . "</td>
				</tr>
				" . $retorno_lugar . "</table></div>";
				$espacio = "<br>";
				$dobleespacio = "<br>";
				$datoscorreo = $datoscorreo . $espacio;
				$datoscorreo = $datoscorreo . $asignados;
				$datoscorreo = $datoscorreo . $espacio . $dobleespacio;
				$nota = "Para validar o rechazar esta pauta, haga <a href='http://intranet/sistemas/_PautasProduccion/' target='_blank'> Click Aqu&iacute;</a>";
				$datoscorreo = $datoscorreo . $nota;
				$datoscorreo = $datoscorreo . $espacio . $dobleespacio . $dobleespacio;
				$piedepagina = "Gerencia de Tecnolog&iacute;a de la Informaci&oacute;n y la Comunicaci&oacute;n.<br />Solicitudes de facilidades t&eacute;cnicas";
				$datoscorreo = $datoscorreo . $piedepagina;
				$asunto = "Lista de recursos asignados por la UAL para la pauta " . $id_pauta . " ";
				$idsgerencia = $this->ObjConsulta->selectidsgerencia($this->conect_sistemas_vtv, $id_pauta);
				foreach ($idsgerencia as $llave5 => $valor5) {
					$id_gerencia = $valor5[1];
					$id_division = $valor5[2];
					$correoao = $this->ObjConsulta->selectcorreosger($this->conect_sistemas_vtv, $id_gerencia); //selecciona correo de la gerencia respectiva.
					$correodiv = $this->ObjConsulta->selectcorreosdiv($this->conect_sistemas_vtv, $id_gerencia, $id_division); //selecciona correo de la division respectiva.
					$correousuario = $this->ObjConsulta->selectcorreousuario($this->conect_sistemas_vtv, $id_gerencia, $id_division, "25"); //selecciona el correo de los responsables respectivos
					$correo = array_merge((array) $correoao, (array) $correodiv, (array) $correousuario); //une todos los correos.
					if ($correo == "") {
						$mensaje = "<div style='color: #FF3300;font-weight: bold;'><br>Disculpe la gerencia () no tiene una direccion de correo asociada<div><br>";
						echo $this->ObjMensaje->InterfazExitosamente($mensaje);
						echo"<script>var pagina='classlistadepautasual.php';                        
						function redireccionar() {
							location.href=pagina;
						}
						setTimeout ('redireccionar()', 2500);
						</script>
						";
					} else {
						foreach ($correo as $llave7 => $valor7) {
							$this->enviarcorreo($valor7[1], $datoscorreo, $asunto);
						}
					}
				}
				$pagina = '9';
				$mensaje = "<div style='color: #009900;font-weight: bold;'><br>Su pauta se a enviado con exito<div><br>";
				echo $this->ObjMensaje->InterfazExitosamente($mensaje,($this->pasosdelproceso($pagina)));
				echo"<script>var pagina='classlistadepautasual.php';                        
				function redireccionar() {
					location.href=pagina;
				}
				setTimeout ('redireccionar()', 2500);
				</script>
				";
			}
		} else {
			echo"<script>var pagina='classRegistro.php';                        
			alert('Disculpa no tiene permitido el acceso a esta pagina. tu id es " . $administrador . "');
			function redireccionar() { 
				location.href=pagina;
			} 
			setTimeout ('redireccionar()', 0);
			</script>
			";
		}
	}

	function enviarsolicitud3() {
		$id_pauta = $_GET['id_pauta'];
		$estatus = 26; // estatus=26 es: informe generado
		$administrador = $_SESSION['id_tipo_usuario'];
		$cedula_sesion = $_SESSION['cedula'];
		if ($administrador == 26 or $administrador==22 or $administrador==24) {

			$verificarestatuspauta = $this->ObjConsulta->verificarestatuspauta($this->conect_sistemas_vtv, $id_pauta);
			$estatusactual = $verificarestatuspauta[1][1];
			if ($estatusactual == 26) {
				echo"<script>var pagina='classlistadepautas.php';                       
				alert('Disculpa el informe ya fue enviado');
				function redireccionar() { 
					location.href=pagina;
				} 
				setTimeout ('redireccionar()', 2000);
				</script>
				";
			} else {
				$updateexpestatus = $this->ObjConsulta->updateexpestatus($this->conect_sistemas_vtv, $id_pauta);
				$insertestatus = $this->ObjConsulta->insertestatus($this->conect_sistemas_vtv, $id_pauta, $estatus, $cedula_sesion);
				$datospauta = $this->ObjConsulta->selectdatospauta($this->conect_sistemas_vtv, $id_pauta);
				$tipo_pauta = $datospauta[1][1];
				$nombre_programa = $datospauta[1][2];
				$nombre_evento = $datospauta[1][3];
				$productor = $datospauta[1][4];
				$pauta_locacion = $datospauta[1][5];
				$tipo_evento = $datospauta[1][6];
				$tipo_traje = $datospauta[1][7];
				$citacion_lugar = $datospauta[1][17];
				$fecha_citacion = $datospauta[1][9];
				$fecha_citacion = $this->Objfechahora->flibInvertirInEs($fecha_citacion);
				$hora_citacion = $datospauta[1][10];
				$montaje_lugar = $datospauta[1][17];
				$fecha_montaje = $datospauta[1][11];
				$fecha_montaje = $this->Objfechahora->flibInvertirInEs($fecha_montaje);
				$hora_montaje = $datospauta[1][12];
				$emision_lugar = $datospauta[1][17];
				$fecha_emision = $datospauta[1][13];
				$fecha_emision = $this->Objfechahora->flibInvertirInEs($fecha_emision);
				$hora_emision = $datospauta[1][14];
				$retorno_lugar = $datospauta[1][17];
				$fecha_retorno = $datospauta[1][15];
				$fecha_retorno = $this->Objfechahora->flibInvertirInEs($fecha_retorno);
				$hora_retorno = $datospauta[1][16];
				if ($retorno_lugar == '0') {
					$retorno_lugar = "";
				} else {
					$retorno_lugar = "<tr><th align='left' style='background:#f1f1f1'>Retorno:</th><td align='left'>" . ucwords($retorno_lugar) . "</td><th align='center'>Fecha:</th><td  align='center'>" . $fecha_retorno . "</td><th align='center'>Hora:</th><td  align='center'>" . $hora_retorno . "</td></tr>";
				}
				if ($nombre_programa != "") {
					$evento = "<th align='left' style='background:#f1f1f1'>Programa:</th><td align='left' colspan='7'>" . ucwords($nombre_programa) . "</td>";
				} else {
					$evento = "<th align='left' style='background:#f1f1f1'>Evento:</th><td align='left' colspan='7'>" . ucwords($nombre_evento) . "</td>";
				}
				$descproductor = $this->ObjConsulta->selectdescproductor($this->conect_sistemas_vtv, $productor);
				$descripcionprod = strtolower($descproductor[1][1]);
				$descripcionprod = ucwords($descripcionprod);
				$titulo1 = "DATOS GENERALES";
				$titulo2 = "INFORME DE PRODUCCI&Oacute;N";
				$datoslistajust = $this->ObjConsulta->selectlistmatjust($this->conect_sistemas_vtv, $id_pauta);
				foreach ($datoslistajust as $llave4 => $valor4) {
					$id_detalle_servicio = $valor4[1];
					$id_informe = $valor4[2];
					$id_estado_informe = $valor4[3];
					$observaciones = $valor4[4];
					$datosnojust = $this->ObjConsulta->selectlistnojust($this->conect_sistemas_vtv, $id_detalle_servicio, $id_pauta);
					$id_estatus_actual = $datosnojust[1][1];
					if ($id_estatus_actual == 35) {
						$datos_estado_inf = $this->ObjConsulta->selectestadoinf($this->conect_sistemas_vtv, $id_estado_informe);
						$desc_estado = $datos_estado_inf[1][1];
						$datosidrecurso = $this->ObjConsulta->selectidrecurso($this->conect_sistemas_vtv, $id_detalle_servicio);
						$id_recurso_asignado = $datosidrecurso[1][1];
						$id_recurso = $datosidrecurso[1][2];
						$datosdesc2 = $this->ObjConsulta->selectlistmatdesc($this->conect_sistemas_vtv, $id_recurso);
						$desc_id_recurso = $datosdesc2[1][1];
						$idtipoinventario = $datosdesc2[1][2];
						$datoscaract = $this->ObjConsulta->selectdatoscedula($this->conect_sistemas_vtv, $id_recurso_asignado);
						$primero = $datoscaract[1][1];
						$id_foto = $primero;
						if ($primero == "") {
							$datoscaract4 = $this->ObjConsulta->selectdatosserial($this->conect_sistemas_vtv, $id_recurso_asignado);
							$primero = $datoscaract4[1][1];
							$datoscaract5 = $this->ObjConsulta->selectdatosmarca($this->conect_sistemas_vtv, $id_recurso_asignado);
							$segundo = $datoscaract5[1][1];
							$datoscaract6 = $this->ObjConsulta->selectdatosagregados($this->conect_sistemas_vtv, $id_recurso_asignado);
							$tercero = $datoscaract6[1][1];
							if ($primero == "") {
								$datoscaract7 = $this->ObjConsulta->selectdatosbiennac($this->conect_sistemas_vtv, $id_recurso_asignado);
								$primero = $datoscaract7[1][1];
							}
							if ($primero != "") {
								$primero = "Serial: " . $primero . "";
							}
							if ($segundo != "") {
								$segundo = "Marca: " . $segundo . "";
							}
							if ($tercero != "") {
								$tercero = "Agregado: " . $tercero . "";
							}
							$id_foto = base64_encode($id_foto);
							$foto = "<img style='width:60px; height:60px; 'src='http://intranet/sistemas/directorio/paginas/download_foto.php?id=" . $id_foto . "=' >";
							$desc_caracteristicas = "" . $foto . "&nbsp;" . $primero . "&nbsp;" . $segundo . "&nbsp;" . $tercero . " ";
						} else {
							$datoscaract2 = $this->ObjConsulta->selectdatospnombre($this->conect_sistemas_vtv, $id_recurso_asignado);
							$segundo = $datoscaract2[1][1];
							$datoscaract3 = $this->ObjConsulta->selectdatospapellido($this->conect_sistemas_vtv, $id_recurso_asignado);
							$tercero = $datoscaract3[1][1];
							$id_foto = base64_encode($id_foto);
							$foto = "<img style='width:60px; height:60px; 'src='http://intranet/sistemas/directorio/paginas/download_foto.php?id=" . $id_foto . "=' >";
							$primero = "Cedula: " . $primero . "";
							$segundo = "Nombre: " . $segundo . "";
							$tercero = "Apellido: " . $tercero . "";
							$desc_caracteristicas = "" . $foto . "&nbsp;" . $primero . "&nbsp;" . $segundo . "&nbsp;" . $tercero . " ";
						}
						$color = "";
						$resto = $llave4 % 2;
						if (($resto == 0) && ($llave4 != 0)) {
							$color = "style='background:#f1f1f1;'"; //par
						} else {
							$color = "style='background:#ffffff;'"; //impar f9f9f9
						}

						$justificados2.="<tr id='$id' $color><td  align='left'>" . strtoupper($desc_id_recurso) . "</td><td  align='left'>" . $desc_caracteristicas . "</td><td  align='center'>" . ucwords($desc_estado) . "</td><th><div align='center'>&nbsp;&nbsp;</div></th><td  align='left'>" . ucwords($observaciones) . "</td></tr>";
					}
				}
				$justificados = "
				<div id='datosp' align='center' style='border: thin solid #d2d2d2; margin-top: 10px; margin-right: 5px; margin-left: 5px;width:850px;'>
				<table style='width:850px';>
				<tr><th colspan='8' style='background-color:#8B0000; color:#FFFFFF' align='center'>" . $titulo2 . "</th></tr>
				<tr><th><div align='center'>Descripci&oacute;n</div></th><th><div align='center'>Detalle</div></th><th><div align='center'>Asistencia&frasl;Estado</div></th><th><div align='center'>&nbsp;&nbsp;</div></th><th><div align='center'>Observaciones</div></th></tr>
				" . $justificados2 . "
				</table>
				";
				$datoscorreo = "<div id='datosp' align='center' style='border: thin solid #d2d2d2; margin-top: 10px; margin-right: 5px; margin-left: 5px;width:850px;'>
				<table style='width:850px';>
				<tr><th colspan='8' style='background-color:#8B0000; color:#FFFFFF' align='center'>" . $titulo1 . "</th></tr><tr>
				<th align='left' width='125' style='background:#f1f1f1'>N&deg; de pauta:</th><td>" . $id_pauta . "</td></tr>
				<tr>" . $evento . "</tr>
				<tr><th align='left' style='background:#f1f1f1'>Productor:</th><td colspan='7' align='left'>" . strtoupper($descripcionprod) . "</td></tr>
				<tr><th align='left' style='background:#f1f1f1'>Locaci&oacute;n:</th><td colspan='7' align='left'>" . ucwords($pauta_locacion) . "</td></tr>
				<tr><th align='left' style='background:#f1f1f1'>Tipo evento:</th><td colspan='7' align='left'>" . ucwords($tipo_evento) . "</td></tr>
				<tr><th align='left' style='background:#f1f1f1'>Tipo traje:</th><td colspan='7' align='left'>" . ucwords($tipo_traje) . "</td></tr>
				<tr>
				<th align='left' style='background:#f1f1f1'> Citaci&oacute;n:</th>
				<td align='left'>" . ucwords($citacion_lugar) . "</td>
				<th align='center'>Fecha:</th><td align='center'>" . $fecha_citacion . "</td>
				<th align='center'>Hora:</th><td align='center'>" . $hora_citacion . "</td>
				</tr>
				<tr>
				<th align='left' style='background:#f1f1f1'>Montaje:</th>
				<td align='left'>" . ucwords($montaje_lugar) . "</td>
				<th align='center'>Fecha:</th><td align='center'>" . $fecha_montaje . "</td>
				<th align='center'>Hora:</th><td align='center'>" . $hora_montaje . "</td>
				</tr>
				<tr>
				<th align='left' style='background:#f1f1f1'>Emisi&oacute;n:</th>
				<td align='left'>" . ucwords($emision_lugar) . "</td>
				<th align='center'>Fecha:</th><td align='center'>" . $fecha_emision . "</td>
				<th align='center'>Hora:</th><td align='center'>" . $hora_emision . "</td>
				</tr>
				" . $retorno_lugar . "
				</table></div>";
				$espacio = "<br>";
				$dobleespacio = "<br>";
				$datoscorreo = $datoscorreo . $espacio;
				$datoscorreo = $datoscorreo . $justificados;
				$datoscorreo = $datoscorreo . $espacio . $dobleespacio;
				$nota = ""; //Para validar o rechazar esta pauta, haga <a href='http://intranet/sistemas/_PautasProduccion/' target='_blank'> Click Aqu&iacute;</a>
				$datoscorreo = $datoscorreo . $nota;
				$datoscorreo = $datoscorreo . $espacio . $dobleespacio . $dobleespacio;
				$piedepagina = "Gerencia de Tecnolog&iacute;a de la Informaci&oacute;n y la Comunicaci&oacute;n. Solicitudes de facilidades t&eacute;cnicas";
				$datoscorreo = $datoscorreo . $piedepagina;
				$asunto = "Informe de produccion de la pauta " . $id_pauta . " ";
				$idsgerencia = $this->ObjConsulta->selectidsgerencia($this->conect_sistemas_vtv, $id_pauta);
				foreach ($idsgerencia as $llave5 => $valor5) {
					$id_gerencia = $valor5[1];
					$id_division = $valor5[2];
					$correoao = $this->ObjConsulta->selectcorreosger($this->conect_sistemas_vtv, $id_gerencia); //gerencia areas operativas
					$correodiv = $this->ObjConsulta->selectcorreosdiv($this->conect_sistemas_vtv, $id_gerencia, $id_division); //selecciona correo de la division respectiva.
					$correousuario = $this->ObjConsulta->selectcorreousuario($this->conect_sistemas_vtv, $id_gerencia, $id_division, "25"); //selecciona el correo de los responsables respectivos
					$correo = array_merge((array) $correoao, (array) $correodiv, (array) $correousuario); //une todos los correos.
					if ($correo == "") {
						$mensaje = "<div style='color: #FF3300;font-weight: bold;'><br>Disculpe la gerencia no tiene una direccion de correo asociada<div><br>";
						echo $this->ObjMensaje->InterfazExitosamente($mensaje);
						echo"<script>var pagina='classlistadepautas.php';                       
						function redireccionar() { 
							location.href=pagina;
						} 
						setTimeout ('redireccionar()', 2500);
						</script>
						";
					} else {
						foreach ($correo as $llave7 => $valor7) {
							$this->enviarcorreo($valor7[1], $datoscorreo, $asunto);
						}
					}
				}
				$mensaje = "<div style='color: #009900;font-weight: bold;'><br>Su informe se a enviado con exito<div><br>";
				echo $this->ObjMensaje->InterfazExitosamente($mensaje);
				echo"<script>var pagina='classlistadeinformes.php';                     
				function redireccionar() { 
					location.href=pagina;
				} 
				setTimeout ('redireccionar()', 3000);
				</script>
				";

			}
		} else {
			echo"<script>var pagina='classRegistro.php';                        
			alert('Disculpa no tiene permitido el acceso a esta pagina.');
			function redireccionar() { 
				location.href=pagina;
			} 
			setTimeout ('redireccionar()', 0);
			</script>
			";
		}
	}

	function guardarcorreos() {
		$cedula = $_SESSION['cedula'];
		$gerencias = explode(",", $_GET['gerencias']);
		$divisiones = explode(",", $_GET['divisiones']);
		//revisar la insercion de gerencias..
		foreach ($gerencias as $llave => $valor) {
			if ($valor != "") {
				list ($idger, $ger) = explode(";", $valor);
				$verificarcorreos = $this->ObjConsulta->selectverificarcorreos($this->conect_sistemas_vtv, $idger);
				//verificar si al menos un solo correo hay de esta gerencia
				if (count($verificarcorreos) == 0) {
					//Si es igual a 0 es porque no hay ninguno y se inserta el nuevo correo
					$insertcorreos = $this->ObjConsulta->insertcorreos($this->conect_sistemas_vtv, $idger, $ger, $cedula);
				} else {
					$expcorreos = $this->ObjConsulta->updateexpcorreos($this->conect_sistemas_vtv, $idger); //donde este el idger exp los correos
					$insertcorreos = $this->ObjConsulta->insertcorreos($this->conect_sistemas_vtv, $idger, $ger, $cedula);
				}
			}
		}
		foreach ($divisiones as $llave2 => $valor2) {
			list ($iddiv, $div) = explode(";", $valor2);
			$datosdiv = $this->ObjConsulta->selectdivision($this->conect_sistemas_vtv, null, $iddiv);
			$idger = $datosdiv[1][3];
			$verificarcorreos_division = $this->ObjConsulta->selectverificarcorreos($this->conect_sistemas_vtv, $idger, $iddiv);
			if (count($verificarcorreos_division) == 0) {
				$insertcorreosdiv = $this->ObjConsulta->insertcorreosdiv($this->conect_sistemas_vtv, $idger, $iddiv, $div, $cedula);
			} else {
				$insertcorreosdiv = $this->ObjConsulta->insertcorreosdiv($this->conect_sistemas_vtv, $idger, $iddiv, $div, $cedula);
			}
		}
		$mensaje = "<div style='color: #009900;font-weight: bold;'><br>Los correos se guardaron con exito<div><br>";
		echo $this->ObjMensaje->InterfazExitosamente($mensaje);
		echo"<script>var pagina='classbienvenida.php';                      
		function redireccionar() { 
			location.href=pagina;
		} 
		setTimeout ('redireccionar()', 3000);
		</script>
		";
	}*/

	function leyenda($tipo=0) {
		switch ($tipo) {
			case 0:
			$leyenda = "";
			break;
			case 1:
			$leyenda = "<table class='tabla'>
			<tr><th class='titulo' colspan='10' align=\"center\">LEYENDA</th></tr>
			<tr align=\"center\"><th><td><img src='../estilos/imagenes/estatus/eliminado.gif' ></td></th>
			<th><td><img src='../estilos/imagenes/estatus/buscar.gif'></th></td>
			<th><td><img src='../estilos/imagenes/estatus/nuevo.gif'></td></th>
			<th><td><img src='../estilos/imagenes/estatus/page_edit.png'></td></th>
			</tr>
			<tr align=\"center\"><th><td>Anular Pauta &nbsp;</td></th>
			<th><td>Mostrar Detalles &nbsp;</td></th>
			<th><td>Agregar Recursos</td></th>
			<th><td>Modificar</td></th>
			</tr>
			</table>";
			break;
			case 2:
			$leyenda = "<table class='tabla'>
			<tr><th class='titulo' colspan='19' align=\"center\">LEYENDA</th></tr>          
			<tr align=\"center\">
			<!--<th><td><img src='../estilos/imagenes/estatus/por-entregar.gif'></td></th>
			<th><td><img src='../estilos/imagenes/estatus/aprobado.gif' ></td></th>
			<th><td><img src='../estilos/imagenes/estatus/stop_round.png' ></td></th>
			<th><td><img src='../estilos/imagenes/estatus/entregado.gif' ></td></th>
			<th><td><img src='../estilos/imagenes/estatus/page_edit.png'></td></th>-->
			<th><td><img src='../estilos/imagenes/estatus/script_add.png'></td></th>
			<th><td><img src='../estilos/imagenes/estatus/eliminado.gif'></td></th>
			<th><td><img src='../estilos/imagenes/estatus/buscar.gif'></td></th>
			<th><td><img src='../estilos/imagenes/estatus/script.png'></td></th></tr>
			<tr align=\"center\">
			<!--<th><td>Por Validar &nbsp;</td></th>
			<th><td>Validada &nbsp;</td></th>
			<th><td>Rechazada &nbsp;</td></th>
			<th><td>Enviado a las<br>Areas Operativas &nbsp;</td></th>
			<th><td>Modificado por<br> las Áreas Operativas&nbsp;</td></th>-->
			<th><td>Asignar Recursos &nbsp;</td></th>
			<th><td>Anular Pauta &nbsp;</td></th>
			<th><td>Observar detalles &nbsp;</td></th>
			<th><td>Informe en<br> Proceso</td></th></tr>
			</table>"; 
			break;
			case 3:
			$leyenda = "<table class='tabla'>
			<tr><th class='titulo' colspan='5' align=\"center\">LEYENDA</th></tr>   
			<tr align=\"center\"><th><td><img src='../estilos/imagenes/estatus/buscar.gif' ></td></th>
			<tr align=\"center\"><th><td>Mostrar Detalles</td></th></tr>
			</table>";
			break;
			case 4:
			$leyenda = "<table class='tabla'>
			<tr><th class='titulo' colspan='10' align=\"center\">LEYENDA</th></tr>
			<tr align=\"center\"><th><td><img src='../estilos/imagenes/estatus/eliminado.gif' ></td></th>
			<th><td><img src='../estilos/imagenes/estatus/buscar.gif'></th></td>
			<th><td><img src='../estilos/imagenes/estatus/stop_round.png'></th></td>
			<th><td><img src='../estilos/imagenes/estatus/nuevo.gif'></td></th>
			<th><td><img src='../estilos/imagenes/estatus/page_edit.png'></td></th>
			</tr>
			<tr align=\"center\"><th><td>Anular Pauta &nbsp;</td></th>
			<th><td>Mostrar Detalles &nbsp;</td></th>
			<th><td>Suspender&nbsp;</td></th>
			<th><td>Agregar Recursos</td></th>
			<th><td>Modificar</td></th>
			</tr>
			</table>"; 
			break;
			case 5:
			$leyenda = "<table class='tabla'>
			<tr><th class='titulo' colspan='6' align=\"center\">LEYENDA</th></tr>       
			<tr align=\"center\"><th><td><img src='../estilos/imagenes/estatus/por-entregar.gif' ></td></th>
			<th><td><img src='../estilos/imagenes/estatus/aprobado.gif' ></td></th>
			<th><td><img src='../estilos/imagenes/estatus/buscar.gif' ></td></th></tr>
			<tr align=\"center\"><th><td>Por confirmar &nbsp;</td></th>
			<th><td>Pauta Aprobada</td></th>
			<th><td>Modificada por las<br> Áreas Operativas</td></th>
			</tr></table>";
			break;
			case 6:
			$leyenda = "<table class='tabla'>
			<tr><th class='titulo' colspan='6' align=\"center\">LEYENDA</th></tr>       
			<tr align=\"center\"><th><td><img src='../estilos/imagenes/estatus/por-entregar.gif' ></td></th>
			<th><td><img src='../estilos/imagenes/estatus/aprobado.gif' ></td></th>
			<th><td><img src='../estilos/imagenes/estatus/buscar.gif' ></td></th></tr>
			<tr align=\"center\"><th><td>Por confirmar &nbsp;</td></th>
			<th><td>Pauta Aprobada</td></th>
			<th><td>Modificada por las<br> Áreas Operativas</td></th>
			</tr></table>"; 
			break;
			case 7:
			$leyenda = "<table class='tabla'>
			<tr><th class='titulo' colspan='6' align=\"center\">LEYENDA</th></tr>       
			<tr align=\"center\"><th><td><img src='../estilos/imagenes/estatus/pdf.png' ></td></th></tr>
			<tr align=\"center\">
			<th><td>Imprimir PDF</td></th>
			</tr></table>"; 
			break;
			case 8:
			$leyenda = "<table class='tabla'>
			<tr><th class='titulo' colspan='6' align=\"center\">LEYENDA</th></tr>       
			<tr align=\"center\"><th><td><img src='../estilos/imagenes/estatus/eliminado.gif' ></td></th></tr>
			<tr align=\"center\">
			<th><td>Eliminar</td></th>
			</tr></table>"; 
			break;
		}
		return $leyenda;
	}
/*
	function listarecursosdisponibles() {
		$todas = $this->ObjConsulta->select_servicios2($this->conect_sistemas_vtv, "1"); //presentacion para dummies
		for ($i = 1; $i <= count($todas); $i++) {
			$fila[] = Array("id" => $todas[$i][1], "valor" => utf8_encode($todas[$i][2]));
		}
		echo json_encode($fila);
	}

	function recursosdragandrog() {
		if ($_POST['idtipomaterial'] != "undefined") {
			$ids = explode(" | ", $_POST['idtipomaterial']); //puede contener valores duplicados
			$array_valores_unicos = array_keys(array_count_values($ids));
			for ($tm = 0; $tm < count($array_valores_unicos); $tm++) {
				$recursos = $this->ObjConsulta->get_recursos_sys_inventario($this->conect_sistemas_vtv, null, null, $array_valores_unicos[$tm], null, null, 1);
				unset($datospersonal);
				for ($i = 1; $i <= count($recursos); $i++) {
					$datospersonalbruto = explode(" | ", $recursos[$i][5]);
					for ($extract = 0; $extract < count($datospersonalbruto); $extract++) {
						list($llave, $valor) = explode(":", $datospersonalbruto[$extract]);
						$llave = str_replace(" ", "_", $llave);
						$datospersonal[utf8_encode($llave)] = $valor;
					}
					$fila[] = Array("1" => utf8_encode($recursos[$i][1]), "2" => "Cédula: " . $datospersonal["Cédula"] . " <br>" . utf8_encode($datospersonal["Primer_Apellido"]) . " " . utf8_encode($datospersonal["Primer_nombre"]) . "<br>" . utf8_encode($recursos[$i][4]), "3" => utf8_encode($datospersonal["Cédula"]), "4" => $array_valores_unicos[$tm]);
				}
			}
			echo json_encode($fila);
		} else {
			echo "null";
		}
	}

	function guardardrop() {
		//preguntar si existe un valor 
		//obtenerFuncion=guardardrop&idmaterialgeneral=6554&idpauta=10&idtmat=177
		//selecciona una pauta que tenga un idrecurso=idtmat y un id_recurso_asignado=1
		$existenoasignado = $this->ObjConsulta->selectlistmatnoasig($this->conect_sistemas_vtv, $_POST['idpauta'], $_POST['idtmat']);
		//si tiene un requerimiento
		if (count($existenoasignado) > 0) {
			//guardar el idmaterial en el requerimiento..
			$guardar_req = $this->ObjConsulta->updatedelistaasignados($this->conect_sistemas_vtv, $existenoasignado[1][1], $_POST['idmaterialgeneral']);
			$act_estatus = $this->ObjConsulta->updateexpestatusrecursos($this->conect_sistemas_vtv, $existenoasignado[1][1]);
			$act_estatus = $this->ObjConsulta->insertestatusrecurso($this->conect_sistemas_vtv, $_POST['idpauta'], $existenoasignado[1][1], '14', $_SESSION['cedula'], $_POST['idmaterialgeneral']);
		} else {
			$idrequerimiento = $this->ObjConsulta->insertmaterial($this->conect_sistemas_vtv, $_POST['idtmat'], $_POST['idpauta'], $_SESSION['cedula'], $_POST['idmaterialgeneral']); //material en pauta_detalle_servicio
			$act_estatus = $this->ObjConsulta->insertestatusrecurso($this->conect_sistemas_vtv, $_POST['idpauta'], $idrequerimiento[0], '14', $_SESSION['cedula'], $_POST['idmaterialgeneral']); // estado del material en 
		}
		//solo UAL
		$selectEstatusPauta=$this->ObjConsulta->selectestatus($this->conect_sistemas_vtv, $_POST['idpauta']);
		$datosobs=$this->ObjConsulta->selectobsrech($this->conect_sistemas_vtv, $_GET['id_pauta']);
		if($selectEstatusPauta[1][2]!="7"){
			$pautaestatusexp=$this->ObjConsulta->updateexpestatus($this->conect_sistemas_vtv, $_POST['idpauta']);
			$pautaestatus=$this->ObjConsulta->insertestatusnuevo($this->conect_sistemas_vtv, $_POST['idpauta'], "7", $_SESSION['cedula'], $datosobs[1][1]);
		}
	}

	function eliminardop() {
		$existenoasignado = $this->ObjConsulta->selectlistmatasig($this->conect_sistemas_vtv, $_POST['idpauta'], $_POST['idmaterialgeneral']);
		$act_estatus = $this->ObjConsulta->updateexpestatusrecursos($this->conect_sistemas_vtv, $existenoasignado[1][3]);
		$act_estatus = $this->ObjConsulta->insertestatusrecurso($this->conect_sistemas_vtv, $_POST['idpauta'], $existenoasignado[1][3], '16', $_SESSION['cedula'], $_POST['idmaterialgeneral']);
		$quitando_recursos = $this->ObjConsulta->desacsignar_recursos($this->conect_sistemas_vtv, $_POST['idpauta'], $_POST['idmaterialgeneral'], $_SESSION['cedula']);
		//solo UAL
		$selectEstatusPauta=$this->ObjConsulta->selectestatus($this->conect_sistemas_vtv, $_POST['idpauta']);
		$datosobs=$this->ObjConsulta->selectobsrech($this->conect_sistemas_vtv, $_GET['id_pauta']);
		if($selectEstatusPauta[1][2]!="7"){
			$pautaestatusexp=$this->ObjConsulta->updateexpestatus($this->conect_sistemas_vtv, $_POST['idpauta']);
			$pautaestatus=$this->ObjConsulta->insertestatusnuevo($this->conect_sistemas_vtv, $_POST['idpauta'], "7", $_SESSION['cedula'], $datosobs[1][1]);
		}
	}

	function select_asignados() {
		$recursos_asignados = $this->ObjConsulta->selectlistmatasig_mod2($this->conect_sistemas_vtv, $_POST['idpauta'], "1");
		for ($rc = 1; $rc <= count($recursos_asignados); $rc++) { //seleccionando cada recurso
			$datos_recurso = $this->ObjConsulta->get_info_recurso_inventario($this->conect_sistemas_vtv, $recursos_asignados[$rc][1]); //requeriendo informacion de cada recurso
			$datospersonalbruto = explode(" | ", $datos_recurso[1][5]);
			unset($datospersonal);
			for ($extract = 0; $extract < count($datospersonalbruto); $extract++) {
				list($llave, $valor) = explode(":", $datospersonalbruto[$extract]);
				$llave = str_replace(" ", "_", $llave);
				$datospersonal[utf8_encode($llave)] = $valor;
			}
			$fila[] = Array("1" => utf8_encode($datos_recurso[1][1]), "2" => "Cédula: " . $datospersonal["Cédula"] . " <br>" . utf8_encode($datospersonal["Primer_Apellido"]) . " " . utf8_encode($datospersonal["Primer_nombre"]) . "<br>" . utf8_encode($datos_recurso[1][4]), "3" => $datospersonal["Cédula"], "4" => $datos_recurso[1][7], "5" => $datos_recurso[1][7]);
		}
		echo json_encode($fila);
	}

	function listadepautasual() {
		if (isset($_GET['parametrobusqueda'])) {
			$busquedaget = $_GET['parametrobusqueda'];
		} else {
			$busquedaget = $_GET['busqueda'];
		}
		$pagina_ajax = "'listadepautasual'";
		$nombre_fn = "0";
		if (isset($_GET['minimo'])) {
			$minimo = $_GET['minimo'];
			$count = $_GET['count'];
			if ($count > 1) {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button' onClick=\"anterior(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Anterior'><img src='../estilos/imagenes/estatus/p_2.gif' style=\"border-width:none;border-style:none;\"></button>";
			} else {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button'><img src='../estilos/imagenes/estatus/p_2_2.gif'style=\"border-width:none;border-style:none;\"></button>";
			}
		} else {
			$minimo = 0;
			$count = 1;
			$boton_Ant = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name=\"button'  id=\"button\"><img src=\"../estilos/imagenes/estatus/p_2_2.gif\" style=\"border-width:none;border-style:none;\"></button>";
		}
		$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "sys_main", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = 'id_estatus in(5,6,7,8,9,10,17,21,22,25)', "NO");
		$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "sys_main", $campo_rn = "id_pauta,nombre_programa,pauta_estatus,id_estatus,descripcion", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = 'id_estatus in(5,6,7,8,9,10,17,21,22,25) order by id_pauta DESC ', $minimo);
		$num_pag = ceil(count($todas) / 10);
		for ($i = 1; $i <= $num_pag; $i++) {
			$cantidad_paginas[] = $i;
		}
		if (count($cantidad_paginas) > 0) {
			$combo = "<SELECT name=\"cantidad_paginas\" id=\"cantidad_paginas\" class='lista' style='width:50px;' onchange=\"cambiopagina(" . $pagina_ajax . "," . $nombre_fn . ");\">";
			foreach ($cantidad_paginas as $llave => $valor) {
				if ($valor == $count) {
					$selected = 'selected';
				} else {
					$selected = "";
				}
				$combo.="<option value='" . $llave . "' $selected >" . $valor . "</option>";
			}
			$combo.="</SELECT> ";
		}
		$lista.="   
		<input name='minimo' id='minimo' type='hidden' value=" . $minimo . " />
		<input name='count' id='count' type='hidden' value=" . $count . " />    
		<input name='parametrobusqueda' id='parametrobusqueda' type='hidden' value='" . $busquedaget . "'>  
		<table  class='tabla' id='tabla_corres'>
		<tr><th class='titulo' colspan='5'>DATOS</th></tr>
		<tr>    
		<th style='200'><div align=\"center\">N#</div></th>
		<th style='200'><div align=\"center\">PROGRAMA/EVENTO</div></th>
		<th><div align=\"center\">ESTATUS</div></th>
		<th><div align=\"center\">ACCIONES</div></th>
		</tr>";
		if (count($ObjListado) > 0) {
			foreach ($ObjListado as $llave2 => $valor2) {

				$resto = $llave2 % 2;
				if (($resto == 0) && ($llave2 != 0)) {
					$color = "style='background:#f1f1f1;'"; //par
				} else {
					$color = "style='background:#ffffff;'"; //impar f9f9f9
				}
				$descripcionprog = $valor2[2];
				if ($descripcionprog == '0' or $descripcionprog == '' or $descripcionprog == null) {
					$evento = $valor2[5];
				} else {
					$evento = $descripcionprog;
				}
				$id_estatus = $valor2[4];
				switch ($id_estatus) {
					case '5':
					$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Observar detalles\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarpautaconasig.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasual.php');><img src=\"../estilos/imagenes/estatus/buscar.gif\"></button>";
					$boton_mod.= "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Asignar Recursos\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classasignarrecursos.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasual.php');><img src=\"../estilos/imagenes/estatus/script_add.png\" style=\"border-width:none;border-style:none;\"></button>";
					break;
					case '7':
					$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Por Validar\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classparavalidarual.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasual.php');><img src=\"../estilos/imagenes/estatus/por-entregar.gif\" style=\"border-width:none;border-style:none;\"></button>";
					case '6':
					$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Observar detalles\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarpautaconasig.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasual.php');><img src=\"../estilos/imagenes/estatus/buscar.gif\"></button>";
					$boton_mod.= "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Asignar Recursos\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classasignarrecursos.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasual.php');><img src=\"../estilos/imagenes/estatus/script_add.png\" style=\"border-width:none;border-style:none;\"></button>";
					break;
					case '8':
					$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Validada\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classasignarrecursos.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasual.php');><img src=\"../estilos/imagenes/estatus/aprobado.gif\" style=\"border-width:none;border-style:none;\"></button>";
					break;
					case '9':
					$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Rechazada\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarpauta.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasual.php');><img src=\"../estilos/imagenes/estatus/stop_round.png\" style=\"border-width:none;border-style:none;\"></button>";
					break;
					case '10':
					$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Enviado a las Areas Operativas\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarpautaconasigual.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasual.php');><img src=\"../estilos/imagenes/estatus/entregado.gif\" style=\"border-width:none;border-style:none;\"></button>";
					break;

					case '21': {
						$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Aprobado por la Unidad de apoyo logistico\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarpautaconasig.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasual.php');><img src=\"../estilos/imagenes/estatus/buscar.gif\"></button>";
						break;
					}
					case '22': {
						$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Modificado por las areas operativas\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarpautaconasigual.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasual.php');><img src=\"../estilos/imagenes/estatus/page_edit.png\"></button>";
						break;
					}
					case '25':
					$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Informe en proceso\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarinforme.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasual.php');><img src=\"../estilos/imagenes/estatus/script.png\" style=\"border-width:none;border-style:none;\"></button>";
					break;

					case '26':
					$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Informe generado\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarinforme.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasual.php');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
					break;

					case '32':
					$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarpauta.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasual.php');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
					break;	

					default:
					$boton_mod = "";
					break;
				}
				$boton_anular = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Anular\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classerechazada.php?id_pauta=" . $valor2[1] . "&estatus=18&lista=classlistadepautasual.php');><img src=\"../estilos/imagenes/estatus/eliminado.gif\" style=\"border-width:none;border-style:none;\"></button>";
				$lista.="           
				<tr id='" . $valor2[1] . "'>
				<td $color><div align=\"left\">" . $valor2[1] . "</div></td>
				<td $color><div align=\"left\">" . utf8_encode (ucwords($evento)) . "</div></td>
				<td $color><div align=\"center\">" . ucwords($valor2[3]) . "</div></td>
				<td $color><div align=\"center\">" . $boton_mod . "&nbsp;" . $boton_anular . "</div></td>
				</tr>";
			}//foreach
		} else {
			$mensaje = "<div style='color: #FF0000;font-weight: bold;'><br>Error: Disculpe su busqueda NO arrojo ningun resultado.<div><br>";
			$lista.="<td colspan='8'><div align=\"center\">" . $mensaje . "</div></td>";
		}
		$lista.="</table>";
		if ($num_pag > $count) {
			$boton_Sig = " <button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name='button'  id=\"button\" onClick=\"siguiente(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Siguiente'><img src=\"../estilos/imagenes/estatus/p_3.gif\"  style=\"border-width:none;border-style:none;\"></button>";
		} else {
			$boton_Sig = "
			<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' name='button' id='button'><img src='../estilos/imagenes/estatus/p_3_2.gif' style=\"border-width:none;border-style:none;\"></button>";
		}
		$lista.=$this->leyenda($tipo = 2);
		$lista.="
		<table class='tabla'>
		<tr><th><div align=\"center\">" . $num_pag . "&nbsp;&nbsp;p&aacute;ginas,&nbsp;" . count($todas) . "&nbsp;&nbsp;pauta(s)</div></th>
		<th>" . $combo . "&nbsp;&nbsp;" . $boton_Ant . " &nbsp; " . $count . " / " . $num_pag . "&nbsp;" . $boton_Sig . "&nbsp;</th>
		</tr></table>";
		echo $lista;
	}

	function listadepautasadm() {
		if (isset($_GET['parametrobusqueda'])) {
			$busquedaget = $_GET['parametrobusqueda'];
		} else {
			$busquedaget = $_GET['busqueda'];
		}
		$pagina_ajax = "'listadepautasadm'";
		$nombre_fn = "0";
		if (isset($_GET['minimo'])) {
			$minimo = $_GET['minimo'];
			$count = $_GET['count'];
			if ($count > 1) {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button' onClick=\"anterior(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Anterior'><img src='../estilos/imagenes/estatus/p_2.gif' style=\"border-width:none;border-style:none;\"></button>";
			} else {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button'><img src='../estilos/imagenes/estatus/p_2_2.gif'style=\"border-width:none;border-style:none;\"></button>";
			}
		} else {
			$minimo = 0;
			$count = 1;
			$boton_Ant = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name=\"button'  id=\"button\"><img src=\"../estilos/imagenes/estatus/p_2_2.gif\" style=\"border-width:none;border-style:none;\"></button>";
		}
		if ($_SESSION['id_tipo_usuario'] == 27) {
			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia", $campo_rn = "id_pauta,nombre_programa,pauta_estatus,id_estatus", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "", $minimo);
		} else {
			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "'", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia", $campo_rn = "id_pauta,nombre_programa,pauta_estatus,id_estatus", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "' order by id_pauta DESC", $minimo);
		}
		$num_pag = ceil(count($todas) / 10);
		for ($i = 1; $i <= $num_pag; $i++) {
			$cantidad_paginas[] = $i;
		}
		if (count($cantidad_paginas) > 0) {
			$combo = "<SELECT name=\"cantidad_paginas\" id=\"cantidad_paginas\" class='lista' style='width:50px;' onchange=\"cambiopagina(" . $pagina_ajax . "," . $nombre_fn . ");\">";
			foreach ($cantidad_paginas as $llave => $valor) {
				if ($valor == $count) {
					$selected = 'selected';
				} else {
					$selected = "";
				}
				$combo.="<option value='" . $llave . "' $selected >" . $valor . "</option>";
			}
			$combo.="</SELECT> ";
		}
		$lista.="   
		<input name='minimo' id='minimo' type='hidden' value=" . $minimo . " />
		<input name='count' id='count' type='hidden' value=" . $count . " />    
		<input name='parametrobusqueda' id='parametrobusqueda' type='hidden' value='" . $busquedaget . "'>  
		<table  class='tabla' id='tabla_corres'>
		<tr><th class='titulo' colspan='5'>DATOS</th></tr>
		<tr>    
		<th style='200'><div align=\"center\">N# DE PAUTA</div></th>
		<th style='200'><div align=\"center\">PROGRAMA/EVENTO</div></th>
		<th><div align=\"center\">ESTATUS</div></th>
		<th><div align=\"center\">ACCIONES</div></th>
		</tr>";
		if (count($ObjListado) > 0) {
			foreach ($ObjListado as $llave2 => $valor2) {

				$resto = $llave2 % 2;
				if (($resto == 0) && ($llave2 != 0)) {
					$color = "style='background:#f1f1f1;'"; //par
				} else {
					$color = "style='background:#ffffff;'"; //impar f9f9f9
				}
				$descripcionprog = $valor2[2];
				if ($descripcionprog != '') {
					$evento = $descripcionprog;
				}
				$id_estatus = $valor2[4];
				switch ($id_estatus) {
					case '2': {
						$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarpauta.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasadm.php');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
					}
					break;
					case '7': {
						$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarpautamod.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasadm.php');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
					}
					break;
					case '9': {
						$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarpauta.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasadm.php');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
					}
					break;
					case '10': {
						$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarpautaconasig.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasadm.php');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
					}
					break;
					case '18': {
						$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarpauta.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasadm.php');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
					}
					break;
					case '22': {
						$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarpautaconasig.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasadm.php');><img src=\"../estilos/imagenes/estatus/buscar.gif\"></button>";
						break;
					}
					case '24':
					$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarpauta.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasadm.php');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
					break;
					case '25':
					$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarinforme.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasadm.php');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
					break;
					case '26':
					$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarinforme.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasadm.php');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
					break;
					case '32':
					$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarpauta.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasadm.php');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
					break;
					default:
					$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarpauta.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasadm.php');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
					break;
				}
				$lista.="           
				<tr id='" . $valor2[1] . "'>
				<td $color><div align=\"left\">" . $valor2[1] . "</div></td>
				<td $color><div align=\"left\">" . utf8_encode (ucwords($evento)) . "</div></td>
				<td $color><div align=\"center\">" . ucwords($valor2[3]) . "</div></td>
				<td $color><div align=\"center\">" . $boton_mod . "</div></td>
				</tr>";
			}
		} else {
			$mensaje = "<div style='color: #FF0000;font-weight: bold;'><br>Error: Disculpe su busqueda NO arrojo ningun resultado.<div><br>";
			$lista.="<td colspan='8'><div align=\"center\">" . $mensaje . "</div></td>";
		}
		$lista.="</table>";
		if ($num_pag > $count) {
			$boton_Sig = " <button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name='button'  id=\"button\" onClick=\"siguiente(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Siguiente'><img src=\"../estilos/imagenes/estatus/p_3.gif\"  style=\"border-width:none;border-style:none;\"></button>";
		} else {
			$boton_Sig = "
			<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' name='button' id='button'><img src='../estilos/imagenes/estatus/p_3_2.gif' style=\"border-width:none;border-style:none;\"></button>";
		}
		$lista.=$this->leyenda($tipo = 3);
		$lista.="
		<table class='tabla'>
		<tr><th><div align=\"center\">" . $num_pag . "&nbsp;&nbsp;p&aacute;ginas,&nbsp;" . count($todas) . "&nbsp;&nbsp;pauta(s)</div></th>
		<th>" . $combo . "&nbsp;&nbsp;" . $boton_Ant . " &nbsp; " . $count . " / " . $num_pag . "&nbsp;" . $boton_Sig . "&nbsp;</th>
		</tr></table>";
		echo $lista;
	}

	function listadepautasger() {
		if (isset($_GET['parametrobusqueda'])) {
			$busquedaget = $_GET['parametrobusqueda'];
		} else {
			$busquedaget = $_GET['busqueda'];
		}
		$pagina_ajax = "'listadepautasger'";
		$nombre_fn = "0";
		if (isset($_GET['minimo'])) {
			$minimo = $_GET['minimo'];
			$count = $_GET['count'];
			if ($count > 1) {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button' onClick=\"anterior(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Anterior'><img src='../estilos/imagenes/estatus/p_2.gif' style=\"border-width:none;border-style:none;\"></button>";
			} else {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button'><img src='../estilos/imagenes/estatus/p_2_2.gif'style=\"border-width:none;border-style:none;\"></button>";
			}
		} else {
			$minimo = 0;
			$count = 1;
			$boton_Ant = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name=\"button'  id=\"button\"><img src=\"../estilos/imagenes/estatus/p_2_2.gif\" style=\"border-width:none;border-style:none;\"></button>";
		}
		$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "'", "NO");
		$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia", $campo_rn = "id_pauta,nombre_programa,pauta_estatus,id_estatus", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "' order by id_pauta DESC", $minimo);
		$num_pag = ceil(count($todas) / 10);
		for ($i = 1; $i <= $num_pag; $i++) {
			$cantidad_paginas[] = $i;
		}
		if (count($cantidad_paginas) > 0) {
			$combo = "<SELECT name=\"cantidad_paginas\" id=\"cantidad_paginas\" class='lista' style='width:50px;' onchange=\"cambiopagina(" . $pagina_ajax . "," . $nombre_fn . ");\">";
			foreach ($cantidad_paginas as $llave => $valor) {
				if ($valor == $count) {
					$selected = 'selected';
				} else {
					$selected = "";
				}
				$combo.="<option value='" . $llave . "' $selected >" . $valor . "</option>";
			}
			$combo.="</SELECT> ";
		}
		$lista.="   
		<input name='minimo' id='minimo' type='hidden' value=" . $minimo . " />
		<input name='count' id='count' type='hidden' value=" . $count . " />    
		<input name='parametrobusqueda' id='parametrobusqueda' type='hidden' value='" . $busquedaget . "'>  
		<table  class='tabla' id='tabla_corres'>
		<tr><th class='titulo' colspan='5'>DATOS</th></tr>
		<tr>    
		<th style='200'><div align=\"center\">N&deg; DE PAUTA</div></th>
		<th style='200'><div align=\"center\">PROGRAMA/EVENTO</div></th>
		<th><div align=\"center\">ESTATUS</div></th>
		</tr>";
		if (count($ObjListado) > 0) {
			foreach ($ObjListado as $llave2 => $valor2) {
				$resto = $llave2 % 2;
				if (($resto == 0) && ($llave2 != 0)) {
					$color = "style='background:#f1f1f1;'"; //par
				} else {
					$color = "style='background:#ffffff;'"; //impar f9f9f9
				}
				$descripcionprog = $valor2[2];
				$descripcioneven = $valor2[3];
				if ($descripcionprog == '0') {
					$evento = $descripcioneven;
				}
				if ($descripcioneven == '0') {
					$evento = $descripcionprog;
				}
				$id_estatus = $valor2[5];
				switch ($id_estatus) {
					case '2': {
						$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classparavalidar.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasger.php');><img src=\"../estilos/imagenes/estatus/por-entregar.gif\" style=\"border-width:none;border-style:none;\"></button>";
					}
					break;
					case '7': {
						$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarpautamod.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasger.php');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
					}
					break;
					case '9': {
						$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarpauta.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasger.php');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
					}
					break;
					case '10': {
						$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarpautaconasig.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasger.php');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
					}
					break;
					case '18': {
						$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarpauta.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasger.php');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
					}
					break;
					case '21': {
						$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Aprobado por la Unidad de apoyo logistico\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarpautaconasig.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasger.php');><img src=\"../estilos/imagenes/estatus/buscar.gif\"></button>";
						break;
					}
					case '22': {
						$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Modificado por las áreas operativas\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarpautaconasig.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasger.php');><img src=\"../estilos/imagenes/estatus/buscar.gif\"></button>";
						break;
					}
					case '24':
					$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Rechazado por la Gerencia\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarpauta.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasger.php');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
					break;
					case '25':
					$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Informe en Proceso\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarinforme.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasger.php');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
					break;
					case '26':
					$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Informe Generado\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarinforme.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasger.php');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
					break;
					case '32': {
						$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarpauta.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasger.php');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
					}
					break;	
					default:
					$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarpauta.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasger.php');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
					break;
				}
				$lista.="           
				<tr id='" . $valor2[1] . "'>
				<td $color><div align=\"left\">" . $valor2[1] . "</div></td>
				<td $color><div align=\"left\">" . utf8_encode (ucwords($evento)) . "</div></td>
				<td $color><div align=\"center\">" . ucwords($valor2[4]) . "</div></td>
				<td $color><div align=\"center\">" . $boton_mod . "</div></td>
				</tr>";
			}
		} else {
			$mensaje = "<div style='color: #FF0000;font-weight: bold;'><br>Error: Disculpe su busqueda NO arrojo ningun resultado.<div><br>";
			$lista.="<td colspan='8'><div align=\"center\">" . $mensaje . "</div></td>";
		}
		$lista.="</table>";
		if ($num_pag > $count) {
			$boton_Sig = " <button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name='button'  id=\"button\" onClick=\"siguiente(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Siguiente'><img src=\"../estilos/imagenes/estatus/p_3.gif\"  style=\"border-width:none;border-style:none;\"></button>";
		} else {
			$boton_Sig = "
			<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' name='button' id='button'><img src='../estilos/imagenes/estatus/p_3_2.gif' style=\"border-width:none;border-style:none;\"></button>";
		}

		$lista.=$this->leyenda($tipo = 4);
		$lista.="
		<table class='tabla'>
		<tr><th><div align=\"center\">" . $num_pag . "&nbsp;&nbsp;p&aacute;ginas,&nbsp;" . count($todas) . "&nbsp;&nbsp;pauta(s)</div></th>
		<th>" . $combo . "&nbsp;&nbsp;" . $boton_Ant . " &nbsp; " . $count . " / " . $num_pag . "&nbsp;" . $boton_Sig . "&nbsp;</th>
		</tr></table>";
		echo $lista;
	}

	function listadepautasjarea() {
		//print_r($_GET);   
		if (isset($_GET['parametrobusqueda'])) {
			$busquedaget = $_GET['parametrobusqueda'];
		} else {
			$busquedaget = $_GET['busqueda'];
		}
		$pagina_ajax = "'listadepautasjarea'";
		$nombre_fn = "0";
		if (isset($_GET['minimo'])) {
			$minimo = $_GET['minimo'];
			$count = $_GET['count'];
			if ($count > 1) {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button' onClick=\"anterior(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Anterior'><img src='../estilos/imagenes/estatus/p_2.gif' style=\"border-width:none;border-style:none;\"></button>";
			} else {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button'><img src='../estilos/imagenes/estatus/p_2_2.gif'style=\"border-width:none;border-style:none;\"></button>";
			}
		} else {
			$minimo = 0;
			$count = 1;
			$boton_Ant = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name=\"button'  id=\"button\"><img src=\"../estilos/imagenes/estatus/p_2_2.gif\" style=\"border-width:none;border-style:none;\"></button>";
		}
		$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "' and iddivision='{$_SESSION['division']}'", "NO");
		$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia", $campo_rn = "id_pauta,nombre_programa,pauta_estatus,id_estatus,descripcion,productor", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "' and iddivision='{$_SESSION['division']}' order by id_pauta DESC ", $minimo);
		$num_pag = ceil(count($todas) / 10);
		for ($i = 1; $i <= $num_pag; $i++) {
			$cantidad_paginas[] = $i;
		}
		if (count($cantidad_paginas) > 0) {
			$combo = "<SELECT name=\"cantidad_paginas\" id=\"cantidad_paginas\" class='lista' style='width:50px;' onchange=\"cambiopagina(" . $pagina_ajax . "," . $nombre_fn . ");\">";
			foreach ($cantidad_paginas as $llave => $valor) {
				if ($valor == $count) {
					$selected = 'selected';
				} else {
					$selected = "";
				}
				$combo.="<option value='" . $llave . "' $selected >" . $valor . "</option>";
			}
			$combo.="</SELECT> ";
		}
		$lista.="   
		<input name='minimo' id='minimo' type='hidden' value=" . $minimo . " />
		<input name='count' id='count' type='hidden' value=" . $count . " />    
		<input name='parametrobusqueda' id='parametrobusqueda' type='hidden' value='" . $busquedaget . "'>  
		<table  class='tabla' id='tabla_corres'>
		<tr><th class='titulo' colspan='4'>DATOS</th></tr>
		<tr>    
		<th style='200'><div align=\"center\">N&deg; DE PAUTA</div></th>
		<th style='200'><div align=\"center\">PROGRAMA/EVENTO</div></th>
		<th><div align=\"center\">ESTATUS</div></th>
		<th><div align=\"center\">ACCIONES</div></th>
		</tr>";
		if (count($ObjListado) > 0) {
			foreach ($ObjListado as $llave2 => $valor2) {
				$boton_mod = "";
				$resto = $llave2 % 2;
				if (($resto == 0) && ($llave2 != 0)) {
					$color = "style='background:#f1f1f1;'"; //par
				} else {
					$color = "style='background:#ffffff;'"; //impar f9f9f9
				}
				$evento = $valor2[2] . " " . $valor2[5];
				$id_estatus = $valor2[4];
				switch ($id_estatus) {
					case '1':
					if ($valor2[6] == $_SESSION['cedula']) {
						$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarpauta.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasjarea.php');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
						$boton_mod.= "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Agregar recursos\"  name=\"button'  id=\"button\" OnClick=CancelarRegresar('classsolicitarservicios.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasjarea.php&pagina=2');><img src=\"../estilos/imagenes/estatus/nuevo.gif\" style=\"border-width:none;border-style:none;\"></button>";
						$boton_mod.= "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Anular\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classerechazada.php?id_pauta=" . $valor2[1] . "&estatus=18&lista=classlistadepautasjarea.php');><img src=\"../estilos/imagenes/estatus/eliminado.gif\" style=\"border-width:none;border-style:none;\"></button>";
					} else {
						$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarpauta.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasjarea.php');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
					}
					break;
					case '3':
					$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarpauta.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasjarea.php');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
					$boton_mod.= "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Agregar recursos\"  name=\"button'  id=\"button\" OnClick=CancelarRegresar('classsolicitarservicios.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasjarea.php');><img src=\"../estilos/imagenes/estatus/nuevo.gif\" style=\"border-width:none;border-style:none;\"></button>";
					$boton_mod.= "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Anular\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classerechazada.php?id_pauta=" . $valor2[1] . "&estatus=18&lista=classlistadepautasjarea.php');><img src=\"../estilos/imagenes/estatus/eliminado.gif\" style=\"border-width:none;border-style:none;\"></button>";
					break;
					case '34':
					$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarpauta.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasjarea.php');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
					$boton_mod.= "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Agregar recursos\"  name=\"button'  id=\"button\" OnClick=CancelarRegresar('classsolicitarservicios.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasjarea.php&mod=1');><img src=\"../estilos/imagenes/estatus/nuevo.gif\" style=\"border-width:none;border-style:none;\"></button>";
					$boton_mod.= "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Anular\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classerechazada.php?id_pauta=" . $valor2[1] . "&estatus=18&lista=classlistadepautasjarea.php');><img src=\"../estilos/imagenes/estatus/eliminado.gif\" style=\"border-width:none;border-style:none;\"></button>";
					break;
					case '2':
					$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Agregar recursos\"  name=\"button'  id=\"button\" OnClick=CancelarRegresar('classsolicitarservicios.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasjarea.php');><img src=\"../estilos/imagenes/estatus/nuevo.gif\" style=\"border-width:none;border-style:none;\"></button>";
					$boton_mod .= "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classparavalidar.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasjarea.php');><img src=\"../estilos/imagenes/estatus/por-entregar.gif\" style=\"border-width:none;border-style:none;\"></button>";
					break;
					case '7':
					$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarpautamod.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasjarea.php');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
					
					break;
					case '9':
					$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarpauta.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasjarea.php');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";

					break;
					case '10':
					$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarpautaconasig.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasjarea.php');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
					break;
					case '18':
					$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarpauta.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasjarea.php');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
					break;
					case '21':
					$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Aprobado por la Unidad de apoyo logistico\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarpautaconasig.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasjarea.php');><img src=\"../estilos/imagenes/estatus/buscar.gif\"></button>";
					break;

					case '22':
					$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Modificado por las áreas operativas\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarpautaconasig.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasjarea.php');><img src=\"../estilos/imagenes/estatus/buscar.gif\"></button>";
					break;
					case '24':
					$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalle\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarpauta.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasjarea.php');><img src=\"../estilos/imagenes/estatus/buscar.gif\"></button>";
					break;
					case '26':
					$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Informe Generado\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarinforme.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasjarea.php');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
					break;
					case '25':
					$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Enviado a las Areas Operativas\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarinforme.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasjareaphp');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
					break;
					case '32':
					$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarpauta.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasjarea.php');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
					break;	
					default:
					$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarpauta.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasjarea.php');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
					break;
				}

				if (($valor2[6] == $_SESSION['cedula']) && ($id_estatus == 1)) {
					$boton_mod.="<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Modificar pauta\"  name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmodificarpauta.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasjarea.php');><img src=\"../estilos/imagenes/estatus/page_edit.png\" style=\"border-width:none;border-style:none;\"></button>";
				}
				$id_estatus = $valor2[4];
				if (($id_estatus == 1) && ($valor2[6] != $_SESSION['cedula'])) {
					$elab_por = " POR INFORMAR";
				} else {
					$elab_por = "";
				}
				$lista.="           
				<tr id='" . $valor2[1] . "'>
				<td $color><div align=\"left\">" . $valor2[1] . "</div></td>
				<td $color><div align=\"left\">" . utf8_encode (ucwords($evento)) . "</div></td>
				<td $color><div align=\"center\">" . ucwords($valor2[3] . $elab_por) . "</div></td>
				<td $color><div align=\"center\">" . $boton_mod . "  </div></td>
				</tr>";
			}
		} else {
			$mensaje = "<div style='color: #FF0000;font-weight: bold;'><br>Error: Disculpe su busqueda NO arrojo ningun resultado.<div><br>";
			$lista.="<td colspan='8'><div align=\"center\">" . $mensaje . "</div></td>";
		}
		$lista.="</table>";
		if ($num_pag > $count) {
			$boton_Sig = " <button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name='button'  id=\"button\" onClick=\"siguiente(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Siguiente'><img src=\"../estilos/imagenes/estatus/p_3.gif\"  style=\"border-width:none;border-style:none;\"></button>";
		} else {
			$boton_Sig = "
			<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' name='button' id='button'><img src='../estilos/imagenes/estatus/p_3_2.gif' style=\"border-width:none;border-style:none;\"></button>";
		}
		$lista.=$this->leyenda($tipo = 4);
		$lista.="
		<table class='tabla'>
		<tr><th><div align=\"center\">" . $num_pag . "&nbsp;&nbsp;p&aacute;ginas,&nbsp;" . count($todas) . "&nbsp;&nbsp;pauta(s)</div></th>
		<th>" . $combo . "&nbsp;&nbsp;" . $boton_Ant . " &nbsp; " . $count . " / " . $num_pag . "&nbsp;" . $boton_Sig . "&nbsp;</th>
		</tr></table>";
		echo $lista;
	}

	function listadepautasao() {
		if (isset($_GET['parametrobusqueda'])) {
			$busquedaget = $_GET['parametrobusqueda'];
		} else {
			$busquedaget = $_GET['busqueda'];
		}
		$pagina_ajax = "'listadepautasao'";
		$nombre_fn = "0";
		if (isset($_GET['minimo'])) {
			$minimo = $_GET['minimo'];
			$count = $_GET['count'];
			if ($count > 1) {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button' onClick=\"anterior(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Anterior'><img src='../estilos/imagenes/estatus/p_2.gif' style=\"border-width:none;border-style:none;\"></button>";
			} else {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button'><img src='../estilos/imagenes/estatus/p_2_2.gif'style=\"border-width:none;border-style:none;\"></button>";
			}
		} else {
			$minimo = 0;
			$count = 1;
			$boton_Ant = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name=\"button'  id=\"button\"><img src=\"../estilos/imagenes/estatus/p_2_2.gif\" style=\"border-width:none;border-style:none;\"></button>";
		}
		$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_ao", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_estatus in(10,21,22) and id_gerencia='" . $_SESSION['gerencia'] . "'", "NO");
		$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_ao", $campo_rn = "id_pauta, descripcion,nombre_programa,pauta_estatus,id_estatus", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_estatus in(10,21,22) and id_gerencia='" . $_SESSION['gerencia'] . "' order by id_pauta DESC", $minimo);
		$num_pag = ceil(count($todas) / 10);
		for ($i = 1; $i <= $num_pag; $i++) {
			$cantidad_paginas[] = $i;
		}
		if (count($cantidad_paginas) > 0) {
			$combo = "<SELECT name=\"cantidad_paginas\" id=\"cantidad_paginas\" class='lista' style='width:50px;' onchange=\"cambiopagina(" . $pagina_ajax . "," . $nombre_fn . ");\">";
			foreach ($cantidad_paginas as $llave => $valor) {
				if ($valor == $count) {
					$selected = 'selected';
				} else {
					$selected = "";
				}
				$combo.="<option value='" . $llave . "' $selected >" . $valor . "</option>";
			}
			$combo.="</SELECT> ";
		}

		$lista.="   
		<input name='minimo' id='minimo' type='hidden' value=" . $minimo . " />
		<input name='count' id='count' type='hidden' value=" . $count . " />    
		<input name='parametrobusqueda' id='parametrobusqueda' type='hidden' value='" . $busquedaget . "'>  
		<table  class='tabla' id='tabla_corres'>
		<tr><th class='titulo' colspan='5'>DATOS</th></tr>
		<tr>    
		<th style='200'><div align=\"center\">N# DE PAUTA</div></th>
		<th style='200'><div align=\"center\">PROGRAMA/EVENTO</div></th>
		<th><div align=\"center\">ESTATUS</div></th>
		</tr>";
		if (count($ObjListado) > 0) {
			foreach ($ObjListado as $llave2 => $valor2) {
				$resto = $llave2 % 2;
				if (($resto == 0) && ($llave2 != 0)) {
					$color = "style='background:#f1f1f1;'"; //par
				} else {
					$color = "style='background:#ffffff;'"; //impar f9f9f9
				}
				$descripcionprog = $valor2[3];
				$descripcioneven = $valor2[2];
				if ($descripcionprog != '0' and $descripcionprog != '') {
					$evento = $descripcionprog;
				} else if ($descripcioneven != '0' and $descripcioneven != '') {
					$evento = $descripcioneven;
				}
				$id_estatus = $valor2[5];
				switch ($id_estatus) {
					//enviado a las areas operativas
					case '10':
					$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Por confirmar\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarpautaconasigao.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasao.php');><img src=\"../estilos/imagenes/estatus/por-entregar.gif\" ></button>";
					break;
					//aprobada por la unidad de apoyo logistico
					case '21':
					$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Pauta Aprobada\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarpautaconasigporao.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasao.php');><img src=\"../estilos/imagenes/estatus/aprobado.gif\"></button>";
					break;

					//modificado por las ao
					case '22':
					$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Modificada por las Áreas Operativas\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarpautaconasigao.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasao.php');><img src=\"../estilos/imagenes/estatus/buscar.gif\"></button>";
					break;
					case '32':
					$boton_mod = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" title=\"Mostrar Detalles\" name=\"button'  id=\"button\" OnClick=CancelarRegresar('classmostrarpauta.php?id_pauta=" . $valor2[1] . "&lista=classlistadepautasao.php');><img src=\"../estilos/imagenes/estatus/buscar.gif\" style=\"border-width:none;border-style:none;\"></button>";
					break;	
					default:
					$boton_mod = "";
					break;
				}
				$lista.="           
				<tr id='" . $valor2[1] . "'>
				<td $color><div align=\"left\">" . $valor2[1] . "</div></td>
				<td $color><div align=\"left\">" . utf8_encode(ucwords($evento)) . "</div></td>
				<td $color><div align=\"center\">" . ucwords($valor2[4]) . "</div></td>
				<td $color><div align=\"center\">" . $boton_mod . "</div></td>
				</tr>";
			}
		} else {
			$mensaje = "<div style='color: #FF0000;font-weight: bold;'><br>Error: Disculpe su busqueda NO arrojo ningun resultado.<div><br>";
			$lista.="<td colspan='8'><div align=\"center\">" . $mensaje . "</div></td>";
		}
		$lista.="</table>";
		if ($num_pag > $count) {
			$boton_Sig = " <button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name='button'  id=\"button\" onClick=\"siguiente(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Siguiente'><img src=\"../estilos/imagenes/estatus/p_3.gif\"  style=\"border-width:none;border-style:none;\"></button>";
		} else {
			$boton_Sig = "
			<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' name='button' id='button'><img src='../estilos/imagenes/estatus/p_3_2.gif' style=\"border-width:none;border-style:none;\"></button>";
		}
		$lista.=$this->leyenda($tipo = 5);
		$lista.="
		<table class='tabla'>
		<tr><th><div align=\"center\">" . $num_pag . "&nbsp;&nbsp;p&aacute;ginas,&nbsp;" . count($todas) . "&nbsp;&nbsp;pauta(s)</div></th>
		<th>" . $combo . "&nbsp;&nbsp;" . $boton_Ant . " &nbsp; " . $count . " / " . $num_pag . "&nbsp;" . $boton_Sig . "&nbsp;</th>
		</tr></table>";
		echo $lista;
	}

	function listadeinformes() {
		if (isset($_GET['parametrobusqueda'])) {
			$busquedaget = $_GET['parametrobusqueda'];
		} else {
			$busquedaget = $_GET['busqueda'];
		}
		$pagina_ajax = "'listadeinformes'";
		$nombre_fn = "0";
		if (isset($_GET['minimo'])) {
			$minimo = $_GET['minimo'];
			$count = $_GET['count'];
			if ($count > 1) {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button' onClick=\"anterior(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Anterior'><img src='../estilos/imagenes/estatus/p_2.gif' style=\"border-width:none;border-style:none;\"></button>";
			} else {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button'><img src='../estilos/imagenes/estatus/p_2_2.gif'style=\"border-width:none;border-style:none;\"></button>";
			}
		} else {
			$minimo = 0;
			$count = 1;
			$boton_Ant = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name=\"button'  id=\"button\"><img src=\"../estilos/imagenes/estatus/p_2_2.gif\" style=\"border-width:none;border-style:none;\"></button>";
		}

		if ($_SESSION['id_tipo_usuario'] == "21" or $_SESSION['id_tipo_usuario'] == "22") {

			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_estatus in(21,25) and id_gerencia='" . $_SESSION['gerencia'] . "'", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia", $campo_rn = "id_pauta,nombre_programa, descripcion,pauta_estatus", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_estatus in(21,25) and id_gerencia='" . $_SESSION['gerencia'] . "' order by id_pauta DESC", $minimo);
		} else {
			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_estatus in(21,25) and productor='" . $_SESSION['cedula'] . "'", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia", $campo_rn = "id_pauta,nombre_programa,descripcion,pauta_estatus", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_estatus in(21,25) and productor='" . $_SESSION['cedula'] . "' order by id_pauta DESC", $minimo);
		}

		$num_pag = ceil(count($todas) / 10);
		for ($i = 1; $i <= $num_pag; $i++) {
			$cantidad_paginas[] = $i;
		}
		if (count($cantidad_paginas) > 0) {
			$combo = "<SELECT name=\"cantidad_paginas\" id=\"cantidad_paginas\" class='lista' style='width:50px;' onchange=\"cambiopagina(" . $pagina_ajax . "," . $nombre_fn . ");\">";
			foreach ($cantidad_paginas as $llave => $valor) {
				if ($valor == $count) {
					$selected = 'selected';
				} else {
					$selected = "";
				}
				$combo.="<option value='" . $llave . "' $selected >" . $valor . "</option>";
			}
			$combo.="</SELECT> ";
		}
		$lista.="   
		<input name='minimo' id='minimo' type='hidden' value=" . $minimo . " />
		<input name='count' id='count' type='hidden' value=" . $count . " />    
		<input name='parametrobusqueda' id='parametrobusqueda' type='hidden' value='" . $busquedaget . "'>  
		<table  class='tabla' id='tabla_corres'>
		<tr><th class='titulo' colspan='6'>DATOS</th></tr>
		<tr>    
		<th style='200'><div align=\"center\">N&deg; DE PAUTA</div></th>
		<th style='200'><div align=\"center\">PROGRAMA/EVENTO</div></th>
		<th><div align=\"center\">ESTATUS</div></th>
		<th><div align=\"center\">ACCIONES</div></th>
		</tr>";
		if (count($ObjListado) > 0) {
			foreach ($ObjListado as $llave2 => $valor2) {
				$resto = $llave2 % 2;
				if (($resto == 0) && ($llave2 != 0)) {
					$color = "style='background:#f1f1f1;'"; //par
				} else {
					$color = "style='background:#ffffff;'"; //impar f9f9f9
				}
				$descripcionprog = $valor2[2];
				$descripcioneven = $valor2[3];
				if ($descripcionprog != '') {
					$evento = $descripcionprog;
				} else if ($descripcioneven != '') {
					$evento = $descripcioneven;
				}
				$boton_I = "<input type=\"button\" class='boton' value=\"Generar Informe\" OnClick=CancelarRegresar('classinformedeprod.php?id_pauta=" . $valor2[1] . "&lista=classlistadeinformes.php');>";
				$lista.="           
				<tr id='" . $valor2[1] . "'>
				<td $color><div align=\"left\">" . $valor2[1] . "</div></td>
				<td $color><div align=\"left\">" . $evento . "</div></td>
				<td $color><div align=\"center\">" . $valor2[4] . "</div></td>
				<td $color><div align=\"center\">" . $boton_I . "</div></td>
				</tr>";
			}
		} else {
			$mensaje = "<div style='color: #FF0000;font-weight: bold;'><br>Error: Disculpe su busqueda NO arrojo ningun resultado.<div><br>";
			$lista.="<td colspan='8'><div align=\"center\">" . $mensaje . "</div></td>";
		}
		$lista.="</table>";

		if ($num_pag > $count) {
			$boton_Sig = " <button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name='button'  id=\"button\" onClick=\"siguiente(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Siguiente'><img src=\"../estilos/imagenes/estatus/p_3.gif\"  style=\"border-width:none;border-style:none;\"></button>";
		} else {
			$boton_Sig = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' name='button' id='button'><img src='../estilos/imagenes/estatus/p_3_2.gif' style=\"border-width:none;border-style:none;\"></button>";
		}

		$lista.=$this->leyenda($tipo = 6);
		$lista.="
		<table class='tabla'>
		<tr><th><div align=\"center\">" . $num_pag . "&nbsp;&nbsp;p&aacute;ginas,&nbsp;" . count($todas) . "&nbsp;&nbsp;pauta(s)</div></th>
		<th>" . $combo . "&nbsp;&nbsp;" . $boton_Ant . " &nbsp; " . $count . " / " . $num_pag . "&nbsp;" . $boton_Sig . "&nbsp;</th>
		</tr></table>";
		echo $lista;
	}

	function listadereportesvalidacion() {
		//print_r($_GET);   
		if (isset($_GET['parametrobusqueda'])) {
			$busquedaget = $_GET['parametrobusqueda'];
		} else {
			$busquedaget = $_GET['busqueda'];
		}
		$pagina_ajax = "'listadereportesvalidacion'";
		$nombre_fn = "0";
		if (isset($_GET['minimo'])) {
			$minimo = $_GET['minimo'];
			$count = $_GET['count'];
			if ($count > 1) {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button' onClick=\"anterior(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Anterior'><img src='../estilos/imagenes/estatus/p_2.gif' style=\"border-width:none;border-style:none;\"></button>";
			} else {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button'><img src='../estilos/imagenes/estatus/p_2_2.gif'style=\"border-width:none;border-style:none;\"></button>";
			}
		} else {
			$minimo = 0;
			$count = 1;
			$boton_Ant = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name=\"button'  id=\"button\"><img src=\"../estilos/imagenes/estatus/p_2_2.gif\" style=\"border-width:none;border-style:none;\"></button>";
		}
		if (($_SESSION['id_tipo_usuario'] == 24) or ($_SESSION['id_tipo_usuario'] == 27) or ($_SESSION['id_tipo_usuario'] == 20)) {
			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = 'id_estatus in(5,6,7,8,9,10,21,22,24,25,26,29,32)', "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta,nombre_programa,pauta_estatus,id_estatus,descripcion", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = 'id_estatus in(5,6,7,8,9,10,21,22,24,25,26,29,32)', $minimo);
		} elseif ($_SESSION['id_tipo_usuario'] == 25) {
			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_ao_rep", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = " id_gerencia='" . $_SESSION['gerencia'] . "'", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_ao_rep", $campo_rn = "id_pauta,nombre_programa,pauta_estatus,id_estatus,descripcion", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "' order by id_pauta DESC", $minimo);
		} else if ($_SESSION['id_tipo_usuario'] == 22) {
			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = " id_gerencia='" . $_SESSION['gerencia'] . "'", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta,nombre_programa,pauta_estatus,id_estatus,descripcion", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "' order by id_pauta DESC", $minimo);
		} else {
			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "productor='" . $_SESSION['cedula'] . "'", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta,nombre_programa,pauta_estatus,id_estatus,descripcion", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "productor='" . $_SESSION['cedula'] . "' order by id_pauta DESC", $minimo);
		}
		$num_pag = ceil(count($todas) / 10);
		for ($i = 1; $i <= $num_pag; $i++) {
			$cantidad_paginas[] = $i;
		}
		if (count($cantidad_paginas) > 0) {
			$combo = "<SELECT name=\"cantidad_paginas\" id=\"cantidad_paginas\" class='lista' style='width:50px;' onchange=\"cambiopagina(" . $pagina_ajax . "," . $nombre_fn . ");\">";
			foreach ($cantidad_paginas as $llave => $valor) {
				if ($valor == $count) {
					$selected = 'selected';
				} else {
					$selected = "";
				}
				$combo.="<option value='" . $llave . "' $selected >" . $valor . "</option>";
			}
			$combo.="</SELECT> ";
		}
		$lista.="   
		<input name='minimo' id='minimo' type='hidden' value=" . $minimo . " />
		<input name='count' id='count' type='hidden' value=" . $count . " />    
		<input name='parametrobusqueda' id='parametrobusqueda' type='hidden' value='" . $busquedaget . "'>  
		<table  class='tabla' id='tabla_corres'>
		<tr><th class='titulo' colspan='6'>DATOS</th></tr>
		<tr>    
		<th style='200'><div align=\"center\">N&deg; DE PAUTA</div></th>
		<th style='200'><div align=\"center\">PROGRAMA/EVENTO</div></th>
		<th><div align=\"center\">ESTATUS</div></th>
		<th><div align=\"center\">ACCIONES</div></th>
		<!--<th><div align=\"center\">DE LA UAL</div></th>-->
		</tr>";
		if (count($ObjListado) > 0) {
			foreach ($ObjListado as $llave2 => $valor2) {
				$resto = $llave2 % 2;
				if (($resto == 0) && ($llave2 != 0)) {
					$color = "style='background:#f1f1f1;'"; //par
				} else {
					$color = "style='background:#ffffff;'"; //impar f9f9f9
				}
				$evento = $valor2[2] . " " . $valor2[5];
				$botonRg = "            
				<a href='../reportes/reporte_val_ger.php?id_pauta=" . $valor2[1] . "&lista=classlistadereportesvalidacion.php&cedula=" . $_SESSION['cedula'] . "' target='_blank'>
				<img src=\"../estilos/imagenes/estatus/pdf.png\" style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\"></a>";
				$botonRu = "            
				<a href='../reportes/reporte_val_ual.php?id_pauta=" . $valor2[1] . "&lista=classlistadereportesvalidacion.php&cedula=" . $_SESSION['cedula'] . "' target='_blank'>
				<img src=\"../estilos/imagenes/estatus/pdf.png\" style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\"></a>";
				$lista.="           
				<tr id='" . $valor2[1] . "'>
				<td $color><div align=\"left\">" . $valor2[1] . "</div></td>
				<td $color><div align=\"left\">" . ucwords($evento) . "</div></td>
				<td $color><div align=\"center\">" . ucwords($valor2[3]) . "</div></td>
				<td $color><div align=\"center\">" . $botonRg . "</div></td>
				<!--<td $color><div align=\"center\">" . $botonRu . "</div></td>-->
				</tr>";
			}
		} else {
			$mensaje = "<div style='color: #FF0000;font-weight: bold;'><br>Error: Disculpe su busqueda NO arrojo ningun resultado.<div><br>";
			$lista.="<td colspan='8'><div align=\"center\">" . $mensaje . "</div></td>";
		}
		$lista.="</table>";
		if ($num_pag > $count) {
			$boton_Sig = " <button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name='button'  id=\"button\" onClick=\"siguiente(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Siguiente'><img src=\"../estilos/imagenes/estatus/p_3.gif\"  style=\"border-width:none;border-style:none;\"></button>";
		} else {
			$boton_Sig = "
			<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' name='button' id='button'><img src='../estilos/imagenes/estatus/p_3_2.gif' style=\"border-width:none;border-style:none;\"></button>";
		}
		$lista.=$this->leyenda($tipo = 7);
		$lista.="<table class='tabla'>
		<tr><th><div align=\"center\">" . $num_pag . "&nbsp;&nbsp;p&aacute;ginas,&nbsp;" . count($todas) . "&nbsp;&nbsp;pauta(s)</div></th>
		<th>" . $combo . "&nbsp;&nbsp;" . $boton_Ant . " &nbsp; " . $count . " / " . $num_pag . "&nbsp;" . $boton_Sig . "&nbsp;</th>
		</tr></table>";
		echo $lista;
	}

	function listadereportes() {
		//print_r($_GET);   
		if (isset($_GET['parametrobusqueda'])) {
			$busquedaget = $_GET['parametrobusqueda'];
		} else {
			$busquedaget = $_GET['busqueda'];
		}
		$pagina_ajax = "'listadereportes'";
		$nombre_fn = "0";
		if (isset($_GET['minimo'])) {
			$minimo = $_GET['minimo'];
			$count = $_GET['count'];
			if ($count > 1) {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button' onClick=\"anterior(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Anterior'><img src='../estilos/imagenes/estatus/p_2.gif' style=\"border-width:none;border-style:none;\"></button>";
			} else {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button'><img src='../estilos/imagenes/estatus/p_2_2.gif'style=\"border-width:none;border-style:none;\"></button>";
			}
		} else {
			$minimo = 0;
			$count = 1;
			$boton_Ant = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name=\"button'  id=\"button\"><img src=\"../estilos/imagenes/estatus/p_2_2.gif\" style=\"border-width:none;border-style:none;\"></button>";
		}
		if (($_SESSION['id_tipo_usuario'] == 24) or ($_SESSION['id_tipo_usuario'] == 27)) {

			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta,nombre_programa,pauta_estatus,id_estatus,descripcion", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "", $minimo);
		} elseif ($_SESSION['id_tipo_usuario'] == 25) {

			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_ao_rep", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "'", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_ao_rep", $campo_rn = "id_pauta,nombre_programa,pauta_estatus,id_estatus,descripcion", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "' order by id_pauta DESC", $minimo);
		} else {

			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "'", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta,nombre_programa,pauta_estatus,id_estatus,descripcion", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "' order by id_pauta DESC", $minimo);
		}
		$num_pag = ceil(count($todas) / 10);
		for ($i = 1; $i <= $num_pag; $i++) {
			$cantidad_paginas[] = $i;
		}

		if (count($cantidad_paginas) > 0) {
			$combo = "<SELECT name=\"cantidad_paginas\" id=\"cantidad_paginas\" class='lista' style='width:50px;' onchange=\"cambiopagina(" . $pagina_ajax . "," . $nombre_fn . ");\">";
			foreach ($cantidad_paginas as $llave => $valor) {
				if ($valor == $count) {
					$selected = 'selected';
				} else {
					$selected = "";
				}
				$combo.="<option value='" . $llave . "' $selected >" . $valor . "</option>";
			}
			$combo.="</SELECT> ";
		}

		$lista.="   
		<input name='minimo' id='minimo' type='hidden' value=" . $minimo . " />
		<input name='count' id='count' type='hidden' value=" . $count . " />    
		<input name='parametrobusqueda' id='parametrobusqueda' type='hidden' value='" . $busquedaget . "'>  
		<table  class='tabla' id='tabla_corres'>
		<tr><th class='titulo' colspan='6'>DATOS</th></tr>
		<tr>    
		<th style='200'><div align=\"center\">N&deg; DE PAUTA</div></th>
		<th style='200'><div align=\"center\">PROGRAMA/EVENTO</div></th>
		<th><div align=\"center\">ESTATUS</div></th>
		<th><div align=\"center\">ACCIONES</div></th>
		
		</tr>";
		if (count($ObjListado) > 0) {
			foreach ($ObjListado as $llave2 => $valor2) {
				$resto = $llave2 % 2;
				if (($resto == 0) && ($llave2 != 0)) {
					$color = "style='background:#f1f1f1;'"; //par
				} else {
					$color = "style='background:#ffffff;'"; //impar f9f9f9
				}
				$evento = $valor2[2] . " " . $valor2[5];
				$botonRep = "           
				<a href='../reportes/reporte_mod.php?id_pauta=" . $valor2[1] . "&lista=classlistadereportes.php&cedula=" . $_SESSION['cedula'] . "' target='_blank'>
				<img src=\"../estilos/imagenes/estatus/pdf.png\" style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\"></a>";
				$lista.="           
				<tr id='" . $valor2[1] . "'>
				<td $color><div align=\"left\">" . $valor2[1] . "</div></td>
				<td $color><div align=\"left\">" . ucwords($evento) . "</div></td>
				<td $color><div align=\"center\">" . ucwords($valor2[3]) . "</div></td>
				<td $color><div align=\"center\">" . $botonRep . "</div></td>
				</tr>";
			}
		} else {
			$mensaje = "<div style='color: #FF0000;font-weight: bold;'><br>Error: Disculpe su busqueda NO arrojo ningun resultado.<div><br>";
			$lista.="<td colspan='8'><div align=\"center\">" . $mensaje . "</div></td>";
		}
		$lista.="</table>";
		if ($num_pag > $count) {
			$boton_Sig = " <button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name='button'  id=\"button\" onClick=\"siguiente(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Siguiente'><img src=\"../estilos/imagenes/estatus/p_3.gif\"  style=\"border-width:none;border-style:none;\"></button>";
		} else {
			$boton_Sig = "
			<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' name='button' id='button'><img src='../estilos/imagenes/estatus/p_3_2.gif' style=\"border-width:none;border-style:none;\"></button>";
		}
		$lista.=$this->leyenda($tipo = 7);
		$lista.="<table class='tabla'>
		<tr><th><div align=\"center\">" . $num_pag . "&nbsp;&nbsp;p&aacute;ginas,&nbsp;" . count($todas) . "&nbsp;&nbsp;pauta(s)</div></th>
		<th>" . $combo . "&nbsp;&nbsp;" . $boton_Ant . " &nbsp; " . $count . " / " . $num_pag . "&nbsp;" . $boton_Sig . "&nbsp;</th>
		</tr></table>";
		echo $lista;
	}

	function listadereportespers() {
		//print_r($_GET);   
		if (isset($_GET['parametrobusqueda'])) {
			$busquedaget = $_GET['parametrobusqueda'];
		} else {
			$busquedaget = $_GET['busqueda'];
		}
		$pagina_ajax = "'listadereportespers'";
		$nombre_fn = "0";
		if (isset($_GET['minimo'])) {
			$minimo = $_GET['minimo'];
			$count = $_GET['count'];
			if ($count > 1) {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button' onClick=\"anterior(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Anterior'><img src='../estilos/imagenes/estatus/p_2.gif' style=\"border-width:none;border-style:none;\"></button>";
			} else {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button'><img src='../estilos/imagenes/estatus/p_2_2.gif'style=\"border-width:none;border-style:none;\"></button>";
			}
		} else {
			$minimo = 0;
			$count = 1;
			$boton_Ant = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name=\"button'  id=\"button\"><img src=\"../estilos/imagenes/estatus/p_2_2.gif\" style=\"border-width:none;border-style:none;\"></button>";
		}
		if (($_SESSION['id_tipo_usuario'] == 24) or ($_SESSION['id_tipo_usuario'] == 27)) {
			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_estatus in(21)", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta,nombre_programa,pauta_estatus,id_estatus,descripcion", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_estatus in(21) order by id_pauta DESC", $minimo);
		} elseif ($_SESSION['id_tipo_usuario'] == 25) {
			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_ao_rep", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "'  and id_estatus in(21)", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_ao_rep", $campo_rn = "id_pauta,nombre_programa,pauta_estatus,id_estatus,descripcion", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "'  and id_estatus in(21) order by id_pauta DESC", $minimo);
		} else {
			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "' and id_estatus in(21)", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta,nombre_programa,pauta_estatus,id_estatus,descripcion", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "' and id_estatus in(21) order by id_pauta DESC", $minimo);
		}

		$num_pag = ceil(count($todas) / 10);
		for ($i = 1; $i <= $num_pag; $i++) {
			$cantidad_paginas[] = $i;
		}
		if (count($cantidad_paginas) > 0) {
			$combo = "<SELECT name=\"cantidad_paginas\" id=\"cantidad_paginas\" class='lista' style='width:50px;' onchange=\"cambiopagina(" . $pagina_ajax . "," . $nombre_fn . ");\">";
			foreach ($cantidad_paginas as $llave => $valor) {
				if ($valor == $count) {
					$selected = 'selected';
				} else {
					$selected = "";
				}
				$combo.="<option value='" . $llave . "' $selected >" . $valor . "</option>";
			}
			$combo.="</SELECT> ";
		}
		$lista.="   
		<input name='minimo' id='minimo' type='hidden' value=" . $minimo . " />
		<input name='count' id='count' type='hidden' value=" . $count . " />    
		<input name='parametrobusqueda' id='parametrobusqueda' type='hidden' value='" . $busquedaget . "'>  
		<table  class='tabla' id='tabla_corres'>
		<tr><th class='titulo' colspan='6'>DATOS</th></tr>
		<tr>    
		<th style='200'><div align=\"center\">N&deg; DE PAUTA</div></th>
		<th style='200'><div align=\"center\">PROGRAMA/EVENTO</div></th>
		<th><div align=\"center\">ESTATUS</div></th>
		<th><div align=\"center\">ACCIONES</div></th>
		</tr>";
		if (count($ObjListado) > 0) {
			foreach ($ObjListado as $llave2 => $valor2) {
				$resto = $llave2 % 2;
				if (($resto == 0) && ($llave2 != 0)) {
					$color = "style='background:#f1f1f1;'"; //par
				} else {
					$color = "style='background:#ffffff;'"; //impar f9f9f9
				}
				$descripcionprog = $valor2[2];
				$descripcioneven = $valor2[5];
				$evento = trim($descripcionprog . "" . $descripcioneven);
				$botonRep = "           
				<a href='../reportes/reporte_pers.php?id_pauta=" . $valor2[1] . "&lista=classlistadereportespers.php&cedula=" . $_SESSION['cedula'] . "' target='_blank'>
				<img src=\"../estilos/imagenes/estatus/pdf.png\" style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\"></a>";
				$lista.="           
				<tr id='" . $valor2[1] . "'>
				<td $color><div align=\"left\">" . $valor2[1] . "</div></td>
				<td $color><div align=\"left\">" . ucwords($evento) . "</div></td>
				<td $color><div align=\"center\">" . ucwords($valor2[3]) . "</div></td>
				<td $color><div align=\"center\">" . $botonRep . "</div></td>
				</tr>";
			}
		} else {
			$mensaje = "<div style='color: #FF0000;font-weight: bold;'><br>Error: Disculpe su busqueda NO arrojo ningun resultado.<div><br>";
			$lista.="<td colspan='8'><div align=\"center\">" . $mensaje . "</div></td>";
		}
		$lista.="</table>";
		if ($num_pag > $count) {
			$boton_Sig = " <button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name='button'  id=\"button\" onClick=\"siguiente(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Siguiente'><img src=\"../estilos/imagenes/estatus/p_3.gif\"  style=\"border-width:none;border-style:none;\"></button>";
		} else {
			$boton_Sig = "
			<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' name='button' id='button'><img src='../estilos/imagenes/estatus/p_3_2.gif' style=\"border-width:none;border-style:none;\"></button>";
		}

		$lista.=$this->leyenda($tipo = 7);
		$lista.="<table class='tabla'>
		<tr><th><div align=\"center\">" . $num_pag . "&nbsp;&nbsp;p&aacute;ginas,&nbsp;" . count($todas) . "&nbsp;&nbsp;pauta(s)</div></th>
		<th>" . $combo . "&nbsp;&nbsp;" . $boton_Ant . " &nbsp; " . $count . " / " . $num_pag . "&nbsp;" . $boton_Sig . "&nbsp;</th>
		</tr></table>";
		echo $lista;
	}

	function listadereportesrecur() {
		//print_r($_GET);   
		if (isset($_GET['parametrobusqueda'])) {
			$busquedaget = $_GET['parametrobusqueda'];
		} else {
			$busquedaget = $_GET['busqueda'];
		}
		$pagina_ajax = "'listadereportesrecur'";
		$nombre_fn = "0";
		if (isset($_GET['minimo'])) {
			$minimo = $_GET['minimo'];
			$count = $_GET['count'];
			if ($count > 1) {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button' onClick=\"anterior(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Anterior'><img src='../estilos/imagenes/estatus/p_2.gif' style=\"border-width:none;border-style:none;\"></button>";
			} else {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button'><img src='../estilos/imagenes/estatus/p_2_2.gif'style=\"border-width:none;border-style:none;\"></button>";
			}
		} else {
			$minimo = 0;
			$count = 1;
			$boton_Ant = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name=\"button'  id=\"button\"><img src=\"../estilos/imagenes/estatus/p_2_2.gif\" style=\"border-width:none;border-style:none;\"></button>";
		}
		if (($_SESSION['id_tipo_usuario'] == 24) or ($_SESSION['id_tipo_usuario'] == 27)) {
			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_estatus in(21)", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta,nombre_programa,pauta_estatus,id_estatus,descripcion", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_estatus in(21) order by id_pauta DESC", $minimo);
		} elseif ($_SESSION['id_tipo_usuario'] == 25) {
			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_ao_rep", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "'  and id_estatus in(21)", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_ao_rep", $campo_rn = "id_pauta,nombre_programa,pauta_estatus,id_estatus,descripcion", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "'  and id_estatus in(21) order by id_pauta DESC", $minimo);
		} else {
			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "' and id_estatus in(21)", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta,nombre_programa,pauta_estatus,id_estatus,descripcion", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "' and id_estatus in(21) order by id_pauta DESC", $minimo);
		}

		$num_pag = ceil(count($todas) / 10);
		for ($i = 1; $i <= $num_pag; $i++) {
			$cantidad_paginas[] = $i;
		}
		if (count($cantidad_paginas) > 0) {
			$combo = "<SELECT name=\"cantidad_paginas\" id=\"cantidad_paginas\" class='lista' style='width:50px;' onchange=\"cambiopagina(" . $pagina_ajax . "," . $nombre_fn . ");\">";
			foreach ($cantidad_paginas as $llave => $valor) {
				if ($valor == $count) {
					$selected = 'selected';
				} else {
					$selected = "";
				}
				$combo.="<option value='" . $llave . "' $selected >" . $valor . "</option>";
			}
			$combo.="</SELECT> ";
		}
		$lista.="   
		<input name='minimo' id='minimo' type='hidden' value=" . $minimo . " />
		<input name='count' id='count' type='hidden' value=" . $count . " />    
		<input name='parametrobusqueda' id='parametrobusqueda' type='hidden' value='" . $busquedaget . "'>  
		<table  class='tabla' id='tabla_corres'>
		<tr><th class='titulo' colspan='6'>DATOS</th></tr>
		<tr>    
		<th style='200'><div align=\"center\">N&deg; DE PAUTA</div></th>
		<th style='200'><div align=\"center\">PROGRAMA/EVENTO</div></th>
		<th><div align=\"center\">ESTATUS</div></th>
		<th><div align=\"center\">ACCIONES</div></th>
		</tr>";
		if (count($ObjListado) > 0) {
			foreach ($ObjListado as $llave2 => $valor2) {
				$resto = $llave2 % 2;
				if (($resto == 0) && ($llave2 != 0)) {
					$color = "style='background:#f1f1f1;'"; //par
				} else {
					$color = "style='background:#ffffff;'"; //impar f9f9f9
				}
				$descripcionprog = $valor2[2];
				$descripcioneven = $valor2[5];
				$evento = trim($descripcionprog . "" . $descripcioneven);
				$botonRep = "
				<a href='../reportes/reporte_recur.php?id_pauta=" . $valor2[1] . "&lista=classlistadereportesrecur.php&cedula=" . $_SESSION['cedula'] . "' target='_blank'>
				<img src=\"../estilos/imagenes/estatus/pdf.png\" style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\"></a>";
				$lista.="           
				<tr id='" . $valor2[1] . "'>
				<td $color><div align=\"left\">" . $valor2[1] . "</div></td>
				<td $color><div align=\"left\">" . ucwords($evento) . "</div></td>
				<td $color><div align=\"center\">" . ucwords($valor2[3]) . "</div></td>
				<td $color><div align=\"center\">" . $botonRep . "</div></td>
				</tr>";
			}
		} else {
			$mensaje = "<div style='color: #FF0000;font-weight: bold;'><br>Error: Disculpe su busqueda NO arrojo ningun resultado.<div><br>";
			$lista.="<td colspan='8'><div align=\"center\">" . $mensaje . "</div></td>";
		}
		$lista.="</table>";
		if ($num_pag > $count) {
			$boton_Sig = " <button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name='button'  id=\"button\" onClick=\"siguiente(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Siguiente'><img src=\"../estilos/imagenes/estatus/p_3.gif\"  style=\"border-width:none;border-style:none;\"></button>";
		} else {
			$boton_Sig = "
			<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' name='button' id='button'><img src='../estilos/imagenes/estatus/p_3_2.gif' style=\"border-width:none;border-style:none;\"></button>";
		}

		$lista.=$this->leyenda($tipo = 7);
		$lista.="<table class='tabla'>
		<tr><th><div align=\"center\">" . $num_pag . "&nbsp;&nbsp;p&aacute;ginas,&nbsp;" . count($todas) . "&nbsp;&nbsp;pauta(s)</div></th>
		<th>" . $combo . "&nbsp;&nbsp;" . $boton_Ant . " &nbsp; " . $count . " / " . $num_pag . "&nbsp;" . $boton_Sig . "&nbsp;</th>
		</tr></table>";
		echo $lista;
	}

	function listadereportesrecurasig() {
		if (isset($_GET['parametrobusqueda'])) {
			$busquedaget = $_GET['parametrobusqueda'];
		} else {
			$busquedaget = $_GET['busqueda'];
		}
		$pagina_ajax = "'listadereportesrecurasig'";
		$nombre_fn = "0";
		if (isset($_GET['minimo'])) {
			$minimo = $_GET['minimo'];
			$count = $_GET['count'];
			if ($count > 1) {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button' onClick=\"anterior(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Anterior'><img src='../estilos/imagenes/estatus/p_2.gif' style=\"border-width:none;border-style:none;\"></button>";
			} else {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button'><img src='../estilos/imagenes/estatus/p_2_2.gif'style=\"border-width:none;border-style:none;\"></button>";
			}
		} else {
			$minimo = 0;
			$count = 1;
			$boton_Ant = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name=\"button'  id=\"button\"><img src=\"../estilos/imagenes/estatus/p_2_2.gif\" style=\"border-width:none;border-style:none;\"></button>";
		}
		if (($_SESSION['id_tipo_usuario'] == 24) or ($_SESSION['id_tipo_usuario'] == 27) or ($_SESSION['id_tipo_usuario'] == 25)) {
			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_estatus in(21)", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta,nombre_programa,pauta_estatus,id_estatus,descripcion", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_estatus in(21) order by id_pauta DESC", $minimo);
		} else {
			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "' and id_estatus in(21)", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta,nombre_programa,pauta_estatus,id_estatus,descripcion", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "' and id_estatus in(21) order by id_pauta DESC", $minimo);
		}
		$num_pag = ceil(count($todas) / 10);
		for ($i = 1; $i <= $num_pag; $i++) {
			$cantidad_paginas[] = $i;
		}
		if (count($cantidad_paginas) > 0) {
			$combo = "<SELECT name=\"cantidad_paginas\" id=\"cantidad_paginas\" class='lista' style='width:50px;' onchange=\"cambiopagina(" . $pagina_ajax . "," . $nombre_fn . ");\">";
			foreach ($cantidad_paginas as $llave => $valor) {
				if ($valor == $count) {
					$selected = 'selected';
				} else {
					$selected = "";
				}
				$combo.="<option value='" . $llave . "' $selected >" . $valor . "</option>";
			}
			$combo.="</SELECT> ";
		}
		$lista.="   
		<input name='minimo' id='minimo' type='hidden' value=" . $minimo . " />
		<input name='count' id='count' type='hidden' value=" . $count . " />    
		<input name='parametrobusqueda' id='parametrobusqueda' type='hidden' value='" . $busquedaget . "'>  
		<table  class='tabla' id='tabla_corres'>
		<tr><th class='titulo' colspan='6'>DATOS</th></tr>
		<tr>    
		<th style='200'><div align=\"center\">N&deg; DE PAUTA</div></th>
		<th style='200'><div align=\"center\">PROGRAMA/EVENTO</div></th>
		<th><div align=\"center\">ESTATUS</div></th>
		<th><div align=\"center\">ACCIONES</div></th>
		</tr>";
		if (count($ObjListado) > 0) {
			foreach ($ObjListado as $llave2 => $valor2) {
				$resto = $llave2 % 2;
				if (($resto == 0) && ($llave2 != 0)) {
					$color = "style='background:#f1f1f1;'"; //par
				} else {
					$color = "style='background:#ffffff;'"; //impar f9f9f9
				}
				$descripcionprog = $valor2[2];
				$descripcioneven = $valor2[5];
				$evento = trim($descripcionprog . "" . $descripcioneven);
				$botonRep = "           
				<a href='../reportes/reporte_recur_aprob.php?id_pauta=" . $valor2[1] . "&lista=classlistadereportesrecurasig.php&cedula=" . $_SESSION['cedula'] . "' target='_blank'>
				<img src=\"../estilos/imagenes/estatus/pdf.png\" style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\"></a>";
				$lista.="           
				<tr id='" . $valor2[1] . "'>
				<td $color><div align=\"left\">" . $valor2[1] . "</div></td>
				<td $color><div align=\"left\">" . ucwords($evento) . "</div></td>
				<td $color><div align=\"center\">" . ucwords($valor2[3]) . "</div></td>
				<td $color><div align=\"center\">" . $botonRep . "</div></td>
				</tr>";
			}
		} else {
			$mensaje = "<div style='color: #FF0000;font-weight: bold;'><br>Error: Disculpe su busqueda NO arrojo ningun resultado.<div><br>";
			$lista.="<td colspan='8'><div align=\"center\">" . $mensaje . "</div></td>";
		}
		$lista.="</table>";
		if ($num_pag > $count) {
			$boton_Sig = " <button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name='button'  id=\"button\" onClick=\"siguiente(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Siguiente'><img src=\"../estilos/imagenes/estatus/p_3.gif\"  style=\"border-width:none;border-style:none;\"></button>";
		} else {
			$boton_Sig = "
			<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' name='button' id='button'><img src='../estilos/imagenes/estatus/p_3_2.gif' style=\"border-width:none;border-style:none;\"></button>";
		}
		$lista.=$this->leyenda($tipo = 7);
		$lista.="<table class='tabla'>
		<tr><th><div align=\"center\">" . $num_pag . "&nbsp;&nbsp;p&aacute;ginas,&nbsp;" . count($todas) . "&nbsp;&nbsp;pauta(s)</div></th>
		<th>" . $combo . "&nbsp;&nbsp;" . $boton_Ant . " &nbsp; " . $count . " / " . $num_pag . "&nbsp;" . $boton_Sig . "&nbsp;</th>
		</tr></table>";
		echo $lista;
	}

	function listadereportesinformes() {
		if (isset($_GET['parametrobusqueda'])) {
			$busquedaget = $_GET['parametrobusqueda'];
		} else {
			$busquedaget = $_GET['busqueda'];
		}
		$pagina_ajax = "'listadereportesinformes'";
		$nombre_fn = "0";
		if (isset($_GET['minimo'])) {
			$minimo = $_GET['minimo'];
			$count = $_GET['count'];
			if ($count > 1) {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button' onClick=\"anterior(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Anterior'><img src='../estilos/imagenes/estatus/p_2.gif' style=\"border-width:none;border-style:none;\"></button>";
			} else {
				$boton_Ant = " <button style='width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer' type='button' name='button'  id='button'><img src='../estilos/imagenes/estatus/p_2_2.gif'style=\"border-width:none;border-style:none;\"></button>";
			}
		} else {
			$minimo = 0;
			$count = 1;
			$boton_Ant = "<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name=\"button'  id=\"button\"><img src=\"../estilos/imagenes/estatus/p_2_2.gif\" style=\"border-width:none;border-style:none;\"></button>";
		}

		if (($_SESSION['id_tipo_usuario'] == 24) or ($_SESSION['id_tipo_usuario'] == 27)) {
			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_estatus in(26)", "NO");

			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta,nombre_programa, descripcion ,pauta_estatus,id_estatus", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_estatus in(26) order by id_pauta DESC", $minimo);
		} elseif ($_SESSION['id_tipo_usuario'] == 25) {

			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_ao_rep", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "'  and id_estatus in(26)", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_ao_rep", $campo_rn = "id_pauta,nombre_programa, descripcion,pauta_estatus,id_estatus", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "'  and id_estatus in(26) order by id_pauta DESC", $minimo);
		} else {
			$todas = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "' and id_estatus in(26)", "NO");
			$ObjListado = $this->ObjConsulta->busquedasys_main($this->conect_sistemas_vtv, $vista_tabla = $this->prefix_table . "lista_pauta_gerencia_rep", $campo_rn = "id_pauta,nombre_programa, descripcion,pauta_estatus,id_estatus", $campo_bq = "busqueda", $busquedaget, $consulta_relacional = "id_gerencia='" . $_SESSION['gerencia'] . "' and id_estatus in(26) order by id_pauta DESC", $minimo);
		}

		$num_pag = ceil(count($todas) / 10);
		for ($i = 1; $i <= $num_pag; $i++) {
			$cantidad_paginas[] = $i;
		}
		if (count($cantidad_paginas) > 0) {
			$combo = "<SELECT name=\"cantidad_paginas\" id=\"cantidad_paginas\" class='lista' style='width:50px;' onchange=\"cambiopagina(" . $pagina_ajax . "," . $nombre_fn . ");\">";
			foreach ($cantidad_paginas as $llave => $valor) {
				if ($valor == $count) {
					$selected = 'selected';
				} else {
					$selected = "";
				}
				$combo.="<option value='" . $llave . "' $selected >" . $valor . "</option>";
			}
			$combo.="</SELECT> ";
		}
		$lista.="   
		<input name='minimo' id='minimo' type='hidden' value=" . $minimo . " />
		<input name='count' id='count' type='hidden' value=" . $count . " />    
		<input name='parametrobusqueda' id='parametrobusqueda' type='hidden' value='" . $busquedaget . "'>  
		<table  class='tabla' id='tabla_corres'>
		<tr><th class='titulo' colspan='6'>DATOS</th></tr>
		<tr>
		<th style='200'><div align=\"center\">N&deg; DE PAUTA</div></th>
		<th style='200'><div align=\"center\">PROGRAMA/EVENTO</div></th>
		<th><div align=\"center\">ESTATUS</div></th>
		<th><div align=\"center\">ACCIONES</div></th>
		</tr>";
		//print_r($ObjListadoVehiculos);
		if (count($ObjListado) > 0) {
			foreach ($ObjListado as $llave2 => $valor2) {

				$resto = $llave2 % 2;
				if (($resto == 0) && ($llave2 != 0)) {
					$color = "style='background:#f1f1f1;'"; //par
				} else {
					$color = "style='background:#ffffff;'"; //impar f9f9f9
				}
				$descripcionprog = $valor2[2];
				$descripcioneven = $valor2[3];
				if ($descripcionprog != '') {
					$evento = $descripcionprog;
				}
				if ($descripcioneven != '') {
					$evento = $descripcioneven;
				}
				$botonRep = "           
				<a href='../reportes/reporte_inf_prod.php?id_pauta=" . $valor2[1] . "&lista=classlistadereportesinformes.php&cedula=" . $_SESSION['cedula'] . "' target='_blank'>
				<img src=\"../estilos/imagenes/estatus/pdf.png\" style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\"></a>";
				$lista.="           
				<tr id='" . $valor2[1] . "'>
				<td $color><div align=\"left\">" . $valor2[1] . "</div></td>
				<td $color><div align=\"left\">" . ucwords($evento) . "</div></td>
				<td $color><div align=\"center\">" . ucwords($valor2[4]) . "</div></td>
				<td $color><div align=\"center\">" . $botonRep . "</div></td>
				</tr>";
			}//foreach
		} else {
			$mensaje = "<div style='color: #FF0000;font-weight: bold;'><br>Error: Disculpe su busqueda NO arrojo ningun resultado.<div><br>";
			$lista.="<td colspan='8'><div align=\"center\">" . $mensaje . "</div></td>";
		}
		$lista.="</table>";
		if ($num_pag > $count) {
			$boton_Sig = " <button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type=\"button\" name='button'  id=\"button\" onClick=\"siguiente(" . $pagina_ajax . "," . $nombre_fn . ");\" title='Siguiente'><img src=\"../estilos/imagenes/estatus/p_3.gif\"  style=\"border-width:none;border-style:none;\"></button>";
		} else {
			$boton_Sig = "
			<button style=\"width:20px;height:20px;border-width:thin;border-style:none;background-color:#F9F9F9;cursor:pointer\" type='button' name='button' id='button'><img src='../estilos/imagenes/estatus/p_3_2.gif' style=\"border-width:none;border-style:none;\"></button>";
		}

		$lista.=$this->leyenda($tipo = 7);
		$lista.="<table class='tabla'>
		<tr><th><div align=\"center\">" . $num_pag . "&nbsp;&nbsp;p&aacute;ginas,&nbsp;" . count($todas) . "&nbsp;&nbsp;pauta(s)</div></th>
		<th>" . $combo . "&nbsp;&nbsp;" . $boton_Ant . " &nbsp; " . $count . " / " . $num_pag . "&nbsp;" . $boton_Sig . "&nbsp;</th>
		</tr></table>";
		echo $lista;
	}

	function combodivision() {
		//print_r($_GET);   
		$gerencia = $_GET['gerencias'];
		$DatosDivision = $this->ObjConsulta->division($this->conect_sistemas_vtv, "", "", $gerencia);
		$division.="<SELECT name=\"division\" id=\"division\" class='lista' style='width:500px;' $onchage>";
		$division.="<option value=\"0\" selected>SELECCIONE</option>";
		foreach ($DatosDivision as $llave => $valor) {
			if ($valor[1] == $id_division) {
				$selected = "selected";
			} else {
				$selected = "";
			}
			$division.="<option value='" . $valor[1] . "' $selected >" . utf8_encode($valor[2]) . "</option>";
		}
		$division.="</SELECT> ";
		echo $division;
	}*/
	/**
	*componente usado en varias paginas.
	*/
	function comp_tablacomentario($titulo){
		$datosobs=$this->ObjConsulta->selectobsrech($this->conect_sistemas_vtv, $_GET['id_pauta']);
		$htm="<table class='tabla' id='tobservaciones_all' pauta='{$_GET['id_pauta']}' style='width:850px;' >
		<tr><th class='titulo' colspan='8'>$titulo</th></tr>
		<tr><td><textarea style='width:100%'>".$datosobs[1][1]."</textarea></td></tr>
		</table></div>";
		return $htm;
	}

	function comentarios_comp_tablacomentario(){
		$datosbdstatuspauta = $this->ObjConsulta->selectestatus($this->conect_sistemas_vtv, $_GET['id_pauta']);
		$id_estatus_pauta = $datosbdstatuspauta[1][2];
		$this->ObjConsulta->guardarcomentarios($this->conect_sistemas_vtv, $id_estatus_pauta, $_GET['mensaje']);
	}

	function opccion_com_pauta($empieza, $termina){
		$hora="";
		for ($e = $empieza; $e <= $termina; $e++){
			if ($e <= 9) {
				$cero = 0;
			} else {
				$cero = "";
			}
			$hora.="<option value='$e'>$cero$e</option>\n";
		}
		return $hora;
	}

	/**
	*componente que te genera los selects del formulario de fechas y horas del formulario de generar pauta
	*/
	function fecha_comp_pauta($idnomfech,$idnomHora,$idnommin){
		$fecha = "<input id='{$idnomfech}' name='{$idnomfech}' readonly='readonly' size='10' class='campo_vl' value='" . date("d/m/Y") . "'>";
		$hora = "<select name='{$idnomHora}' id='{$idnomHora}' onChange='validarhoras(this);' class='campo_vl'>";
		$hora.="<option value='24' selected='selected'>Hora</option>";
		$hora.= $this->opccion_com_pauta(0, 23);
		$hora.="</select>";
		$min = "<select name='{$idnommin}' id='{$idnommin}' onChange='validarhoras(this);' class='campo_vl'>";
		$min.="<option value='60' selected='selected'>Min</option>";
		$min.=$this->opccion_com_pauta(0, 59);
		$min.="</select>";
		eval ('$data = array("'.$idnomfech.'" => $fecha, "'.$idnomHora.'" => $hora, "'.$idnommin.'" => $min);');
		return $data;
	}

	/**
	*componente que se encarga de generar un combo_select a partir de una consulta con argumentos.
	*/
	function basic_combo($consulta, $idnomcombo, $javascript_acc, $style='style="width:150px;"', $arg=null, $size=true, $defValuemsg="Seleccione"){
		if($arg!=null){
			$arg=",".$arg;
		}else{
			$arg="";
		}
		eval('$query = $this->ObjConsulta->$consulta($this->conect_sistemas_vtv'.$arg.');');
		if($size){
			$size='size="'.(count($query)+1).'"';
		}
		$select = '<select id="'.$idnomcombo.'" name="'.$idnomcombo.'" '.$style.' '.$size.' onChange="'.$javascript_acc.'">';
		$select.='<option value="0" selected >'.$defValuemsg.'</option>';
		foreach ($query as $llave => $valor) {
			$select.='<option value="'. $valor[1] . '" $selected >' . strtoupper($valor[2]) . '</option>';
		}
		$select.='</SELECT>';
		return $select;
	}

	/**
	*funcion destinada en crear la ventana de asignaciones de recursos.
	**/
	function ual_asignacionrecursos(){
		$cedula = $_SESSION['cedula'];
		$administrador = $_SESSION['id_tipo_usuario'];
		$nombres = $_SESSION['nombres'];
		$apellidos = $_SESSION['apellidos'];
		$pauta = $_GET['id_pauta'];
		$idspauta = $this->ObjConsulta->selectidspauta($this->conect_sistemas_vtv, $pauta);
		$id_pauta = $idspauta[1][1];
		$id_tipo_pauta = $idspauta[1][2];
		$id_locacion = $idspauta[1][3];
		$id_tipo_traje = $idspauta[1][4];
		$id_program = $idspauta[1][5];
		$id_citacion = $idspauta[1][6];
		$id_montaje = $idspauta[1][7];
		$id_emision_grabacion = $idspauta[1][8];
		$id_retorno = $idspauta[1][9];
		$id_tipo_evento = $idspauta[1][10];
		$user_reg = $idspauta[1][11];
		$pauta_lugar = $idspauta[1][12];
		$pauta_descripcionr = $idspauta[1][13];
		if ($id_program == 0 or $id_program == null) {
			$descprograma = $this->ObjConsulta->selectdescprograma($this->conect_sistemas_vtv, $id_program);
			$idprograma = $descprograma[1][1];
			$descripcionprog = ucwords($descprograma[1][2]);
		} else {
			$descripcionprog = ucwords($pauta_descripcionr);
		}
		$desclocacion = $this->ObjConsulta->selectdesclocacion($this->conect_sistemas_vtv, $id_locacion);
		$idlocacion = $desclocacion[1][1];
		$descripcionloc = ucwords($desclocacion[1][2]);
		$desctipotraje = $this->ObjConsulta->selectdesctipotraje($this->conect_sistemas_vtv, $id_tipo_traje);
		$idtipotraje = $desctipotraje[1][1];
		$descripciontraje = ucwords($desctipotraje[1][2]);
		$descproductor = $this->ObjConsulta->selectdescproductor($this->conect_sistemas_vtv, $user_reg);
		$descripcionprod = strtolower($descproductor[1][1]);
		$descripcionprod = ucwords($descripcionprod);
		$desctipoevento = $this->ObjConsulta->selectdesctipoevento($this->conect_sistemas_vtv, $id_tipo_evento);
		$idtipoevento = $desctipoevento[1][1];
		$descripcionteven = ucwords($desctipoevento[1][2]);
		$datoscitacion = $this->ObjConsulta->selectcitacion($this->conect_sistemas_vtv, $id_citacion);
		$idcitacion = $datoscitacion[1][1];
		$fechacitacion = $datoscitacion[1][2];
		$horacitacion = $datoscitacion[1][3];
		$id_lugar_citacion = $datoscitacion[1][4];
		$fechacitacion = $this->Objfechahora->flibInvertirInEs($fechacitacion);
		$lugarcitacion = $this->ObjConsulta->selectlugar($this->conect_sistemas_vtv, $id_lugar_citacion);
		$idcitacion = $lugarcitacion[1][1];
		$descripcitacion = ucwords($lugarcitacion[1][2]);
		$datosmontaje = $this->ObjConsulta->selectmontaje($this->conect_sistemas_vtv, $id_montaje);
		$idmontaje = $datosmontaje[1][1];
		$fechamontaje = $datosmontaje[1][2];
		$horamontaje = $datosmontaje[1][3];
		$id_lugar_montaje = $datosmontaje[1][4];
		$fechamontaje = $this->Objfechahora->flibInvertirInEs($fechamontaje);
		$descripmontaje = ucwords($pauta_lugar);
		$datosemision = $this->ObjConsulta->selectemision($this->conect_sistemas_vtv, $id_emision_grabacion);
		$idemision = $datosemision[1][1];
		$fechaemision = $datosemision[1][2];
		$horaemision = $datosemision[1][3];
		$id_lugar_emision = $datosemision[1][4];
		$fechaemision = $this->Objfechahora->flibInvertirInEs($fechaemision);
		$descripemision = ucwords($pauta_lugar);
		$datosretorno = $this->ObjConsulta->selectretorno($this->conect_sistemas_vtv, $id_retorno);
		$idretorno = $datosretorno[1][1];
		$fecharetorno = $datosretorno[1][2];
		$horaretorno = $datosretorno[1][3];
		$id_lugar_retorno = $datosretorno[1][4];
		$fecharetorno = $this->Objfechahora->flibInvertirInEs($fecharetorno);
		$descripretorno = ucwords($pauta_lugar);
		if ($descripretorno == '0') {
			$descripretorno = "";
		} else {
			$descripretorno = "<tr><th>Retorno:</th><td>" . $descripretorno . "</td><th>Fecha:</th><td>" . $fecharetorno . "</td><th>Hora:</th><td>" . $horaretorno . "</td></tr>";
		}
		if ($descripcionprog == '0') {
			$evento = "<th>Evento:</th><td>" . $descripcioneven . "</td>";
		}
		if ($descripcioneven == '0') {
			$evento = "<th>Programa:</th><td>" . $descripcionprog . "</td>";
		}
		$datosservicios = $this->ObjConsulta->select_servicios($this->conect_sistemas_vtv, "2");
		$servicios = "<select id='servicios' name='servicios' style='width:400px;'>";
		$servicios.="<option value='0' selected >Seleccione </option>";
		foreach ($datosservicios as $llave => $valor) {
			$servicios.="<option value='" . $valor[1] . "'>" . ucwords($valor[3]) . "&nbsp;&frasl;&nbsp;" . ucwords($valor[4]) . "</option>";
		}
		$servicios.="</SELECT> ";
		$cantidad = "<input type='text' name='cantidad' id='cantidad' onkeypress='return validaN(event)' size='4' maxlength='4'/>";
		$datosrec = $this->ObjConsulta->selectdatosrec($this->conect_sistemas_vtv, $pauta);
		$rectotal = count($datosrec);
        $materiales = "<div id='materiales_porasignar'></div>"; //quinatdo duplicidad en el codigo
        $datoslista2 = $this->ObjConsulta->selectlistmatasig($this->conect_sistemas_vtv, $pauta);
        
        if($_GET['tm']!="rm"){
        	$mod = '4';
        	$botonA = "<input type=\"button\" class='boton' value=\"Siguiente\" OnClick=CancelarRegresar('classasignarrecursos.php?id_pauta=".$pauta."&lista=".$_GET['lista']."&tm=rm');>";
        }else{
        	$botonA="<input type=\"button\" class='boton' value=\"Regresar\" OnClick=CancelarRegresar('classasignarrecursos.php?id_pauta=".$pauta."&lista=".$_GET['lista']."');> <input type=\"button\" class='boton' value=\"Vista Previa\" OnClick=CancelarRegresar('classmostrarpautagenerada.php?id_pauta=" . $id_pauta . "&lista=classlistadepautasual.php?id_pauta={$id_pauta}&mod=".$mod."');>";
        }
        $asignados = "<div id='recursos_hb_asignados'></div>";
        $botonC = "<input type=\"button\" class='boton' value=\"Guardar para futuros Cambios\" OnClick=\"CancelarRegresar('" . $_GET['lista'] . "', this);\" >";
        $botonB = "<input type=\"button\" class='boton' value=\"Buscar\" OnClick=buscarpersona();>";
        $botonAg = "<input type=\"button\" class='boton' value=\"Agregar\" OnClick=\"agregarenlalista($pauta, '".$_GET['tm']."');\" >";
        $titulo1 = "DATOS GENERALES DE LA PAUTA";
        $titulo2 = "RECURSOS";
        if($_GET['tm']!="rm"){
        	//recurso humano..
        	$pagina = '7';
        	$tablaAdmPers = "<table class='tabla' style='width:850px';>
        	<tr><td ><div align='center' ><button class='boton modalInput' onclick='callback_select_personal(null,$pauta); ' rel='#agregar'>Administrador de personal</button></div></td></tr>
        	</table>";
        }else{
        	//recurso material.
        	$pagina = '8';
        	$tablaRecursoAsig = "<table class='tabla' style='width:850px';>
        	<tr><th class='titulo' colspan='8'>" . $titulo2 . "</th></tr>
        	<tr><th>Materiales:</th><td>" . $servicios . "</td><th>Cantidades:</th><td>" . $cantidad . "</td><td><div>" . $botonAg . "</td></tr>
        	</table>";
        }
        
        $pasos = $this->pasosdelproceso($pagina);
        $htm.=$this->ObjMensaje->interfazasignarrecursos($pauta, $descripcionloc, $descripciontraje, $descripcionprod, $descripcionteven, $descripcitacion, $fechacitacion, $horacitacion, $descripmontaje, $fechamontaje, $horamontaje, $descripemision, $fechaemision, $horaemision, $descripretorno, $fecharetorno, $horaretorno, $servicios, $cantidad, $evento, $materiales, $asignados, $titulo1, $titulo2, $titulo3, $titulo4, $botonA, $botonC, $botonAg, $tablaAdmPers, $tablaRecursoAsig, $pasos, $this->comp_tablacomentario("Observaciones"));
        return $htm;
    }

	/**
	*funcion dedicada a crear el diseño de pagina para solicitar servicios.
	**/
	function pag_solicitarservicios(){
		$cedula = $_SESSION['cedula'];
		$administrador = $_SESSION['id_tipo_usuario'];
		$nombres = $_SESSION['nombres'];
		$apellidos = $_SESSION['apellidos'];
		$id_gerencia = $_SESSION['gerencia'];
		$pauta = $_GET['id_pauta'];
		$verificarestatuspauta = $this->ObjConsulta->verificarestatuspauta($this->conect_sistemas_vtv, $pauta);
		$estatusactual = $verificarestatuspauta[1][1];
		if (($estatusactual == 9) or ($estatusactual == 24)) {
			$estatuspauta = 29;
			$updateexpestatus2 = $this->ObjConsulta->updateexpestatus($this->conect_sistemas_vtv, $pauta);
			$insertestatusnuevo = $this->ObjConsulta->insertestatusnuevo($this->conect_sistemas_vtv, $pauta, $estatuspauta, $cedula);
			$estatus_recurso = 13;
			$recursosdelapauta = $this->ObjConsulta->selectrecursosdelapautaeditada($this->conect_sistemas_vtv, $pauta);
			foreach ($recursosdelapauta as $llave => $valor) {
				$id_detalle_servicio = $valor[1];
				$updateexpestatusrecursos = $this->ObjConsulta->updateexpestatusrecursosf5($this->conect_sistemas_vtv, $pauta, $id_detalle_servicio);
				$verrecursoasignado = $this->ObjConsulta->selectverrecursoasignado($this->conect_sistemas_vtv, $id_detalle_servicio);
				$id_recurso_asignado = $verrecursoasignado[1][1];
				$insertestatusrecursoanulado = $this->ObjConsulta->insertestatusrecursoanulado($this->conect_sistemas_vtv, $pauta, $cedula, $id_detalle_servicio, $estatus_recurso, $id_recurso_asignado); //insertado
			}
		}
		$idspauta = $this->ObjConsulta->selectidspauta($this->conect_sistemas_vtv, $pauta);
		$id_pauta = $idspauta[1][1];
		$id_tipo_pauta = $idspauta[1][2];
		$id_locacion = $idspauta[1][3];
		$id_tipo_traje = $idspauta[1][4];
		$id_program = $idspauta[1][5];
		$id_citacion = $idspauta[1][6];
		$id_montaje = $idspauta[1][7];
		$id_emision_grabacion = $idspauta[1][8];
		$id_retorno = $idspauta[1][9];
		$id_tipo_evento = $idspauta[1][10];
		$user_reg = $idspauta[1][11];
		$pauta_lugar = $idspauta[1][12];
		$pauta_descripcionr = $idspauta[1][13];
		$desclocacion = $this->ObjConsulta->selectdesclocacion($this->conect_sistemas_vtv, $id_locacion);
		$idlocacion = $desclocacion[1][1];
		$descripcionloc = ucwords($desclocacion[1][2]);
		$desctipotraje = $this->ObjConsulta->selectdesctipotraje($this->conect_sistemas_vtv, $id_tipo_traje);
		$idtipotraje = $desctipotraje[1][1];
		$descripciontraje = ucwords($desctipotraje[1][2]);
		$descproductor = $this->ObjConsulta->selectdescproductor($this->conect_sistemas_vtv, $user_reg);
		$descripcionprod = strtolower($descproductor[1][1]);
		$descripcionprod = ucwords($descripcionprod);
		$desctipoevento = $this->ObjConsulta->selectdesctipoevento($this->conect_sistemas_vtv, $id_tipo_evento);
		$idtipoevento = $desctipoevento[1][1];
		$descripcionteven = ucwords($desctipoevento[1][2]);
		$datoscitacion = $this->ObjConsulta->selectcitacion($this->conect_sistemas_vtv, $id_citacion);
		$idcitacion = $datoscitacion[1][1];
		$fechacitacion = $datoscitacion[1][2];
		$horacitacion = $datoscitacion[1][3];
		$id_lugar_citacion = $datoscitacion[1][4];
		$fechacitacion = $this->Objfechahora->flibInvertirInEs($fechacitacion);
		$lugarcitacion = $this->ObjConsulta->selectlugar($this->conect_sistemas_vtv, $id_lugar_citacion);
		$idcitacion = $lugarcitacion[1][1];
		$descripcitacion = ucwords($lugarcitacion[1][2]);
		$datosmontaje = $this->ObjConsulta->selectmontaje($this->conect_sistemas_vtv, $id_montaje);
		$idmontaje = $datosmontaje[1][1];
		$fechamontaje = $datosmontaje[1][2];
		$horamontaje = $datosmontaje[1][3];
		$fechamontaje = $this->Objfechahora->flibInvertirInEs($fechamontaje);
		$descripmontaje = ucwords($pauta_lugar);
		$datosemision = $this->ObjConsulta->selectemision($this->conect_sistemas_vtv, $id_emision_grabacion);
		$idemision = $datosemision[1][1];
		$fechaemision = $datosemision[1][2];
		$horaemision = $datosemision[1][3];
		$fechaemision = $this->Objfechahora->flibInvertirInEs($fechaemision);
		$descripemision = ucwords($pauta_lugar);
		$datosretorno = $this->ObjConsulta->selectretorno($this->conect_sistemas_vtv, $id_retorno);
		$idretorno = $datosretorno[1][1];
		$fecharetorno = $datosretorno[1][2];
		$horaretorno = $datosretorno[1][3];
		$id_lugar_retorno = $datosretorno[1][4];
		$fecharetorno = $this->Objfechahora->flibInvertirInEs($fecharetorno);
		$descripretorno = ucwords($pauta_lugar);
		if ($descripretorno == '0') {
			$descripretorno = "";
		} else {
			$descripretorno = "<tr><th>Retorno:</th><td>" . $descripretorno . "</td><th>Fecha:</th><td>" . $fecharetorno . "</td><th>Hora:</th><td>" . $horaretorno . "</td></tr>";
		}
		if ($descripcionprog == '0') {
			$evento = "<th>Evento:</th><td>" . $descripcioneven . "</td>";
		}
		if ($descripcioneven == '0') {
			$evento = "<th>Programa:</th><td>" . $descripcionprog . "</td>";
		}
		$datosservicios = $this->ObjConsulta->select_servicios($this->conect_sistemas_vtv);
		$servicios = "<select id='servicios' name='servicios' style='width:380px;'>";
		$servicios.="<option value='0' selected >Seleccione </option>";
		foreach ($datosservicios as $llave => $valor) {
			$servicios.="<option value='" . $valor[1] . "' $selected >" . ucwords($valor[4]) . "&nbsp;&frasl;&nbsp;" . ucwords($valor[3]) . "</option>";
		}
		$servicios.="</SELECT> ";
		$cantidad = "<input type='text' name='cantidad' id='cantidad' onkeypress='return validaN(event)' size='2' maxlength='4'/>";
		$botonAg = "<input type=\"button\" class='boton' value=\"Agregar\" OnClick=agregarenlista_f5(this,$pauta);>";
		$tr_materiales= "<tr><th>Materiales:</th><td>" . $servicios . "</td><th>Cant:</th><td><input type=\"button\" class=\"boton\" value=\"+\" OnClick=\"textsum(this,'+')\" style=\"margin-right:0px;\" />" . $cantidad . "<input type=\"button\" class=\"boton\" value=\"-\" OnClick=\"textsum(this,'-')\"/></td><td><div>" . $botonAg . "</div></td></tr>";
		$datosrecursoh = $this->ObjConsulta->select_servicios($this->conect_sistemas_vtv,"1");
		$servicios = "<select id='servicios2' name='servicios2' style='width:380px;'>";
		$servicios.="<option value='0' selected >Seleccione </option>";
		foreach ($datosrecursoh as $llave => $valor) {
			$servicios.="<option value='" . $valor[1] . "' $selected >" . ucwords($valor[4]) . "&nbsp;&frasl;&nbsp;" . ucwords($valor[3]) . "</option>";
		}
		$servicios.="</SELECT> ";
		$botonAg = "<input type=\"button\" class='boton' value=\"Agregar\" OnClick='agregarenlista_f5_rh(this,$pauta);'>";
		$cantidad = "<input type='text' name='cantidad2' id='cantidad2' onkeypress='return validaN(event)' size='2' maxlength='4'/>";
		$tr_recursoh="<tr><th>Recursos Humanos:</th><td>" . $servicios . "</td><th>Cant:</th><td><input type=\"button\" class=\"boton\" value=\"+\" OnClick=\"textsum(this,'+')\" style=\"margin-right:0px;\" />" . $cantidad . "<input type=\"button\" class=\"boton\" value=\"-\" OnClick=\"textsum(this,'-')\"/></td><td><div>" . $botonAg . "</div></td></tr>";
		$materiales = "<div id='hb_solicitarmateriales'></div>";
		$verificarestatuspauta = $this->ObjConsulta->verificarestatuspauta($this->conect_sistemas_vtv, $pauta);
		$estatusactual = $verificarestatuspauta[1][1];
		if ($administrador == 26) {
			if ($estatusactual == 1) {
				$estatus_pt = 3;
				$botonA = "<input type=\"button\" class='boton' value=\"Vista Previa\" OnClick=CancelarRegresar('classmostrarpautagenerada.php?id_pauta=".$id_pauta."&mod={$_GET['mod']}',this);>";
				$botonC = "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('{$_GET['lista']}');>";
			} else {
				$botonA = "<input type=\"button\" class='boton' value=\"Guardar para futuros Cambios\" OnClick=CancelarRegresar('classlistadepautas.php',this);>";
				$botonC = "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('classlistadepautas.php');>";   
			}
		}
		if ($administrador == 22) {
			if ($estatusactual==1 or $estatusactual == 3 or $estatusactual == 34) {
				$estatus_pt = 5;
				$botonA="<input type=\"button\" class='boton' value=\"Vista Previa\" OnClick=CancelarRegresar('classmostrarpautagenerada.php?id_pauta=" . $id_pauta . "&lista=classlistadepautasjarea.php?id_pauta={$id_pauta}&mod={$_GET['mod']}',this);>";
				$botonC = "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('{$_GET['lista']}');>";
			} else {
				$botonA = "<input type=\"button\" class='boton' value=\"Guardar para futuros Cambios\" OnClick=CancelarRegresar('{$_GET['lista']}',this);>";
				$botonC = "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('{$_GET['lista']}');>";
			}
		}
		$pagina = $_GET['pagina'];
		$pasos = $this->pasosdelproceso($pagina);
		$botonB = "<input type=\"button\" class='boton' value=\"Buscar\" OnClick=buscarpersona();>";
		$titulo1 = "DATOS GENERALES DE LA PAUTA";
		$titulo2 = "RECURSOS";
		$htm.=$this->ObjMensaje->interfazsolicitarservicios($pauta, $descripcionloc, $descripciontraje, $descripcionprod, $descripcionteven, $descripcitacion, $fechacitacion, $horacitacion, $descripmontaje, $fechamontaje, $horamontaje, $descripemision, $fechaemision, $horaemision, $descripretorno, $fecharetorno, $horaretorno, $tr_materiales, $tr_recursoh, $evento, $materiales, $titulo1, $titulo2, $titulo3, $botonA, $botonC, $pasos, $this->comp_tablacomentario("Observaciones"));
		return $htm;
	}

	/**
	*pagina responsable en crear la interfaz de la pauta.
	**/
	function pag_crearpauta(){
		$cedula = $_SESSION['cedula'];
		$administrador = $_SESSION['id_tipo_usuario'];
		$nombres = $_SESSION['nombres'];
		$apellidos = $_SESSION['apellidos'];
        /**
         * jefe de area = 22
         * productor = 26
         * analista de apoyo logistico = 24
         */
        if (($administrador == 26) || ($administrador == 22) || ($administrador == 24)) {
        	$pauta=$this->basic_combo("selectpauta", "pauta", "combopauta_f5()");
        	$locacion=$this->basic_combo("selectlocacion", "locacion", "combolocacion_f5()");
        	$tipoevento=$this->basic_combo("selecttipoevento", "tipoevento", "cambianomenclatura(this)");
        	$tipotraje=$this->basic_combo("selecttipotraje", "tipotraje", "");
        	$datoslugarcitacionlef = $this->ObjConsulta->selectlugar2($this->conect_sistemas_vtv, 2);
        	$lugar_citacion_lef = "<select id='lugar_citacion_lef' name='lugar_citacion_lef' style='width:150px;'>";
        	$lugar_citacion_lef.="<option value='0' selected >Seleccione </option>";
        	foreach ($datoslugarcitacionlef as $llave => $valor) {
        		$lugar_citacion_lef.="<option value='" . $valor[1] . "' $selected >" . $valor[2] . "</option>";
        	}
        	$lugar_citacion_lef.="</SELECT> ";
			//horas y minutos del formulario de pautas.
        	$data=$this->fecha_comp_pauta("fecha1","hora1","min1");
        	extract($data);
        	$data=$this->fecha_comp_pauta("fecha2","hora2","min2");
        	extract($data);
        	$data=$this->fecha_comp_pauta("fecha3","hora3","min3");
        	extract($data);
        	$data=$this->fecha_comp_pauta("fecha4","hora4","min4");
        	extract($data);
        	$botonA = "<input type=\"button\" class='boton' value=\"Continuar\" OnClick='guardarpauta_f5()';>";
        	if ($administrador == 24) {
        		$botonC = "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick='ir_a(\"classlistadepautasual.php\")'>";
        	}else if ($administrador == 22){
        		$botonC = "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick='ir_a(\"classlistadepautasjarea.php\")'>";
        	}else {
        		$botonC = "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick='ir_a(\"classlistadepautas.php\")'>";
        	}
        	if($_SESSION["id_tipo_usuario"]==22){
        		$elegirProductor=$this->ObjMensaje->interfazelegirprod($this->basic_combo("tributa_a", 'selectProductor', "","", $_SESSION['gerencia'].", ".$_SESSION['division'].", '26'", false,utf8_decode("Tú")));
        	}elseif ($_SESSION["id_tipo_usuario"]==24) {
        		$elegirProductor=$this->ObjMensaje->interfazelegirprod($this->basic_combo("tributa_a", 'selectProductor', "","", "null, null, '26'", false,utf8_decode("Tú")));
        	}

        	$html="<div id='datosp' align='center'>
        	<div id='cargando' align='center'></div>
        	".$this->pasosdelproceso("1")."
        	".$elegirProductor."
        	".$this->ObjMensaje->form_solfactecn_bas($pauta,$locacion,$tipoevento,$tipotraje)."       	
        	".$this->ObjMensaje->form_solfactecn_bas_citaciones($fecha1,$fecha2,$fecha3,$fecha4,$hora1,$hora2,$hora3,$hora4,$min1,$min2,$min3,$min4,$lugar_citacion_lef)."
        	".$this->ObjMensaje->tablabotones($botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC)."
        	</div>";
        	return $html;
        }
    }

    function pag_listatipotra(){
    	$cedula=$_SESSION['cedula'];
		$administrador=$_SESSION['id_tipo_usuario'];
		$nombres=$_SESSION['nombres'];
		$apellidos=$_SESSION['apellidos'];
		if ($administrador==27 or $administrador==20 ){
			$campo_busqueda="<input type='text' name='busqueda' id='busqueda' class='campo' onkeypress='return enter(event,buscartrajes)' size='70'>";
			$botonB="<input type=\"button\" class='boton' value=\"Buscar\" OnClick=pag_listatipotra();>";
			$htm=$this->ObjMensaje->InterfazListaPersona($campo_busqueda,$botonB);		
		}
		return $htm;
    }

    function pag_listaprogra(){
    	$cedula=$_SESSION['cedula'];
		$administrador=$_SESSION['id_tipo_usuario'];
		$nombres=$_SESSION['nombres'];
		$apellidos=$_SESSION['apellidos'];
		if ($administrador==27 or $administrador==20 ){
			$campo_busqueda="<input type='text' name='busqueda' id='busqueda' class='campo' onkeypress='return enter(event,buscarprog)' size='70'>";
			$botonB="<input type=\"button\" class='boton' value=\"Buscar\" OnClick=pag_listaprogra();>";
			$htm=$this->ObjMensaje->InterfazListaPersona($campo_busqueda,$botonB);		
		}
		return $htm;
    }


}

if (isset($_GET['obtenerFuncion'])) {
	$ObjBienvenida = new classDirectorioFunciones();
}else if (isset($_POST['obtenerFuncion'])) {
	$ObjBienvenida = new classDirectorioFunciones();
}
?>
