<?php
//ini_set("error_reporting","E_ALL & ~E_NOTICE");
require("classDirectorioFunciones.php");
class elimvariables{
    public $html;
    function __construct(){
        $this->ObjclasslibSession = new classlibSession();
        $this->ObjCabPie=new classlibCabPie("Solicitud ","");
        $this->ObjOther=new classOtherMenu();
        $this->ObjMensaje=new classMensaje("","mostrar");
        $this->classDirectorioFunciones = new classDirectorioFunciones(true);
        $this->ObjConsulta = new classbdConsultas();
        $this->Objfechahora = new classlibFecHor();
        $this->conect_sistemas_vtv = "../database/archi_conex/sistemas_vtv_5431";
        
        $ficherosjs = "
        <script type='text/javascript' src='../class/other/ms-Dropdown/msdropdown/js/uncompressed.jquery.dd.js'></script>
        <script type='text/javascript' src='../class/other/classjavascript.js'></script>
        <script type='text/javascript' src='../librerias/datepick/jquery.datepick.pack.js'></script>
        <script type='text/javascript' src='../librerias/datepick/jquery.datepick-es.js'></script>
        <link rel='stylesheet' type='text/css' href='../class/other/ms-Dropdown/msdropdown/dd.css' />
        <link rel='stylesheet' href='../librerias/datepick/jquery.datepick.css' type='text/css' media='screen' charset='utf-8' />

        ";

        $administrador=$_SESSION['id_tipo_usuario'];
        if(isset($_SESSION['cedula'])){    
            $this->htm = $this->ObjCabPie->flibHtmCab(0, $ficherosjs, '', $this->ObjOther->fomArregloAsocia2($administrador), 0, "");
        }else{
            echo"<script>var pagina='classRegistro.php';                        
            alert('Disculpa la session ha expirado, debe iniciar sesion nuevamente.');
            function redireccionar() { 
                location.href=pagina;
            } 
            setTimeout ('redireccionar()', 0);
            </script>";
        }
    }

    function elimvariables(){

        $id_tipo_var=$_GET['id_tipo_var'];

        //pasar $id_tipo_sol por solicitud()
        //dependiendo del tipo de solicitud mostrar los combos

        if($id_tipo_var==1){
            $titulo="Eliminar Estado";
            $subtitulo="Estado";
            $descripcion_estado = $this->ObjConsulta->estado($this->conect_sistemas_vtv);
            $variable = "<select id='variable' name='variable' style='width:105px;'>";
            $variable.="<option value='0' selected >Seleccione </option>";
            foreach ($descripcion_estado as $llave => $valor) {
                    $variable.="<option value='" . $valor[1] . "' $selected >" . $valor[2] . "</option>";
                }
            $variable.="</SELECT> ";

        }elseif($id_tipo_var==2){
            $titulo="Eliminar Unidad de medida";
            $subtitulo="Unidad de medida";
            $desc_unidad_medidacant = $this->ObjConsulta->unidad_medida($this->conect_sistemas_vtv);
            $variable = "<select id='variable' name='variable' style='width:105px;'>";
            $variable.="<option value='0' selected >Seleccione </option>";
            foreach ($desc_unidad_medidacant as $llave => $valor) {
                    $variable.="<option value='" . $valor[1] . "' $selected >" . $valor[2] . "</option>";
            }
            $variable.="</SELECT> ";
        
        }elseif($id_tipo_var==3){
            $titulo="Eliminar Categoria";
            $subtitulo="Categoria";
            $descripcion_categoria = $this->ObjConsulta->categoria($this->conect_sistemas_vtv);
            $variable = "<select id='variable' name='variable' style='width:105px;'>";
            $variable.="<option value='0' selected >Seleccione </option>";
            foreach ($descripcion_categoria as $llave => $valor) {
                    $variable.="<option value='" . $valor[1] . "' $selected >" . $valor[2] . "</option>";
                }
            $variable.="</SELECT> "; 

        }elseif($id_tipo_var==4){
            $titulo="Eliminar Programa";
            $subtitulo="Programa";
            $datosprogramas = $this->ObjConsulta->programas($this->conect_sistemas_vtv);
                $variable = "<select id='variable' name='variable' style='width:105px;'>";
                $variable.="<option value='0' selected >Selecione</option>";
                foreach ($datosprogramas as $llave => $valor) {
                    $variable.="<option value='" . $valor[1] . "' >" . $valor[2] . "</option>";
                }
                $variable.="</SELECT> ";
                
        }else{
            $titulo="Eliminar Tipo Descripci&oacute;n";
            $subtitulo="Tipo Descripci&oacute;n";
            $datosdesc = $this->ObjConsulta->tipodescripcion($this->conect_sistemas_vtv);
                $variable = "<select id='variable' name='variable' style='width:260px;'>";
                $variable.="<option value='0' selected >Selecione</option>";
                foreach ($datosdesc as $llave => $valor) {
                    $variable.="<option value='" . $valor[1] . "' >" . $valor[2] . "</option>";
                }
                $variable.="</SELECT> ";
                
        }


        $botonA = "<input type=\"button\" class='boton' value=\"Aceptar\" OnClick=elimvariable('".$id_tipo_var."');>";
        $botonC = "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('classbienvenida.php');>";

        $this->htm.="<div id='datosp' align='center'><table class='tabla' align='center' style='width:500px;' >
        <tr><th colspan='8' class='titulo' >".$titulo."</th></tr>
        <tr><th>".$subtitulo."</th><td>" . $variable . "</td></tr>
        </table>
        <table class='tabla' style='width:500px;'>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table></div>";
    }


    function __destruct(){
        if(isset($_SESSION['cedula'])){
            $this->htm.=$this->ObjCabPie->flibCerrarHtm("");
            echo $this->htm;
        }
    }
}


$elimvariables = new elimvariables();
$elimvariables->elimvariables();
?>