<?php
//ini_set("error_reporting","E_ALL & ~E_NOTICE");
require("classDirectorioFunciones.php");
class incidencia{
    public $html;
    function __construct(){
        $this->ObjclasslibSession = new classlibSession();
        $this->ObjCabPie=new classlibCabPie("Incidencia ","");
        $this->ObjOther=new classOtherMenu();
        $this->ObjMensaje=new classMensaje("","mostrar");
        $this->classDirectorioFunciones = new classDirectorioFunciones(true);
        $this->ObjConsulta = new classbdConsultas();
        $this->Objfechahora = new classlibFecHor();
        $this->conect_sistemas_vtv = "../../../database/archi_conex/sistemas_vtv_5431";
        $this->conect_sigesp = "../../../database/archi_conex/sistema_sigesp";
        
        $ficherosjs = "

        <script type='text/javascript' src='../class/other/ms-Dropdown/msdropdown/js/uncompressed.jquery.dd.js'></script>
        <script type='text/javascript' src='../class/other/classjavascript.js'></script>
        <script type='text/javascript' src='../../../librerias/datepick/jquery.datepick.pack.js'></script>
        <script type='text/javascript' src='../../../librerias/datepick/jquery.datepick-es.js'></script>
        <link rel='stylesheet' type='text/css' href='../class/other/ms-Dropdown/msdropdown/dd.css' />
        <link rel='stylesheet' href='../../../librerias/datepick/jquery.datepick.css' type='text/css' media='screen' charset='utf-8' />
        
        ";

        $administrador=$_SESSION['id_tipo_usuario'];
        if(isset($_SESSION['cedula'])){    
            $this->htm = $this->ObjCabPie->flibHtmCab(0, $ficherosjs, '', $this->ObjOther->fomArregloAsocia2($administrador), 0, "");
        }else{
            echo"<script>var pagina='classRegistro.php';                        
            alert('Disculpa la session ha expirado, debe iniciar sesion nuevamente.');
            function redireccionar() { 
                location.href=pagina;
            } 
            setTimeout ('redireccionar()', 0);
            </script>";
        }
    }

    function incidencia(){
        $idsolicitud=$_GET['idsolicitud'];
        $identrada=1;// no afecta nada
            
            $solicitud = $this->ObjConsulta->selectsolicitud($this->conect_sistemas_vtv, $idsolicitud);
            $tipo_prestamo=$solicitud[1][2];
            $existe= count ($solicitud);

            if ($existe==0){
                ////***** Consultar si la solicitud existe, si no mostrar un mensaje*****////
                $mensaje = "<div style='color: #009900;font-weight: bold;'><br>La solicitud no exite, intente de nuevo<div><br>";
                $this->htm.=$this->ObjMensaje->InterfazExitosamente($mensaje);

                //deberia ir a la lista de las entradas
                echo"<script>var pagina='classlista.php?modulo=listadeprestamos';                     
                function redireccionar() { 
                    location.href=pagina;
                }   
                setTimeout ('redireccionar()', 3000);
                </script>";

            }else{

                ////***** Consultar si tienen articulos por ingresar, si no mostrar un mensaje*****//// 
                if($tipo_prestamo==1){//es de grupos
                    $datosmaterialespres = $this->ObjConsulta->selectgruposprestados($this->conect_sistemas_vtv, $idsolicitud);

                }elseif($tipo_prestamo==2){//es de equipos
                    $datosmaterialespres = $this->ObjConsulta->selectequiposprestados($this->conect_sistemas_vtv, $idsolicitud);

                }elseif ($tipo_prestamo==9) {//Son grupos y equipos
                    $datosmaterialespresgrup = $this->ObjConsulta->selectgruposprestados($this->conect_sistemas_vtv, $idsolicitud);
                    $datosmaterialespres = $this->ObjConsulta->selectequiposprestados($this->conect_sistemas_vtv, $idsolicitud);

                }else{//es camaras

                    $datosmaterialespres = $this->ObjConsulta->selectccamaraprestada($this->conect_sistemas_vtv, $idsolicitud);

                }

                $artpres= count ($datosmaterialespres);
                $artpres2= count ($datosmaterialespresgrup);
                
                if($artpres==0 and $artpres2==0){
                    $mensaje = "<div style='color: #009900;font-weight: bold;'><br>La solicitud ya fue procesada<div><br>";
                    $this->htm.=$this->ObjMensaje->InterfazExitosamente($mensaje);

                    echo"<script>var pagina='classlista.php?modulo=listadeprestamos';                     
                    function redireccionar() { 
                        location.href=pagina;
                    }   
                    setTimeout ('redireccionar()', 3000);
                    </script>";
                }else{
                    $botonA = "<input type=\"button\" class='boton' value=\"Aceptar\" OnClick=incidencia('".$idsolicitud."');>";
                    $botonC = "<input type=\"button\" class='boton' value=\"Cancelar\" OnClick=CancelarRegresar('classlista.php?modulo=listadeprestamos');>";

                    $botones="<table class='tabla' style='width:650px;'>
                    <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
                    </table></div>";
                                   
                    //Se encarga de consultar la informacion de la solicitud

                    $datossolicitud = $this->ObjConsulta->selectdatossol($this->conect_sistemas_vtv, $idsolicitud);
                    $id_destino=$datossolicitud[1][1];
                    $id_desc_dest=$datossolicitud[1][2];
                    $resp_prestamo=$datossolicitud[1][3];
                    $user_reg=$datossolicitud[1][4];
                    $fecha_reg=$datossolicitud[1][5];
                    $fecha_reg = $this->Objfechahora->flibInvertirInEs($fecha_reg);
                    $fecha_sol=$datossolicitud[1][6];
                    $fecha_sol = $this->Objfechahora->flibInvertirInEs($fecha_sol);
                    $fecha_exp=$datossolicitud[1][7];
                    $fecha_exp = $this->Objfechahora->flibInvertirInEs($fecha_exp);
                    $id_estatus_prestamo=$datossolicitud[1][8];
                    $observacion =$datossolicitud[1][9];
                    
                    if($id_destino==1){//es para un gerencia
                        $tipo_destino="Gerencia:";
                        $datosgerencia=$this->ObjConsulta->selectgerenciadesc($this->conect_sigesp, $id_desc_dest);
                        $destino=utf8_encode($datosgerencia[1][2]);
                        
                    }elseif($id_destino==2){//es para un programa
                        $tipo_destino=" Programa:";
                        $datosdestino = $this->ObjConsulta->selectprograma($this->conect_sistemas_vtv, $id_desc_dest);
                        $destino=$datosdestino[1][2];

                    }elseif($id_destino==3){//es un remoto (oficial o programa)
                        if(is_numeric ($id_desc_dest)==true){
                            $tipo_destino=" Programa:";
                            $datosdestino = $this->ObjConsulta->selectprograma($this->conect_sistemas_vtv, $id_desc_dest);
                            $destino=$datosdestino[1][2];
                        }else{
                            $tipo_destino="Lugar";
                            $destino=$id_desc_dest;
                        }
                        
                    }else{//es un remoto ente externo, una camara asignada o un F5
                        $tipo_destino="Lugar";
                        $destino=$id_desc_dest;
                    }

                    if($observacion == ''){
                        $observacion ='Sin Observaciones';
                    }
                    
                    $responsable=$this->ObjConsulta->selectpersonalresp($this->conect_sigesp, $resp_prestamo);
                    $nombres=utf8_encode($responsable[1][2]);
                    $apellidos=utf8_encode($responsable[1][3]);
                    
                    $img="<img style='width:60px; height:60px; 'src='http://intranet/sistemas/directorio/paginas/download_foto.php?id=".base64_encode($resp_prestamo)."'>";
                    //"http://intranet/sistemas/directorio/paginas/download_foto.php?id=".base64_encode($valor[1])."'";
                        $datossolicitud= "<div id='datosp' align='center'>
                        <table class='tabla' align='center' style='width:650px;' >
                        <tbody> 
                        <tr><th class='titulo' colspan='2'>" . $titulo1 . "</th></tr>
                        <tr><th></th><td>".$img."</td></tr>
                        <tr><th>Solicitud:</th><td>".$idsolicitud."</td></tr>
                        <tr><th>Responsable:</th><td>".$nombres." ".$apellidos."</td></tr>
                        <tr><th>".$tipo_destino."</th><td>".$destino."</td></tr>
                        <tr><th>Fecha de solicitud:</th><td>".$fecha_sol."</td></tr>
                        <!--<tr><th>Fecha de entrega:</th><td>".$fecha_exp."</td></tr>-->
                        <tr><th>Observaci&oacute;n:</th><td>".$observacion."</td></tr>
                        </tbody>
                        </table>";

                        $datossolicitud= $datossolicitud.$botones;
                    
                    //echo $datossolicitud;
                }
      
            }
            
        $this->htm.=$datossolicitud;

        /*$this->htm.="<div id='datosp' align='center'><table class='tabla' align='center' style='width:650px;' >
        <tr><th colspan='12' class='titulo' >".$titulo."</th></tr>
        <tr>".$radios."</tr>
        </table>
        <table class='tabla' style='width:650px;'>
        <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
        </table></div>";*/
    }


    function __destruct(){
        if(isset($_SESSION['cedula'])){
            $this->htm.=$this->ObjCabPie->flibCerrarHtm("");
            echo $this->htm;
        }
    }
}


$incidencia = new incidencia();
$incidencia->incidencia();
?>