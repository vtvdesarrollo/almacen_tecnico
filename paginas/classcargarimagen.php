<?php
//ini_set("error_reporting","E_ALL & ~E_NOTICE");
require("classDirectorioFunciones.php");
class cargarimagen{
    public $html;
    function __construct(){
        $this->ObjclasslibSession = new classlibSession();
        $this->ObjCabPie=new classlibCabPie("Recepci&oacute;n ","");
        $this->ObjOther=new classOtherMenu();
        $this->ObjMensaje=new classMensaje("","mostrar");
        $this->classDirectorioFunciones = new classDirectorioFunciones(true);
        $ficherosjs = "
        <script type='text/javascript' src='../class/other/classjavascript.js'></script>
        <script type='text/javascript' src='../../../librerias/datepick/jquery.datepick.pack.js'></script>
        <script type='text/javascript' src='../../../librerias/datepick/jquery.datepick-es.js'></script>
        <link rel='stylesheet' href='../../../librerias/datepick/jquery.datepick.css' type='text/css' media='screen' charset='utf-8' />
        <link rel='stylesheet' href='../css/f5.css' type='text/css' media='screen' charset='utf-8' />
        <script type=\"text/javascript\">
        $(document).ready(function(){
            $('#fecha1').datepick({beforeShow: customRange, showOn: 'both', buttonImageOnly: true, buttonImage: '../../../estilos/imagenes/estatus/calendar.png', onSelect:validadorfechas}); 
            $('#fecha2').datepick({beforeShow: customRange_2, showOn: 'both', buttonImageOnly: true, buttonImage: '../../../estilos/imagenes/estatus/calendar.png', onSelect:validadorfechas});
            $('#fecha3').datepick({beforeShow: customRange_3, showOn: 'both', buttonImageOnly: true, buttonImage: '../../../estilos/imagenes/estatus/calendar.png', onSelect:validadorfechas}); 
            $('#fecha4').datepick({beforeShow: customRange_4, showOn: 'both', buttonImageOnly: true, buttonImage: '../../../estilos/imagenes/estatus/calendar.png' , onSelect:validadorfechas});
        })
        </script>";

        $administrador=$_SESSION['id_tipo_usuario'];
        if(isset($_SESSION['cedula'])){    
            $this->htm = $this->ObjCabPie->flibHtmCab(0, $ficherosjs, '', $this->ObjOther->fomArregloAsocia2($administrador), 0, "");
        }else{
            echo"<script>var pagina='classRegistro.php';                        
            alert('Disculpa la session ha expirado, debe iniciar sesion nuevamente.');
            function redireccionar() { 
                location.href=pagina;
            } 
            setTimeout ('redireccionar()', 0);
            </script>";
        }
    }

    function cargarimagen(){
        $lista= 'classlista.php?modulo='.$_GET['lista'.''];

        $botonA = "<input type=\"button\" class='boton' value=\"Aceptar\" OnClick=cargarimagen();>";

        $botonC = "<input type=\"button\" class='boton' value=\"Continuar\" OnClick=CancelarRegresar('".$lista."');>"; 

        $this->htm.="<div id='datosp' align='center'><table class='tabla' align='center' style='width:500px;' >
            <tr><th colspan='7' class='titulo' >Imagen del art&iacute;culo</th></tr>
            <tr><th>Imagen:</th><td><input type='file' size='50' name='upload'></td></tr>
            </table>
            <table class='tabla' style='width:500px;'>
            <tr><th colspan='2'><div align='center'>" . $botonA . "&nbsp;&nbsp;&nbsp;&nbsp;" . $botonC . "</div></tr>
            </table></div>";
    }


    function __destruct(){
        if(isset($_SESSION['cedula'])){
            $this->htm.=$this->ObjCabPie->flibCerrarHtm("");
            echo $this->htm;
        }
    }
}


$cargarimagen = new cargarimagen();
$cargarimagen->cargarimagen();
?>